import 'dart:math';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/extensions/double.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_param.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';

void main() {
  const int defaultFractionDigits = 2;
  late EvaluateExpressionUseCase useCase;

  EvaluateExpressionParam getParam(String expression, {int? fractionDigits}) {
    return EvaluateExpressionParam(
      expression: expression,
      fractionDigits: fractionDigits ?? defaultFractionDigits,
    );
  }

  void rightTest(String exp, double expectedResult) {
    test(exp, () async {
      final res = await useCase(getParam(exp));
      expect(res, Right(expectedResult));
    });
  }

  void invalidInputTest(String exp) {
    test(exp, () async {
      final res = await useCase(getParam(exp));
      expect(res, const Left(Failure(message: 'Invalid input')));
    });
  }

  void divideByZeroTest(String exp) {
    test(exp, () async {
      final res = await useCase(getParam(exp));
      expect(res, const Left(Failure(message: "Can't divide by zero")));
    });
  }

  setUp(() {
    useCase = EvaluateExpressionUseCase();
  });

  group('simple test', () {
    rightTest('2+2', 4);
    rightTest('8×6', 48);
    rightTest('250÷20', 12.5);
    rightTest('3.5-0', 3.5);
    rightTest('10%', 0.1);
    rightTest('5+0.', 5);
  });

  group('no operator', () {
    rightTest('1', 1);
    rightTest('92836.5311', 92836.53);
    rightTest('5.987', 5.99);
    rightTest('-989', -989);
    rightTest('0', 0);
    rightTest('-0', 0);
    rightTest('.89', 0.89);
  });

  group('many operators', () {
    rightTest('45+0-34', 11);
    rightTest('50%×4', 2);
    rightTest('89÷35-11', -8.46);
    rightTest('24%+89-7+72816×0', 82.24);
    rightTest('77×77-00-321-3-56.432-3.0×1.3232+2-34.7-5', 5506.9);
    rightTest('99+500%%×2', 99.1);
    rightTest('5+100%-2', 4);
  });

  group('negative case', () {
    rightTest('0-12', -12);
    rightTest('-8', -8);
    rightTest('4-5.5', -1.5);
  });

  group('invalid input', () {
    invalidInputTest('12×89+');
    invalidInputTest('×12+');
    invalidInputTest('-');
    invalidInputTest('');
    invalidInputTest('67×89++');
    invalidInputTest('67+÷89');
  });

  group('divide by zero', () {
    divideByZeroTest('9/0');
    divideByZeroTest('-78/0');
    divideByZeroTest('-6.8/0');
    divideByZeroTest('0/0');
  });

  group('large number', () {
    rightTest(
      '92847937429837492874938749274932×712837183719381793817932893',
      92847937429837492874938749274932.0 * 712837183719381793817932893.0,
    );
  });

  group('random', () {
    test('ensure returning Right', () async {
      Random randomizer = Random();
      List<Operator> operators = Operator.values;
      int operatorLength = operators.length;

      List<String> sequences = [];
      bool pickNumber = true;
      int pickNumCount = 300;
      for (var i = 0; i < pickNumCount;) {
        if (pickNumber) {
          // Random double between 0.01 and 1000
          double number = 0.01 + randomizer.nextDouble() * 50000;
          sequences.add('${number.roundToDecimal(3)}');
          pickNumber = false;
          i++;
        } else {
          // Random operator
          int operatorIdx = randomizer.nextInt(operatorLength);
          Operator op = operators[operatorIdx];
          sequences.add(op.text);
          if (op == Operator.percent) {
            pickNumber = false;
          } else {
            pickNumber = true;
          }
        }
      }

      String exp = sequences.join();
      final res = await useCase(getParam(exp));
      expect(res.isRight(), true);
    });
  });
}
