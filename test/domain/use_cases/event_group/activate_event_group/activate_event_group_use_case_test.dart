import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/use_cases/event_group/activate_event_group/activate_event_group_use_case.dart';

import '../../../../mocks.dart';

void main() {
  late ActivateEventGroupUseCase useCase;
  late EventGroupRepository eventGroupRepository;

  const eventGroupId = '2';

  setUp(() {
    eventGroupRepository = MockEventGroupRepository();
    useCase =
        ActivateEventGroupUseCase(eventGroupRepository: eventGroupRepository);
    when(() => eventGroupRepository.setAsActive(any()))
        .thenAnswer((_) async => const Right(true));
  });

  test('should setAsActive', () async {
    await useCase(eventGroupId);
    verify(() => eventGroupRepository.setAsActive(eventGroupId));
  });
}
