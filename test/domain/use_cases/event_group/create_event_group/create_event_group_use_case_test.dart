import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late CreateEventGroupUseCase useCase;
  late EventGroupRepository eventGroupRepository;
  late PersonRepository personRepository;

  CreateEventGroupParam param = const CreateEventGroupParam(
    eventGroup: EventGroup.initial(
      name: 'Trip to Jogja',
      description: 'With ayank',
    ),
    members: [
      Person(id: '123', name: 'Rangga', avatar: PersonAvatar.pa001),
      Person(id: '234', name: 'Cinta', avatar: PersonAvatar.pa002),
    ],
  );

  setUp(() {
    eventGroupRepository = MockEventGroupRepository();
    personRepository = MockPersonRepository();
    useCase = CreateEventGroupUseCase(
      eventGroupRepository: eventGroupRepository,
      personRepository: personRepository,
    );
    when(() => eventGroupRepository.createEventGroup(any(), any()))
        .thenAnswer((_) async => Right(param.eventGroup));
  });

  setUpAll(() {
    registerFallbackValue(FakeCreateEventGroupParam());
    registerFallbackValue(FakeEventGroup());
    registerFallbackValue(FakePerson());
  });

  test('should call createEventGroup', () async {
    await useCase(param);
    verify(() => eventGroupRepository
        .createEventGroup(param.eventGroup, ['234', '123']));
  });

  test('should save new member', () async {
    const generatedPersonId = '567';
    when(() => personRepository.createPerson(any())).thenAnswer((inv) async {
      Person newPerson = inv.positionalArguments[0];
      return Right(newPerson.copyWith(id: generatedPersonId));
    });

    // Add person with empty ID (new person)
    const Person newMember =
        Person(id: '', name: 'Asep', avatar: PersonAvatar.pa001);
    param = param.copyWith(members: List.of(param.members)..add(newMember));

    await useCase(param);

    final capturedParam =
        verify(() => personRepository.createPerson(captureAny())).captured;
    Person capturedNewPerson = capturedParam[0];
    expect(capturedNewPerson, newMember);

    verify(() => eventGroupRepository
        .createEventGroup(param.eventGroup, ['567', '234', '123']));
  });

}
