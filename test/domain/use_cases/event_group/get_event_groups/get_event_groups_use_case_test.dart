import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_select_item.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_param.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_use_case.dart';

import '../../../../mocks.dart';

void main() {
  late GetEventGroupsUseCase useCase;
  late EventGroupRepository eventGroupRepository;
  late PersonRepository personRepository;
  late GetEventGroupsParam param;

  final currentTime = DateTime.now();

  final eventGroup1 =
      EventGroup(id: '1', name: 'Abc', creationTime: currentTime);
  final eventGroup2 =
      EventGroup(id: '2', name: 'Def', creationTime: currentTime);
  final eventGroup3 =
      EventGroup(id: '3', name: 'Ghi', creationTime: currentTime);

  final member1 = Person(
    id: 'a',
    name: 'Tony',
    creationTime: currentTime,
    avatar: PersonAvatar.pa001,
  );
  final member2 = Person(
    id: 'b',
    name: 'Bruce',
    creationTime: currentTime,
    avatar: PersonAvatar.pa002,
  );
  final member3 = Person(
    id: 'c',
    name: 'Steve',
    creationTime: currentTime,
    avatar: PersonAvatar.pa003,
  );

  setUp(() {
    eventGroupRepository = MockEventGroupRepository();
    personRepository = MockPersonRepository();
    useCase = GetEventGroupsUseCase(
        eventGroupRepository: eventGroupRepository,
        personRepository: personRepository);

    when(() => eventGroupRepository.getEventGroups()).thenAnswer(
        (_) async => Right([eventGroup1, eventGroup2, eventGroup3]));
    when(() => eventGroupRepository.getMembers(any()))
        .thenAnswer((_) async => Right([member1, member2, member3]));
    when(() => personRepository.getSelf())
        .thenAnswer((_) async => Right(member3));
  });

  test('activeOnTop, should return the correct result', () async {
    param = GetEventGroupsParam(activeOnTop: true);

    when(() => eventGroupRepository.getActive())
        .thenAnswer((_) async => const Right('2'));

    final result = await useCase(param);

    verify(() => eventGroupRepository.getActive());
    verify(() => eventGroupRepository.getEventGroups());
    result.fold(
      (l) => fail('should not be left'),
      (r) {
        final expectedMembers = [
          const EventGroupMember('Steve', isSelf: true),
          const EventGroupMember('Bruce'),
          const EventGroupMember('Tony'),
        ];

        // Active group (eventGroup2) should be at first position
        expect(r, [
          EventGroupSelectItem(
            eventGroup2,
            isCurrentlyActive: true,
            members: expectedMembers,
          ),
          EventGroupSelectItem(eventGroup1, members: expectedMembers),
          EventGroupSelectItem(eventGroup3, members: expectedMembers),
        ]);
      },
    );
  });

  test('activeOnTop = false, should return the correct result', () async {
    param = GetEventGroupsParam();

    final result = await useCase(param);

    verifyNever(() => eventGroupRepository.getActive());
    verify(() => eventGroupRepository.getEventGroups());
    result.fold(
      (l) => fail('should not be left'),
      (r) {
        expect(r[0].eventGroup, eventGroup1);
        expect(r[1].eventGroup, eventGroup2);
        expect(r[2].eventGroup, eventGroup3);
      },
    );
  });
}
