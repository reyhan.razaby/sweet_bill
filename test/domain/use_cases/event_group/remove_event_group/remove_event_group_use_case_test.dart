import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/use_cases/event_group/remove_event_group/remove_event_group_use_case.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks.dart';

void main() {
  late RemoveEventGroupUseCase useCase;
  late EventGroupRepository eventGroupRepository;

  const String param = 'xxx';

  setUp(() {
    eventGroupRepository = MockEventGroupRepository();
    useCase =
        RemoveEventGroupUseCase(eventGroupRepository: eventGroupRepository);
    when(() => eventGroupRepository.removeEventGroup(any()))
        .thenAnswer((_) async => const Right(true));
    when(() => eventGroupRepository.getActive())
        .thenAnswer((_) async => const Right('ooo'));
    when(() => eventGroupRepository.getEventGroupIds())
        .thenAnswer((_) async => const Right([]));
    when(() => eventGroupRepository.setAsActive(any()))
        .thenAnswer((_) async => const Right(true));
  });

  test('should removeEventGroup', () async {
    await useCase(param);
    verify(() => eventGroupRepository.removeEventGroup(param));
  });

  group('deleted id is currently active', () {
    setUp(() {
      when(() => eventGroupRepository.getActive())
          .thenAnswer((_) async => const Right(param));
    });

    test('should activate the other', () async {
      when(() => eventGroupRepository.getEventGroupIds())
          .thenAnswer((_) async => const Right(['ccc', 'vvv']));

      await useCase(param);

      verify(() => eventGroupRepository.setAsActive('ccc'));
    });

    test('no data anymore, should not return Failure', () async {
      when(() => eventGroupRepository.getEventGroupIds())
          .thenAnswer((_) async => const Right([]));

      final result = await useCase(param);

      expect(result, const Right(true));
    });
  });
}
