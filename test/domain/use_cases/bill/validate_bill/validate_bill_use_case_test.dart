import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/repositories/settings_repository.dart';
import 'package:sweet_bill/domain/use_cases/bill/validate_bill/validate_bill_use_case.dart';

import '../../../../mocks.dart';

void main() {
  late ValidateBillUseCase useCase;
  late SettingsRepository settingsRepository;

  setUp(() {
    settingsRepository = MockSettingsRepository();
    useCase = ValidateBillUseCase(settingsRepository: settingsRepository);

    when(() => settingsRepository.getMaxFractionDigits())
        .thenAnswer((_) async => 2);
  });

  group('return Left', () {
    test("total expense doesn't add up to total payment", () async {
      Bill bill = Bill.asDefault(
        id: '',
        title: 'Cafe',
        personalCosts: const [
          BillPersonalCost(personId: '1', cost: 800, additionalCost: 200),
          BillPersonalCost(personId: '2', cost: 800, additionalCost: 0),
        ],
        personalPayments: const [
          BillPersonalPayment(personId: '2', amount: 2500),
        ],
      );

      final res = await useCase(bill);
      expect(res.isLeft(), true);
    });

    test("no expense and payment", () async {
      Bill bill = Bill.asDefault(
          id: '',
          title: 'Cafe',
          personalCosts: const [],
          personalPayments: const []);

      final res = await useCase(bill);
      expect(res.isLeft(), true);
    });
  });

  group('return Right', () {
    test("total expense equals to total payment", () async {
      Bill bill = Bill.asDefault(
        id: '',
        title: 'Cafe',
        personalCosts: const [
          BillPersonalCost(personId: '1', cost: 800.00001, additionalCost: 200),
          BillPersonalCost(personId: '2', cost: 800),
        ],
        personalPayments: const [
          BillPersonalPayment(personId: '2', amount: 1800),
        ],
      );

      final res = await useCase(bill);
      expect(res.isRight(), true);
    });
  });
}
