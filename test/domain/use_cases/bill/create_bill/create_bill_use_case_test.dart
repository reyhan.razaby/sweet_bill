import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_param.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/validate_bill/validate_bill_use_case.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late CreateBillUseCase useCase;
  late BillRepository billRepository;
  late ValidateBillUseCase validateBillUseCase;

  const generatedId = '22';

  setUpAll(() {
    registerFallbackValue(FakeCreateBillParam());
    registerFallbackValue(FakeBill());
  });

  setUp(() {
    billRepository = MockBillRepository();
    validateBillUseCase = MockValidateBillUseCase();
    useCase = CreateBillUseCase(
      billRepository: billRepository,
      validateBillUseCase: validateBillUseCase,
    );

    when(() => billRepository.createBill(any()))
        .thenAnswer((_) async => const Right(generatedId));
  });

  final CreateBillParam param = CreateBillParam(
    bill: Bill.asDefault(
      id: '',
      title: 'Cafe',
      personalCosts: const [
        BillPersonalCost(personId: '1', cost: 1000, additionalCost: 0),
        BillPersonalCost(personId: '2', cost: 0, additionalCost: 200),
      ],
      personalPayments: const [
        BillPersonalPayment(personId: '2', amount: 1200),
      ],
    ),
  );

  test("return bill ID when bill successfully created", () async {
    when(() => validateBillUseCase(any()))
        .thenAnswer((_) async => const Right(true));

    final result = await useCase(param);
    result.fold(
      (l) => fail('should not be left'),
      (r) => expect(result, const Right(generatedId)),
    );
  });

  test('return Failure when having invalid bill', () async {
    const failure = Failure();
    when(() => validateBillUseCase(any()))
        .thenAnswer((_) async => const Left(failure));

    final result = await useCase(param);
    result.fold(
      (l) => expect(l, failure),
      (r) => fail('should not be left'),
    );
  });
}
