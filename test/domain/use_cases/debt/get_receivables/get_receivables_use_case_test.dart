import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_use_case.dart';

import '../../../../mocks.dart';

void main() {
  final GetReceivablesUseCase useCase = GetReceivablesUseCase();

  const ann = 'ann';
  const boo = 'boo';
  const rey = 'rey';

  test('valid bills', () async {
    // Mock bill1
    Bill bill1 = MockBill();
    when(() => bill1.personalCosts).thenReturn(const [
      BillPersonalCost(personId: ann, cost: 180, additionalCost: 20),
      BillPersonalCost(personId: boo, cost: 180, additionalCost: 20),
      BillPersonalCost(personId: rey, cost: 180, additionalCost: 20),
    ]);
    when(() => bill1.personalPayments).thenReturn(const [
      BillPersonalPayment(personId: boo, amount: 600),
    ]);

    // Mock bill2
    Bill bill2 = MockBill();
    when(() => bill2.personalCosts).thenReturn(const [
      BillPersonalCost(personId: boo, cost: 40),
      BillPersonalCost(personId: rey, cost: 60),
    ]);
    when(() => bill2.personalPayments).thenReturn(const [
      BillPersonalPayment(personId: ann, amount: 10),
      BillPersonalPayment(personId: rey, amount: 90),
    ]);

    // Mock bill3
    Bill bill3 = MockBill();
    when(() => bill3.personalCosts).thenReturn(const [
      BillPersonalCost(personId: ann, cost: 80, additionalCost: 20),
      BillPersonalCost(personId: boo, cost: 10),
      BillPersonalCost(personId: rey, cost: 30),
    ]);
    when(() => bill3.personalPayments).thenReturn(const [
      BillPersonalPayment(personId: rey, amount: 140),
    ]);

    final bills = [bill1, bill2, bill3];

    final res = await useCase(GetReceivablesParam(bills: bills));

    expectRight(res.asRight(), const [
      Receivable(
        personId: ann,
        receivableAmount: (0 + 10 + 0) - (200 + 0 + 100),
        totalPayment: 0 + 10 + 0,
        totalCost: 200 + 0 + 100,
      ),
      Receivable(
        personId: boo,
        receivableAmount: (600 + 0 + 0) - (200 + 40 + 10),
        totalPayment: 600 + 0 + 0,
        totalCost: 200 + 40 + 10,
      ),
      Receivable(
        personId: rey,
        receivableAmount: (0 + 90 + 140) - (200 + 60 + 30),
        totalPayment: 0 + 90 + 140,
        totalCost: 200 + 60 + 30,
      ),
    ]);
  });
}

void expectRight(List<Receivable> actual, List<Receivable> expected) {
  actual = List.of(actual);
  expected = List.of(expected);
  _sortList(actual);
  _sortList(expected);
  expect(actual, expected);
}

void _sortList(List<Receivable> list) {
  list.sort((a, b) {
    int personIdComp = a.personId.compareTo(b.personId);
    if (personIdComp != 0) return personIdComp;
    return a.receivableAmount.compareTo(b.receivableAmount);
  });
}
