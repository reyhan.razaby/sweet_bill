import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/debt/debts_breakdown.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_breakdown/get_debts_breakdown_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_breakdown/get_debts_breakdown_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_use_case.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late GetDebtsBreakdownUseCase useCase;
  late GenerateDebtsUseCase generateDebtsUseCase;
  late GetReceivablesUseCase getReceivablesUseCase;
  late BillRepository billRepository;
  late PersonRepository personRepository;

  final fakeDateTime = DateTime.now();

  Person _generatePerson(String id) {
    return Person.unknown(id: id, creationTime: fakeDateTime);
  }

  setUp(() {
    generateDebtsUseCase = MockGenerateDebtsUseCase();
    getReceivablesUseCase = MockGetReceivablesUseCase();
    billRepository = MockBillRepository();
    personRepository = MockPersonRepository();
    useCase = GetDebtsBreakdownUseCase(
      generateDebtsUseCase: generateDebtsUseCase,
      getReceivablesUseCase: getReceivablesUseCase,
      billRepository: billRepository,
      personRepository: personRepository,
    );
    when(() => billRepository.getBills(any()))
        .thenAnswer((_) async => Right([FakeBill()]));
  });

  setUpAll(() {
    registerFallbackValue(FakeGetBillsParam());
    registerFallbackValue(FakeGenerateDebtsParam());
    registerFallbackValue(FakeGetReceivablesParam());
  });

  test('should return breakdowns', () async {
    final debts = [FakeDebt()];
    when(() => generateDebtsUseCase(any()))
        .thenAnswer((_) async => Right(debts));

    when(() => getReceivablesUseCase(any()))
        .thenAnswer((_) async => const Right([
              Receivable(
                personId: 'a',
                totalPayment: 500,
                totalCost: 300,
                receivableAmount: 200,
              ),
              Receivable(
                personId: 'b',
                totalPayment: 700,
                totalCost: 400,
                receivableAmount: 300,
              ),
              Receivable(
                personId: 'c',
                totalPayment: 0,
                totalCost: 500,
                receivableAmount: -500,
              ),
            ]));

    when(() => personRepository.getPerson(any())).thenAnswer(
        (inv) async => Right(_generatePerson(inv.positionalArguments[0])));

    const param = GetDebtsBreakdownParam(eventGroupId: '1');
    final res = await useCase(param);

    expect(
        res,
        Right(
          DebtsBreakdown(
            debts: debts,
            personalBreakdowns: [
              PersonalBreakdown(
                person: _generatePerson('a'),
                totalPayment: 500,
                totalCost: 300,
              ),
              PersonalBreakdown(
                person: _generatePerson('b'),
                totalPayment: 700,
                totalCost: 400,
              ),
              PersonalBreakdown(
                person: _generatePerson('c'),
                totalPayment: 0,
                totalCost: 500,
              ),
            ],
          ),
        ));
  });
}
