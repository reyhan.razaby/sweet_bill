import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/receivable_key_builder.dart';

void main() {
  test('description', () {
    const receivables = [
      Receivable(
        personId: 'b',
        receivableAmount: -20.0,
      ),
      Receivable(
        personId: 'c',
        receivableAmount: 5.0,
      ),
      Receivable(
        personId: 'a',
        receivableAmount: 15.0,
      ),
      Receivable(
        personId: 'd',
        receivableAmount: 0.0,
      ),
    ];

    final key = buildReceivablesKey(receivables);
    expect(key, 'a:${15.0}_b:${-20.0}_c:${5.0}');
  });
}
