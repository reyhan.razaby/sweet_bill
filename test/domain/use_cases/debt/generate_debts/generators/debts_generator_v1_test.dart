import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generators/debts_generator.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generators/debts_generator_v1.dart';

void main() {
  final DebtsGeneratorV1 generator = DebtsGeneratorV1();

  const ann = 'ann';
  const boo = 'boo';
  const jun = 'jun';
  const mat = 'mat';
  const ned = 'ned';
  const rey = 'rey';
  const zoe = 'zoe';

  test('case 1', () async {
    const receivables = [
      Receivable(personId: ann, receivableAmount: 300),
      Receivable(personId: jun, receivableAmount: -300),
      Receivable(personId: rey, receivableAmount: -300),
      Receivable(personId: zoe, receivableAmount: 300),
    ];

    expectRight(generator.generate(receivables), [
      const RawDebt(borrowerId: jun, lenderId: ann, amount: 300),
      const RawDebt(borrowerId: rey, lenderId: zoe, amount: 300)
    ]);
  });

  test('case 2', () async {
    const receivables = [
      Receivable(personId: ann, receivableAmount: -200),
      Receivable(personId: boo, receivableAmount: -500),
      Receivable(personId: jun, receivableAmount: -200),
      Receivable(personId: rey, receivableAmount: 200),
      Receivable(personId: zoe, receivableAmount: 700),
    ];

    expectRight(generator.generate(receivables), [
      const RawDebt(borrowerId: ann, lenderId: rey, amount: 200),
      const RawDebt(borrowerId: boo, lenderId: zoe, amount: 500),
      const RawDebt(borrowerId: jun, lenderId: zoe, amount: 200)
    ]);
  });

  test('case 3', () async {
    const receivables = [
      Receivable(personId: ann, receivableAmount: -200),
      Receivable(personId: boo, receivableAmount: -400),
      Receivable(personId: jun, receivableAmount: -600),
      Receivable(personId: rey, receivableAmount: 500),
      Receivable(personId: zoe, receivableAmount: 700),
    ];

    expectRight(generator.generate(receivables), [
      const RawDebt(borrowerId: jun, lenderId: zoe, amount: 600),
      const RawDebt(borrowerId: boo, lenderId: rey, amount: 400),
      const RawDebt(borrowerId: ann, lenderId: rey, amount: 100),
      const RawDebt(borrowerId: ann, lenderId: zoe, amount: 100),
    ]);
  });

  test('case 4', () async {
    const receivables = [
      Receivable(personId: ann, receivableAmount: 100),
      Receivable(personId: boo, receivableAmount: 600),
      Receivable(personId: jun, receivableAmount: 125),
      Receivable(personId: mat, receivableAmount: -100),
      Receivable(personId: rey, receivableAmount: -700),
      Receivable(personId: zoe, receivableAmount: -25),
    ];

    expectRight(generator.generate(receivables), [
      const RawDebt(borrowerId: mat, lenderId: ann, amount: 100),
      const RawDebt(borrowerId: rey, lenderId: boo, amount: 600),
      const RawDebt(borrowerId: rey, lenderId: jun, amount: 100),
      const RawDebt(borrowerId: zoe, lenderId: jun, amount: 25),
    ]);
  });

  test('case 5', () {
    const receivables = [
      Receivable(personId: ann, receivableAmount: 100),
      Receivable(personId: boo, receivableAmount: 80),
      Receivable(personId: jun, receivableAmount: 40),
      Receivable(personId: mat, receivableAmount: 30),
      Receivable(personId: ned, receivableAmount: -150),
      Receivable(personId: rey, receivableAmount: -70),
      Receivable(personId: zoe, receivableAmount: -30),
    ];

    expectRight(generator.generate(receivables), [
      const RawDebt(borrowerId: zoe, lenderId: mat, amount: 30),
      const RawDebt(borrowerId: ned, lenderId: ann, amount: 100),
      const RawDebt(borrowerId: ned, lenderId: boo, amount: 50),
      const RawDebt(borrowerId: rey, lenderId: jun, amount: 40),
      const RawDebt(borrowerId: rey, lenderId: boo, amount: 30),
    ]);
  });

  test('case 6', () {
    const List<Receivable> receivables = [];
    expectRight(generator.generate(receivables), []);
  });

  group('unbalanced', () {
    test('case 1', () {
      const receivables = [
        Receivable(personId: ann, receivableAmount: -100),
        Receivable(personId: boo, receivableAmount: 80),
        Receivable(personId: jun, receivableAmount: 40),
      ];

      expectRight(generator.generate(receivables), [
        const RawDebt(borrowerId: ann, lenderId: boo, amount: 80),
        const RawDebt(borrowerId: ann, lenderId: jun, amount: 20),
      ]);
    });

    test('case 2', () {
      const receivables = [
        Receivable(personId: ann, receivableAmount: 100),
        Receivable(personId: boo, receivableAmount: 80),
        Receivable(personId: jun, receivableAmount: 40),
      ];

      expectRight(generator.generate(receivables), []);
    });

    test('case 3', () {
      const receivables = [
        Receivable(personId: ann, receivableAmount: 100),
        Receivable(personId: boo, receivableAmount: -80),
        Receivable(personId: jun, receivableAmount: 40),
      ];

      expectRight(generator.generate(receivables), [
        const RawDebt(borrowerId: boo, lenderId: ann, amount: 80),
      ]);
    });
  });
}

void expectRight(List<RawDebt> actual, List<RawDebt> expected) {
  actual = List.of(actual);
  expected = List.of(expected);
  _sortRawDebts(actual);
  _sortRawDebts(expected);
  expect(actual, expected);
}

void _sortRawDebts(List<RawDebt> list) {
  list.sort((a, b) {
    int borrowerComp = a.borrowerId.compareTo(b.borrowerId);
    if (borrowerComp != 0) return borrowerComp;
    int lenderComp = a.lenderId.compareTo(b.lenderId);
    if (lenderComp != 0) return lenderComp;
    return a.amount.compareTo(b.amount);
  });
}
