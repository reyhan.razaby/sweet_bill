import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/debt/calculated_debts.dart';
import 'package:sweet_bill/domain/entities/debt/debt.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/repositories/debt_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generators/debts_generator.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/receivable_key_builder.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_use_case.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late GenerateDebtsUseCase useCase;
  late PersonRepository personRepository;
  late DebtsGenerator debtsGenerator;
  late GetReceivablesUseCase getReceivablesUseCase;
  late DebtRepository debtRepository;
  late BillRepository billRepository;

  setUp(() {
    getReceivablesUseCase = MockGetReceivablesUseCase();
    debtRepository = MockDebtRepository();
    personRepository = MockPersonRepository();
    debtsGenerator = MockDebtsGenerator();
    billRepository = MockBillRepository();
    useCase = GenerateDebtsUseCase(
      personRepository: personRepository,
      debtsGenerator: debtsGenerator,
      getReceivablesUseCase: getReceivablesUseCase,
      debtRepository: debtRepository,
      billRepository: billRepository,
    );

    when(() => debtsGenerator.generate(any())).thenReturn([
      const RawDebt(borrowerId: fakeString, lenderId: fakeString, amount: 0)
    ]);

    when(() => getReceivablesUseCase(any()))
        .thenAnswer((_) async => Right([FakeReceivable()]));
    when(() => debtRepository.createCalculatedDebts(any(), any()))
        .thenAnswer((_) async => const Right(true));
  });

  setUpAll(() {
    registerFallbackValue(FakeGetReceivablesParam());
    registerFallbackValue(FakeCalculatedDebts());
  });

  test('matched receivablesKey', () async {
    const receivables = [
      Receivable(personId: 'a', receivableAmount: 15),
      Receivable(personId: 'b', receivableAmount: -20),
      Receivable(personId: 'c', receivableAmount: 5),
    ];
    final existingDebts = [Debt(borrower: FakePerson(), lender: FakePerson())];
    when(() => getReceivablesUseCase(any()))
        .thenAnswer((_) async => const Right(receivables));
    when(() => debtRepository.getCalculatedDebts(any())).thenAnswer(
      (_) async => Right(CalculatedDebts(
        receivablesKey: buildReceivablesKey(receivables),
        debts: existingDebts,
      )),
    );

    final res = await useCase(
        GenerateDebtsParam(eventGroupId: fakeString, bills: [FakeBill()]));

    verifyNever(() => debtsGenerator.generate(any()));
    verifyNever(() => debtRepository.createCalculatedDebts(any(), any()));

    expect(res, Right(existingDebts));
  });

  test('unmatched receivablesKey', () async {
    const receivables = [
      Receivable(personId: 'a', receivableAmount: 15),
      Receivable(personId: 'b', receivableAmount: -20),
      Receivable(personId: 'c', receivableAmount: 5),
    ];
    when(() => getReceivablesUseCase(any()))
        .thenAnswer((_) async => const Right(receivables));
    when(() => debtRepository.getCalculatedDebts(any())).thenAnswer(
      (_) async => Right(CalculatedDebts(
        receivablesKey: 'xxx',
        debts: [FakeDebt()],
      )),
    );
    when(() => debtsGenerator.generate(any())).thenReturn(
        [const RawDebt(borrowerId: '1', lenderId: '2', amount: 200)]);
    when(() => personRepository.getPerson(any()))
        .thenAnswer((_) async => Right(FakePerson()));

    final res = await useCase(
        GenerateDebtsParam(eventGroupId: fakeString, bills: [FakeBill()]));

    verify(() => debtsGenerator.generate(receivables));
    verify(() => debtRepository.createCalculatedDebts(any(), any()));

    expect(res.isRight(), true);
  });

  test('given absent person, should not error', () async {
    const receivables = [
      Receivable(personId: 'a', receivableAmount: 15),
      Receivable(personId: 'b', receivableAmount: -20),
      Receivable(personId: 'c', receivableAmount: 5),
    ];
    when(() => getReceivablesUseCase(any()))
        .thenAnswer((_) async => const Right(receivables));
    when(() => debtRepository.getCalculatedDebts(any())).thenAnswer(
      (_) async => Right(CalculatedDebts(
        receivablesKey: 'xxx',
        debts: [FakeDebt()],
      )),
    );
    when(() => personRepository.getPerson(any()))
        .thenAnswer((_) async => const Left(Failure()));

    final res = await useCase(
        GenerateDebtsParam(eventGroupId: fakeString, bills: [FakeBill()]));

    expect(res.isRight(), true);
  });
}
