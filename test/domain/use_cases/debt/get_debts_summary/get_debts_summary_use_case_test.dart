import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_summary/get_debts_summary_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_summary/get_debts_summary_use_case.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late GetDebtsSummaryUseCase useCase;
  late GenerateDebtsUseCase generateDebtsUseCase;
  late BillRepository billRepository;

  setUp(() {
    generateDebtsUseCase = MockGenerateDebtsUseCase();
    billRepository = MockBillRepository();
    useCase = GetDebtsSummaryUseCase(
        generateDebtsUseCase: generateDebtsUseCase,
        billRepository: billRepository);
    when(() => generateDebtsUseCase(any()))
        .thenAnswer((_) async => Right([FakeDebt()]));
  });

  setUpAll(() {
    registerFallbackValue(FakeGetBillsParam());
    registerFallbackValue(FakeGenerateDebtsParam());
  });

  test("should generate debts", () async {
    Bill bill = MockBill();

    when(() => billRepository.getBills(any()))
        .thenAnswer((_) async => Right([bill]));
    when(() => bill.totalCost).thenReturn(50);

    final res =
        await useCase(const GetDebtsSummaryParam(eventGroupId: fakeString));

    verify(() => generateDebtsUseCase(any())).called(1);
    expect(res.isRight(), true);
  });
}
