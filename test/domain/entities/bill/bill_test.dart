import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';

void main() {
  group('Bill', () {
    final expenses = [
      const BillPersonalCost(personId: '1', cost: 20000, additionalCost: 1000),
      const BillPersonalCost(personId: '2', cost: 3500, additionalCost: 0),
      const BillPersonalCost(personId: '3', cost: 0, additionalCost: 500),
    ];
    final payments = [
      const BillPersonalPayment(personId: '1', amount: 20000),
      const BillPersonalPayment(personId: '2', amount: 5000),
    ];

    Bill bill = Bill(
      id: '100',
      title: 'Cafe',
      eventGroupId: 'x',
      icon: BillIcons.umbrella,
      transactionTime: DateTime.now(),
      personalCosts: expenses,
      personalPayments: payments,
      splitStrategy: SplitStrategies.equally,
      isAutoAdjustTax: true,
    );

    test('get the correct totalExpenses and totalPayments', () {
      expect(bill.totalCost, 25000);
      expect(bill.totalPayment, 25000);
    });

    test('return identical bill with copyWith method', () {
      Bill newBill = bill.copyWith();
      expect(newBill, bill);
    });
  });

  group('BillExpense', () {
    test('get the correct totalAmount', () {
      BillPersonalCost expense = const BillPersonalCost(
        personId: '1',
        cost: 20000,
        additionalCost: 1000,
      );
      expect(expense.totalCost, 21000);
    });
  });
}
