import 'package:sweet_bill/core/extensions/iterable.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('firstWhereOrNull', () {
    final list = ['abc', 'def', 'ghi'];

    test('the result should be null', () {
      final res = list.firstWhereOrNull((element) => element == 'xxxx');
      expect(res, null);
    });

    test('the result should be found', () {
      final res = list.firstWhereOrNull((element) => element == 'def');
      expect(res, 'def');
    });
  });
}
