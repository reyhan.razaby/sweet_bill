import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/extensions/double.dart';

void main() {
  test('roundToDecimal', () {
    expect(1.459.roundToDecimal(2), 1.46);
    expect(1.451.roundToDecimal(2), 1.45);
    expect(1.499.roundToDecimal(1), 1.5);
  });

  test('equals', () {
    expect(1.555.equals(1.55, fractionDigits: 2), true);
    expect(1.558.equals(1.56, fractionDigits: 2), true);
    expect(8.000000001.equals(8, fractionDigits: 2), true);
    expect(8.000000001.equals(8), true);
  });
}
