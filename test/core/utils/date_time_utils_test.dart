import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/utils/date_time_utils.dart';

void main() {
  DateTime dt1 = DateTime(2019, 1, 21);
  DateTime dt2 = DateTime(2019, 1, 22);
  DateTime dt3 = DateTime(2019, 1, 22);

  test('test case 1', () {
    int res = DateTimeUtils.compareNullable(dt1, dt2);
    expect(-1, res);
  });

  test('test case 2', () {
    int res = DateTimeUtils.compareNullable(dt2, dt1);
    expect(1, res);
  });

  test('test case 3', () {
    int res = DateTimeUtils.compareNullable(dt1, null, putNullToFirst: true);
    expect(1, res);
  });

  test('test case 4', () {
    int res = DateTimeUtils.compareNullable(null, dt1, putNullToFirst: true);
    expect(-1, res);
  });

  test('test case 5', () {
    int res = DateTimeUtils.compareNullable(dt2, dt2);
    expect(0, res);
  });

  test('test case 6', () {
    int res = DateTimeUtils.compareNullable(dt2, dt3);
    expect(0, res);
  });

  test('test case 7', () {
    int res = DateTimeUtils.compareNullable(dt2, null);
    expect(-1, res);
  });

  test('test case 8', () {
    int res = DateTimeUtils.compareNullable(null, dt2);
    expect(1, res);
  });
}
