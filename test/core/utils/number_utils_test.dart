import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/extensions/double.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';

void main() {
  group('divideEvenly (1 fraction digit)', () {
    const fractionDigits = 1;

    test('12 / 5', () {
      expect(
        NumberUtils.divideEvenly(12, 5, fractionDigits),
        [2.4, 2.4, 2.4, 2.4, 2.4],
      );
    });

    test('10 / 3', () {
      expect(
        NumberUtils.divideEvenly(10, 3, fractionDigits),
        [3.3, 3.3, 3.4],
      );
    });

    test('13 / 7', () {
      expect(
        NumberUtils.divideEvenly(13, 7, fractionDigits),
        [1.9, 1.9, 1.9, 1.9, 1.8, 1.8, 1.8],
      );
    });

    test('17 / 8', () {
      expect(
        NumberUtils.divideEvenly(17, 8, fractionDigits),
        [2.1, 2.1, 2.1, 2.1, 2.1, 2.1, 2.2, 2.2],
      );
    });
  });

  group('divideEvenly (2 fraction digit)', () {
    const fractionDigits = 2;

    test('22 / 7', () {
      expect(
        NumberUtils.divideEvenly(22, 7, fractionDigits),
        [3.14, 3.14, 3.14, 3.14, 3.14, 3.15, 3.15],
      );
    });

    test('12 / 5', () {
      expect(
        NumberUtils.divideEvenly(12, 5, fractionDigits),
        [2.4, 2.4, 2.4, 2.4, 2.4],
      );
    });
  });

  group('divideEvenly (no fraction digit)', () {
    const fractionDigits = 0;

    test('22 / 7', () {
      expect(
        NumberUtils.divideEvenly(22, 7, fractionDigits),
        [3, 3, 3, 3, 3, 3, 4],
      );
    });

    test('1 / 3', () {
      expect(
        NumberUtils.divideEvenly(1, 3, fractionDigits),
        [0, 0, 1],
      );
    });
  });

  group('divideEvenly (random)', () {
    Random randomizer = Random();

    for (var i = 0; i < 200; i++) {
      int fractionDigits = randomizer.nextInt(5);

      double value = randomizer.nextDouble() * randomizer.nextInt(999999) + 100;
      value = value.roundToDecimal(fractionDigits);

      int divisor = randomizer.nextInt(9990) + 1;

      test('$value / $divisor ~ $fractionDigits', () {
        final res = NumberUtils.divideEvenly(value, divisor, fractionDigits);
        expect(
          res.reduce((a, b) => a + b).roundToDecimal(fractionDigits),
          value,
        );
      });
    }
  });
}
