import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/core/utils/avatar_generator.dart';

void main() {
  Set<PersonAvatar> scopedAvatars = {
    PersonAvatar.pa002,
    PersonAvatar.pa003,
    PersonAvatar.pa005,
    PersonAvatar.pa007,
    PersonAvatar.pa011,
    PersonAvatar.pa013,
  };

  test('no any arguments', () {
    run(() {
      PersonAvatar res = AvatarGenerator.generatePersonAvatar();
      expect(PersonAvatar.values.contains(res), true);
    }, attempts: 3);
  });

  test('no owned', () {
    run(() {
      PersonAvatar res =
          AvatarGenerator.generatePersonAvatar(scopedAvatars: scopedAvatars);
      expect(scopedAvatars.contains(res), true);
    }, attempts: 3);
  });

  test('empty owned', () {
    run(() {
      PersonAvatar res = AvatarGenerator.generatePersonAvatar(
          scopedAvatars: scopedAvatars, ownedAvatars: []);
      expect(scopedAvatars.contains(res), true);
    }, attempts: 3);
  });

  test('with owned', () {
    final ownedAvatars = [
      PersonAvatar.pa001,
      PersonAvatar.pa002,
      PersonAvatar.pa003,
    ];

    run(() {
      PersonAvatar res = AvatarGenerator.generatePersonAvatar(
          scopedAvatars: scopedAvatars, ownedAvatars: ownedAvatars);
      expect(scopedAvatars.contains(res), true);
      expect(ownedAvatars.contains(res), false);
    }, attempts: 5);
  });

  test('fully owned with balanced occurrences', () {
    final ownedAvatars = [
      PersonAvatar.pa001,
      PersonAvatar.pa002,
      PersonAvatar.pa003,
      PersonAvatar.pa005,
      PersonAvatar.pa007,
      PersonAvatar.pa011,
      PersonAvatar.pa013,
    ];

    run(() {
      PersonAvatar res = AvatarGenerator.generatePersonAvatar(
          scopedAvatars: scopedAvatars, ownedAvatars: ownedAvatars);
      expect(scopedAvatars.contains(res), true);
    }, attempts: 3);
  });

  test('should avoid the most occurrence', () {
    final ownedAvatars = [
      PersonAvatar.pa002,
      PersonAvatar.pa002,
      PersonAvatar.pa003,
      PersonAvatar.pa003,
      PersonAvatar.pa005,
      PersonAvatar.pa005,
      PersonAvatar.pa007,
      PersonAvatar.pa011,
      PersonAvatar.pa013,
    ];

    run(() {
      PersonAvatar res = AvatarGenerator.generatePersonAvatar(
          scopedAvatars: scopedAvatars, ownedAvatars: ownedAvatars);
      expect(scopedAvatars.contains(res), true);
      expect(res != PersonAvatar.pa002, true);
      expect(res != PersonAvatar.pa003, true);
      expect(res != PersonAvatar.pa005, true);
    }, attempts: 5);
  });

  test('no intersection between owned and scoped', () {
    final ownedAvatars = [
      PersonAvatar.pa001,
      PersonAvatar.pa004,
      PersonAvatar.pa006,
      PersonAvatar.pa008,
      PersonAvatar.pa010,
      PersonAvatar.pa030,
    ];

    run(() {
      PersonAvatar res = AvatarGenerator.generatePersonAvatar(
          scopedAvatars: scopedAvatars, ownedAvatars: ownedAvatars);
      expect(scopedAvatars.contains(res), true);
    }, attempts: 3);
  });
}

run(VoidCallback executor, {int attempts = 1}) {
  int attempt = 1;
  while (attempt <= attempts) {
    executor();
    attempt++;
  }
}
