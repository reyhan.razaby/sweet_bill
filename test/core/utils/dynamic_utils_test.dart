import 'package:sweet_bill/core/utils/dynamic_utils.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group('isAllNotNull', () {
    test('should return the correct result', () {
      expect(
        DynamicUtils.isAllNotNull(['abc', Dummy(), '', 0]),
        true,
      );
      expect(
        DynamicUtils.isAllNotNull(['abc', null, Dummy(), '', 0]),
        false,
      );
      expect(
        DynamicUtils.isAllNotNull([]),
        true,
      );
    });
  });
}

class Dummy {}
