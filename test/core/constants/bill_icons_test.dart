import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';

void main() {
  test('convert icon from string', () {
    const icon = BillIcons.umbrella;
    final fromStringIcon = BillIcons.fromString(icon.toString());
    expect(fromStringIcon, icon);
  });

  test('get default icon from string when enum does not exist', () {
    final fromStringIcon = BillIcons.fromString('blablablabla');
    expect(fromStringIcon, BillIcons.receipt);
  });
}
