import 'dart:io';

import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/bill_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/core/io/io_gateway.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/repositories/debt_repository.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/repositories/settings_repository.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/validate_bill/validate_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generators/debts_generator.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/activate_event_group/activate_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_active_event_group/get_active_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_group_detail/get_event_group_detail_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_members/get_members_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/register_event_group_members/register_event_group_members_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/remove_event_group/remove_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/person/create_self/create_self_use_case.dart';
import 'package:sweet_bill/domain/use_cases/person/get_self/get_self_use_case.dart';
import 'package:sweet_bill/domain/use_cases/settings/get_settings/get_settings_use_case.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

class MockBillSpGateway extends Mock implements BillSpGateway {}

class MockPersonSpGateway extends Mock implements PersonSpGateway {}

class MockEventGroupSpGateway extends Mock implements EventGroupSpGateway {}

class MockBillRepository extends Mock implements BillRepository {}

class MockEventGroupRepository extends Mock implements EventGroupRepository {}

class MockPersonRepository extends Mock implements PersonRepository {}

class MockDebtRepository extends Mock implements DebtRepository {}

class MockSettingsRepository extends Mock implements SettingsRepository {}

class MockGetEventGroupsUseCase extends Mock implements GetEventGroupsUseCase {}

class MockRemoveEventGroupUseCase extends Mock
    implements RemoveEventGroupUseCase {}

class MockActivateEventGroupUseCase extends Mock
    implements ActivateEventGroupUseCase {}

class MockEventGroupTransactionsUseCase extends Mock
    implements GetActiveEventGroupUseCase {}

class MockCreateEventGroupUseCase extends Mock
    implements CreateEventGroupUseCase {}

class MockUpdateEventGroupUseCase extends Mock
    implements UpdateEventGroupUseCase {}

class MockGetEventGroupDetailUseCase extends Mock
    implements GetEventGroupDetailUseCase {}

class MockGetBillsUseCase extends Mock implements GetBillsUseCase {}

class MockGetReceivablesUseCase extends Mock implements GetReceivablesUseCase {}

class MockGenerateDebtsUseCase extends Mock implements GenerateDebtsUseCase {}

class MockCreateSelfUseCase extends Mock implements CreateSelfUseCase {}

class MockGetSelfUseCase extends Mock implements GetSelfUseCase {}

class MockGetSettingsUseCase extends Mock implements GetSettingsUseCase {}

class MockValidateBillUseCase extends Mock implements ValidateBillUseCase {}

class MockGetMembersUseCase extends Mock implements GetMembersUseCase {}

class MockRegisterEventGroupMembersUseCase extends Mock
    implements RegisterEventGroupMembersUseCase {}

class MockCreateBillUseCase extends Mock implements CreateBillUseCase {}

class MockUpdateBillUseCase extends Mock implements UpdateBillUseCase {}

class MockProfileCubit extends Mock implements ProfileCubit {}

class MockSettings extends Mock implements Settings {}

class MockToastShower extends Mock implements ToastShower {}

class MockBillFormBloc extends Mock implements BillFormBloc {}

class MockBill extends Mock implements Bill {}

class MockDebtsGenerator extends Mock implements DebtsGenerator {}

class MockFile extends Mock implements File {}

class MockIoGateway extends Mock implements IoGateway {}
