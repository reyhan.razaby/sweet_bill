import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/api/shared_preferences/models/debt_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';

import '../../../fakes.dart';

void main() {
  DebtModel model = const DebtModel(
    borrowerId: '1',
    lenderId: '3',
    amount: 8000,
  );

  test('can convert to json and revert it back', () {
    final json = model.toJson();
    DebtModel reverted = DebtModel.fromJson(json);
    expect(reverted, model);
  });

  test('return identical model with copyWith method', () {
    final newModel = model.copyWith();
    expect(newModel, model);
  });

  test('can convert to entity and revert it back', () {
    PersonModel borrower = PersonModel(
      id: '1',
      name: fakeString,
      creationTime: FakeDateTime(),
      avatar: fakeAvatar,
    );
    PersonModel lender = PersonModel(
      id: '3',
      name: fakeString,
      creationTime: FakeDateTime(),
      avatar: fakeAvatar,
    );
    final entity = model.toEntity(borrower: borrower, lender: lender);
    DebtModel reverted = DebtModel.fromEntity(entity);
    expect(reverted, model);
  });
}
