import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/api/shared_preferences/models/bill_model.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';

import '../../../fakes.dart';

void main() {
  final currentTime = DateTime.now();
  BillModel model = BillModel(
    id: fakeString,
    title: fakeString,
    eventGroupId: fakeString,
    creationTime: currentTime,
    personalCosts: const [
      BillPersonalCostModel(personId: '1', cost: 1000, additionalCost: 0),
      BillPersonalCostModel(personId: '2', cost: 1000, additionalCost: 0),
    ],
    personalPayments: const [BillPersonalPaymentModel(personId: '2', amount: 2000)],
    splitStrategy: SplitStrategies.equally,
    isAutoAdjustTax: true,
    transactionTime: currentTime,
    icon: BillIcons.hotel,
  );

  test('can convert to json and revert it back', () {
    final json = model.toJson();
    BillModel reverted = BillModel.fromJson(json);
    expect(reverted, model);
  });

  test('return identical model with copyWith method', () {
    final newModel = model.copyWith();
    expect(newModel, model);
  });

  test('can convert to entity and revert it back', () {
    final entity = model.toEntity();
    BillModel reverted = BillModel.fromEntity(entity);
    expect(reverted, model);
  });
}
