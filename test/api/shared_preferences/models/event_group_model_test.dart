import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/api/shared_preferences/models/event_group_model.dart';

void main() {
  EventGroupModel model = EventGroupModel(
    id: '123',
    name: 'Trip to Bali',
    description: "Wow it's so amazing",
    creationTime: DateTime.now(),
  );

  test('can convert to json and revert it back', () {
    final json = model.toJson();
    EventGroupModel reverted = EventGroupModel.fromJson(json);
    expect(reverted, model);
  });

  test('return identical model with copyWith method', () {
    EventGroupModel newModel = model.copyWith();
    expect(newModel, model);
  });

  test('can convert to entity and revert it back', () {
    final entity = model.toEntity();
    EventGroupModel reverted = EventGroupModel.fromEntity(entity);
    expect(reverted, model);
  });
}
