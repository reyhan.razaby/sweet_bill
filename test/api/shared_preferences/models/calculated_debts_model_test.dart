import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/api/shared_preferences/models/calculated_debts_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/debt_model.dart';

void main() {
  CalculatedDebtsModel model = const CalculatedDebtsModel(
    receivablesKey: 'xxxxx',
    debts: [
      DebtModel(borrowerId: '1', lenderId: '2', amount: 8752),
      DebtModel(borrowerId: '5', lenderId: '3', amount: 5432),
    ],
  );

  test('can convert to json and revert it back', () {
    final json = model.toJson();
    CalculatedDebtsModel reverted = CalculatedDebtsModel.fromJson(json);
    expect(reverted, model);
  });

  test('return identical model with copyWith method', () {
    final newModel = model.copyWith();
    expect(newModel, model);
  });
}
