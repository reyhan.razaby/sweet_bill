import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';

void main() {
  PersonModel model = PersonModel(
    id: '666',
    name: 'Raditya Dika',
    creationTime: DateTime.now(),
    avatar: PersonAvatar.pa001,
  );

  test('can convert to json and revert it back', () {
    final json = model.toJson();
    PersonModel reverted = PersonModel.fromJson(json);
    expect(reverted, model);
  });

  test('return identical model with copyWith method', () {
    PersonModel newModel = model.copyWith();
    expect(newModel, model);
  });

  test('can convert to entity and revert it back', () {
    final entity = model.toEntity();
    PersonModel reverted = PersonModel.fromEntity(entity);
    expect(reverted, model);
  });
}
