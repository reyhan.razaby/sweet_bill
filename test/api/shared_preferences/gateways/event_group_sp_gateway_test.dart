import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/event_group_model.dart';
import 'package:sweet_bill/core/io/io_gateway.dart';

import '../../../mocks.dart';

void main() {
  late EventGroupSpGateway gateway;
  late SharedPreferences sp;
  late IoGateway ioGateway;

  const path = 'test/api/shared_preferences/gateways';

  setUp(() {
    sp = MockSharedPreferences();
    ioGateway = IoGatewayImpl();
    gateway = EventGroupSpGateway(sp);
  });

  group('save', () {
    setUp(() {
      when(() => sp.setString(any(), any())).thenAnswer((_) async => true);
      when(() => sp.setStringList(any(), any())).thenAnswer((_) async => true);
      when(() => sp.getStringList(any())).thenReturn([]);
    });

    test('set string and string list', () async {
      EventGroupModel modelAsParam = EventGroupModel(
        id: '',
        name: 'Bali',
        creationTime: DateTime.now(),
      );
      EventGroupModel result = await gateway.save(modelAsParam);

      expect(result.id != '', true);
      verify(() => sp.setString(any(), any()));
      verify(() => sp.setStringList(any(), any()));
    });
  });

  group('getById', () {
    EventGroupModel model =
        EventGroupModel(id: '100', name: 'Bali', creationTime: DateTime.now());

    setUp(() {
      String jsonString = jsonEncode(model.toJson());
      when(() => sp.getString(any())).thenReturn(jsonString);
    });

    test('call getString and return the expected model', () async {
      EventGroupModel? result = await gateway.getById('100');

      expect(result, model);
      verify(() => sp.getString(any()));
    });
  });

  group('appendBillIds', () {
    test('should set string list with correct number of data', () async {
      when(() => sp.setStringList(any(), any())).thenAnswer((_) async => true);
      // Mock existing (current) data on the storage
      final stringJsonBill1 = ioGateway.readFile('$path/fixtures/bill_1.json');
      when(() => sp.getStringList(any())).thenReturn([stringJsonBill1]);

      await gateway.appendBillIds('x', ['bbb', 'nnn']);

      final captured =
          verify(() => sp.setStringList(any(), captureAny())).captured;

      final capturedData = captured[0] as List<String>;
      // 3 comes from 1 existing bill + 2 new bills
      expect(capturedData.length, 3);
    });
  });

  group('removeBillId', () {
    test('should set string list with correct number of data', () async {
      when(() => sp.setStringList(any(), any())).thenAnswer((_) async => true);
      // Mock existing (current) data on the storage
      when(() => sp.getStringList(any())).thenReturn(['111', '222']);

      await gateway.removeBillId('x', '111');

      final captured =
          verify(() => sp.setStringList(any(), captureAny())).captured;

      final capturedData = captured[0] as List<String>;
      // Only set one data
      expect(capturedData.length, 1);
    });
  });

  group('appendPersonIds', () {
    setUp(() {
      when(() => sp.setStringList(any(), any())).thenAnswer((_) async => true);
    });

    test('should set string list with correct number of data', () async {
      // Mock existing (current) data
      when(() => sp.getStringList(any())).thenReturn(['111']);

      await gateway.appendPersonIds('x', ['222', '333']);

      final captured =
          verify(() => sp.setStringList(any(), captureAny())).captured;
      expect(captured[0], ['111', '222', '333']);
    });

    test('some incoming person ids already exist', () async {
      // Mock existing (current) data
      when(() => sp.getStringList(any())).thenReturn(['111', '222']);

      await gateway.appendPersonIds('x', ['333', '222']);

      final captured =
          verify(() => sp.setStringList(any(), captureAny())).captured;
      expect(captured[0], ['111', '222', '333']);
    });
  });

  group('removePersonId', () {
    test('should set string list with correct number of data', () async {
      when(() => sp.setStringList(any(), any())).thenAnswer((_) async => true);
      // Mock existing (current) data on the storage
      when(() => sp.getStringList(any())).thenReturn(['111', '222']);

      await gateway.removePersonId('x', '111');

      final captured =
          verify(() => sp.setStringList(any(), captureAny())).captured;

      final capturedData = captured[0] as List<String>;
      // Only set one data
      expect(capturedData.length, 1);
    });
  });
}
