import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';

import '../../../mocks.dart';

void main() {
  late PersonSpGateway gateway;
  late SharedPreferences sp;

  setUp(() {
    sp = MockSharedPreferences();
    gateway = PersonSpGateway(sp);
  });

  group('save', () {
    setUp(() {
      when(() => sp.setString(any(), any())).thenAnswer((_) async => true);
      when(() => sp.setStringList(any(), any())).thenAnswer((_) async => true);
      when(() => sp.getStringList(any())).thenReturn([]);
    });

    test('set string and string list', () async {
      PersonModel modelAsParam = PersonModel(
        id: '',
        name: 'Rangga',
        avatar: PersonAvatar.pa001,
        creationTime: DateTime.now(),
      );
      PersonModel result = await gateway.save(modelAsParam);

      expect(result.id != '', true);
      verify(() => sp.setString(any(), any()));
      verify(() => sp.setStringList(any(), any()));
    });
  });

  group('saveSelfPersonId', () {
    setUp(() {
      when(() => sp.setString(any(), any())).thenAnswer((_) async => true);
    });

    test('set string with correct key', () async {
      const personId = '999';
      await gateway.saveSelfPersonId(personId);

      verify(() => sp.setString(PersonSpGateway.selfPerson, personId));
    });
  });

  group('getSelfPersonId', () {
    const personId = '999';
    setUp(() {
      when(() => sp.getString(any())).thenReturn(personId);
    });

    test('get string with correct key', () async {
      final result = await gateway.getSelfPersonId();

      expect(result, personId);
      verify(() => sp.getString(PersonSpGateway.selfPerson));
    });
  });
}
