import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/bill_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/bill_model.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';

import '../../../mocks.dart';

void main() {
  late BillSpGateway gateway;
  late SharedPreferences sp;
  final currentTime = DateTime.now();

  setUp(() {
    sp = MockSharedPreferences();
    gateway = BillSpGateway(sp);
  });

  group('save', () {
    setUp(() {
      when(() => sp.setString(any(), any())).thenAnswer((_) async => true);
      when(() => sp.setStringList(any(), any())).thenAnswer((_) async => true);
      when(() => sp.getStringList(any())).thenReturn([]);
    });

    test('set string and string list', () async {
      BillModel modelAsParam = BillModel(
        id: '',
        title: 'FnB',
        eventGroupId: 'x',
        creationTime: currentTime,
        personalCosts: const [
          BillPersonalCostModel(
            personId: '1',
            cost: 1000,
            additionalCost: 0,
          ),
          BillPersonalCostModel(
            personId: '2',
            cost: 1000,
            additionalCost: 0,
          ),
        ],
        personalPayments: const [
          BillPersonalPaymentModel(
            personId: '2',
            amount: 2000,
          ),
        ],
        splitStrategy: SplitStrategies.equally,
        isAutoAdjustTax: true,
        transactionTime: currentTime,
        icon: BillIcons.hotel,
      );

      BillModel result = await gateway.save(modelAsParam);

      expect(result.id != '', true);
      verify(() => sp.setString(any(), any()));
      verify(() => sp.setStringList(any(), any()));
    });
  });

  group('getById', () {
    BillModel model = BillModel(
      id: '100',
      creationTime: currentTime,
      eventGroupId: 'x',
      title: 'Cafe',
      icon: BillIcons.receipt,
      transactionTime: currentTime,
      personalCosts: const [],
      personalPayments: const [],
      splitStrategy: SplitStrategies.equally,
      isAutoAdjustTax: true,
    );

    setUp(() {
      String jsonString = jsonEncode(model.toJson());
      when(() => sp.getString(any())).thenReturn(jsonString);
    });

    test('call getString and return the expected model', () async {
      BillModel? result = await gateway.getById('100');
      expect(result, model);
      verify(() => sp.getString(any()));
    });
  });
}
