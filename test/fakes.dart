import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/api/shared_preferences/models/bill_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/event_group_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/debt/calculated_debts.dart';
import 'package:sweet_bill/domain/entities/debt/debt.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_select_item.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_param.dart';
import 'package:sweet_bill/domain/use_cases/empty_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_group_detail/get_event_groups_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_members/get_members_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/register_event_group_members/register_event_group_members_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/person/create_self/create_self_param.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';

const String fakeString = '';
const bool fakeBool = false;
const BillIcons fakeBillIcon = BillIcons.receipt;
const PersonAvatar fakeAvatar = PersonAvatar.pa001;

class FakePersonModel extends Fake implements PersonModel {}

class FakeBillModel extends Fake implements BillModel {}

class FakeEventGroupModel extends Fake implements EventGroupModel {}

class FakeCreateBillParam extends Fake implements CreateBillParam {}

class FakeUpdateBillParam extends Fake implements UpdateBillParam {}

class FakeBill extends Fake implements Bill {}

class FakeCreateEventGroupParam extends Fake implements CreateEventGroupParam {}

class FakeUpdateEventGroupParam extends Fake implements UpdateEventGroupParam {}

class FakeGetEventGroupsParam extends Fake implements GetEventGroupsParam {}

class FakeCreateSelfParam extends Fake implements CreateSelfParam {}

class FakeGetMembersParam extends Fake implements GetMembersParam {}

class FakeRegisterEventGroupMembersParam extends Fake
    implements RegisterEventGroupMembersParam {}

class FakeGetBillsParam extends Fake implements GetBillsParam {}

class FakeGenerateDebtsParam extends Fake implements GenerateDebtsParam {}

class FakeGetReceivablesParam extends Fake implements GetReceivablesParam {}

class FakeEventGroup extends Fake implements EventGroup {}

class FakePerson extends Fake implements Person {}

class FakeDebt extends Fake implements Debt {}

class FakeCalculatedDebts extends Fake implements CalculatedDebts {}

class FakeReceivable extends Fake implements Receivable {}

class FakeEventGroupSelectItem extends Fake implements EventGroupSelectItem {}

class FakeSettings extends Fake implements Settings {}

class FakeEmptyParam extends Fake implements EmptyParam {}

class FakeDateTime extends Fake implements DateTime {}

class FakeBillPersonalPayment extends Fake implements BillPersonalPayment {}

class FakeBillPersonalCost extends Fake implements BillPersonalCost {}

class FakeBillFormEvent extends Fake implements BillFormEvent {}

class FakeFile extends Fake implements File {}

class FakeGetEventGroupDetailParam extends Fake
    implements GetEventGroupDetailParam {}
