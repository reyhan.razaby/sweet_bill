import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late BillFormCostBloc billFormCostBloc;
  late BillFormBloc billFormBloc;
  late ProfileCubit profileCubit;
  late Settings settings;

  const zoe = Person(id: 'zoe', name: 'Zoe', avatar: fakeAvatar);
  const jun = Person(id: 'jun', name: 'Jun', avatar: fakeAvatar);
  const rey = Person(id: 'rey', name: 'Rey', avatar: fakeAvatar);
  const ann = Person(id: 'ann', name: 'Ann', avatar: fakeAvatar);
  const members = [zoe, jun, rey, ann];

  setUp(() {
    billFormBloc = MockBillFormBloc();
    profileCubit = MockProfileCubit();
    settings = MockSettings();
    billFormCostBloc = BillFormCostBloc(
      billFormBloc: billFormBloc,
      profileCubit: profileCubit,
    );
    when(() => billFormBloc.members).thenReturn(members);
    when(() => settings.maxFractionDigits).thenReturn(2);
    when(() => profileCubit.state).thenReturn(ProfileAdded(
      self: FakePerson(),
      settings: settings,
    ));
  });

  setUpAll(() {
    registerFallbackValue(FakeBillFormEvent());
  });

  group('LoadCostInitialValues', () {
    final currentBill = Bill(
      id: fakeString,
      title: fakeString,
      icon: fakeBillIcon,
      transactionTime: FakeDateTime(),
      eventGroupId: fakeString,
      personalCosts: [FakeBillPersonalCost()],
      personalPayments: [FakeBillPersonalPayment()],
      splitStrategy: SplitStrategies.custom,
      isAutoAdjustTax: false,
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'split equally',
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalCosts: [
              BillPersonalCost(personId: zoe.id, cost: 40, additionalCost: 0),
              BillPersonalCost(personId: jun.id, cost: 40, additionalCost: 0),
              BillPersonalCost(personId: ann.id, cost: 40, additionalCost: 0),
            ],
            splitStrategy: SplitStrategies.equally,
          ),
          totalCost: 120,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(LoadCostInitialValues()),
      expect: () {
        const expectedState = BillFormCostLoaded(
          costs: [
            MemberCost(person: ann, cost: 40, isInvolved: true),
            MemberCost(person: jun, cost: 40, isInvolved: true),
            MemberCost(person: rey, cost: 0),
            MemberCost(person: zoe, cost: 40, isInvolved: true)
          ],
          splitStrategy: SplitStrategies.equally,
          expectedTotalCost: 120,
        );
        return [expectedState];
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'split equally, with zero target and empty existing expenses',
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalCosts: [],
            splitStrategy: SplitStrategies.equally,
          ),
          totalCost: 0,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(LoadCostInitialValues()),
      expect: () {
        const expectedState = BillFormCostLoaded(
          costs: [
            MemberCost(person: ann, cost: 0),
            MemberCost(person: jun, cost: 0),
            MemberCost(person: rey, cost: 0),
            MemberCost(person: zoe, cost: 0)
          ],
          splitStrategy: SplitStrategies.equally,
          expectedTotalCost: 0,
        );
        return [expectedState];
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'split equally, with zero target and some existing expenses',
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalCosts: [
              BillPersonalCost(personId: zoe.id, cost: 0, additionalCost: 0),
              BillPersonalCost(personId: jun.id, cost: 0, additionalCost: 0),
            ],
            splitStrategy: SplitStrategies.equally,
          ),
          totalCost: 0,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(LoadCostInitialValues()),
      expect: () {
        const expectedState = BillFormCostLoaded(
          costs: [
            MemberCost(person: ann, cost: 0, isInvolved: false),
            MemberCost(person: jun, cost: 0, isInvolved: true),
            MemberCost(person: rey, cost: 0, isInvolved: false),
            MemberCost(person: zoe, cost: 0, isInvolved: true)
          ],
          splitStrategy: SplitStrategies.equally,
          expectedTotalCost: 0,
        );
        return [expectedState];
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'split by custom amount',
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalCosts: [
              BillPersonalCost(personId: jun.id, cost: 50, additionalCost: 0),
              BillPersonalCost(personId: rey.id, cost: 40, additionalCost: 0),
            ],
            splitStrategy: SplitStrategies.custom,
          ),
          totalCost: 90,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(LoadCostInitialValues()),
      expect: () {
        const expectedState = BillFormCostLoaded(
          costs: [
            MemberCost(person: ann, cost: 0),
            MemberCost(person: jun, cost: 50),
            MemberCost(person: rey, cost: 40),
            MemberCost(person: zoe, cost: 0)
          ],
          splitStrategy: SplitStrategies.custom,
          expectedTotalCost: 90,
        );
        return [expectedState];
      },
    );
  });

  group('ToggleInvolved', () {
    const expenses = [
      MemberCost(person: ann, cost: 60, isInvolved: true),
      MemberCost(person: jun, cost: 60, isInvolved: true),
      MemberCost(person: rey, cost: 60, isInvolved: true),
      MemberCost(person: zoe, cost: 60, isInvolved: true)
    ];
    const initialState = BillFormCostLoaded(
      splitStrategy: SplitStrategies.equally,
      costs: expenses,
      expectedTotalCost: 240,
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'toggle index 1 from true to false',
      seed: () => initialState,
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(const ToggleInvolved(index: 1)),
      expect: () {
        final expectedState = initialState.copyWith(costs: [
          expenses[0].copyWith(cost: 80),
          expenses[1].copyWith(cost: 0, isInvolved: false),
          expenses[2].copyWith(cost: 80),
          expenses[3].copyWith(cost: 80),
        ]);
        return [expectedState];
      },
    );
  });

  group('ChangeInvolvedAll', () {
    blocTest<BillFormCostBloc, BillFormCostState>(
      'from partially involved to involved all',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.equally,
        costs: [
          MemberCost(person: ann, cost: 60, isInvolved: true),
          MemberCost(person: jun, cost: 60, isInvolved: true),
          MemberCost(person: rey, cost: 0),
          MemberCost(person: zoe, cost: 0)
        ],
        expectedTotalCost: 120,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(const ChangeInvolvedAll(isAllInvolved: true)),
      expect: () {
        return [
          const BillFormCostLoaded(
            splitStrategy: SplitStrategies.equally,
            costs: [
              MemberCost(person: ann, cost: 30, isInvolved: true),
              MemberCost(person: jun, cost: 30, isInvolved: true),
              MemberCost(person: rey, cost: 30, isInvolved: true),
              MemberCost(person: zoe, cost: 30, isInvolved: true)
            ],
            expectedTotalCost: 120,
          )
        ];
      },
    );
  });

  group('ChangeSplitStrategy', () {
    blocTest<BillFormCostBloc, BillFormCostState>(
      'change from custom to equally',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 200),
          MemberCost(person: jun, cost: 10),
          MemberCost(person: rey, cost: 0),
          MemberCost(person: zoe, cost: 30)
        ],
        expectedTotalCost: 240,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(
          const ChangeSplitStrategy(splitStrategy: SplitStrategies.equally)),
      expect: () {
        const expectedState = BillFormCostLoaded(
          costs: [
            MemberCost(person: ann, cost: 80, isInvolved: true),
            MemberCost(person: jun, cost: 80, isInvolved: true),
            MemberCost(person: rey, cost: 0),
            MemberCost(person: zoe, cost: 80, isInvolved: true)
          ],
          splitStrategy: SplitStrategies.equally,
          expectedTotalCost: 240,
        );
        return [expectedState];
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'change from equally to custom',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.equally,
        costs: [
          MemberCost(person: ann, cost: 80, isInvolved: true),
          MemberCost(person: jun, cost: 0),
          MemberCost(person: rey, cost: 80, isInvolved: true),
          MemberCost(person: zoe, cost: 0)
        ],
        expectedTotalCost: 160,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(
          const ChangeSplitStrategy(splitStrategy: SplitStrategies.custom)),
      expect: () {
        const expectedState = BillFormCostLoaded(
          costs: [
            MemberCost(person: ann, cost: 80),
            MemberCost(person: jun, cost: 0),
            MemberCost(person: rey, cost: 80),
            MemberCost(person: zoe, cost: 0)
          ],
          splitStrategy: SplitStrategies.custom,
          expectedTotalCost: 160,
        );
        return [expectedState];
      },
    );
  });

  group('ChangeCostAmount', () {
    blocTest<BillFormCostBloc, BillFormCostState>(
      'change cost amount',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 0),
          MemberCost(person: jun, cost: 100),
          MemberCost(person: rey, cost: 70),
          MemberCost(person: zoe, cost: 30)
        ],
        expectedTotalCost: 200,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(const ChangeCostAmount(cost: 50, index: 1)),
      expect: () {
        return [
          const BillFormCostLoaded(
            splitStrategy: SplitStrategies.custom,
            costs: [
              MemberCost(person: ann, cost: 0),
              MemberCost(person: jun, cost: 50),
              MemberCost(person: rey, cost: 70),
              MemberCost(person: zoe, cost: 30)
            ],
            expectedTotalCost: 200,
          ),
        ];
      },
    );
  });

  group('SaveCost: split by custom amount', () {
    blocTest<BillFormCostBloc, BillFormCostState>(
      "normal case",
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 0),
          MemberCost(person: jun, cost: 60),
          MemberCost(person: rey, cost: 30),
          MemberCost(person: zoe, cost: 0)
        ],
        expectedTotalCost: 90,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(SaveCost()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          ChangeForm(
            splitStrategy: SplitStrategies.custom,
            costs: [
              BillPersonalCost(personId: jun.id, cost: 60),
              BillPersonalCost(personId: rey.id, cost: 30)
            ],
          ),
        );
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'with all zero amounts',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 0),
          MemberCost(person: jun, cost: 0),
          MemberCost(person: rey, cost: 0),
          MemberCost(person: zoe, cost: 0)
        ],
        expectedTotalCost: 0,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(SaveCost()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          const ChangeForm(
            splitStrategy: SplitStrategies.custom,
            costs: [],
          ),
        );
      },
    );
  });

  group('SaveCost: split equally', () {
    blocTest<BillFormCostBloc, BillFormCostState>(
      "normal case",
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.equally,
        costs: [
          MemberCost(person: ann, cost: 30, isInvolved: true),
          MemberCost(person: jun, cost: 30, isInvolved: true),
          MemberCost(person: rey, cost: 30, isInvolved: true),
          MemberCost(person: zoe, cost: 30, isInvolved: true)
        ],
        expectedTotalCost: 120,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(SaveCost()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          ChangeForm(
            splitStrategy: SplitStrategies.equally,
            costs: [
              BillPersonalCost(personId: ann.id, cost: 30),
              BillPersonalCost(personId: jun.id, cost: 30),
              BillPersonalCost(personId: rey.id, cost: 30),
              BillPersonalCost(personId: zoe.id, cost: 30)
            ],
          ),
        );
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'with all zero amounts, but some involved',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.equally,
        costs: [
          MemberCost(person: ann, cost: 0),
          MemberCost(person: jun, cost: 0),
          MemberCost(person: rey, cost: 0, isInvolved: true),
          MemberCost(person: zoe, cost: 0, isInvolved: true)
        ],
        expectedTotalCost: 0,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(SaveCost()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          ChangeForm(
            splitStrategy: SplitStrategies.equally,
            costs: [
              BillPersonalCost(personId: rey.id, cost: 0),
              BillPersonalCost(personId: zoe.id, cost: 0),
            ],
          ),
        );
      },
    );
  });

  group('AdjustTotalCostToExact', () {
    blocTest<BillFormCostBloc, BillFormCostState>(
      'add 200 to adjust to exact',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 0),
          MemberCost(person: jun, cost: 100),
          MemberCost(person: rey, cost: 50),
          MemberCost(person: zoe, cost: 150)
        ],
        expectedTotalCost: 500,
        focusedCostIndex: 1,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(AdjustFocusedCostToExact()),
      expect: () {
        return [
          const BillFormCostLoaded(
            splitStrategy: SplitStrategies.custom,
            costs: [
              MemberCost(person: ann, cost: 0),
              MemberCost(person: jun, cost: 100 + 200),
              MemberCost(person: rey, cost: 50),
              MemberCost(person: zoe, cost: 150)
            ],
            expectedTotalCost: 500,
            focusedCostIndex: 1,
          ),
        ];
      },
      verify: (bloc) {
        BillFormCostLoaded state = bloc.state as BillFormCostLoaded;
        expect(state.totalCostStatus.isExact, true);
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'subtract 500 to adjust to exact',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 50),
          MemberCost(person: jun, cost: 250),
          MemberCost(person: rey, cost: 500),
        ],
        expectedTotalCost: 300,
        focusedCostIndex: 2,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(AdjustFocusedCostToExact()),
      expect: () {
        return [
          const BillFormCostLoaded(
            splitStrategy: SplitStrategies.custom,
            costs: [
              MemberCost(person: ann, cost: 50),
              MemberCost(person: jun, cost: 250),
              MemberCost(person: rey, cost: 0),
            ],
            expectedTotalCost: 300,
            focusedCostIndex: 2,
          ),
        ];
      },
      verify: (bloc) {
        BillFormCostLoaded state = bloc.state as BillFormCostLoaded;
        expect(state.totalCostStatus.isExact, true);
      },
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'focusedCostIndex out of costs index, should emit nothing',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 0),
          MemberCost(person: jun, cost: 100),
        ],
        expectedTotalCost: 500,
        focusedCostIndex: 2,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(AdjustFocusedCostToExact()),
      expect: () => [],
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'focusedCostIndex < 0, should emit nothing',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 0),
          MemberCost(person: jun, cost: 100),
        ],
        expectedTotalCost: 500,
        focusedCostIndex: -1,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(AdjustFocusedCostToExact()),
      expect: () => [],
    );

    blocTest<BillFormCostBloc, BillFormCostState>(
      'need to subtract by 600, but the focused cost is not adjustable',
      seed: () => const BillFormCostLoaded(
        splitStrategy: SplitStrategies.custom,
        costs: [
          MemberCost(person: ann, cost: 100),
          MemberCost(person: jun, cost: 1000),
        ],
        expectedTotalCost: 500,
        focusedCostIndex: 0,
      ),
      build: () => billFormCostBloc,
      act: (bloc) => bloc.add(AdjustFocusedCostToExact()),
      expect: () => [],
    );
  });
}
