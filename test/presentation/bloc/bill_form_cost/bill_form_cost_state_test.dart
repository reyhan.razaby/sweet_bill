import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';

import '../../../fakes.dart';

void main() {
  group('BillFormCostLoaded', () {
    test('Case 1', () {
      BillFormCostLoaded state = BillFormCostLoaded(
        costs: [
          MemberCost(person: FakePerson(), cost: 200),
          MemberCost(person: FakePerson(), cost: 150),
          MemberCost(person: FakePerson(), cost: 300),
        ],
        splitStrategy: SplitStrategies.custom,
        expectedTotalCost: 650,
      );

      expect(state.totalCost, 650);
      expect(state.totalCostStatus, TotalCostStatus.exact);
      expect(state.focusedMemberCost, null);
      expect(state.isInvalidFocusedCostIndex, true);
      expect(state.isAdjustable, false);
    });

    test('Case 2', () {
      BillFormCostLoaded state = BillFormCostLoaded(
        costs: [
          MemberCost(person: FakePerson(), cost: 100),
          MemberCost(person: FakePerson(), cost: 200),
          MemberCost(person: FakePerson(), cost: 99),
        ],
        splitStrategy: SplitStrategies.custom,
        expectedTotalCost: 400,
        focusedCostIndex: 1,
      );

      expect(state.totalCost, 399);
      expect(state.totalCostStatus, TotalCostStatus.under);
      expect(state.focusedMemberCost!.cost, 200);
      expect(state.isInvalidFocusedCostIndex, false);
      expect(state.remainingTotalCost, 1);
      expect(state.isAdjustable, true);
    });

    test('Case 3', () {
      BillFormCostLoaded state = BillFormCostLoaded(
        costs: [
          MemberCost(person: FakePerson(), cost: 100),
          MemberCost(person: FakePerson(), cost: 250),
        ],
        splitStrategy: SplitStrategies.custom,
        expectedTotalCost: 300,
        focusedCostIndex: 2,
      );

      expect(state.totalCost, 350);
      expect(state.totalCostStatus, TotalCostStatus.over);
      expect(state.focusedMemberCost, null);
      expect(state.isInvalidFocusedCostIndex, true);
      expect(state.remainingTotalCost, -50);
      expect(state.isAdjustable, false);
    });

    test('Case 4', () {
      BillFormCostLoaded state = BillFormCostLoaded(
        costs: [
          MemberCost(person: FakePerson(), cost: 100),
          MemberCost(person: FakePerson(), cost: 1111),
        ],
        splitStrategy: SplitStrategies.custom,
        expectedTotalCost: 1211,
        focusedCostIndex: 1,
      );

      expect(state.totalCost, 1211);
      expect(state.totalCostStatus, TotalCostStatus.exact);
      expect(state.focusedMemberCost!.cost, 1111);
      expect(state.isInvalidFocusedCostIndex, false);
      expect(state.remainingTotalCost, 0);
      expect(state.isAdjustable, true);
    });

    test('Case 5', () {
      BillFormCostLoaded state = BillFormCostLoaded(
        costs: [
          MemberCost(person: FakePerson(), cost: 20),
          MemberCost(person: FakePerson(), cost: 30),
        ],
        splitStrategy: SplitStrategies.custom,
        expectedTotalCost: 30,
        focusedCostIndex: 0,
      );

      expect(state.totalCost, 50);
      expect(state.totalCostStatus, TotalCostStatus.over);
      expect(state.remainingTotalCost, -20);
      expect(state.focusedMemberCost!.cost, 20);
      expect(state.isInvalidFocusedCostIndex, false);
      expect(state.isAdjustable, true);
    });
  });

  group('MemberCost', () {
    test('sumCost', () {
      List<MemberCost> costs = [
        MemberCost(person: FakePerson(), cost: 300),
        MemberCost(person: FakePerson(), cost: 20),
        MemberCost(person: FakePerson(), cost: 10),
      ];
      expect(MemberCost.sumCost(costs), 330);
    });

    test('sumCost with empty list', () {
      expect(MemberCost.sumCost([]), 0);
    });
  });
}
