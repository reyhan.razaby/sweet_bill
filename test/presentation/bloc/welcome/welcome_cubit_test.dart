import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/person/create_self/create_self_use_case.dart';
import 'package:sweet_bill/presentation/bloc/welcome/welcome_cubit.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late WelcomeCubit welcomeCubit;
  late CreateSelfUseCase createSelfUseCase;
  late CreateEventGroupUseCase createEventGroupUseCase;

  const friends = [
    Person.initial(name: 'Joe', avatar: fakeAvatar),
    Person.initial(name: 'Walt', avatar: fakeAvatar),
    Person.initial(name: 'Hank', avatar: fakeAvatar),
  ];

  setUp(() {
    createSelfUseCase = MockCreateSelfUseCase();
    createEventGroupUseCase = MockCreateEventGroupUseCase();
    welcomeCubit = WelcomeCubit(
      createSelfUseCase: createSelfUseCase,
      createEventGroupUseCase: createEventGroupUseCase,
    );
  });

  group('generatePerson', () {
    blocTest<WelcomeCubit, WelcomeState>(
      'adding self',
      seed: () => const WelcomeState(),
      build: () => welcomeCubit,
      act: (cubit) =>
          cubit.generatePerson('Flynn', isSelf: true, avatar: fakeAvatar),
      expect: () => [
        const WelcomeState(
            self: Person.initial(name: 'Flynn', avatar: fakeAvatar))
      ],
    );

    blocTest<WelcomeCubit, WelcomeState>(
      'adding non-self, should put into friends',
      seed: () => const WelcomeState(
        self: Person.initial(name: 'Me', avatar: fakeAvatar),
        friends: friends,
      ),
      build: () => welcomeCubit,
      act: (cubit) => cubit.generatePerson('Ryan', avatar: fakeAvatar),
      expect: () => [
        WelcomeState(
          self: const Person.initial(name: 'Me', avatar: fakeAvatar),
          friends: List.of(friends)
            ..add(const Person.initial(name: 'Ryan', avatar: fakeAvatar)),
        ),
      ],
    );
  });

  group('removeFriend', () {
    blocTest<WelcomeCubit, WelcomeState>(
      'successfully remove one friend',
      seed: () => const WelcomeState(friends: friends),
      build: () => welcomeCubit,
      act: (cubit) => cubit.removeFriend(1),
      expect: () => [
        const WelcomeState(friends: [
          Person.initial(name: 'Joe', avatar: fakeAvatar),
          Person.initial(name: 'Hank', avatar: fakeAvatar),
        ])
      ],
    );

    blocTest<WelcomeCubit, WelcomeState>(
      'invalid index',
      seed: () => const WelcomeState(friends: friends),
      build: () => welcomeCubit,
      act: (cubit) => cubit.removeFriend(3),
      expect: () => [],
    );

    blocTest<WelcomeCubit, WelcomeState>(
      'with null friends state',
      seed: () => WelcomeState(self: FakePerson(), friends: null),
      build: () => welcomeCubit,
      act: (cubit) => cubit.removeFriend(0),
      expect: () => [],
    );
  });

  group('submitAll', () {
    setUpAll(() {
      registerFallbackValue(FakeCreateSelfParam());
      registerFallbackValue(FakeCreateEventGroupParam());
    });

    blocTest<WelcomeCubit, WelcomeState>(
      'with null self, should return failed',
      seed: () => const WelcomeState(self: null),
      build: () => welcomeCubit,
      act: (cubit) => cubit.submitAll(),
      expect: () => [
        const WelcomeState(self: null, status: WelcomeStatus.submitting),
        const WelcomeState(self: null, status: WelcomeStatus.submitFailed),
        const WelcomeState(self: null, status: WelcomeStatus.idle),
      ],
      verify: (_) {
        verifyNever(() => createEventGroupUseCase(any()));
      },
    );

    const self = Person.initial(name: 'Joe', avatar: fakeAvatar);
    final selfWithId = self.copyWith(id: '89');
    const friends = [
      Person.initial(name: 'Ken', avatar: fakeAvatar),
      Person.initial(name: 'Vin', avatar: fakeAvatar),
    ];
    setUp(() {
      when(() => createSelfUseCase(any())).thenAnswer(
        (_) async => Right(selfWithId),
      );
    });

    WelcomeState initialState = const WelcomeState(
      self: self,
      friends: friends,
      groupName: 'Bali 2020',
    );
    blocTest<WelcomeCubit, WelcomeState>('failed to create event group',
        setUp: () {
          when(() => createEventGroupUseCase(any())).thenAnswer(
            (_) async => const Left(Failure()),
          );
        },
        seed: () => initialState,
        build: () => welcomeCubit,
        act: (cubit) => cubit.submitAll(),
        expect: () => [
              initialState.copyWith(status: WelcomeStatus.submitting),
              initialState.copyWith(status: WelcomeStatus.submitFailed),
              initialState.copyWith(status: WelcomeStatus.idle),
            ]);

    blocTest<WelcomeCubit, WelcomeState>('successfully submitted',
        setUp: () {
          when(() => createEventGroupUseCase(any())).thenAnswer((inv) async {
            CreateEventGroupParam param = inv.positionalArguments[0];
            return Right(param.eventGroup.copyWith(id: '776'));
          });
        },
        seed: () => initialState,
        build: () => welcomeCubit,
        act: (cubit) => cubit.submitAll(),
        expect: () => [
              initialState.copyWith(status: WelcomeStatus.submitting),
              initialState.copyWith(
                  self: selfWithId, status: WelcomeStatus.submitSucceed),
            ],
        verify: (cubit) {
          final captured =
              verify(() => createEventGroupUseCase(captureAny())).captured;
          expect(captured.length, 1);
          expect(
            captured[0],
            CreateEventGroupParam(
              eventGroup: const EventGroup.initial(name: 'Bali 2020'),
              members: [selfWithId, ...friends],
              setAsActive: true,
            ),
          );
        });
  });
}
