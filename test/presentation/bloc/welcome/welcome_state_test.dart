import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/presentation/bloc/welcome/welcome_cubit.dart';

void main() {
  const person1 = Person.initial(name: '1', avatar: PersonAvatar.pa001);
  const person2 = Person.initial(name: '2', avatar: PersonAvatar.pa002);
  const person3 = Person.initial(name: '3', avatar: PersonAvatar.pa003);

  test('get members with null self', () {
    const state = WelcomeState(
      self: null,
      friends: [person1, person2, person3],
    );
    expect(state.members, [person1, person2, person3]);
  });

  test('get members with null friends', () {
    const state = WelcomeState(
      self: person2,
      friends: null,
    );
    expect(state.members, [person2]);
  });

  test('get members with complete members', () {
    const state = WelcomeState(
      self: person2,
      friends: [person1, person3],
    );
    expect(state.members, [person2, person1, person3]);
  });

  test('get empty members', () {
    const state = WelcomeState(self: null, friends: null);
    expect(state.members, []);
  });

  test('get avatars', () {
    const state = WelcomeState(
      self: person3,
      friends: [person2, person1],
    );
    expect(
      state.avatars,
      [PersonAvatar.pa003, PersonAvatar.pa002, PersonAvatar.pa001],
    );
  });

  test('get empty avatars', () {
    const state = WelcomeState(self: null, friends: null);
    expect(state.avatars, []);
  });
}
