import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/use_cases/person/get_self/get_self_use_case.dart';
import 'package:sweet_bill/domain/use_cases/settings/get_settings/get_settings_use_case.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late ProfileCubit profileCubit;
  late GetSelfUseCase getSelfUseCase;
  late GetSettingsUseCase getSettingsUseCase;

  setUp(() {
    getSelfUseCase = MockGetSelfUseCase();
    getSettingsUseCase = MockGetSettingsUseCase();
    profileCubit = ProfileCubit(
      getSelfUseCase: getSelfUseCase,
      getSettingsUseCase: getSettingsUseCase,
    );
  });

  setUpAll(() {
    registerFallbackValue(FakeCreateSelfParam());
    registerFallbackValue(FakeSettings());
    registerFallbackValue(FakeEmptyParam());
  });

  group('addProfile', () {
    const self = Person(id: '222', name: 'Rangga', avatar: PersonAvatar.pa001);
    final settings = FakeSettings();

    blocTest<ProfileCubit, ProfileState>(
      'should add profile',
      setUp: () {
        when(() => getSettingsUseCase(any()))
            .thenAnswer((_) async => Right(settings));
      },
      build: () => profileCubit,
      act: (cubit) => cubit.addProfile(self: self),
      expect: () => [
        ProfileAdded(self: self, settings: settings),
      ],
    );
  });
}
