import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_detail.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_group_detail/get_event_group_detail_use_case.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late EventGroupFormBloc eventGroupFormBloc;
  late ProfileCubit profileCubit;
  late GetEventGroupDetailUseCase getEventGroupDetailUseCase;

  const self = Person.initial(id: '11', name: 'Joe', avatar: fakeAvatar);

  setUp(() {
    profileCubit = MockProfileCubit();
    getEventGroupDetailUseCase = MockGetEventGroupDetailUseCase();
    eventGroupFormBloc = EventGroupFormBloc(
      profileCubit: profileCubit,
      createEventGroupUseCase: MockCreateEventGroupUseCase(),
      updateEventGroupUseCase: MockUpdateEventGroupUseCase(),
      getEventGroupDetailUseCase: getEventGroupDetailUseCase,
      getBillsUseCase: MockGetBillsUseCase(),
      ioGateway: MockIoGateway(),
    );

    when(() => profileCubit.state)
        .thenReturn(ProfileAdded(self: self, settings: FakeSettings()));
  });

  setUpAll(() {
    registerFallbackValue(FakeGetEventGroupDetailParam());
  });

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'create new form',
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(InitializeForm()),
    expect: () => [
      FormLoading(),
      FormLoaded(
        name: '',
        members: [EventGroupMember.selfPerson(self)],
      ),
    ],
  );

  const member2 = Person(id: '12', name: 'Budi', avatar: PersonAvatar.pa002);
  const member3 = Person(id: '13', name: 'Cedi', avatar: PersonAvatar.pa003);
  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'init form with existing event group',
    setUp: () {
      when(() => getEventGroupDetailUseCase(any()))
          .thenAnswer((_) async => const Right(
                EventGroupDetail(
                    eventGroup: EventGroup(id: '666', name: 'The gank'),
                    members: [self, member2, member3]),
              ));
    },
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(InitializeForm(eventGroupId: '666')),
    expect: () => [
      FormLoading(),
      FormLoaded(
        eventGroupId: '666',
        name: 'The gank',
        members: [
          EventGroupMember.selfPerson(self),
          EventGroupMember.friendPerson(member2),
          EventGroupMember.friendPerson(member3),
        ],
      ),
    ],
  );
}
