import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_use_case.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/handlers/remove_member_handler.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late EventGroupFormBloc eventGroupFormBloc;
  late GetBillsUseCase getBillsUseCase;

  setUp(() {
    getBillsUseCase = MockGetBillsUseCase();
    eventGroupFormBloc = EventGroupFormBloc(
      profileCubit: MockProfileCubit(),
      createEventGroupUseCase: MockCreateEventGroupUseCase(),
      updateEventGroupUseCase: MockUpdateEventGroupUseCase(),
      getEventGroupDetailUseCase: MockGetEventGroupDetailUseCase(),
      getBillsUseCase: getBillsUseCase,
      ioGateway: MockIoGateway(),
    );
  });

  setUpAll(() {
    registerFallbackValue(FakeGetBillsParam());
  });

  const initialMembers = [
    EventGroupMember.self('aa', 'Joe', fakeAvatar),
    EventGroupMember.friend('bb', 'Walt', fakeAvatar),
    EventGroupMember.newMember('Flynn', fakeAvatar),
  ];
  final initialState = FormLoaded(
    name: 'Our group',
    imagePath: 'the/path',
    members: initialMembers,
  );

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'remove invalid index, should emit nothing',
    seed: () => initialState,
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc
      ..add(RemoveMember(index: -1))
      ..add(RemoveMember(index: 1000)),
    expect: () => [],
  );

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'disallow removing self (should emit nothing)',
    seed: () => initialState,
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(RemoveMember(index: 0)),
    expect: () => [],
  );

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'should not validate involvement in creation mode (null eventGroupId)',
    seed: () => initialState,
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(RemoveMember(index: 1)),
    expect: () => [
      FormLoaded.copyWithState(
        initialState,
        members: [initialMembers[0], initialMembers[2]],
        buildForm: false,
      ),
    ],
    verify: (bloc) => verifyNever(() => getBillsUseCase(any())),
  );

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'should not validate involvement of new member',
    seed: () => initialState,
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(RemoveMember(index: 2)),
    expect: () => [
      FormLoaded.copyWithState(
        initialState,
        members: [initialMembers[0], initialMembers[1]],
        buildForm: false,
      ),
    ],
    verify: (bloc) => verifyNever(() => getBillsUseCase(any())),
  );

  group('validate involvement', () {
    final involvedBills = [
      Bill.asDefault(
          id: '1', title: 'bill1', personalCosts: const [], personalPayments: const [])
    ];
    final editModeState =
        FormLoaded.copyWithState(initialState, eventGroupId: 'egeg');

    verifyGetMembersUseCase() {
      final captured = verify(() => getBillsUseCase(captureAny())).captured;
      expect(captured.length, 1);
      expect(captured[0],
          const GetBillsParam(eventGroupId: 'egeg', involvedPersonId: 'bb'));
    }

    blocTest<EventGroupFormBloc, EventGroupFormState>(
      'involved bills found',
      seed: () => editModeState,
      setUp: () {
        when(() => getBillsUseCase(any()))
            .thenAnswer((_) async => Right(involvedBills));
      },
      build: () => eventGroupFormBloc,
      act: (bloc) => bloc.add(RemoveMember(index: 1)),
      expect: () => [
        FormError(
          editModeState,
          titleMessage: RemoveMemberHandler.errorTitleMessage,
          bodyMessage: RemoveMemberHandler.buildBodyMessage(
              initialState.members[1], involvedBills),
        ),
        FormLoaded.copyWithState(editModeState, buildForm: true),
      ],
      verify: (_) => verifyGetMembersUseCase(),
    );

    blocTest<EventGroupFormBloc, EventGroupFormState>(
      'no involved bills found',
      seed: () => editModeState,
      setUp: () {
        when(() => getBillsUseCase(any()))
            .thenAnswer((_) async => const Right([]));
      },
      build: () => eventGroupFormBloc,
      act: (bloc) => bloc.add(RemoveMember(index: 1)),
      expect: () => [
        FormLoaded.copyWithState(
          editModeState,
          members: [editModeState.members[0], editModeState.members[2]],
          buildForm: false,
        ),
      ],
      verify: (_) => verifyGetMembersUseCase(),
    );
  });
}
