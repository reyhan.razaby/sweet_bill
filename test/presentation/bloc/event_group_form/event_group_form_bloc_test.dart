import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/core/io/io_gateway.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_group_detail/get_event_group_detail_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_use_case.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late EventGroupFormBloc eventGroupFormBloc;
  late ProfileCubit profileCubit;
  late CreateEventGroupUseCase createEventGroupUseCase;
  late UpdateEventGroupUseCase updateEventGroupUseCase;
  late GetEventGroupDetailUseCase getEventGroupDetailUseCase;
  late GetBillsUseCase getBillsUseCase;
  late IoGateway ioGateway;

  setUp(() {
    profileCubit = MockProfileCubit();
    createEventGroupUseCase = MockCreateEventGroupUseCase();
    updateEventGroupUseCase = MockUpdateEventGroupUseCase();
    getEventGroupDetailUseCase = MockGetEventGroupDetailUseCase();
    getBillsUseCase = MockGetBillsUseCase();
    ioGateway = MockIoGateway();
    eventGroupFormBloc = EventGroupFormBloc(
      profileCubit: profileCubit,
      createEventGroupUseCase: createEventGroupUseCase,
      updateEventGroupUseCase: updateEventGroupUseCase,
      getEventGroupDetailUseCase: getEventGroupDetailUseCase,
      getBillsUseCase: getBillsUseCase,
      ioGateway: ioGateway,
    );
  });

  group('ChangeImageFile:', () {
    final initialState = FormLoaded(
      name: 'Travel to Bali',
      imagePath: 'the/path',
      members: const [EventGroupMember.newMember('Joe', fakeAvatar)],
    );

    blocTest<EventGroupFormBloc, EventGroupFormState>(
      'change image successfully',
      seed: () => initialState,
      build: () => eventGroupFormBloc,
      act: (bloc) => bloc.add(ChangeImagePath(imagePath: 'new/path')),
      expect: () => [
        FormLoaded(
          name: initialState.name,
          imagePath: 'new/path',
          members: initialState.members,
          buildForm: false,
        )
      ],
    );
  });

  group('RemoveImageFile:', () {
    final initialEvent = FormLoaded(
      name: 'Travel to Bali',
      imagePath: 'the/path',
      members: const [EventGroupMember.newMember('Joe', fakeAvatar)],
    );

    blocTest<EventGroupFormBloc, EventGroupFormState>(
      'should make imagePath to be null',
      seed: () => initialEvent,
      build: () => eventGroupFormBloc,
      act: (bloc) => bloc.add(RemoveImagePath()),
      expect: () => [
        FormLoaded(
          name: initialEvent.name,
          imagePath: null,
          members: initialEvent.members,
          buildForm: false,
        )
      ],
    );
  });
}
