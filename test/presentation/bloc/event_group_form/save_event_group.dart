import 'dart:io';

import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/io/io_gateway.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_use_case.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late EventGroupFormBloc eventGroupFormBloc;
  late CreateEventGroupUseCase createEventGroupUseCase;
  late UpdateEventGroupUseCase updateEventGroupUseCase;
  late IoGateway ioGateway;

  final File savedFile = MockFile();

  setUp(() {
    createEventGroupUseCase = MockCreateEventGroupUseCase();
    updateEventGroupUseCase = MockUpdateEventGroupUseCase();
    ioGateway = MockIoGateway();
    eventGroupFormBloc = EventGroupFormBloc(
      profileCubit: MockProfileCubit(),
      createEventGroupUseCase: createEventGroupUseCase,
      updateEventGroupUseCase: updateEventGroupUseCase,
      getEventGroupDetailUseCase: MockGetEventGroupDetailUseCase(),
      getBillsUseCase: MockGetBillsUseCase(),
      ioGateway: ioGateway,
    );

    when(() => ioGateway.saveFileToAppDir(any()))
        .thenAnswer((_) async => savedFile);
    when(() => savedFile.path).thenReturn('savedFilePath');
    when(() => createEventGroupUseCase(any()))
        .thenAnswer((_) async => Right(FakeEventGroup()));
    when(() => updateEventGroupUseCase(any()))
        .thenAnswer((_) async => Right(FakeEventGroup()));
    when(() => ioGateway.deleteFile(any())).thenAnswer((_) async => true);
  });

  setUpAll(() {
    registerFallbackValue(FakeCreateEventGroupParam());
    registerFallbackValue(FakeUpdateEventGroupParam());
  });

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'create event group',
    seed: () => FormLoaded(
      name: 'Foo',
      imagePath: 'cachedFilePath',
      members: const [
        EventGroupMember.self('1', 'Steve', fakeAvatar),
        EventGroupMember.friend('2', 'Walt', fakeAvatar),
        EventGroupMember.newMember('Joe', fakeAvatar)
      ],
    ),
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(SaveEventGroup()),
    verify: (bloc) {
      final capturedParam =
          verify(() => createEventGroupUseCase(captureAny())).captured;
      expect(
        capturedParam[0],
        const CreateEventGroupParam(
          eventGroup:
              EventGroup.initial(name: 'Foo', imagePath: 'savedFilePath'),
          members: [
            Person(id: '1', name: 'Steve', avatar: fakeAvatar),
            Person(id: '2', name: 'Walt', avatar: fakeAvatar),
            Person(id: '', name: 'Joe', avatar: fakeAvatar),
          ],
        ),
      );
    },
  );

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'update event group without change imagePath',
    setUp: () {
      eventGroupFormBloc.initialLoadedState = FormLoaded(
        eventGroupId: '111',
        name: 'Foo',
        imagePath: 'filePath',
        members: const [
          EventGroupMember.self('1', 'Steve', fakeAvatar),
          EventGroupMember.friend('2', 'Walt', fakeAvatar),
        ],
      );
    },
    seed: () => FormLoaded(
      eventGroupId: '111',
      name: 'Bar',
      imagePath: 'filePath',
      members: const [
        EventGroupMember.self('1', 'Steve', fakeAvatar),
        EventGroupMember.friend('3', 'Hank', fakeAvatar),
      ],
    ),
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(SaveEventGroup()),
    verify: (bloc) {
      // Should not delete anything. Because the imagePath isn't changed
      verifyNever(() => ioGateway.deleteFile(any()));
      verifyNever(() => ioGateway.saveFileToAppDir('filePath'));

      final capturedParam =
          verify(() => updateEventGroupUseCase(captureAny())).captured;
      expect(
        capturedParam[0],
        const UpdateEventGroupParam(
          eventGroup: EventGroup(id: '111', name: 'Bar', imagePath: 'filePath'),
          members: [
            Person(id: '1', name: 'Steve', avatar: fakeAvatar),
            Person(id: '3', name: 'Hank', avatar: fakeAvatar),
          ],
        ),
      );
    },
  );

  blocTest<EventGroupFormBloc, EventGroupFormState>(
    'update event group with new imagePath',
    setUp: () {
      eventGroupFormBloc.initialLoadedState = FormLoaded(
        eventGroupId: '111',
        name: 'Foo',
        imagePath: 'oldImagePath',
        members: const [
          EventGroupMember.self('1', 'Steve', fakeAvatar),
          EventGroupMember.friend('2', 'Walt', fakeAvatar),
        ],
      );
    },
    seed: () => FormLoaded(
      eventGroupId: '111',
      name: 'Foo',
      imagePath: 'newCachedImagePath',
      members: const [
        EventGroupMember.self('1', 'Steve', fakeAvatar),
        EventGroupMember.newMember('Walt', fakeAvatar),
      ],
    ),
    build: () => eventGroupFormBloc,
    act: (bloc) => bloc.add(SaveEventGroup()),
    verify: (bloc) {
      // Should delete old imagePath because will use the new one
      verify(() => ioGateway.deleteFile('oldImagePath'));
      verify(() => ioGateway.saveFileToAppDir('newCachedImagePath'));

      final capturedParam =
          verify(() => updateEventGroupUseCase(captureAny())).captured;
      expect(
        capturedParam[0],
        const UpdateEventGroupParam(
          eventGroup:
              EventGroup(id: '111', name: 'Foo', imagePath: 'savedFilePath'),
          members: [
            Person(id: '1', name: 'Steve', avatar: fakeAvatar),
            Person(id: '', name: 'Walt', avatar: fakeAvatar),
          ],
        ),
      );
    },
  );
}
