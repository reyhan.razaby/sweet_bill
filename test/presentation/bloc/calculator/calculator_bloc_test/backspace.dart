import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late CalculatorBloc calculatorBloc;
  late ProfileCubit profileCubit;
  late Settings settings;
  late ToastShower toastShower;
  final EvaluateExpressionUseCase evaluateExpressionUseCase =
      EvaluateExpressionUseCase();
  TextEditingController fc = TextEditingController();

  setUp(() {
    settings = MockSettings();
    when(() => settings.decimalSeparator).thenReturn(',');
    when(() => settings.thousandSeparator).thenReturn('.');
    when(() => settings.maxFractionDigits).thenReturn(2);
    when(() => settings.maxWholeDigits).thenReturn(10);
    when(() => settings.locale).thenReturn('ID');

    profileCubit = MockProfileCubit();
    when(() => profileCubit.state).thenReturn(ProfileAdded(
      self: FakePerson(),
      settings: settings,
    ));

    toastShower = MockToastShower();
    when(() => toastShower.showToast(any())).thenAnswer((_) {});

    calculatorBloc = CalculatorBloc(
      profileCubit: profileCubit,
      toastShower: toastShower,
      evaluateExpressionUseCase: evaluateExpressionUseCase,
    );
    fc = TextEditingController();
  });

  void setupFc(String text, int start, [int? end]) {
    fc.text = text;
    if (end != null) {
      fc.selection = TextSelection(baseOffset: start, extentOffset: end);
    } else {
      fc.selection = TextSelection.collapsed(offset: start);
    }
  }

  void expectFc(String text, int start, [int? end]) {
    expect(fc.text, text);
    expect(fc.selection.start, start);
    if (end != null) {
      expect(fc.selection.end, end);
    }
  }

  void addEvent(CalculatorBloc bloc) => bloc.add(Backspace());

  CalculatorOpened openedState({
    required double value,
    double? expressionResult,
    bool hasOperator = false,
  }) {
    return CalculatorOpened(
      fieldController: fc,
      value: value,
      expressionResult: expressionResult,
      hasOperator: hasOperator,
    );
  }

  group('Backspace (simple)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 35| → do backspace → 3|',
      setUp: () => setupFc('35', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 35),
      act: addEvent,
      expect: () => [openedState(value: 3)],
      verify: (bloc) => expectFc('3', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 9| → do backspace → |',
      setUp: () => setupFc('9', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 9),
      act: addEvent,
      expect: () => [openedState(value: 0)],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0| → do backspace → |',
      setUp: () => setupFc('0', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → do backspace → |',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -1| → do backspace → -|',
      setUp: () => setupFc('-1', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: -1),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('-', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -| → do backspace → |',
      setUp: () => setupFc('-', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: -1),
      act: addEvent,
      expect: () => [openedState(value: 0)],
      verify: (bloc) => expectFc('', 0),
    );
  });

  group('Backspace (formatted)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 8.976| → do backspace → 897|',
      setUp: () => setupFc('8.976', 5),
      build: () => calculatorBloc,
      seed: () => openedState(value: 8976),
      act: addEvent,
      expect: () => [openedState(value: 897)],
      verify: (bloc) => expectFc('897', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 70.572.123| → do backspace → 7.057.212|',
      setUp: () => setupFc('70.572.123', 10),
      build: () => calculatorBloc,
      seed: () => openedState(value: 70572123),
      act: addEvent,
      expect: () => [openedState(value: 7057212)],
      verify: (bloc) => expectFc('7.057.212', 9),
    );
  });

  group('Backspace (decimal)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 24,87| → do backspace → 24,8|',
      setUp: () => setupFc('24,87', 5),
      build: () => calculatorBloc,
      seed: () => openedState(value: 24.87),
      act: addEvent,
      expect: () => [openedState(value: 24.8)],
      verify: (bloc) => expectFc('24,8', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 14,8| → do backspace → 14,|',
      setUp: () => setupFc('14,8', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 14.8),
      act: addEvent,
      expect: () => [openedState(value: 14)],
      verify: (bloc) => expectFc('14,', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,| → do backspace → 0|',
      setUp: () => setupFc('0,', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('0', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 14.509,8| → do backspace → 14.509,|',
      setUp: () => setupFc('14.509,8', 8),
      build: () => calculatorBloc,
      seed: () => openedState(value: 14509.8),
      act: addEvent,
      expect: () => [openedState(value: 14509)],
      verify: (bloc) => expectFc('14.509,', 7),
    );
  });

  group('Backspace (contains operator)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 24+3| → do backspace → 24+|',
      setUp: () => setupFc('24+3', 4),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 24, expressionResult: 27, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 24, hasOperator: true)],
      verify: (bloc) => expectFc('24+', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 14×0,1| → do backspace → 14×0|',
      setUp: () => setupFc('14×0,1', 6),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 14, expressionResult: 1.4, hasOperator: true),
      act: addEvent,
      expect: () =>
          [openedState(value: 14, expressionResult: 0, hasOperator: true)],
      verify: (bloc) => expectFc('14×0,', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0+| → do backspace → 0|',
      setUp: () => setupFc('0+', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 0, hasOperator: false)],
      verify: (bloc) => expectFc('0', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 900÷14.509| → do backspace → 900÷1.450|',
      setUp: () => setupFc('900÷14.509', 10),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 10, expressionResult: 0.06, hasOperator: true),
      act: addEvent,
      expect: () =>
          [openedState(value: 10, expressionResult: 0.62, hasOperator: true)],
      verify: (bloc) => expectFc('900÷1.450', 9),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 50%| → do backspace → 50|',
      setUp: () => setupFc('50%', 3),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 50, expressionResult: 0.5, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 50)],
      verify: (bloc) => expectFc('50', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 10÷0| → do backspace → 10÷|',
      setUp: () => setupFc('10÷0', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 10, hasOperator: true),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('10÷', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 50%-| → do backspace → 50%|',
      setUp: () => setupFc('50%-', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 50, hasOperator: true),
      act: addEvent,
      expect: () =>
          [openedState(value: 50, expressionResult: 0.5, hasOperator: true)],
      verify: (bloc) => expectFc('50%', 3),
    );
  });

  group('Backspace (max length exceeded)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 5.023.091| → do backspace → 502.309|',
      setUp: () {
        when(() => settings.maxWholeDigits).thenReturn(5);
        setupFc('5.023.091', 9);
      },
      build: () => calculatorBloc,
      seed: () => openedState(value: 5023091),
      act: addEvent,
      expect: () => [openedState(value: 502309)],
      verify: (bloc) => expectFc('502.309', 7),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 591,1234| → do backspace → 591,123|',
      setUp: () {
        when(() => settings.maxFractionDigits).thenReturn(3);
        setupFc('591,1234', 8);
      },
      build: () => calculatorBloc,
      seed: () => openedState(value: 591.1234),
      act: addEvent,
      expect: () => [openedState(value: 591.123)],
      verify: (bloc) => expectFc('591,123', 7),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5.023.091,1234| → do backspace → 5.023.091,123|',
      setUp: () {
        when(() => settings.maxWholeDigits).thenReturn(5);
        when(() => settings.maxFractionDigits).thenReturn(3);
        setupFc('5.023.091,1234', 14);
      },
      build: () => calculatorBloc,
      seed: () => openedState(value: 5023091.1234),
      act: addEvent,
      expect: () => [openedState(value: 5023091.123)],
      verify: (bloc) => expectFc('5.023.091,123', 13),
    );
  });
}
