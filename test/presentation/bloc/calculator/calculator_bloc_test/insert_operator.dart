import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late CalculatorBloc calculatorBloc;
  late ProfileCubit profileCubit;
  late Settings settings;
  late ToastShower toastShower;
  final EvaluateExpressionUseCase evaluateExpressionUseCase =
      EvaluateExpressionUseCase();
  TextEditingController fc = TextEditingController();

  setUp(() {
    settings = MockSettings();
    when(() => settings.decimalSeparator).thenReturn(',');
    when(() => settings.thousandSeparator).thenReturn('.');
    when(() => settings.maxFractionDigits).thenReturn(2);
    when(() => settings.maxWholeDigits).thenReturn(10);
    when(() => settings.locale).thenReturn('ID');

    profileCubit = MockProfileCubit();
    when(() => profileCubit.state).thenReturn(ProfileAdded(
      self: FakePerson(),
      settings: settings,
    ));

    toastShower = MockToastShower();
    when(() => toastShower.showToast(any())).thenAnswer((_) {});

    calculatorBloc = CalculatorBloc(
      profileCubit: profileCubit,
      toastShower: toastShower,
      evaluateExpressionUseCase: evaluateExpressionUseCase,
    );
    fc = TextEditingController();
  });

  void setupFc(String text, int start, [int? end]) {
    fc.text = text;
    if (end != null) {
      fc.selection = TextSelection(baseOffset: start, extentOffset: end);
    } else {
      fc.selection = TextSelection.collapsed(offset: start);
    }
  }

  void expectFc(String text, int start, [int? end]) {
    expect(fc.text, text);
    expect(fc.selection.start, start);
    if (end != null) {
      expect(fc.selection.end, end);
    }
  }

  void addEvent(CalculatorBloc bloc, Operator operator) =>
      bloc.add(InsertOperator(operator: operator));

  CalculatorOpened openedState({
    required double value,
    double? expressionResult,
    bool hasOperator = false,
  }) {
    return CalculatorOpened(
      fieldController: fc,
      value: value,
      expressionResult: expressionResult,
      hasOperator: hasOperator,
    );
  }

  group('InsertOperator (simple)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 35| → insert + → 35+|',
      setUp: () => setupFc('35', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 35),
      act: (bloc) => addEvent(bloc, Operator.add),
      expect: () => [openedState(value: 35, hasOperator: true)],
      verify: (bloc) => expectFc('35+', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 72| → insert - → 72-|',
      setUp: () => setupFc('72', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 72),
      act: (bloc) => addEvent(bloc, Operator.subtract),
      expect: () => [openedState(value: 72, hasOperator: true)],
      verify: (bloc) => expectFc('72-', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 98| → insert × → 98×|',
      setUp: () => setupFc('98', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 98),
      act: (bloc) => addEvent(bloc, Operator.multiply),
      expect: () => [openedState(value: 98, hasOperator: true)],
      verify: (bloc) => expectFc('98×', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 104| → insert ÷ → 104÷|',
      setUp: () => setupFc('104', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 104),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [openedState(value: 104, hasOperator: true)],
      verify: (bloc) => expectFc('104÷', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 208| → insert % → 208%|',
      setUp: () => setupFc('208', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 208),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () =>
          [openedState(value: 208, expressionResult: 2.08, hasOperator: true)],
      verify: (bloc) => expectFc('208%', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert + → |',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.add),
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert - → |',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.subtract),
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert × → |',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.multiply),
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert ÷ → |',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert % → |',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0| → insert ÷ → 0÷|',
      setUp: () => setupFc('0', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [openedState(value: 0, hasOperator: true)],
      verify: (bloc) => expectFc('0÷', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0| → insert % → 0%|',
      setUp: () => setupFc('0', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () =>
          [openedState(value: 0, expressionResult: 0, hasOperator: true)],
      verify: (bloc) => expectFc('0%', 2),
    );
  });

  group('InsertOperator (negative)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given -1| → insert + → -1+|',
      setUp: () => setupFc('-1', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: -1),
      act: (bloc) => addEvent(bloc, Operator.add),
      expect: () => [openedState(value: -1, hasOperator: true)],
      verify: (bloc) => expectFc('-1+', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -10| → insert % → -10%|',
      setUp: () => setupFc('-10', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: -10),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () =>
          [openedState(value: -10, expressionResult: -0.1, hasOperator: true)],
      verify: (bloc) => expectFc('-10%', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -10%| → insert + → -10%+|',
      setUp: () => setupFc('-10%', 4),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: -10, expressionResult: -0.1, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.add),
      expect: () => [openedState(value: -10, hasOperator: true)],
      verify: (bloc) => expectFc('-10%+', 5),
    );
  });

  group('InsertOperator (many operators)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 32-1×2| → insert ÷ → 32-1×2÷|',
      setUp: () => setupFc('32-1×2', 6),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 32, expressionResult: 30, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [openedState(value: 32, hasOperator: true)],
      verify: (bloc) => expectFc('32-1×2÷', 7),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 20%| → insert % → 20%%|',
      setUp: () {
        when(() => settings.maxFractionDigits).thenReturn(3);
        setupFc('20%', 3);
      },
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 20, expressionResult: 0.2, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () =>
          [openedState(value: 20, expressionResult: 0.002, hasOperator: true)],
      verify: (bloc) => expectFc('20%%', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 32-2| → insert % → 32-2%|',
      setUp: () => setupFc('32-2', 4),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 32, expressionResult: 30, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () =>
          [openedState(value: 32, expressionResult: 31.98, hasOperator: true)],
      verify: (bloc) => expectFc('32-2%', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 20%| → insert % → 20%%|',
      setUp: () => setupFc('20%', 3),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 20, expressionResult: 0.2, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () =>
          [openedState(value: 20, expressionResult: 0, hasOperator: true)],
      verify: (bloc) => expectFc('20%%', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 32-10×2| → insert % → 32-10×2%|',
      setUp: () => setupFc('32-10×2', 7),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 32, expressionResult: 12, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.percent),
      expect: () =>
          [openedState(value: 32, expressionResult: 31.8, hasOperator: true)],
      verify: (bloc) => expectFc('32-10×2%', 8),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 12÷0| → insert ÷ → 12÷0÷|',
      setUp: () => setupFc('12÷0', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 12, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [],
      verify: (bloc) => expectFc('12÷0÷', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 32-1×| → insert + → 32-1+|',
      setUp: () => setupFc('32-1×', 5),
      build: () => calculatorBloc,
      seed: () => openedState(value: 32, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.add),
      expect: () => [],
      verify: (bloc) => expectFc('32-1+', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 3%| → insert + → 3%+|',
      setUp: () => setupFc('3%', 2),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 3, expressionResult: 0.03, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.add),
      expect: () => [openedState(value: 3, hasOperator: true)],
      verify: (bloc) => expectFc('3%+', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 2÷| → insert % → 2÷|',
      setUp: () => setupFc('2÷', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 2, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [],
      verify: (bloc) => expectFc('2÷', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 298+| → insert - → 298-|',
      setUp: () => setupFc('298+', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 298, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.subtract),
      expect: () => [],
      verify: (bloc) => expectFc('298-', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0÷0| → insert - → 0÷0-|',
      setUp: () => setupFc('0÷0', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.subtract),
      expect: () => [],
      verify: (bloc) => expectFc('0÷0-', 4),
    );
  });

  group('InsertOperator (decimal)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 33.0| → insert × → 33.0×|',
      setUp: () => setupFc('33.0', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 33),
      act: (bloc) => addEvent(bloc, Operator.multiply),
      expect: () => [openedState(value: 33, hasOperator: true)],
      verify: (bloc) => expectFc('33.0×', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,| → insert - → 0,-|',
      setUp: () => setupFc('0,', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.subtract),
      expect: () => [openedState(value: 0, hasOperator: true)],
      verify: (bloc) => expectFc('0,-', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given ,| → insert ÷ → ,÷|',
      setUp: () => setupFc(',', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [openedState(value: 0, hasOperator: true)],
      verify: (bloc) => expectFc(',÷', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 9,8-| → insert ÷ → 9,8÷|',
      setUp: () => setupFc('9,8-', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 9.8, hasOperator: true),
      act: (bloc) => addEvent(bloc, Operator.divide),
      expect: () => [],
      verify: (bloc) => expectFc('9,8÷', 4),
    );
  });
}
