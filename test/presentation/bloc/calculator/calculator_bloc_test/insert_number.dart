import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late CalculatorBloc calculatorBloc;
  late ProfileCubit profileCubit;
  late Settings settings;
  late ToastShower toastShower;
  final EvaluateExpressionUseCase evaluateExpressionUseCase =
      EvaluateExpressionUseCase();
  TextEditingController fc = TextEditingController();

  setUp(() {
    settings = MockSettings();
    when(() => settings.decimalSeparator).thenReturn(',');
    when(() => settings.thousandSeparator).thenReturn('.');
    when(() => settings.maxFractionDigits).thenReturn(2);
    when(() => settings.maxWholeDigits).thenReturn(10);
    when(() => settings.locale).thenReturn('ID');

    profileCubit = MockProfileCubit();
    when(() => profileCubit.state).thenReturn(ProfileAdded(
      self: FakePerson(),
      settings: settings,
    ));

    toastShower = MockToastShower();
    when(() => toastShower.showToast(any())).thenAnswer((_) {});

    calculatorBloc = CalculatorBloc(
      profileCubit: profileCubit,
      toastShower: toastShower,
      evaluateExpressionUseCase: evaluateExpressionUseCase,
    );
    fc = TextEditingController();
  });

  void setupFc(String text, int start, [int? end]) {
    fc.text = text;
    if (end != null) {
      fc.selection = TextSelection(baseOffset: start, extentOffset: end);
    } else {
      fc.selection = TextSelection.collapsed(offset: start);
    }
  }

  void expectFc(String text, int start, [int? end]) {
    expect(fc.text, text);
    expect(fc.selection.start, start);
    if (end != null) {
      expect(fc.selection.end, end);
    }
  }

  void addEvent(CalculatorBloc bloc, String number) =>
      bloc.add(InsertNumber(number: number));

  void addEvents(CalculatorBloc bloc, List<String> valueSequences) {
    for (String val in valueSequences) {
      addEvent(bloc, val);
    }
  }

  CalculatorOpened openedState({
    required double value,
    double? expressionResult,
    bool hasOperator = false,
  }) {
    return CalculatorOpened(
      fieldController: fc,
      value: value,
      expressionResult: expressionResult,
      hasOperator: hasOperator,
    );
  }

  group('InsertNumber (simple)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 35| → insert 8 → 358|',
      setUp: () {
        fc.text = '35';
        fc.selection = const TextSelection.collapsed(offset: 2);
      },
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 35),
      act: (bloc) => bloc.add(const InsertNumber(number: '8')),
      expect: () => [CalculatorOpened(fieldController: fc, value: 358)],
      verify: (bloc) => expectFc('358', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert 9 → 9|',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, '9'),
      expect: () => [openedState(value: 9)],
      verify: (bloc) => expectFc('9', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert 000 → 0|',
      setUp: () {
        fc.text = '';
        fc.selection = const TextSelection.collapsed(offset: 0);
      },
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => bloc.add(const InsertNumber(number: '000')),
      expect: () => [],
      verify: (bloc) => expectFc('0', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -1| → insert 2 → -12|',
      setUp: () => setupFc('-1', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: -1),
      act: (bloc) => addEvent(bloc, '2'),
      expect: () => [openedState(value: -12)],
      verify: (bloc) => expectFc('-12', 3),
    );
  });

  group('InsertNumber (decimal)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 3,14| → insert 8 → 3,14|',
      setUp: () {
        fc.text = '3,14';
        fc.selection = const TextSelection.collapsed(offset: 4);
      },
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 3.14),
      act: (bloc) => bloc.add(const InsertNumber(number: '8')),
      expect: () => [],
      verify: (bloc) => expectFc('3,14', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 312,| → insert 0 → 312,0|',
      setUp: () {
        fc.text = '312,';
        fc.selection = const TextSelection.collapsed(offset: 4);
      },
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 312),
      act: (bloc) => bloc.add(const InsertNumber(number: '0')),
      expect: () => [],
      verify: (bloc) => expectFc('312,0', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 312,8| → insert 0 → 312,80|',
      setUp: () {
        fc.text = '312,8';
        fc.selection = const TextSelection.collapsed(offset: 5);
      },
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 312.8),
      act: (bloc) => bloc.add(const InsertNumber(number: '0')),
      expect: () => [],
      verify: (bloc) => expectFc('312,80', 6),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 312| → insert , → 312,|',
      setUp: () {
        fc.text = '312';
        fc.selection = const TextSelection.collapsed(offset: 3);
      },
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 312),
      act: (bloc) => bloc.add(const InsertNumber(number: ',')),
      expect: () => [],
      verify: (bloc) => expectFc('312,', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 312,| → insert , → 312,|',
      setUp: () {
        fc.text = '312,';
        fc.selection = const TextSelection.collapsed(offset: 4);
      },
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 312),
      act: (bloc) => bloc.add(const InsertNumber(number: ',')),
      expect: () => [],
      verify: (bloc) => expectFc('312,', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 312,| → insert 000 → 312,00|',
      setUp: () {
        fc.text = '312,';
        fc.selection = const TextSelection.collapsed(offset: 4);
      },
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 312),
      act: (bloc) => bloc.add(const InsertNumber(number: '000')),
      expect: () => [],
      verify: (bloc) => expectFc('312,00', 6),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert , → ,|',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc('0,', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0| → insert , → 0,|',
      setUp: () => setupFc('0', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc('0,', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given ,| → insert 3 → ,3|',
      setUp: () => setupFc(',', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => bloc.add(const InsertNumber(number: '3')),
      expect: () => [openedState(value: 0.3)],
      verify: (bloc) => expectFc(',3', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given ,| → insert , → ,|',
      setUp: () => setupFc(',', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc(',', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given ,| → insert 000 → ,00|',
      setUp: () => setupFc(',', 1),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvent(bloc, '000'),
      expect: () => [],
      verify: (bloc) => expectFc(',00', 3),
    );
  });

  group('InsertNumber (formatted)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 234| → insert 7 → 2.347|',
      setUp: () => setupFc('234', 3),
      build: () => calculatorBloc,
      seed: () => CalculatorOpened(fieldController: fc, value: 234),
      act: (bloc) => bloc.add(const InsertNumber(number: '7')),
      expect: () => [CalculatorOpened(fieldController: fc, value: 2347)],
      verify: (bloc) => expectFc('2.347', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 1.987,0| → insert 7 → 1.987,07|',
      setUp: () => setupFc('1.987,0', 7),
      build: () => calculatorBloc,
      seed: () => openedState(value: 1987),
      act: (bloc) => bloc.add(const InsertNumber(number: '7')),
      expect: () => [openedState(value: 1987.07)],
      verify: (bloc) => expectFc('1.987,07', 8),
    );
  });

  group('InsertNumber (contains operator)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 5+1| → insert 2 → 5+12|',
      setUp: () => setupFc('5+1', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 5, expressionResult: 6, hasOperator: true),
      act: (bloc) => addEvent(bloc, '2'),
      expect: () =>
          [openedState(value: 5, expressionResult: 17, hasOperator: true)],
      verify: (bloc) => expectFc('5+12', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 2%| → insert 5 → 2%×5|',
      setUp: () => setupFc('2%', 2),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 2, expressionResult: 0.02, hasOperator: true),
      act: (bloc) => addEvent(bloc, '5'),
      expect: () =>
          [openedState(value: 2, expressionResult: 0.1, hasOperator: true)],
      verify: (bloc) => expectFc('2%×5', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0÷0÷| → insert 0 → 0÷0÷0|',
      setUp: () => setupFc('0÷0÷', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, '0'),
      expect: () => [],
      verify: (bloc) => expectFc('0÷0÷0', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0÷0+| → insert 3 → 0÷0+3|',
      setUp: () => setupFc('0÷0+', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, '3'),
      expect: () => [],
      verify: (bloc) => expectFc('0÷0+3', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 2%| → insert , → 2%|',
      setUp: () => setupFc('2%', 2),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 2, expressionResult: 0.02, hasOperator: true),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc('2%', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 2%| → insert 000 → 2%×0|',
      setUp: () => setupFc('2%', 2),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 2, expressionResult: 0.02, hasOperator: true),
      act: (bloc) => addEvent(bloc, '000'),
      expect: () =>
          [openedState(value: 2, expressionResult: 0, hasOperator: true)],
      verify: (bloc) => expectFc('2%×0', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 1%| → insert , → 1%|',
      setUp: () => setupFc('1%', 2),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 1, expressionResult: 0.01, hasOperator: true),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc('1%', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 2%×0,| → insert , → 2%×0,|',
      setUp: () => setupFc('2%×0,', 5),
      build: () => calculatorBloc,
      seed: () => openedState(value: 2, expressionResult: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc('2%×0,', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 7×0| → insert 1 → 7×1|',
      setUp: () => setupFc('7×0', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 7, expressionResult: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, '1'),
      expect: () =>
          [openedState(value: 7, expressionResult: 7, hasOperator: true)],
      verify: (bloc) => expectFc('7×1', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5÷| → insert 0 → 5÷0|',
      setUp: () => setupFc('5÷', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 5, hasOperator: true),
      act: (bloc) => addEvent(bloc, '0'),
      expect: () => [],
      verify: (bloc) => expectFc('5÷0', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5÷0| → insert 000 → 5÷0|',
      setUp: () => setupFc('5÷0', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 5, hasOperator: true),
      act: (bloc) => addEvent(bloc, '000'),
      expect: () => [],
      verify: (bloc) => expectFc('5÷0', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5.000÷0| → insert 2 → 5.000÷2|',
      setUp: () => setupFc('5.000÷0', 7),
      build: () => calculatorBloc,
      seed: () => openedState(value: 5000, hasOperator: true),
      act: (bloc) => addEvent(bloc, '2'),
      expect: () =>
          [openedState(value: 5000, expressionResult: 2500, hasOperator: true)],
      verify: (bloc) => expectFc('5.000÷2', 7),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 97-,| → insert 3 → 97-3|',
      setUp: () => setupFc('97-', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 97, hasOperator: true),
      act: (bloc) => addEvent(bloc, '3'),
      expect: () =>
          [openedState(value: 97, expressionResult: 94, hasOperator: true)],
      verify: (bloc) => expectFc('97-3', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -1+| → insert 4 → -1+4|',
      setUp: () => setupFc('-1+', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: -1, hasOperator: true),
      act: (bloc) => addEvent(bloc, '4'),
      expect: () =>
          [openedState(value: -1, expressionResult: 3, hasOperator: true)],
      verify: (bloc) => expectFc('-1+4', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -100+| → insert 000 → -100+0|',
      setUp: () => setupFc('-100+', 5),
      build: () => calculatorBloc,
      seed: () => openedState(value: -100, hasOperator: true),
      act: (bloc) => addEvent(bloc, '000'),
      expect: () =>
          [openedState(value: -100, expressionResult: -100, hasOperator: true)],
      verify: (bloc) => expectFc('-100+0', 6),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 1×500| → insert 2 → 1×5.002|',
      setUp: () => setupFc('1×500', 5),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 1, expressionResult: 500, hasOperator: true),
      act: (bloc) => addEvent(bloc, '2'),
      expect: () =>
          [openedState(value: 1, expressionResult: 5002, hasOperator: true)],
      verify: (bloc) => expectFc('1×5.002', 7),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,-| → insert 1 → 0,-1|',
      setUp: () => setupFc('0,-', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, '1'),
      expect: () =>
          [openedState(value: 0, expressionResult: -1, hasOperator: true)],
      verify: (bloc) => expectFc('0,-1', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,÷| → insert 0 → 0,÷0|',
      setUp: () => setupFc('0,÷', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, '0'),
      expect: () => [],
      verify: (bloc) => expectFc('0,÷0', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,÷1.000| → insert 5 → 0,÷10.005|',
      setUp: () => setupFc('0,÷1.000', 8),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, expressionResult: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, '5'),
      expect: () => [],
      verify: (bloc) => expectFc('0,÷10.005', 9),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -200×2,87| → insert 6 → -200×2,87|',
      setUp: () => setupFc('-200×2,87', 9),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: -200, expressionResult: -574, hasOperator: true),
      act: (bloc) => addEvent(bloc, '6'),
      expect: () => [],
      verify: (bloc) => expectFc('-200×2,87', 9),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 666+| → insert , → 666+|',
      setUp: () => setupFc('666+', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 666, hasOperator: true),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc('666+', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -200×2,8| → insert 6 → -200×2,86|',
      setUp: () => setupFc('-200×2,8', 8),
      build: () => calculatorBloc,
      seed: () => openedState(
        value: -200,
        expressionResult: -560,
        hasOperator: true,
      ),
      act: (bloc) => addEvent(bloc, '6'),
      expect: () => [
        openedState(
          value: -200,
          expressionResult: -572,
          hasOperator: true,
        )
      ],
      verify: (bloc) => expectFc('-200×2,86', 9),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 1.200,01-1.000| → insert 0 → 1.200,01-10.000|',
      setUp: () => setupFc('1.200,01-1.000', 14),
      build: () => calculatorBloc,
      seed: () => openedState(
        value: 1200.01,
        expressionResult: 200.01,
        hasOperator: true,
      ),
      act: (bloc) => addEvent(bloc, '0'),
      expect: () => [
        openedState(
          value: 1200.01,
          expressionResult: -8799.99,
          hasOperator: true,
        )
      ],
      verify: (bloc) => expectFc('1.200,01-10.000', 15),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5.555-1,54÷1.010,1| → insert 000 → 5.555-1,54÷1.010,10|',
      setUp: () => setupFc('5.555-1,54÷1.010,1', 18),
      build: () => calculatorBloc,
      seed: () => openedState(
        value: 5555,
        expressionResult: 5555,
        hasOperator: true,
      ),
      act: (bloc) => addEvent(bloc, '000'),
      expect: () => [],
      verify: (bloc) => expectFc('5.555-1,54÷1.010,10', 19),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,5-25%| → insert 2 → 0,5-25%×2|',
      setUp: () => setupFc('0,5-25%', 7),
      build: () => calculatorBloc,
      seed: () => openedState(
        value: 0.5,
        expressionResult: 0.375,
        hasOperator: true,
      ),
      act: (bloc) => addEvent(bloc, '2'),
      expect: () => [
        openedState(
          value: 0.5,
          expressionResult: 0,
          hasOperator: true,
        )
      ],
      verify: (bloc) => expectFc('0,5-25%×2', 9),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 500-200%| → insert 000 → 500-200%×0|',
      setUp: () => setupFc('500-200%', 8),
      build: () => calculatorBloc,
      seed: () => openedState(
        value: 500,
        expressionResult: 498,
        hasOperator: true,
      ),
      act: (bloc) => addEvent(bloc, '000'),
      expect: () => [
        openedState(
          value: 500,
          expressionResult: 500,
          hasOperator: true,
        )
      ],
      verify: (bloc) => expectFc('500-200%×0', 10),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5%×0| → insert , → 5%×0,|',
      setUp: () => setupFc('5%×0', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 5, expressionResult: 0, hasOperator: true),
      act: (bloc) => addEvent(bloc, ','),
      expect: () => [],
      verify: (bloc) => expectFc('5%×0,', 5),
    );
  });

  group('InsertNumber (exceed max length)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 52.789| → insert 2 → 52.789|',
      setUp: () {
        when(() => settings.maxWholeDigits).thenReturn(5);
        setupFc('52.789', 6);
      },
      build: () => calculatorBloc,
      seed: () => openedState(value: 52789),
      act: (bloc) => addEvent(bloc, '2'),
      expect: () => [],
      verify: (bloc) => expectFc('52.789', 6),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 9.103,09| → insert 000 → 9.103,090|',
      setUp: () {
        when(() => settings.maxFractionDigits).thenReturn(3);
        setupFc('9.103,09', 8);
      },
      build: () => calculatorBloc,
      seed: () => openedState(value: 9103.09),
      act: (bloc) => addEvent(bloc, '000'),
      expect: () => [],
      verify: (bloc) => expectFc('9.103,090', 9),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 2.507,08×99,4| → insert 6 → 2.507,08×99,46|',
      setUp: () {
        when(() => settings.maxWholeDigits).thenReturn(4);
        setupFc('2.507,08×99,4', 13);
      },
      build: () => calculatorBloc,
      seed: () => openedState(
        value: 2507.08,
        expressionResult: 249203.75,
        hasOperator: true,
      ),
      act: (bloc) => addEvent(bloc, '6'),
      expect: () => [
        openedState(
          value: 2507.08,
          expressionResult: 249354.18,
          hasOperator: true,
        )
      ],
      verify: (bloc) => expectFc('2.507,08×99,46', 14),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 123.456| → insert 7 → 123.456|',
      setUp: () {
        when(() => settings.maxWholeDigits).thenReturn(4);
        setupFc('123.456', 7);
      },
      build: () => calculatorBloc,
      seed: () => openedState(value: 123456),
      act: (bloc) => addEvent(bloc, '7'),
      expect: () => [],
      verify: (bloc) => expectFc('123.456', 7),
    );
  });

  group('InsertNumber (multiple events)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert 0 → insert 3 → 3|',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) {
        addEvent(bloc, '0');
        addEvent(bloc, '3');
      },
      expect: () => [
        openedState(value: 3),
      ],
      verify: (bloc) => expectFc('3', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → insert 5 → insert 000 → 5.000|',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: (bloc) => addEvents(bloc, ['5', '000']),
      expect: () => [
        openedState(value: 5),
        openedState(value: 5000),
      ],
      verify: (bloc) => expectFc('5.000', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 10÷| → insert 2 → insert 0 → 10÷20|',
      setUp: () => setupFc('10÷', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 10, hasOperator: true),
      act: (bloc) => addEvents(bloc, ['2', '0']),
      expect: () => [
        openedState(value: 10, expressionResult: 5, hasOperator: true),
        openedState(value: 10, expressionResult: 0.5, hasOperator: true),
      ],
      verify: (bloc) => expectFc('10÷20', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 8-| → insert 8 → insert 8 → 8-88|',
      setUp: () => setupFc('8-', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 8, hasOperator: true),
      act: (bloc) => addEvents(bloc, ['8', '8']),
      expect: () => [
        openedState(value: 8, expressionResult: 0, hasOperator: true),
        openedState(value: 8, expressionResult: -80, hasOperator: true),
      ],
      verify: (bloc) => expectFc('8-88', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 8-| → insert 8 → insert 000 → 8-8.000|',
      setUp: () => setupFc('8-', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 8, hasOperator: true),
      act: (bloc) => addEvents(bloc, ['8', '000']),
      expect: () => [
        openedState(value: 8, expressionResult: 0, hasOperator: true),
        openedState(value: 8, expressionResult: -7992, hasOperator: true),
      ],
      verify: (bloc) => expectFc('8-8.000', 7),
    );
  });
}
