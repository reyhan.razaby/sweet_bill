import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

import '../../../../fakes.dart';
import '../../../../mocks.dart';

void main() {
  late CalculatorBloc calculatorBloc;
  late ProfileCubit profileCubit;
  late Settings settings;
  late ToastShower toastShower;
  final EvaluateExpressionUseCase evaluateExpressionUseCase =
      EvaluateExpressionUseCase();
  TextEditingController fc = TextEditingController();

  setUp(() {
    settings = MockSettings();
    when(() => settings.decimalSeparator).thenReturn(',');
    when(() => settings.thousandSeparator).thenReturn('.');
    when(() => settings.maxFractionDigits).thenReturn(2);
    when(() => settings.maxWholeDigits).thenReturn(10);
    when(() => settings.locale).thenReturn('ID');

    profileCubit = MockProfileCubit();
    when(() => profileCubit.state).thenReturn(ProfileAdded(
      self: FakePerson(),
      settings: settings,
    ));

    toastShower = MockToastShower();
    when(() => toastShower.showToast(any())).thenAnswer((_) {});

    calculatorBloc = CalculatorBloc(
      profileCubit: profileCubit,
      toastShower: toastShower,
      evaluateExpressionUseCase: evaluateExpressionUseCase,
    );
    fc = TextEditingController();
  });

  void setupFc(String text, int start, [int? end]) {
    fc.text = text;
    if (end != null) {
      fc.selection = TextSelection(baseOffset: start, extentOffset: end);
    } else {
      fc.selection = TextSelection.collapsed(offset: start);
    }
  }

  void expectFc(String text, int start, [int? end]) {
    expect(fc.text, text);
    expect(fc.selection.start, start);
    if (end != null) {
      expect(fc.selection.end, end);
    }
  }

  void addEvent(CalculatorBloc bloc) => bloc.add(Evaluate());

  CalculatorOpened openedState({
    required double value,
    double? expressionResult,
    bool hasOperator = false,
  }) {
    return CalculatorOpened(
      fieldController: fc,
      value: value,
      expressionResult: expressionResult,
      hasOperator: hasOperator,
    );
  }

  group('Evaluate (simple)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 432| → evaluate → 432|',
      setUp: () => setupFc('432', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 432),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('432', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given | → evaluate → |',
      setUp: () => setupFc('', 0),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('', 0),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 1+4| → evaluate → 5|',
      setUp: () => setupFc('1+4', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: 1, expressionResult: 5, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 5)],
      verify: (bloc) => expectFc('5', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 500+699| → evaluate → 1.199|',
      setUp: () => setupFc('500+699', 7),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 500, expressionResult: 1199, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 1199)],
      verify: (bloc) => expectFc('1.199', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5.000-4.800| → evaluate → 200|',
      setUp: () => setupFc('5.000-4.800', 11),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 5000, expressionResult: 200, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 200)],
      verify: (bloc) => expectFc('200', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 20-5+1+1+1+2+80×0| → evaluate → 20|',
      setUp: () => setupFc('20-5+1+1+1+2+80×0', 17),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 20, expressionResult: 20, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 20)],
      verify: (bloc) => expectFc('20', 2),
    );
  });

  group('Evaluate (trailing operator)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 5.000-| → evaluate → 5.000-|',
      setUp: () => setupFc('5.000-', 5),
      build: () => calculatorBloc,
      seed: () => openedState(value: 5000, hasOperator: true),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('5.000-', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 10%| → evaluate → 0,1|',
      setUp: () => setupFc('10%', 3),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 10, expressionResult: 0.1, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 0.1)],
      verify: (bloc) => expectFc('0,1', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 10%%| → evaluate → 0,001|',
      setUp: () {
        when(() => settings.maxFractionDigits).thenReturn(3);
        setupFc('10%%', 4);
      },
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 10, expressionResult: 0.001, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 0.001)],
      verify: (bloc) => expectFc('0,001', 5),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 78%×| → evaluate → 78%×|',
      setUp: () => setupFc('78%×', 4),
      build: () => calculatorBloc,
      seed: () => openedState(value: 78, hasOperator: true),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('78%×', 4),
    );
  });

  group('Evaluate (decimal)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,| → evaluate → 0|',
      setUp: () => setupFc('0,', 2),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('0', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,01+0,01| → evaluate → 0,02|',
      setUp: () => setupFc('0,01+0,01', 9),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 0.01, expressionResult: 0.02, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 0.02)],
      verify: (bloc) => expectFc('0,02', 4),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,+0,-0,×0,| → evaluate → 0|',
      setUp: () => setupFc('0,+0,-0,×0,', 11),
      build: () => calculatorBloc,
      seed: () => openedState(value: 0, expressionResult: 0, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 0)],
      verify: (bloc) => expectFc('0', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 0,03÷0,02| → evaluate → 1,5|',
      setUp: () => setupFc('0,03÷0,02', 9),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 0.03, expressionResult: 1.5, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 1.5)],
      verify: (bloc) => expectFc('1,5', 3),
    );
  });

  group('Evaluate (negative)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given -45| → evaluate → -45|',
      setUp: () => setupFc('-45', 3),
      build: () => calculatorBloc,
      seed: () => openedState(value: -45),
      act: addEvent,
      expect: () => [],
      verify: (bloc) => expectFc('-45', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 30-88| → evaluate → -58|',
      setUp: () => setupFc('30-88', 5),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 30, expressionResult: -58, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: -58)],
      verify: (bloc) => expectFc('-58', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 1.9-2.56| → evaluate → -58|',
      setUp: () => setupFc('30-88', 5),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 30, expressionResult: -58, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: -58)],
      verify: (bloc) => expectFc('-58', 3),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given -2+15| → evaluate → 13|',
      setUp: () => setupFc('-2+15', 5),
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: -2, expressionResult: 13, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 13)],
      verify: (bloc) => expectFc('13', 2),
    );
  });

  group('Evaluate (exceed max length)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 999×999| → evaluate → 998.001|',
      setUp: () {
        when(() => settings.maxWholeDigits).thenReturn(4);
        setupFc('999×999', 7);
      },
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 999, expressionResult: 998001, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 998001)],
      verify: (bloc) => expectFc('998.001', 7),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5,09×8,17| → evaluate → 41,59|',
      setUp: () {
        when(() => settings.maxFractionDigits).thenReturn(2);
        setupFc('5,09×8,17', 9);
      },
      build: () => calculatorBloc,
      seed: () =>
          openedState(value: 5.09, expressionResult: 41.59, hasOperator: true),
      act: addEvent,
      expect: () => [openedState(value: 41.59)],
      verify: (bloc) => expectFc('41,59', 5),
    );
  });

  group('Evaluate (invalid previous value & exp result)', () {
    blocTest<CalculatorBloc, CalculatorState>(
      'given 5+6| → evaluate → 11|',
      setUp: () => setupFc('5+6', 3),
      build: () => calculatorBloc,
      seed: () {
        // This expressionResult should not be 5.
        // But it will be removed after evaluation
        return openedState(value: 5, expressionResult: 5, hasOperator: true);
      },
      act: addEvent,
      expect: () => [openedState(value: 11)],
      verify: (bloc) => expectFc('11', 2),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 5| → evaluate → 5|',
      setUp: () => setupFc('5', 1),
      build: () => calculatorBloc,
      seed: () {
        // This expressionResult should be null
        return openedState(value: 5, expressionResult: 5);
      },
      act: addEvent,
      expect: () => [openedState(value: 5)],
      verify: (bloc) => expectFc('5', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 1+2| → evaluate → 3|',
      setUp: () => setupFc('1+2', 3),
      build: () => calculatorBloc,
      seed: () {
        // This expressionResult should not be null
        return openedState(value: 1, expressionResult: null, hasOperator: true);
      },
      act: addEvent,
      expect: () => [openedState(value: 3)],
      verify: (bloc) => expectFc('3', 1),
    );

    blocTest<CalculatorBloc, CalculatorState>(
      'given 2+2| → evaluate → 4|',
      setUp: () => setupFc('2+2', 3),
      build: () => calculatorBloc,
      seed: () {
        // This value should not be 0
        return openedState(value: 0, expressionResult: 4, hasOperator: true);
      },
      act: addEvent,
      expect: () => [openedState(value: 4)],
      verify: (bloc) => expectFc('4', 1),
    );
  });
}
