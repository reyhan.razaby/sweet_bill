import 'package:flutter_test/flutter_test.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_utils.dart';

void main() {
  group('splitUntilLastOp', () {
    test('1234+890', () {
      List res = splitUntilLastOp('1234+890');
      expect(res[0], '1234+');
      expect(res[1], '890');
    });

    test('1234+890-', () {
      List res = splitUntilLastOp('1234+890-');
      expect(res[0], '1234+890-');
      expect(res[1], '');
    });

    test('-50', () {
      List res = splitUntilLastOp('-50');
      expect(res, []);
    });

    test('-1234+8÷90', () {
      List res = splitUntilLastOp('-1234+8÷90');
      expect(res[0], '-1234+8÷');
      expect(res[1], '90');
    });

    test('(empty string)', () {
      List res = splitUntilLastOp('');
      expect(res, []);
    });

    test('12345', () {
      List res = splitUntilLastOp('12345');
      expect(res, []);
    });

    test('-50+9', () {
      List res = splitUntilLastOp('-50+9');
      expect(res, ['-50+', '9']);
    });

    test('90%', () {
      List res = splitUntilLastOp('90%');
      expect(res, ['90%', '']);
    });

    test('2+90%', () {
      List res = splitUntilLastOp('2+90%');
      expect(res, ['2+90%', '']);
    });
  });
}
