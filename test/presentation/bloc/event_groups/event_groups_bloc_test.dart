import 'package:bloc_test/bloc_test.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_select_item.dart';
import 'package:sweet_bill/domain/use_cases/event_group/activate_event_group/activate_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/remove_event_group/remove_event_group_use_case.dart';
import 'package:sweet_bill/presentation/bloc/event_groups/event_groups_bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late EventGroupsBloc eventGroupsBloc;
  late GetEventGroupsUseCase getEventGroupsUseCase;
  late RemoveEventGroupUseCase removeEventGroupUseCase;
  late ActivateEventGroupUseCase activateEventGroupUseCase;
  final current = DateTime.now();

  setUp(() {
    getEventGroupsUseCase = MockGetEventGroupsUseCase();
    removeEventGroupUseCase = MockRemoveEventGroupUseCase();
    activateEventGroupUseCase = MockActivateEventGroupUseCase();
    eventGroupsBloc = EventGroupsBloc(
      getEventGroupsUseCase: getEventGroupsUseCase,
      removeEventGroupUseCase: removeEventGroupUseCase,
      activateEventGroupUseCase: activateEventGroupUseCase,
    );
  });

  setUpAll(() {
    registerFallbackValue(FakeGetEventGroupsParam());
  });

  group('LoadEventGroupsEvent:', () {
    final event = LoadEventGroups();
    final data = [
      EventGroupSelectItem(
          EventGroup(id: '1', name: 'a', creationTime: current),
          isCurrentlyActive: true),
      EventGroupSelectItem(
          EventGroup(id: '2', name: 'b', creationTime: current)),
    ];
    const failure = Failure(message: 'some error message');

    blocTest<EventGroupsBloc, EventGroupsState>(
      'data is loaded successfully',
      setUp: () {
        when(() => getEventGroupsUseCase(any()))
            .thenAnswer((_) async => Right(data));
      },
      build: () => eventGroupsBloc,
      act: (bloc) => bloc.add(event),
      expect: () => [
        EventGroupsLoading(),
        EventGroupsLoaded(
            eventGroups: data, activeEventGroupId: '1', focusedId: '1'),
      ],
      verify: (_) => verify(() => getEventGroupsUseCase(any())),
    );

    blocTest<EventGroupsBloc, EventGroupsState>(
      'fail to get event groups',
      setUp: () {
        when(() => getEventGroupsUseCase(any()))
            .thenAnswer((_) async => const Left(failure));
      },
      build: () => eventGroupsBloc,
      act: (bloc) => bloc.add(event),
      expect: () => [
        EventGroupsLoading(),
        EventGroupsError(message: failure.message!),
      ],
    );
  });

  group('RemoveEventGroup:', () {
    final event = RemoveEventGroup(eventGroupId: '2');

    blocTest<EventGroupsBloc, EventGroupsState>(
      'remove event successfully',
      setUp: () {
        when(() => removeEventGroupUseCase(any()))
            .thenAnswer((_) async => const Right(true));
        when(() => getEventGroupsUseCase(any()))
            .thenAnswer((_) async => const Right([]));
      },
      build: () => eventGroupsBloc,
      act: (bloc) => bloc.add(event),
      verify: (_) => verify(() => removeEventGroupUseCase(any())),
    );
  });

  group('ActivateEventGroup:', () {
    blocTest<EventGroupsBloc, EventGroupsState>(
      'activate the given eventGroupId',
      setUp: () {
        when(() => activateEventGroupUseCase(any()))
            .thenAnswer((_) async => const Right(true));
        when(() => getEventGroupsUseCase(any()))
            .thenAnswer((_) async => const Right([]));
      },
      build: () => eventGroupsBloc,
      act: (bloc) => bloc.add(ActivateEventGroup(eventGroupId: '2')),
      verify: (_) => verify(() => activateEventGroupUseCase('2')),
    );

    blocTest<EventGroupsBloc, EventGroupsState>(
      'not bring eventGroupId, should activate from focusedId',
      setUp: () {
        when(() => activateEventGroupUseCase(any()))
            .thenAnswer((_) async => const Right(true));
        when(() => getEventGroupsUseCase(any()))
            .thenAnswer((_) async => const Right([]));
      },
      seed: () => EventGroupsLoaded(
        eventGroups: [
          EventGroupSelectItem(
              EventGroup(id: '3', name: 'a', creationTime: current))
        ],
        focusedId: '3',
      ),
      build: () => eventGroupsBloc,
      act: (bloc) => bloc.add(ActivateEventGroup()),
      verify: (_) => verify(() => activateEventGroupUseCase('3')),
    );
  });

  group('FocusEventGroup:', () {
    final event = FocusEventGroup(focusedId: '2');
    final currentEventGroups = [
      EventGroupSelectItem(
          EventGroup(id: '1', name: 'a', creationTime: current)),
      EventGroupSelectItem(
          EventGroup(id: '2', name: 'b', creationTime: current)),
    ];

    blocTest<EventGroupsBloc, EventGroupsState>(
      'changed to correct focusedId',
      seed: () => EventGroupsLoaded(
        eventGroups: currentEventGroups,
        focusedId: '1',
      ),
      build: () => eventGroupsBloc,
      act: (bloc) => bloc.add(event),
      expect: () => [
        EventGroupsLoaded(
          eventGroups: currentEventGroups,
          focusedId: '2',
          buildList: false,
        ),
      ],
    );
  });
}
