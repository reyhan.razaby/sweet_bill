import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_payment/bill_form_payment_bloc.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

void main() {
  late BillFormPaymentBloc billFormPaymentBloc;
  late BillFormBloc billFormBloc;

  const zoe = Person(id: 'zoe', name: 'Zoe', avatar: fakeAvatar);
  const jun = Person(id: 'jun', name: 'Jun', avatar: fakeAvatar);
  const rey = Person(id: 'rey', name: 'Rey', avatar: fakeAvatar);
  const ann = Person(id: 'ann', name: 'Ann', avatar: fakeAvatar);
  const members = [zoe, jun, rey, ann];

  setUp(() {
    billFormBloc = MockBillFormBloc();
    billFormPaymentBloc = BillFormPaymentBloc(billFormBloc: billFormBloc);
    when(() => billFormBloc.members).thenReturn(members);
  });

  setUpAll(() {
    registerFallbackValue(FakeBillFormEvent());
  });

  group('LoadPaymentInitialValues: single payer', () {
    final currentBill = Bill(
      id: fakeString,
      title: fakeString,
      icon: fakeBillIcon,
      transactionTime: FakeDateTime(),
      eventGroupId: fakeString,
      personalCosts: [FakeBillPersonalCost()],
      personalPayments: [FakeBillPersonalPayment()],
      splitStrategy: SplitStrategies.custom,
      isAutoAdjustTax: false,
    );

    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'single payer',
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalPayments: [
              BillPersonalPayment(personId: jun.id, amount: 120)
            ],
          ),
          totalCost: 120,
          isMultiplePayers: false,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(LoadPaymentInitialValues()),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 120, isPayer: true),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: false,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );
  });

  group('LoadPaymentInitialValues: multiple payers', () {
    final currentBill = Bill(
      id: fakeString,
      title: fakeString,
      icon: fakeBillIcon,
      transactionTime: FakeDateTime(),
      eventGroupId: fakeString,
      personalCosts: [FakeBillPersonalCost()],
      personalPayments: [FakeBillPersonalPayment()],
      splitStrategy: SplitStrategies.custom,
      isAutoAdjustTax: false,
    );

    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      "matched with total target",
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalPayments: [
              BillPersonalPayment(personId: jun.id, amount: 80),
              BillPersonalPayment(personId: zoe.id, amount: 40),
            ],
          ),
          totalCost: 120,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(LoadPaymentInitialValues()),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 80),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 40),
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );

    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      "not matched with total target",
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalPayments: [
              BillPersonalPayment(personId: jun.id, amount: 80),
            ],
          ),
          totalCost: 120,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(LoadPaymentInitialValues()),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 80),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );

    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      "only 1 payer and matched with target, but multiple-payers switched on",
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: currentBill.copyWith(
            personalPayments: [
              BillPersonalPayment(personId: jun.id, amount: 120),
            ],
          ),
          totalCost: 120,
          isMultiplePayers: true, // multiple-payers switched on
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(LoadPaymentInitialValues()),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 120),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );
  });

  group('ToggleMultiplePayers: to single payer', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'select the most paid as payer',
      seed: () => const BillFormPaymentLoaded(
        isMultiplePayers: true,
        payments: [
          MemberPayment(person: ann, amount: 20),
          MemberPayment(person: jun, amount: 50),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 50),
        ],
        expectedTotalPayment: 120,
      ),
      setUp: () {
        when(() => billFormBloc.state)
            .thenReturn(BillFormLoaded(bill: FakeBill(), totalCost: 120));
      },
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(ToggleMultiplePayers()),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 120, isPayer: true),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: false,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );
  });

  group('ToggleMultiplePayers: to multiple payers', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'update with the correct amount',
      seed: () => const BillFormPaymentLoaded(
        isMultiplePayers: false,
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 120, isPayer: true),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 0),
        ],
        expectedTotalPayment: 120,
      ),
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(ToggleMultiplePayers()),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 120),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );
  });

  group('SelectPayer', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'change payer',
      setUp: () {
        when(() => billFormBloc.state)
            .thenReturn(BillFormLoaded(bill: FakeBill(), totalCost: 120));
      },
      seed: () => const BillFormPaymentLoaded(
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 0),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 120, isPayer: true)
        ],
        isMultiplePayers: false,
        expectedTotalPayment: 120,
      ),
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(const SelectPayer(index: 0)),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 120, isPayer: true),
            MemberPayment(person: jun, amount: 0),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0)
          ],
          isMultiplePayers: false,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );
  });

  group('ChangeAmount', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'change amount',
      seed: () => const BillFormPaymentLoaded(
        payments: [
          MemberPayment(person: ann, amount: 33),
          MemberPayment(person: jun, amount: 20),
          MemberPayment(person: rey, amount: 50),
          MemberPayment(person: zoe, amount: 16)
        ],
        isMultiplePayers: true,
        expectedTotalPayment: 120,
      ),
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(const ChangeAmount(amount: 0, index: 2)),
      expect: () {
        const expectedState = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 33),
            MemberPayment(person: jun, amount: 20),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 16)
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );
        return [expectedState];
      },
    );
  });

  group('SavePayment: single payer', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'save payments',
      seed: () => const BillFormPaymentLoaded(
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 60),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 30)
        ],
        isMultiplePayers: true,
        expectedTotalPayment: 90,
      ),
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(SavePayment()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          ChangeForm(
            payments: [
              BillPersonalPayment(personId: jun.id, amount: 60),
              BillPersonalPayment(personId: zoe.id, amount: 30),
            ],
            isMultiplePayers: true,
          ),
        );
      },
    );
  });

  group('SavePayment: multiple payers', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'multiple payers',
      seed: () => const BillFormPaymentLoaded(
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 60),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 30)
        ],
        isMultiplePayers: true,
        expectedTotalPayment: 90,
      ),
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(SavePayment()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          ChangeForm(
            payments: [
              BillPersonalPayment(personId: jun.id, amount: 60),
              BillPersonalPayment(personId: zoe.id, amount: 30),
            ],
            isMultiplePayers: true,
          ),
        );
      },
    );

    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'multiple payers, but empty',
      seed: () => const BillFormPaymentLoaded(
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 0),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 0)
        ],
        isMultiplePayers: true,
        expectedTotalPayment: 0,
      ),
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(SavePayment()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          const ChangeForm(payments: [], isMultiplePayers: true),
        );
      },
    );

    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'multiple payers, but only 1 payer and matched with target',
      seed: () => const BillFormPaymentLoaded(
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 0),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 120)
        ],
        isMultiplePayers: true,
        expectedTotalPayment: 120,
      ),
      setUp: () {
        BillFormLoaded billFormState = BillFormLoaded(
          bill: FakeBill(),
          totalCost: 120,
        );
        when(() => billFormBloc.state).thenReturn(billFormState);
      },
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(SavePayment()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          ChangeForm(
            payments: [BillPersonalPayment(personId: zoe.id, amount: 120)],
            isMultiplePayers: true,
          ),
        );
      },
    );

    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      "multiple payers, but doesn't matched with expected total",
      seed: () => const BillFormPaymentLoaded(
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 0),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 120)
        ],
        isMultiplePayers: true,
        expectedTotalPayment: 100,
      ),
      build: () => billFormPaymentBloc,
      act: (bloc) => bloc.add(SavePayment()),
      verify: (bloc) {
        final captured = verify(() => billFormBloc.add(captureAny())).captured;
        final capturedEvent = captured[0] as BillFormEvent;
        expect(
          capturedEvent,
          ChangeForm(
            payments: [BillPersonalPayment(personId: zoe.id, amount: 120)],
            isMultiplePayers: true,
          ),
        );
      },
    );
  });

  group('ToggleMultiplePayers, ChangeAmount, ToggleMultiplePayers', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'ToggleMultiplePayers to multiple, '
      'ChangeAmount to zero, '
      'ToggleMultiplePayers again',
      seed: () => const BillFormPaymentLoaded(
        isMultiplePayers: false,
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 120, isPayer: true),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 0),
        ],
        expectedTotalPayment: 120,
      ),
      setUp: () {
        when(() => billFormBloc.state).thenReturn(BillFormLoaded(
          bill: Bill(
            id: fakeString,
            title: fakeString,
            icon: fakeBillIcon,
            transactionTime: FakeDateTime(),
            eventGroupId: fakeString,
            personalCosts: [FakeBillPersonalCost()],
            personalPayments: [
              BillPersonalPayment(personId: jun.id, amount: 120)
            ],
            splitStrategy: SplitStrategies.custom,
            isAutoAdjustTax: fakeBool,
          ),
          totalCost: 120,
          isMultiplePayers: false,
        ));
      },
      build: () => billFormPaymentBloc,
      act: (bloc) {
        bloc.add(ToggleMultiplePayers());
        bloc.add(const ChangeAmount(index: 1, amount: 0));
        bloc.add(ToggleMultiplePayers());
      },
      expect: () {
        const expectedState0 = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 120),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );

        const expectedState1 = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 0),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );

        const expectedState2 = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 0),
            MemberPayment(person: jun, amount: 120, isPayer: true),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: false,
          expectedTotalPayment: 120,
        );

        return [expectedState0, expectedState1, expectedState2];
      },
    );
  });

  group('SelectPayer, ToggleMultiplePayers', () {
    blocTest<BillFormPaymentBloc, BillFormPaymentState>(
      'From single payer,'
      'SelectPayer to another'
      'ToggleMultiplePayers to multiple ',
      seed: () => const BillFormPaymentLoaded(
        isMultiplePayers: false,
        payments: [
          MemberPayment(person: ann, amount: 0),
          MemberPayment(person: jun, amount: 120, isPayer: true),
          MemberPayment(person: rey, amount: 0),
          MemberPayment(person: zoe, amount: 0),
        ],
        expectedTotalPayment: 120,
      ),
      setUp: () {
        when(() => billFormBloc.state).thenReturn(BillFormLoaded(
          bill: Bill(
            id: fakeString,
            title: fakeString,
            icon: fakeBillIcon,
            transactionTime: FakeDateTime(),
            eventGroupId: fakeString,
            personalCosts: [FakeBillPersonalCost()],
            personalPayments: [
              BillPersonalPayment(personId: jun.id, amount: 120)
            ],
            splitStrategy: SplitStrategies.custom,
            isAutoAdjustTax: fakeBool,
          ),
          totalCost: 120,
          isMultiplePayers: false,
        ));
      },
      build: () => billFormPaymentBloc,
      act: (bloc) {
        bloc.add(const SelectPayer(index: 0));
        bloc.add(ToggleMultiplePayers());
      },
      expect: () {
        const expectedState0 = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 120, isPayer: true),
            MemberPayment(person: jun, amount: 0),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: false,
          expectedTotalPayment: 120,
        );

        const expectedState1 = BillFormPaymentLoaded(
          payments: [
            MemberPayment(person: ann, amount: 120),
            MemberPayment(person: jun, amount: 0),
            MemberPayment(person: rey, amount: 0),
            MemberPayment(person: zoe, amount: 0),
          ],
          isMultiplePayers: true,
          expectedTotalPayment: 120,
        );

        return [expectedState0, expectedState1];
      },
    );
  });
}
