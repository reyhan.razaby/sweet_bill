import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_members/get_members_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/register_event_group_members/register_event_group_members_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/register_event_group_members/register_event_group_members_use_case.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';

import '../../../fakes.dart';
import '../../../mocks.dart';

class Dummy {
  void dummyFunction(Person person) {}
}

class MockDummy extends Mock implements Dummy {}

void main() {
  late BillFormBloc billFormBloc;
  late GetMembersUseCase getMembersUseCase;
  late CreateBillUseCase createBillUseCase;
  late UpdateBillUseCase updateBillUseCase;
  late RegisterEventGroupMembersUseCase registerEventGroupMembersUseCase;
  late ProfileCubit profileCubit;
  late Settings settings;

  setUp(() {
    getMembersUseCase = MockGetMembersUseCase();
    createBillUseCase = MockCreateBillUseCase();
    updateBillUseCase = MockUpdateBillUseCase();
    settings = MockSettings();
    registerEventGroupMembersUseCase = MockRegisterEventGroupMembersUseCase();
    profileCubit = MockProfileCubit();
    billFormBloc = BillFormBloc(
      getMembersUseCase: getMembersUseCase,
      createBillUseCase: createBillUseCase,
      updateBillUseCase: updateBillUseCase,
      registerEventGroupMembersUseCase: registerEventGroupMembersUseCase,
      profileCubit: profileCubit,
    );


    when(() => settings.maxFractionDigits).thenReturn(2);
    when(() => profileCubit.state).thenReturn(ProfileAdded(
      self: FakePerson(),
      settings: settings,
    ));
  });

  setUpAll(() {
    registerFallbackValue(FakeGetMembersParam());
    registerFallbackValue(FakeCreateBillParam());
    registerFallbackValue(FakeUpdateBillParam());
    registerFallbackValue(FakeRegisterEventGroupMembersParam());
    registerFallbackValue(FakePerson());
  });

  group('LoadInitialValues', () {
    const String eventGroupId = '123';
    const self = Person(id: '1', name: '1', avatar: PersonAvatar.pa001);
    const List<Person> members = [
      self,
      Person(id: '2', name: '2', avatar: PersonAvatar.pa001),
      Person(id: '3', name: '3', avatar: PersonAvatar.pa001),
    ];
    final transactionTime = DateTime.now();
    final initialState = BillFormEmpty(
        bill: Bill.initial().copyWith(
      transactionTime: transactionTime,
    ));

    blocTest<BillFormBloc, BillFormState>(
      'load with new bill',
      setUp: () {
        when(() => getMembersUseCase(any()))
            .thenAnswer((_) async => const Right(members));
        when(() => profileCubit.state).thenReturn(ProfileAdded(
          self: self,
          settings: FakeSettings(),
        ));
      },
      build: () => billFormBloc,
      seed: () => initialState,
      act: (bloc) =>
          bloc.add(const LoadInitialValues(eventGroupId: eventGroupId)),
      expect: () {
        final expectedLoadedBill = initialState.bill.copyWith(
          id: '',
          eventGroupId: eventGroupId,
          transactionTime: transactionTime,
          personalPayments: [BillPersonalPayment(personId: self.id, amount: 0)],
          personalCosts: [
            const BillPersonalCost(personId: '1', cost: 0, additionalCost: 0),
            const BillPersonalCost(personId: '2', cost: 0, additionalCost: 0),
            const BillPersonalCost(personId: '3', cost: 0, additionalCost: 0),
          ],
        );
        return [
          BillFormLoading(currentState: initialState),
          BillFormLoaded(
            bill: expectedLoadedBill,
            totalCost: 0,
            isMultiplePayers: false,
          ),
        ];
      },
    );
  });

  group('RegisterNewMember', () {
    List<Person> currentMembers = const [
      Person(id: '1', name: 'Hank', avatar: PersonAvatar.pa001),
      Person(id: '2', name: 'Joe', avatar: PersonAvatar.pa002),
    ];
    Person newMember =
        const Person(id: '', name: 'Walt', avatar: PersonAvatar.pa003);
    Person newMemberWithId = newMember.copyWith(id: '3');
    final initialState = BillFormLoaded(
      bill: Bill.asDefault(
        id: '',
        eventGroupId: '12321',
        title: 'Bar',
        personalCosts: const [
          BillPersonalCost(personId: '1', cost: 80),
          BillPersonalCost(personId: '2', cost: 20),
        ],
        personalPayments: const [
          BillPersonalPayment(personId: '2', amount: 100)
        ],
      ),
      totalCost: 100,
    );

    final dummyObject = MockDummy();
    final event = RegisterNewMember(
      newMember: newMember,
      onRegistered: (p) {
        dummyObject.dummyFunction(p);
      },
    );

    blocTest<BillFormBloc, BillFormState>('register new member',
        setUp: () {
          billFormBloc.members = currentMembers;
          when(() => registerEventGroupMembersUseCase(any()))
              .thenAnswer((_) async => Right([newMemberWithId]));
        },
        build: () => billFormBloc,
        seed: () => initialState,
        act: (bloc) => bloc.add(event),
        expect: () {
          return [
            BillFormLoading(currentState: initialState),
            BillFormLoaded.copyWithState(initialState),
          ];
        },
        verify: (bloc) {
          final captured =
              verify(() => registerEventGroupMembersUseCase(captureAny()))
                  .captured;
          expect(captured.length, 1);
          expect(
            captured[0],
            RegisterEventGroupMembersParam(
              eventGroupId: '12321',
              members: [newMember],
            ),
          );

          verify(() => dummyObject.dummyFunction(newMemberWithId));

          expect(bloc.members, List.of(currentMembers)..add(newMemberWithId));
        });
  });

  group('SaveBill', () {
    const billId = '666';

    setUp() {
      when(() => createBillUseCase(any()))
          .thenAnswer((_) async => const Right(billId));
      when(() => updateBillUseCase(any()))
          .thenAnswer((_) async => const Right(true));
    }

    final creInitialState = BillFormLoaded(
      bill: Bill.initial(),
      totalCost: 15200,
    );
    blocTest<BillFormBloc, BillFormState>(
      'create new bill',
      setUp: setUp,
      build: () => billFormBloc,
      seed: () => creInitialState,
      act: (bloc) => bloc.add(SaveBill()),
      expect: () {
        return [
          BillFormLoading(currentState: creInitialState),
          BillFormSaved.copyWithState(
            creInitialState,
            bill: creInitialState.bill.copyWith(id: billId),
          ),
        ];
      },
    );

    final updInitialState = BillFormLoaded(
      bill: Bill.initial().copyWith(id: billId),
      totalCost: 15200,
    );
    blocTest<BillFormBloc, BillFormState>(
      'update existing bill',
      setUp: setUp,
      build: () => billFormBloc,
      seed: () => updInitialState,
      act: (bloc) => bloc.add(SaveBill()),
      expect: () {
        return [
          BillFormLoading(currentState: updInitialState),
          BillFormSaved.copyWithState(updInitialState),
        ];
      },
    );
  });

  group('ChangeForm: totalCostTarget', () {
    final initialState = BillFormLoaded(
      bill: Bill(
        id: fakeString,
        title: fakeString,
        icon: fakeBillIcon,
        transactionTime: FakeDateTime(),
        eventGroupId: fakeString,
        personalCosts: const [
          BillPersonalCost(personId: fakeString, cost: 60),
          BillPersonalCost(personId: fakeString, cost: 40),
        ],
        personalPayments: const [
          BillPersonalPayment(personId: fakeString, amount: 100),
        ],
        splitStrategy: SplitStrategies.custom,
        isAutoAdjustTax: fakeBool,
      ),
      totalCost: 100,
    );

    blocTest<BillFormBloc, BillFormState>(
      'split: by custom amount -- payment: single payer',
      build: () => billFormBloc,
      seed: () => BillFormLoaded.copyWithState(
        initialState,
        totalCost: 100,
        isMultiplePayers: false,
      ),
      act: (bloc) => bloc.add(const ChangeForm(totalCost: 150)),
      expect: () {
        return [
          BillFormLoaded.copyWithState(
            initialState,
            totalCost: 150,
            bill: initialState.bill.copyWith(
              personalPayments: const [
                BillPersonalPayment(personId: fakeString, amount: 150),
              ],
            ),
            remainingCost: 50,
            remainingPayment: 0,
            isMultiplePayers: false,
          ),
        ];
      },
    );

    blocTest<BillFormBloc, BillFormState>(
      'split: equally -- payment: multiple payers',
      build: () => billFormBloc,
      seed: () => BillFormLoaded.copyWithState(
        initialState,
        bill: initialState.bill.copyWith(
          splitStrategy: SplitStrategies.equally,
          personalCosts: const [
            BillPersonalCost(personId: fakeString, cost: 50),
            BillPersonalCost(personId: fakeString, cost: 50),
          ],
          personalPayments: const [
            BillPersonalPayment(personId: fakeString, amount: 80),
            BillPersonalPayment(personId: fakeString, amount: 20),
          ],
        ),
        totalCost: 100,
        isMultiplePayers: true,
      ),
      act: (bloc) => bloc.add(const ChangeForm(totalCost: 120)),
      expect: () {
        return [
          BillFormLoaded.copyWithState(
            initialState,
            bill: initialState.bill.copyWith(
              personalCosts: const [
                BillPersonalCost(personId: fakeString, cost: 60),
                BillPersonalCost(personId: fakeString, cost: 60),
              ],
              personalPayments: const [
                BillPersonalPayment(personId: fakeString, amount: 80),
                BillPersonalPayment(personId: fakeString, amount: 20),
              ],
              splitStrategy: SplitStrategies.equally,
            ),
            totalCost: 120,
            remainingPayment: 20,
            isMultiplePayers: true,
          ),
        ];
      },
    );
  });

  group('ChangeForm: payments', () {
    final initialBill = Bill(
      id: fakeString,
      title: fakeString,
      icon: fakeBillIcon,
      transactionTime: FakeDateTime(),
      eventGroupId: fakeString,
      personalCosts: const [
        BillPersonalCost(personId: fakeString, cost: 100),
      ],
      personalPayments: const [
        BillPersonalPayment(personId: fakeString, amount: 100),
      ],
      splitStrategy: SplitStrategies.custom,
      isAutoAdjustTax: fakeBool,
    );

    final initialState = BillFormLoaded(
      bill: initialBill,
      totalCost: 100,
    );

    blocTest<BillFormBloc, BillFormState>(
      'single to multiple',
      build: () => billFormBloc,
      seed: () => BillFormLoaded.copyWithState(
        initialState,
        isMultiplePayers: false,
      ),
      act: (bloc) => bloc.add(const ChangeForm(
        payments: [
          BillPersonalPayment(personId: fakeString, amount: 80),
          BillPersonalPayment(personId: fakeString, amount: 20),
        ],
        isMultiplePayers: true,
      )),
      expect: () {
        return [
          BillFormLoaded.copyWithState(
            initialState,
            bill: initialState.bill.copyWith(
              personalPayments: const [
                BillPersonalPayment(personId: fakeString, amount: 80),
                BillPersonalPayment(personId: fakeString, amount: 20),
              ],
            ),
            isMultiplePayers: true,
            remainingCost: 0,
            remainingPayment: 0,
          ),
        ];
      },
    );

    blocTest<BillFormBloc, BillFormState>(
      "single to multiple, but new payments don't up to target",
      build: () => billFormBloc,
      seed: () => BillFormLoaded.copyWithState(
        initialState,
        isMultiplePayers: false,
      ),
      act: (bloc) => bloc.add(const ChangeForm(
        payments: [
          BillPersonalPayment(personId: '1', amount: 80),
          BillPersonalPayment(personId: '2', amount: 30),
        ],
        isMultiplePayers: true,
      )),
      expect: () {
        return [
          BillFormLoaded.copyWithState(
            initialState,
            bill: initialState.bill.copyWith(
              personalPayments: const [
                BillPersonalPayment(personId: '1', amount: 80),
                BillPersonalPayment(personId: '2', amount: 30),
              ],
            ),
            remainingCost: 0,
            remainingPayment: -10,
            isMultiplePayers: true,
          ),
        ];
      },
    );
  });
}
