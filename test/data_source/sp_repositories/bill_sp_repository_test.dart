import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/bill_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/bill_model.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/core/failures/not_found_failure.dart';
import 'package:sweet_bill/data_source/sp_repositories/bill_sp_repository.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';

import '../../fakes.dart';
import '../../mocks.dart';

void main() {
  late BillSpRepository repository;
  late BillSpGateway billSpGateway;
  late PersonSpGateway personSpGateway;
  late EventGroupSpGateway eventGroupSpGateway;

  setUp(() {
    billSpGateway = MockBillSpGateway();
    personSpGateway = MockPersonSpGateway();
    eventGroupSpGateway = MockEventGroupSpGateway();
    repository = BillSpRepository(
      personSpGateway: personSpGateway,
      billSpGateway: billSpGateway,
      eventGroupSpGateway: eventGroupSpGateway,
    );
  });

  setUpAll(() {
    registerFallbackValue(FakeBillModel());
    registerFallbackValue(FakeEventGroupModel());
  });

  group('getBills', () {
    const eventGroupId = 'xxx';

    setUp(() {
      when(() => eventGroupSpGateway.getBillIds(any()))
          .thenAnswer((_) async => ['1', '2', '3']);
    });

    test('should return the correct result', () async {
      final current = DateTime.now();
      final billModel = BillModel(
        id: 'abc',
        creationTime: current,
        title: 'Cafe',
        eventGroupId: 'x',
        icon: BillIcons.receipt,
        transactionTime: current,
        personalCosts: const [],
        personalPayments: const [],
        splitStrategy: SplitStrategies.equally,
        isAutoAdjustTax: true,
      );
      when(() => billSpGateway.getById(any()))
          .thenAnswer((_) async => billModel);

      final result = await repository
          .getBills(const GetBillsParam(eventGroupId: eventGroupId));

      verify(() => eventGroupSpGateway.getBillIds(eventGroupId));
      result.fold(
        (l) => fail('should not be left'),
        (r) {
          final bill = billModel.toEntity();
          expect(r, [bill, bill, bill]);
        },
      );
    });

    BillModel generateBillModel(
      String id, {
      required List<BillPersonalCostModel> expenses,
      required List<BillPersonalPaymentModel> payments,
    }) {
      return BillModel(
        id: id,
        creationTime: DateTime.now(),
        title: '1',
        icon: fakeBillIcon,
        eventGroupId: eventGroupId,
        transactionTime: DateTime.now(),
        personalCosts: expenses,
        personalPayments: payments,
        splitStrategy: SplitStrategies.custom,
        isAutoAdjustTax: false,
      );
    }

    BillPersonalCostModel generatePersonalCostModel(
        String personId, double cost) {
      return BillPersonalCostModel(
          personId: personId, cost: cost, additionalCost: 0);
    }

    BillPersonalPaymentModel generatePersonalPaymentModel(
        String personId, double amount) {
      return BillPersonalPaymentModel(personId: personId, amount: amount);
    }

    test('return bills with involvedPersonId', () async {
      final billModel1 = generateBillModel('1', expenses: [
        generatePersonalCostModel('A', 100),
        generatePersonalCostModel('B', 100),
      ], payments: [
        generatePersonalPaymentModel('A', 200)
      ]);
      final billModel2 = generateBillModel('2', expenses: [
        generatePersonalCostModel('A', 100),
        generatePersonalCostModel('C', 100),
      ], payments: [
        generatePersonalPaymentModel('B', 200)
      ]);
      final billModel3 = generateBillModel('3', expenses: [
        generatePersonalCostModel('A', 100),
        generatePersonalCostModel('C', 100),
      ], payments: [
        generatePersonalPaymentModel('C', 200)
      ]);
      when(() => billSpGateway.getById('1'))
          .thenAnswer((_) async => billModel1);
      when(() => billSpGateway.getById('2'))
          .thenAnswer((_) async => billModel2);
      when(() => billSpGateway.getById('3'))
          .thenAnswer((_) async => billModel3);

      final result = await repository.getBills(const GetBillsParam(
          eventGroupId: eventGroupId, involvedPersonId: 'B'));

      verify(() => eventGroupSpGateway.getBillIds(eventGroupId));
      result.fold(
        (l) => fail('should not be left'),
        (r) {
          expect(r, [billModel1.toEntity(), billModel2.toEntity()]);
        },
      );
    });
  });

  group('createBill', () {
    const generatedId = '400';

    CreateBillParam param = CreateBillParam(
      bill: Bill.asDefault(
        id: '',
        title: 'Cafe',
        personalCosts: const [
          BillPersonalCost(personId: '1', cost: 1000, additionalCost: 0),
          BillPersonalCost(personId: '2', cost: 1000, additionalCost: 0),
        ],
        personalPayments: const [
          BillPersonalPayment(personId: '2', amount: 2000),
        ],
      ),
    );

    test("given the absent person, should return Failure", () async {
      when(() => personSpGateway.getById('1'))
          .thenAnswer((_) async => FakePersonModel());
      when(() => personSpGateway.getById('2')).thenAnswer((_) async => null);

      final result = await repository.createBill(param);

      result.fold(
        (l) => expect(l is NotFoundFailure, true),
        (r) => fail('Should not be right'),
      );
    });

    test("given the absent event group, should return Failure", () async {
      when(() => personSpGateway.getById(any()))
          .thenAnswer((_) async => FakePersonModel());
      when(() => eventGroupSpGateway.getById(any()))
          .thenAnswer((_) async => null);

      final result = await repository.createBill(param);

      result.fold(
        (l) => expect(l is NotFoundFailure, true),
        (r) => fail('Should not be right'),
      );
    });

    test("should save data", () async {
      when(() => personSpGateway.getById(any()))
          .thenAnswer((_) async => FakePersonModel());
      when(() => eventGroupSpGateway.getById(any()))
          .thenAnswer((_) async => FakeEventGroupModel());
      when(() => billSpGateway.save(any())).thenAnswer((inv) async {
        BillModel model = inv.positionalArguments[0];
        return model.copyWith(id: generatedId);
      });
      when(() => eventGroupSpGateway.appendBillIds(any(), any()))
          .thenAnswer((_) async => Future.value());

      final result = await repository.createBill(param);

      verify(() => billSpGateway.save(any()));
      verify(() => eventGroupSpGateway.appendBillIds(any(), any()));
      result.fold(
        (l) => fail('Should not be left'),
        (r) => expect(r, generatedId),
      );
    });
  });
}
