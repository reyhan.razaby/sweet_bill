import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/core/failures/not_found_failure.dart';
import 'package:sweet_bill/data_source/sp_repositories/person_sp_repository.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/entities/person/person_sorting_field.dart';

import '../../fakes.dart';
import '../../mocks.dart';

void main() {
  late PersonSpRepository repository;
  late PersonSpGateway personSpGateway;

  setUpAll(() {
    registerFallbackValue(FakePersonModel());
  });

  setUp(() {
    personSpGateway = MockPersonSpGateway();
    repository = PersonSpRepository(
      personSpGateway: personSpGateway,
    );
  });

  group('createPerson', () {
    const generatedPersonId = '222';
    const person = Person(
      id: '',
      name: 'Rangga',
      avatar: PersonAvatar.pa001,
    );

    setUp(() {
      when(() => personSpGateway.save(any())).thenAnswer((inv) async {
        PersonModel model = inv.positionalArguments[0];
        return model.copyWith(id: generatedPersonId);
      });
    });

    test('should save the data', () async {
      final result = await repository.createPerson(person);

      result.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, person.copyWith(id: generatedPersonId)),
      );
    });
  });

  group('getSelf', () {
    const selfPersonId = '222';
    final selfPersonModel = PersonModel(
      id: selfPersonId,
      name: 'Rangga',
      avatar: PersonAvatar.pa001,
      creationTime: DateTime.now(),
    );

    setUp(() {
      when(() => personSpGateway.getSelfPersonId())
          .thenAnswer((_) async => selfPersonId);
      when(() => personSpGateway.getById(any()))
          .thenAnswer((_) async => selfPersonModel);
    });

    test('should return data', () async {
      final result = await repository.getSelf();
      result.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, selfPersonModel.toEntity()),
      );
    });
  });

  group('getPersons', () {
    const selfPersonId = '222';
    final creationTime = DateTime.now();
    late List<PersonModel> modelList;
    late Person personRangga;
    late Person personCinta;
    late Person personCepot;

    setUp(() {
      modelList = [
        PersonModel(
            id: '111',
            name: 'Rangga',
            creationTime: creationTime,
            avatar: PersonAvatar.pa001),
        PersonModel(
            id: selfPersonId,
            name: 'Cinta',
            creationTime: creationTime,
            avatar: PersonAvatar.pa002),
        PersonModel(
            id: '333',
            name: 'Cepot',
            creationTime: creationTime,
            avatar: PersonAvatar.pa003)
      ];

      when(() => personSpGateway.getAll()).thenAnswer((_) async => modelList);
      when(() => personSpGateway.getSelfPersonId())
          .thenAnswer((_) async => selfPersonId);

      personRangga = Person(
          id: '111',
          name: 'Rangga',
          creationTime: creationTime,
          avatar: PersonAvatar.pa001);
      personCinta = Person(
          id: '222',
          name: 'Cinta',
          creationTime: creationTime,
          avatar: PersonAvatar.pa002);
      personCepot = Person(
          id: '333',
          name: 'Cepot',
          creationTime: creationTime,
          avatar: PersonAvatar.pa003);
    });

    test('should include self', () async {
      final result = await repository.getPersons();
      result.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [personRangga, personCinta, personCepot]),
      );
    });

    test('should exclude self', () async {
      final result = await repository.getPersons(includeSelf: false);
      result.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [personRangga, personCepot]),
      );
    });

    test('should return with excluded ids', () async {
      final result =
          await repository.getPersons(includeSelf: true, excludedIds: ['333']);
      result.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [personRangga, personCinta]),
      );
    });

    test('should return with sorted name', () async {
      final ascResult =
          await repository.getPersons(sortBy: PersonSortingField.nameAsc);
      final descResult =
          await repository.getPersons(sortBy: PersonSortingField.nameDesc);

      ascResult.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [personCepot, personCinta, personRangga]),
      );
      descResult.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [personRangga, personCinta, personCepot]),
      );
    });

    test('should return with sorted creation time', () async {
      // Override rangga
      final ranggaCreationTime = DateTime(2019, 3, 21);
      modelList[0] = modelList[0].copyWith(creationTime: ranggaCreationTime);
      personRangga = personRangga.copyWith(creationTime: ranggaCreationTime);

      // Override cinta
      final cintaCreationTime = DateTime(2019, 3, 20);
      modelList[1] = modelList[1].copyWith(creationTime: cintaCreationTime);
      personCinta = personCinta.copyWith(creationTime: cintaCreationTime);

      // Override cepot
      final cepotCreationTime = DateTime(2020, 1, 1);
      modelList[2] = modelList[2].copyWith(creationTime: cepotCreationTime);
      personCepot = personCepot.copyWith(creationTime: cepotCreationTime);

      when(() => personSpGateway.getAll()).thenAnswer((_) async => modelList);

      final ascResult = await repository.getPersons(
          sortBy: PersonSortingField.creationTimeAsc);

      final descResult = await repository.getPersons(
          sortBy: PersonSortingField.creationTimeDesc);

      ascResult.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [personCinta, personRangga, personCepot]),
      );
      descResult.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [personCepot, personRangga, personCinta]),
      );
    });
  });

  group('getPerson', () {
    const id = '1';
    final creationTime = DateTime.now();
    final personModel = PersonModel(
      id: '111',
      name: 'Rangga',
      creationTime: creationTime,
      avatar: PersonAvatar.pa001,
    );

    test('return the correct result', () async {
      when(() => personSpGateway.getById(any()))
          .thenAnswer((_) async => personModel);

      final result = await repository.getPerson(id);

      verify(() => personSpGateway.getById(id));
      expect(result, Right(personModel.toEntity()));
    });

    test('return Failure if get null', () async {
      when(() => personSpGateway.getById(any())).thenAnswer((_) async => null);

      final result = await repository.getPerson(id);

      verify(() => personSpGateway.getById(id));
      result.fold(
        (failure) => expect(failure is NotFoundFailure, true),
        (_) => fail('should not be Right'),
      );
    });
  });
}
