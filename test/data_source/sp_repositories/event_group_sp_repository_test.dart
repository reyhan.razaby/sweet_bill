import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/event_group_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/data_source/sp_repositories/event_group_sp_repository.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';

import '../../fakes.dart';
import '../../mocks.dart';

void main() {
  late EventGroupSpRepository repository;
  late EventGroupSpGateway eventGroupSpGateway;
  late PersonSpGateway personSpGateway;

  setUpAll(() {
    registerFallbackValue(FakeEventGroupModel());
    registerFallbackValue(FakePersonModel());
  });

  setUp(() {
    eventGroupSpGateway = MockEventGroupSpGateway();
    personSpGateway = MockPersonSpGateway();
    repository = EventGroupSpRepository(
      eventGroupSpGateway: eventGroupSpGateway,
      personSpGateway: personSpGateway,
    );
  });

  group('createEventGroup', () {
    const generatedEventGroupId = '222';
    const eventGroup = EventGroup.initial(
      name: 'Trip to Jogja',
      description: 'With ayank',
    );
    const memberIds = ['123', '234'];

    setUp(() {
      when(() => eventGroupSpGateway.save(any())).thenAnswer((inv) async {
        EventGroupModel model = inv.positionalArguments[0];
        return model.copyWith(id: generatedEventGroupId);
      });
      when(() => eventGroupSpGateway.appendPersonIds(any(), any()))
          .thenAnswer((_) async => Future.value());
    });

    test('should save the data', () async {
      final result = await repository.createEventGroup(eventGroup, memberIds);

      EventGroupModel capturedModel =
          verify(() => eventGroupSpGateway.save(captureAny())).captured[0];
      expect(capturedModel.name, eventGroup.name);
      expect(capturedModel.description, eventGroup.description);
      result.fold(
        (l) => fail('should not be left'),
        (r) => expect(r.id, generatedEventGroupId),
      );
      verify(() => eventGroupSpGateway.appendPersonIds(
          generatedEventGroupId, memberIds));
    });

    test('set as active', () async {
      when(() => eventGroupSpGateway.setActiveEventGroupId(any()))
          .thenAnswer((_) async => Future.value());

      await repository.createEventGroup(eventGroup, memberIds,
          setAsActive: true);

      verify(() =>
          eventGroupSpGateway.setActiveEventGroupId(generatedEventGroupId));
    });
  });

  group('getEventGroups', () {
    test('should getAll and return the correct result', () async {
      final current = DateTime.now();
      final modelsResult = [
        EventGroupModel(id: '1', name: 'Abc', creationTime: current),
        EventGroupModel(id: '2', name: 'Def', creationTime: current)
      ];
      when(() => eventGroupSpGateway.getAll())
          .thenAnswer((_) async => modelsResult);

      final result = await repository.getEventGroups();

      verify(() => eventGroupSpGateway.getAll());
      result.fold(
        (l) => fail('should not be left'),
        (r) => expect(r, [
          EventGroup(id: '1', name: 'Abc', creationTime: current),
          EventGroup(id: '2', name: 'Def', creationTime: current)
        ]),
      );
    });
  });

  group('getMembers', () {
    test('should getPersons and return the correct result', () async {
      final current = DateTime.now();
      final personModel = PersonModel(
        id: 'a',
        name: 'aaa',
        avatar: PersonAvatar.pa001,
        creationTime: current,
      );
      when(() => eventGroupSpGateway.getPersonIds(any()))
          .thenAnswer((_) async => ['1', '2']);
      when(() => personSpGateway.getById(any()))
          .thenAnswer((_) async => personModel);

      const eventGroupId = 'xxx';
      final result = await repository.getMembers(eventGroupId);

      verify(() => eventGroupSpGateway.getPersonIds(eventGroupId));
      result.fold(
        (l) => fail('should not be left'),
        (r) {
          final person = personModel.toEntity();
          expect(r, [person, person]);
        },
      );
    });
  });

  group('getEventGroup', () {
    test('should return the correct result', () async {
      final current = DateTime.now();
      final model =
          EventGroupModel(id: '1', name: 'Abc', creationTime: current);
      when(() => eventGroupSpGateway.getById(any()))
          .thenAnswer((_) async => model);

      final result = await repository.getEventGroup('1');

      verify(() => eventGroupSpGateway.getById('1'));
      result.fold(
        (l) => fail('should not be left'),
        (r) =>
            expect(r, EventGroup(id: '1', name: 'Abc', creationTime: current)),
      );
    });
  });

  group('registerMember', () {
    const eventGroupId = '100';
    const newPersonId = '4';

    setUp(() {
      when(() => eventGroupSpGateway.appendPersonIds(any(), any()))
          .thenAnswer((_) async => Future.value());
    });

    test('should append person to event group', () async {
      await repository.registerMember(eventGroupId, newPersonId);
      verify(() =>
          eventGroupSpGateway.appendPersonIds(eventGroupId, [newPersonId]));
    });
  });
}
