import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/calculated_debts_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/debt_model.dart';
import 'package:sweet_bill/data_source/sp_repositories/debt_sp_repository.dart';

import '../../fakes.dart';
import '../../mocks.dart';

void main() {
  late DebtSpRepository repository;
  late PersonSpGateway personSpGateway;
  late EventGroupSpGateway eventGroupSpGateway;

  setUp(() {
    personSpGateway = MockPersonSpGateway();
    eventGroupSpGateway = MockEventGroupSpGateway();
    repository = DebtSpRepository(
      personSpGateway: personSpGateway,
      eventGroupSpGateway: eventGroupSpGateway,
    );
  });

  group('getCalculatedDebts', () {
    test('given absent calculatedDebts, should return Right anyway', () async {
      when(() => personSpGateway.getById(any()))
          .thenAnswer((_) async => FakePersonModel());
      when(() => eventGroupSpGateway.getCalculatedDebts(any()))
          .thenAnswer((_) async => null);

      final res = await repository.getCalculatedDebts(fakeString);
      expect(res.isRight(), true);
    });

    test('given absent person, should return Right anyway', () async {
      when(() => personSpGateway.getById(any())).thenAnswer((_) async => null);

      List<DebtModel> debtModels = [
        const DebtModel(borrowerId: fakeString, lenderId: fakeString),
      ];
      when(() => eventGroupSpGateway.getCalculatedDebts(any())).thenAnswer(
          (_) async =>
              CalculatedDebtsModel(receivablesKey: 'xxx', debts: debtModels));

      final res = await repository.getCalculatedDebts(fakeString);
      expect(res.isRight(), true);
    });
  });
}
