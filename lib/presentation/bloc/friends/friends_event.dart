part of 'friends_bloc.dart';

@immutable
abstract class FriendsEvent extends Equatable {}

class LoadFriends extends FriendsEvent {
  final bool includeSelf;
  final String? name;
  final List<String>? excludeIds;

  LoadFriends({
    this.includeSelf = false,
    this.name,
    this.excludeIds,
  });

  @override
  List<Object?> get props => [includeSelf, name, excludeIds];
}

class ClearFriends extends FriendsEvent {
  @override
  List<Object?> get props => [];
}
