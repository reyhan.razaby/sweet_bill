import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/entities/person/person_sorting_field.dart';
import 'package:sweet_bill/domain/use_cases/person/get_persons/get_persons_param.dart';
import 'package:sweet_bill/domain/use_cases/person/get_persons/get_persons_use_case.dart';

part 'friends_event.dart';
part 'friends_state.dart';

class FriendsBloc extends Bloc<FriendsEvent, FriendsState> {
  final GetPersonsUseCase _getPersonsUseCase;

  FriendsBloc({
    required GetPersonsUseCase getPersonsUseCase,
  })  : _getPersonsUseCase = getPersonsUseCase,
        super(FriendsEmpty()) {
    on<LoadFriends>(_onLoadFriends);
    on<ClearFriends>(_onClearFriends);
  }

  _onLoadFriends(LoadFriends event, emit) async {
    emit(FriendsLoading());

    final res = await _getPersonsUseCase(
      GetPersonsParam(
        includeSelf: event.includeSelf,
        sortBy: PersonSortingField.nameAsc,
        excludeIds: event.excludeIds,
      ),
    );
    res.fold(
      (l) => emit(FriendsError()),
      (r) => emit(FriendsLoaded(friends: r)),
    );
  }

  _onClearFriends(ClearFriends event, emit) async {
    emit(FriendsEmpty());
  }
}
