part of 'friends_bloc.dart';

@immutable
abstract class FriendsState extends Equatable {
  final List<Person>? friends;

  const FriendsState({
    this.friends,
  });

  @override
  List<Object?> get props => [friends];
}

class FriendsEmpty extends FriendsState {}

class FriendsError extends FriendsState {}

class FriendsLoading extends FriendsState {}

class FriendsLoaded extends FriendsState {
  const FriendsLoaded({
    required List<Person> friends,
  }) : super(friends: friends);
}
