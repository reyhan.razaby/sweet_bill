part of 'bill_form_cost_bloc.dart';

abstract class BillFormCostEvent extends Equatable {
  const BillFormCostEvent();

  @override
  List<Object?> get props => [];
}

class LoadCostInitialValues extends BillFormCostEvent {}

class ChangeSplitStrategy extends BillFormCostEvent {
  final SplitStrategies splitStrategy;

  const ChangeSplitStrategy({
    required this.splitStrategy,
  });

  @override
  List<Object?> get props => [splitStrategy];
}

class ChangeCostAmount extends BillFormCostEvent {
  final double cost;
  final int index;

  const ChangeCostAmount({
    required this.cost,
    required this.index,
  });

  @override
  List<Object?> get props => [cost, index];
}

class AdjustFocusedCostToExact extends BillFormCostEvent {}

class ToggleInvolved extends BillFormCostEvent {
  final int index;

  const ToggleInvolved({
    required this.index,
  });

  @override
  List<Object?> get props => [index];
}

class FocusAmountInput extends BillFormCostEvent {
  final int index;

  const FocusAmountInput({
    required this.index,
  });

  @override
  List<Object?> get props => [index];
}

class ChangeInvolvedAll extends BillFormCostEvent {
  final bool isAllInvolved;

  const ChangeInvolvedAll({
    required this.isAllInvolved,
  });

  @override
  List<Object?> get props => [isAllInvolved];
}

class SaveCost extends BillFormCostEvent {}

class AddNewMember extends BillFormCostEvent {
  final Person member;

  const AddNewMember({required this.member});

  @override
  List<Object?> get props => [member];
}
