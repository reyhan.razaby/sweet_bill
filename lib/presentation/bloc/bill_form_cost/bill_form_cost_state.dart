part of 'bill_form_cost_bloc.dart';

abstract class BillFormCostState extends Equatable {
  const BillFormCostState();

  @override
  List<Object?> get props => [];
}

class BillFormCostEmpty extends BillFormCostState {}

class BillFormCostLoading extends BillFormCostState {}

class BillFormCostLoaded extends BillFormCostState {
  final List<MemberCost> costs;
  final SplitStrategies splitStrategy;
  final double expectedTotalCost;
  final int focusedCostIndex;

  const BillFormCostLoaded({
    required this.costs,
    required this.splitStrategy,
    required this.expectedTotalCost,
    this.focusedCostIndex = -1,
  });

  TotalCostStatus get totalCostStatus {
    double actual = MemberCost.sumCost(costs);
    if (actual == expectedTotalCost) {
      return TotalCostStatus.exact;
    } else if (actual > expectedTotalCost) {
      return TotalCostStatus.over;
    } else {
      return TotalCostStatus.under;
    }
  }

  double get totalCost => costs.map((e) => e.cost).fold(0, (a, b) => a + b);

  double get remainingTotalCost => expectedTotalCost - totalCost;

  bool get isInvalidFocusedCostIndex =>
      focusedCostIndex < 0 || focusedCostIndex >= costs.length;

  MemberCost? get focusedMemberCost =>
      isInvalidFocusedCostIndex ? null : costs[focusedCostIndex];

  bool get isAdjustable =>
      focusedMemberCost != null &&
      focusedMemberCost!.cost + remainingTotalCost >= 0;

  @override
  List<Object?> get props =>
      [costs, splitStrategy, expectedTotalCost, focusedCostIndex];

  BillFormCostLoaded copyWith({
    List<MemberCost>? costs,
    SplitStrategies? splitStrategy,
    double? expectedTotalCost,
    int? focusedCostIndex,
  }) {
    return BillFormCostLoaded(
      costs: costs ?? this.costs,
      splitStrategy: splitStrategy ?? this.splitStrategy,
      expectedTotalCost: expectedTotalCost ?? this.expectedTotalCost,
      focusedCostIndex: focusedCostIndex ?? this.focusedCostIndex,
    );
  }
}

class MemberCost extends Equatable {
  final Person person;
  final double cost;
  final String? note;
  final bool isInvolved;

  const MemberCost({
    required this.person,
    required this.cost,
    this.note,
    this.isInvolved = false,
  });

  double get totalCost => cost;

  static double sumCost(List<MemberCost> costs) {
    return costs.map((e) => e.totalCost).fold(0, (a, b) => a + b);
  }

  MemberCost copyWith({
    Person? person,
    double? cost,
    String? note,
    bool? isInvolved,
  }) {
    return MemberCost(
      person: person ?? this.person,
      cost: cost ?? this.cost,
      note: note ?? this.note,
      isInvolved: isInvolved ?? this.isInvolved,
    );
  }

  @override
  List<Object?> get props => [person, cost, note, isInvolved];
}

enum TotalCostStatus { over, under, exact }

extension TotalCostStatusExtension on TotalCostStatus {
  bool get isExact => this == TotalCostStatus.exact;

  bool get isOver => this == TotalCostStatus.over;

  bool get isUnder => this == TotalCostStatus.under;
}
