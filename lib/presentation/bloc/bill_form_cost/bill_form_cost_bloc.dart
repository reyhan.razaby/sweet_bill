import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';

part 'bill_form_cost_event.dart';
part 'bill_form_cost_state.dart';

class BillFormCostBloc extends Bloc<BillFormCostEvent, BillFormCostState> {
  final BillFormBloc _billFormBloc;
  final ProfileCubit _profileCubit;

  BillFormCostBloc({
    required BillFormBloc billFormBloc,
    required ProfileCubit profileCubit,
  })  : _billFormBloc = billFormBloc,
        _profileCubit = profileCubit,
        super(BillFormCostEmpty()) {
    on<LoadCostInitialValues>(_onLoadInitialValues);
    on<ChangeSplitStrategy>(_onChangeSplitStrategy);
    on<ToggleInvolved>(_onToggleInvolved);
    on<ChangeInvolvedAll>(_onChangeInvolvedAll);
    on<ChangeCostAmount>(_onChangeCostAmount);
    on<SaveCost>(_onSaveCost);
    on<AddNewMember>(_onAddNewMember);
    on<FocusAmountInput>(_onFocusAmountInput);
    on<AdjustFocusedCostToExact>(_onAdjustFocusedCostToExact);
  }

  void _onLoadInitialValues(LoadCostInitialValues event, emit) {
    Bill currentBill = _billFormBloc.state.bill;

    // Group costs by personId
    Map<String, BillPersonalCost> personalCosts = {
      for (var personalCost in currentBill.personalCosts)
        personalCost.personId: personalCost
    };

    // Build costs from members
    List<MemberCost> costs = _billFormBloc.members.map((member) {
      BillPersonalCost? personalCost = personalCosts[member.id];
      bool isInvolved = personalCost != null &&
          currentBill.splitStrategy == SplitStrategies.equally;

      return MemberCost(
        person: member,
        cost: personalCost != null ? personalCost.cost : 0,
        note: personalCost?.note,
        isInvolved: isInvolved,
      );
    }).toList()
      ..sort((a, b) =>
          StringUtils.compareAlphabetically(a.person.name, b.person.name));

    emit(BillFormCostLoaded(
      costs: costs,
      splitStrategy: currentBill.splitStrategy,
      expectedTotalCost: _billFormBloc.state.totalCost,
    ));
  }

  void _onChangeSplitStrategy(ChangeSplitStrategy event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded) {
      return;
    }

    var newStrategy = event.splitStrategy;

    List<MemberCost> costs = [];
    if (newStrategy == SplitStrategies.equally) {
      costs = currentState.costs
          .map((e) => e.copyWith(isInvolved: e.totalCost > 0))
          .toList();
      costs = _divideEvenly(costs, currentState.expectedTotalCost,
          isInvolvedOnly: false);
    } else if (newStrategy == SplitStrategies.custom) {
      costs =
          currentState.costs.map((e) => e.copyWith(isInvolved: false)).toList();
    }

    emit(currentState.copyWith(splitStrategy: newStrategy, costs: costs));
  }

  void _onFocusAmountInput(FocusAmountInput event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded) {
      return;
    }

    emit(currentState.copyWith(focusedCostIndex: event.index));
  }

  void _onAdjustFocusedCostToExact(AdjustFocusedCostToExact event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded ||
        currentState.isInvalidFocusedCostIndex) {
      return;
    }

    MemberCost? focusedCost = currentState.focusedMemberCost;
    if (focusedCost == null) {
      return;
    }

    double newCost = focusedCost.cost + currentState.remainingTotalCost;
    if (newCost < 0) {
      return;
    }

    List<MemberCost> newMemberCosts = List.of(currentState.costs);
    newMemberCosts[currentState.focusedCostIndex] =
        focusedCost.copyWith(cost: newCost);
    emit(currentState.copyWith(costs: newMemberCosts));
  }

  void _onToggleInvolved(ToggleInvolved event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded) {
      return;
    }

    List<MemberCost> memberCosts = List.of(currentState.costs);

    MemberCost changed = memberCosts[event.index];
    bool isInvolved = !changed.isInvolved;
    memberCosts[event.index] = changed.copyWith(
      isInvolved: isInvolved,
      cost: isInvolved ? changed.cost : 0,
    );

    memberCosts = _divideEvenly(memberCosts, currentState.expectedTotalCost);

    emit(currentState.copyWith(
      costs: memberCosts,
    ));
  }

  void _onChangeCostAmount(ChangeCostAmount event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded) {
      return;
    }

    List<MemberCost> memberCosts = List.of(currentState.costs);
    MemberCost changed = memberCosts[event.index];
    memberCosts[event.index] = changed.copyWith(
      cost: event.cost,
    );

    emit(currentState.copyWith(
      costs: memberCosts,
    ));
  }

  void _onChangeInvolvedAll(ChangeInvolvedAll event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded) {
      return;
    }

    List<MemberCost> memberCosts = [];
    for (var memberCost in currentState.costs) {
      memberCosts.add(memberCost.copyWith(
        isInvolved: event.isAllInvolved,
        cost: event.isAllInvolved ? memberCost.cost : 0,
      ));
    }

    memberCosts = _divideEvenly(memberCosts, currentState.expectedTotalCost);

    emit(currentState.copyWith(costs: memberCosts));
  }

  void _onAddNewMember(AddNewMember event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded) {
      return;
    }

    emit(BillFormCostLoading());

    // Append new member at the last index with zero amount
    List<MemberCost> costs = List.of(currentState.costs)
      ..add(MemberCost(
        person: event.member,
        cost: 0,
        isInvolved: false,
      ));
    emit(currentState.copyWith(costs: costs));

    if (currentState.splitStrategy == SplitStrategies.equally) {
      int newMemberIndex = costs.length - 1;
      add(ToggleInvolved(index: newMemberIndex));
    }
    add(SaveCost());
  }

  void _onSaveCost(SaveCost event, emit) {
    BillFormCostState currentState = state;
    if (currentState is! BillFormCostLoaded) {
      return;
    }

    List<BillPersonalCost> newPersonalCosts = currentState.costs
        .where((e) {
          if (currentState.splitStrategy == SplitStrategies.equally) {
            return e.isInvolved;
          } else {
            return e.cost > 0;
          }
        })
        .map((e) => BillPersonalCost(
              personId: e.person.id,
              cost: e.cost,
              note: e.note,
              additionalCost: 0,
            ))
        .toList();

    _billFormBloc.add(ChangeForm(
      splitStrategy: currentState.splitStrategy,
      costs: newPersonalCosts,
    ));
  }

  List<MemberCost> _divideEvenly(
    List<MemberCost> costs,
    double expectedTotalCost, {
    bool isInvolvedOnly = true,
  }) {
    costs = List.of(costs);
    int numberOfPerson = costs
        .where((e) => isInvolvedOnly ? e.isInvolved : e.totalCost > 0)
        .length;
    int maxFractionDigits =
        (_profileCubit.state as ProfileAdded).settings.maxFractionDigits;
    List<double> newAmounts = NumberUtils.divideEvenly(
        expectedTotalCost, numberOfPerson, maxFractionDigits);

    for (int i = 0, j = 0; i < costs.length && j < newAmounts.length; i++) {
      double newAmount = 0;
      bool giveDividedAmount = (isInvolvedOnly && costs[i].isInvolved) ||
          (!isInvolvedOnly && costs[i].totalCost > 0);

      if (giveDividedAmount) {
        newAmount = newAmounts[j];
        j++;
      }
      costs[i] = costs[i].copyWith(
        cost: newAmount,
      );
    }

    return costs;
  }
}
