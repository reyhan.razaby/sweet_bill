import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/core/io/io_gateway.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_group_detail/get_event_group_detail_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_use_case.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/handlers/remove_member_handler.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';

import 'handlers/add_member_handler.dart';
import 'handlers/initialize_form_handler.dart';
import 'handlers/save_event_group_handler.dart';

part 'event_group_form_event.dart';
part 'event_group_form_state.dart';

class EventGroupFormBloc
    extends Bloc<EventGroupFormEvent, EventGroupFormState> {
  late EventGroupFormState? initialLoadedState;

  late AddMemberHandler _addMemberHandler;
  late SaveEventGroupHandler _saveEventGroupHandler;
  late InitializeFormHandler _initializeFormHandler;
  late RemoveMemberHandler _removeMemberHandler;

  EventGroupFormBloc({
    required ProfileCubit profileCubit,
    required CreateEventGroupUseCase createEventGroupUseCase,
    required UpdateEventGroupUseCase updateEventGroupUseCase,
    required GetEventGroupDetailUseCase getEventGroupDetailUseCase,
    required GetBillsUseCase getBillsUseCase,
    required IoGateway ioGateway,
  }) : super(FormEmpty()) {
    initialLoadedState = null;
    _addMemberHandler = AddMemberHandler();
    _saveEventGroupHandler = SaveEventGroupHandler(
        createEventGroupUseCase: createEventGroupUseCase,
        updateEventGroupUseCase: updateEventGroupUseCase,
        ioGateway: ioGateway);
    _initializeFormHandler = InitializeFormHandler(
        profileCubit: profileCubit,
        getEventGroupDetailUseCase: getEventGroupDetailUseCase);
    _removeMemberHandler =
        RemoveMemberHandler(getBillsUseCase: getBillsUseCase);

    on<InitializeForm>(_onInitializeForm);
    on<AddMember>(_onAddMember);
    on<EditMember>(_onEditMember);
    on<RemoveMember>(_onRemoveMember);
    on<SaveEventGroup>(_onSaveEventGroup);
    on<ChangeName>(_onChangeName);
    on<ChangeImagePath>(_onChangeImagePath);
    on<RemoveImagePath>(_onRemoveImagePath);
  }

  bool isChanged() {
    if (initialLoadedState == null) {
      return false;
    }
    if (state.name != initialLoadedState!.name) return true;
    if (state.eventGroupId != initialLoadedState!.eventGroupId) return true;
    if (state.imagePath != initialLoadedState!.imagePath) return true;
    if (!state.memberIds.containsAll(initialLoadedState!.memberIds) ||
        !initialLoadedState!.memberIds.containsAll(state.memberIds)) {
      return true;
    }
    return false;
  }

  Future<void> _onInitializeForm(event, emit) async {
    await _initializeFormHandler.handle(event, state, emit);
    initialLoadedState = state;
  }

  Future<void> _onAddMember(event, emit) async {
    await _addMemberHandler.handle(event, state, emit);
  }

  Future<void> _onSaveEventGroup(event, emit) async {
    await _saveEventGroupHandler.handle(event, state, initialLoadedState, emit);
  }

  void _onRemoveMember(RemoveMember event, emit) async {
    await _removeMemberHandler.handle(event, state, initialLoadedState, emit);
  }

  void _onEditMember(EditMember event, emit) {
    if (event.index >= 0) {
      final newMembers = List.of(state.members);
      newMembers.removeAt(event.index);
      newMembers.insert(event.index, event.member);

      emit(FormLoaded.copyWithState(
        state,
        members: newMembers,
        buildForm: false,
      ));
    }
  }

  void _onChangeName(ChangeName event, emit) {
    emit(FormLoaded.copyWithState(
      state,
      name: event.name,
      buildForm: false,
    ));
  }

  void _onChangeImagePath(ChangeImagePath event, emit) {
    emit(FormLoaded.copyWithState(
      state,
      imagePath: event.imagePath,
      buildForm: false,
    ));
  }

  void _onRemoveImagePath(RemoveImagePath event, emit) {
    emit(FormLoaded.copyWithState(
      state,
      imagePath: '',
      buildForm: false,
    ));
  }
}
