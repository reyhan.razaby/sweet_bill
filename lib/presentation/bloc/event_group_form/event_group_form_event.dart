// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'event_group_form_bloc.dart';

@immutable
abstract class EventGroupFormEvent extends Equatable {}

class ChangeName extends EventGroupFormEvent {
  final String name;

  ChangeName({
    required this.name,
  });

  @override
  List<Object?> get props => [name];
}

class ChangeImagePath extends EventGroupFormEvent {
  final String imagePath;

  ChangeImagePath({
    required this.imagePath,
  });

  @override
  List<Object?> get props => [imagePath];
}

class RemoveImagePath extends EventGroupFormEvent {
  @override
  List<Object?> get props => [];
}

class SaveEventGroup extends EventGroupFormEvent {
  @override
  List<Object?> get props => [];
}

class AddMember extends EventGroupFormEvent {
  final EventGroupMember member;

  AddMember({
    required this.member,
  });

  @override
  List<Object?> get props => [member];
}

class EditMember extends EventGroupFormEvent {
  final EventGroupMember member;
  final int index;

  EditMember({
    required this.member,
    required this.index,
  });

  @override
  List<Object?> get props => [member];
}

class RemoveMember extends EventGroupFormEvent {
  final int index;
  final bool validateInvolvement;

  RemoveMember({
    required this.index,
    this.validateInvolvement = true,
  });

  @override
  List<Object?> get props => [index];
}

class InitializeForm extends EventGroupFormEvent {
  final String? eventGroupId;
  final VoidCallback? onInitialized;

  InitializeForm({
    this.eventGroupId,
    this.onInitialized,
  });

  @override
  List<Object?> get props => [eventGroupId];
}
