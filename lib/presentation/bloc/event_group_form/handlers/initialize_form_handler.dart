import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/extensions/either.dart';

import '../../../../core/utils/string_utils.dart';
import '../../../../domain/entities/person/person.dart';
import '../../../../domain/use_cases/event_group/get_event_group_detail/get_event_group_detail_use_case.dart';
import '../../../../domain/use_cases/event_group/get_event_group_detail/get_event_groups_param.dart';
import '../../profile/profile_cubit.dart';
import '../event_group_form_bloc.dart';

class InitializeFormHandler {
  final ProfileCubit profileCubit;
  final GetEventGroupDetailUseCase getEventGroupDetailUseCase;

  const InitializeFormHandler({
    required this.profileCubit,
    required this.getEventGroupDetailUseCase,
  });

  Future<void> handle(
    InitializeForm event,
    EventGroupFormState state,
    Emitter<EventGroupFormState> emit,
  ) async {
    emit(FormLoading());
    final profileState = profileCubit.state;
    if (profileState is! ProfileAdded) {
      profileCubit.clearProfile();
      emit(FormError(state));
      return;
    }

    Person self = profileState.self;
    final defaultInitialState = FormLoaded(
      name: '',
      members: [EventGroupMember.selfPerson(self)],
    );

    if (event.eventGroupId != null) {
      GetEventGroupDetailParam param = GetEventGroupDetailParam(
        eventGroupId: event.eventGroupId!,
        fetchMembers: true,
      );
      final egDetailEither = await getEventGroupDetailUseCase(param);
      if (egDetailEither.isRight()) {
        final egDetail = egDetailEither.asRight();
        final eg = egDetail.eventGroup;
        final egMembers = egDetail.members ?? [];
        final newMembers = egMembers.map(
          (m) {
            if (m.id == self.id) {
              return EventGroupMember.selfPerson(m);
            } else {
              return EventGroupMember.friendPerson(m);
            }
          },
        ).toList()
          ..sort((a, b) {
            if (a.isSelf) {
              return -1;
            } else if (b.isSelf) {
              return 1;
            }
            return StringUtils.compareAlphabetically(a.name, b.name);
          });

        emit(FormLoaded(
          eventGroupId: eg.id,
          name: eg.name,
          imagePath: eg.imagePath,
          members: newMembers,
        ));
      } else {
        emit(defaultInitialState);
      }
    } else {
      emit(defaultInitialState);
    }

    if (event.onInitialized != null) {
      event.onInitialized!();
    }
  }
}
