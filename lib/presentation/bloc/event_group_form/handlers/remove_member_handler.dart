import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';

import '../../../../domain/entities/bill/bill.dart';
import '../../../../domain/use_cases/bill/get_bills/get_bills_use_case.dart';
import '../event_group_form_bloc.dart';

class RemoveMemberHandler {
  final GetBillsUseCase getBillsUseCase;
  static const errorTitleMessage = 'Cannot remove';

  const RemoveMemberHandler({
    required this.getBillsUseCase,
  });

  Future<void> handle(
    RemoveMember event,
    EventGroupFormState state,
    EventGroupFormState? initialLoadedState,
    Emitter<EventGroupFormState> emit,
  ) async {
    if (event.index < 0 || event.index >= state.members.length) {
      return;
    }

    EventGroupMember member = state.members[event.index];
    if (member.isSelf) {
      return; // Disallow removing self
    }

    if (event.validateInvolvement && state.eventGroupId != null) {
      bool isValid = await validateInvolvement(member, state, emit);
      if (!isValid) {
        return;
      }
    }

    emit(FormLoaded.copyWithState(
      state,
      members: List.of(state.members)..removeAt(event.index),
      buildForm: false,
    ));
  }

  Future<bool> validateInvolvement(
      EventGroupMember member, EventGroupFormState state, emit) async {
    final billsEither = await getBillsUseCase(GetBillsParam(
      eventGroupId: state.eventGroupId!,
      involvedPersonId: member.id,
    ));
    if (billsEither.isLeft()) {
      return false;
    }

    List<Bill> involvedBills = billsEither.asRight();
    if (involvedBills.isNotEmpty) {
      emit(FormError(
        state,
        titleMessage: errorTitleMessage,
        bodyMessage: buildBodyMessage(member, involvedBills),
      ));
      emit(FormLoaded.copyWithState(state, buildForm: true));
      return false;
    }
    return true;
  }

  static String? buildBodyMessage(EventGroupMember member, List<Bill> bills) {
    List<String> billNames = bills.map((e) => e.title).toList();
    return 'Because ${member.name} is involved in '
        '${bills.length} ${bills.length > 1 ? 'bills' : 'bill'}:'
        '\n'
        '${StringUtils.join(billNames, isWithAnd: true)}';
  }
}
