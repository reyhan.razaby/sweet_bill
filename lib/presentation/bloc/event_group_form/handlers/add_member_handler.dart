import 'package:flutter_bloc/flutter_bloc.dart';

import '../event_group_form_bloc.dart';

class AddMemberHandler {
  Future<void> handle(
    AddMember event,
    EventGroupFormState state,
    Emitter<EventGroupFormState> emit,
  ) async {
    emit(FormLoaded.copyWithState(
      state,
      members: List.of(state.members)..add(event.member),
      buildForm: false,
      spawnNewMember: true,
    ));
  }
}
