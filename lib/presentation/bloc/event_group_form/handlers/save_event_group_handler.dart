import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/io/io_gateway.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_use_case.dart';

import '../event_group_form_bloc.dart';

class SaveEventGroupHandler {
  final CreateEventGroupUseCase createEventGroupUseCase;
  final UpdateEventGroupUseCase updateEventGroupUseCase;
  final IoGateway ioGateway;

  const SaveEventGroupHandler({
    required this.createEventGroupUseCase,
    required this.updateEventGroupUseCase,
    required this.ioGateway,
  });

  Future<void> handle(
    SaveEventGroup event,
    EventGroupFormState state,
    EventGroupFormState? initialLoadedState,
    Emitter<EventGroupFormState> emit,
  ) async {
    emit(EventGroupSaving.copyWithState(state));

    String? oldImagePath = initialLoadedState?.imagePath;
    bool isImageChanged = state.imagePath != oldImagePath;
    File? newImageFile;

    if (isImageChanged) {
      if (oldImagePath != null) {
        await ioGateway.deleteFile(oldImagePath);
      }
      if (state.imagePath != null) {
        newImageFile = await ioGateway.saveFileToAppDir(state.imagePath!);
      }
    }

    if (state.eventGroupId != null) {
      UpdateEventGroupParam param = UpdateEventGroupParam(
        eventGroup: EventGroup(
          id: state.eventGroupId!,
          name: state.name,
          imagePath: newImageFile != null ? newImageFile.path : state.imagePath,
        ),
        members: state.members
            .map((e) => Person(id: e.id, name: e.name, avatar: e.avatar))
            .toList(),
      );
      await updateEventGroupUseCase(param);
      emit(EventGroupSaved.copyWithState(state));
    } else {
      CreateEventGroupParam param = CreateEventGroupParam(
        eventGroup: EventGroup.initial(
          name: state.name,
          imagePath: newImageFile?.path,
        ),
        members: state.members
            .map((e) => Person(id: e.id, name: e.name, avatar: e.avatar))
            .toList(),
      );
      final either = await createEventGroupUseCase(param);
      if (either.isRight()) {
        emit(EventGroupSaved.copyWithState(
          state,
          createdEventGroup: either.asRight(),
        ));
      } else {
        emit(FormError(state));
      }
    }
  }
}
