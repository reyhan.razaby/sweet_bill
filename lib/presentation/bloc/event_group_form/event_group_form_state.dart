part of 'event_group_form_bloc.dart';

@immutable
abstract class EventGroupFormState extends Equatable {
  final String name;
  final List<EventGroupMember> members;
  final String? eventGroupId;
  final String? imagePath;
  final Set<String> memberIds;

  /* This way is too hard. When new field introduced on parent state, make sure
  all children state should have the new field as well.
  If it missed, it's too dangerous! */

  EventGroupFormState({
    required this.name,
    required this.members,
    this.eventGroupId,
    String? imagePath,
  })  : memberIds = members.map((member) => member.id).toSet(),
        imagePath =
            StringUtils.isNotNullAndNotEmpty(imagePath) ? imagePath : null;

  EventGroupFormState.copyWith(EventGroupFormState state)
      : this(
          name: state.name,
          members: state.members,
          eventGroupId: state.eventGroupId,
          imagePath: state.imagePath,
        );

  bool equals(EventGroupFormState state) {
    return this == state;
  }

  @override
  List<Object?> get props => [name, members, imagePath, eventGroupId];
}

class FormEmpty extends EventGroupFormState {
  FormEmpty() : super(name: '', members: const []);
}

class FormLoaded extends EventGroupFormState {
  final bool buildForm;
  final bool spawnNewMember;

  FormLoaded({
    String? name,
    required List<EventGroupMember> members,
    String? imagePath,
    String? eventGroupId,
    this.buildForm = true,
    this.spawnNewMember = false,
  }) : super(
          members: members,
          name: name ?? '',
          imagePath: imagePath,
          eventGroupId: eventGroupId,
        );

  FormLoaded.copyWithState(
    EventGroupFormState state, {
    String? name,
    List<EventGroupMember>? members,
    String? imagePath,
    String? eventGroupId,
    this.buildForm = true,
    this.spawnNewMember = false,
  }) : super(
          name: name ?? state.name,
          members: members ?? state.members,
          imagePath: imagePath ?? state.imagePath,
          eventGroupId: eventGroupId ?? state.eventGroupId,
        );

  @override
  List<Object?> get props => super.props..addAll([buildForm, spawnNewMember]);
}

class FormLoading extends EventGroupFormState {
  FormLoading() : super(name: '', members: []);
}

class EventGroupSaving extends EventGroupFormState {
  EventGroupSaving.copyWithState(EventGroupFormState state)
      : super(
          name: state.name,
          members: state.members,
          imagePath: state.imagePath,
          eventGroupId: state.eventGroupId,
        );
}

class EventGroupSaved extends EventGroupFormState {
  final EventGroup? createdEventGroup;

  EventGroupSaved.copyWithState(
    EventGroupFormState state, {
    String? name,
    String? imagePath,
    List<EventGroupMember>? members,
    String? eventGroupId,
    this.createdEventGroup,
  }) : super(
          name: name ?? state.name,
          members: members ?? state.members,
          imagePath: imagePath ?? state.imagePath,
          eventGroupId: eventGroupId ?? state.eventGroupId,
        );
}

class FormError extends EventGroupFormState {
  final String? titleMessage;
  final String? bodyMessage;

  FormError(
    EventGroupFormState state, {
    this.titleMessage,
    this.bodyMessage,
  }) : super.copyWith(state);

  @override
  List<Object?> get props => super.props..addAll([titleMessage, bodyMessage]);
}

class EventGroupMember extends Equatable {
  final String id;
  final String name;
  final bool isNew;
  final bool isSelf;
  final PersonAvatar avatar;

  const EventGroupMember({
    required this.id,
    required this.name,
    required this.isNew,
    required this.isSelf,
    required this.avatar,
  });

  const EventGroupMember.newMember(this.name, this.avatar)
      : id = '',
        isNew = true,
        isSelf = false;

  const EventGroupMember.self(this.id, this.name, this.avatar)
      : isNew = false,
        isSelf = true;

  EventGroupMember.selfPerson(Person person)
      : id = person.id,
        name = person.name,
        avatar = person.avatar,
        isNew = false,
        isSelf = true;

  const EventGroupMember.friend(this.id, this.name, this.avatar)
      : isNew = false,
        isSelf = false;

  EventGroupMember.friendPerson(Person person)
      : id = person.id,
        name = person.name,
        avatar = person.avatar,
        isNew = false,
        isSelf = false;

  @override
  List<Object?> get props => [id, name, isNew, isSelf, avatar];

  EventGroupMember copyWith({
    String? id,
    String? name,
    bool? isNew,
    bool? isSelf,
    PersonAvatar? avatar,
  }) {
    return EventGroupMember(
      id: id ?? this.id,
      name: name ?? this.name,
      isNew: isNew ?? this.isNew,
      isSelf: isSelf ?? this.isSelf,
      avatar: avatar ?? this.avatar,
    );
  }
}
