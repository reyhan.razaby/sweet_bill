part of 'event_groups_bloc.dart';

@immutable
abstract class EventGroupsState extends Equatable {
  final List<EventGroupSelectItem>? eventGroups;
  final String? activeEventGroupId;
  final String? focusedId;

  List<EventGroupSelectItem> get getEventGroupsOrEmpty => eventGroups ?? [];

  const EventGroupsState({
    this.eventGroups,
    this.activeEventGroupId,
    this.focusedId,
  });

  @override
  List<Object?> get props => [eventGroups, activeEventGroupId, focusedId];
}

class EventGroupsEmpty extends EventGroupsState {}

class ActiveEventGroupLoading extends EventGroupsState {}

class ActiveEventGroupLoaded extends EventGroupsState {
  final String eventGroupId;

  const ActiveEventGroupLoaded({
    required this.eventGroupId,
  });

  @override
  List<Object?> get props => super.props..add(eventGroupId);
}

class EventGroupsLoading extends EventGroupsState {}

class EventGroupsLoaded extends EventGroupsState {
  final bool buildList;

  const EventGroupsLoaded({
    required List<EventGroupSelectItem> eventGroups,
    String? activeEventGroupId,
    String? focusedId,
    this.buildList = true,
  }) : super(
          eventGroups: eventGroups,
          activeEventGroupId: activeEventGroupId,
          focusedId: focusedId,
        );

  EventGroupsLoaded.copyWithState(
    EventGroupsState currentState, {
    List<EventGroupSelectItem>? eventGroups,
    String? activeEventGroupId,
    String? focusedId,
    this.buildList = true,
  }) : super(
          eventGroups: eventGroups ?? currentState.eventGroups,
          activeEventGroupId:
              activeEventGroupId ?? currentState.activeEventGroupId,
          focusedId: focusedId ?? currentState.focusedId,
        );

  @override
  List<Object?> get props => super.props..add(buildList);
}

class EventGroupsError extends EventGroupsState {
  final String message;

  const EventGroupsError({
    required this.message,
  });

  @override
  List<Object?> get props => super.props..add(message);
}
