import 'package:bloc/bloc.dart';
import 'package:sweet_bill/core/extensions/iterable.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_select_item.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/activate_event_group/activate_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/remove_event_group/remove_event_group_use_case.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'event_groups_event.dart';
part 'event_groups_state.dart';

class EventGroupsBloc extends Bloc<EventGroupsEvent, EventGroupsState> {
  final GetEventGroupsUseCase _getEventGroupsUseCase;
  final RemoveEventGroupUseCase _removeEventGroupUseCase;
  final ActivateEventGroupUseCase _activateEventGroupUseCase;

  EventGroupsBloc({
    required GetEventGroupsUseCase getEventGroupsUseCase,
    required RemoveEventGroupUseCase removeEventGroupUseCase,
    required ActivateEventGroupUseCase activateEventGroupUseCase,
  })  : _getEventGroupsUseCase = getEventGroupsUseCase,
        _removeEventGroupUseCase = removeEventGroupUseCase,
        _activateEventGroupUseCase = activateEventGroupUseCase,
        super(EventGroupsEmpty()) {
    on<LoadEventGroups>(_onLoadEventGroups);
    on<RemoveEventGroup>(_onRemoveEventGroup);
    on<ActivateEventGroup>(_onActivateEventGroup);
    on<FocusEventGroup>(_onFocusEventGroup);
  }

  void _onLoadEventGroups(event, emit) async {
    emit(EventGroupsLoading());

    final result =
        await _getEventGroupsUseCase(GetEventGroupsParam(activeOnTop: true));

    const defaultErrorMessage = 'Failed to load groups';
    result.fold(
      (l) => emit(EventGroupsError(message: l.message ?? defaultErrorMessage)),
      (r) {
        String? activeEventGroupId =
            r.firstWhereOrNull((el) => el.isCurrentlyActive)?.eventGroup.id;
        emit(EventGroupsLoaded(
          eventGroups: r,
          activeEventGroupId: activeEventGroupId,
          focusedId: activeEventGroupId,
        ));
      },
    );
  }

  void _onRemoveEventGroup(RemoveEventGroup event, emit) async {
    await _removeEventGroupUseCase(event.eventGroupId);
    add(LoadEventGroups());
  }

  void _onActivateEventGroup(ActivateEventGroup event, emit) async {
    String? eventGroupId = event.eventGroupId ?? state.focusedId;
    if (eventGroupId != null) {
      await _activateEventGroupUseCase(eventGroupId);
    }
    add(LoadEventGroups());
  }

  void _onFocusEventGroup(FocusEventGroup event, emit) async {
    emit(EventGroupsLoaded.copyWithState(
      state,
      focusedId: event.focusedId,
      buildList: false,
    ));
  }
}
