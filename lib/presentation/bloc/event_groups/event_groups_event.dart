part of 'event_groups_bloc.dart';

@immutable
abstract class EventGroupsEvent extends Equatable {}

class LoadEventGroups extends EventGroupsEvent {
  @override
  List<Object?> get props => [];
}

class RemoveEventGroup extends EventGroupsEvent {
  final String eventGroupId;

  RemoveEventGroup({
    required this.eventGroupId,
  });

  @override
  List<Object?> get props => [eventGroupId];
}

class ActivateEventGroup extends EventGroupsEvent {
  final String? eventGroupId;

  @override
  List<Object?> get props => [eventGroupId];

  ActivateEventGroup({
    this.eventGroupId,
  });
}

class FocusEventGroup extends EventGroupsEvent {
  final String focusedId;

  FocusEventGroup({
    required this.focusedId,
  });

  @override
  List<Object?> get props => [focusedId];
}
