part of 'bills_bloc.dart';

@immutable
abstract class BillsState extends Equatable {
  @override
  List<Object?> get props => [];
}

class BillsEmpty extends BillsState {}

class BillsLoading extends BillsState {}

class BillsLoaded extends BillsState {
  final List<Bill> bills;
  final List<Bill> displayedBills;

  BillsLoaded({
    required this.bills,
    required this.displayedBills,
  });

  @override
  List<Object> get props => [bills, displayedBills];

  BillsLoaded copyWith({
    List<Bill>? bills,
    List<Bill>? displayedBills,
  }) {
    return BillsLoaded(
      bills: bills ?? this.bills,
      displayedBills: displayedBills ?? this.displayedBills,
    );
  }
}

class BillsError extends BillsState {}
