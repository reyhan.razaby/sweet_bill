import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/remove_bill/remove_bill_use_case.dart';

import '../../../domain/entities/bill/bill.dart';

part 'bills_event.dart';
part 'bills_state.dart';

class BillsBloc extends Bloc<BillsEvent, BillsState> {
  final GetBillsUseCase _getBillsUseCase;
  final RemoveBillUseCase _removeBillUseCase;

  BillsBloc({
    required GetBillsUseCase getBillsUseCase,
    required RemoveBillUseCase removeBillUseCase,
  })  : _getBillsUseCase = getBillsUseCase,
        _removeBillUseCase = removeBillUseCase,
        super(BillsEmpty()) {
    on<LoadBills>(_onLoadBills);
    on<FilterBills>(_onFilterBills);
    on<RemoveBill>(_onRemoveBill);
  }

  Future<void> _onRemoveBill(RemoveBill event, emit) async {
    BillsState currentState = state;
    if (currentState is! BillsLoaded) {
      return;
    }

    Bill bill = currentState.bills.firstWhere((b) => b.id == event.billId);
    final removeRes = await _removeBillUseCase(bill.id);
    if (removeRes.isLeft()) {
      return;
    }

    List<Bill> newBills = _removeFromList(currentState.bills, event.billId);
    List<Bill> newDisplayedBills =
        _removeFromList(currentState.displayedBills, event.billId);

    emit(currentState.copyWith(
      bills: newBills,
      displayedBills: newDisplayedBills,
    ));

    if (event.onSucceed != null) {
      event.onSucceed!();
    }
  }

  List<Bill> _removeFromList(List<Bill> list, String billId) {
    int billIdx = list.indexWhere((b) => b.id == billId);
    List<Bill> newBills = List.of(list);
    newBills.removeAt(billIdx);
    return newBills;
  }

  Future<void> _onLoadBills(LoadBills event, emit) async {
    emit(BillsLoading());

    final billsEither =
        await _getBillsUseCase(GetBillsParam(eventGroupId: event.eventGroupId));
    if (billsEither.isLeft()) {
      emit(BillsError());
      return;
    }

    List<Bill> bills = billsEither.asRight();
    emit(BillsLoaded(
      bills: bills,
      displayedBills: bills,
    ));
  }

  Future<void> _onFilterBills(FilterBills event, emit) async {
    BillsState currentState = state;
    if (currentState is! BillsLoaded) {
      return;
    }

    var filtered = currentState.bills;
    if (event.searchInput != null) {
      filtered = filtered
          .where((bill) => bill.title
              .toLowerCase()
              .contains(event.searchInput!.toLowerCase()))
          .toList();
    }

    emit(currentState.copyWith(displayedBills: filtered));
  }
}
