part of 'bills_bloc.dart';

@immutable
abstract class BillsEvent extends Equatable {}

class LoadBills extends BillsEvent {
  final String eventGroupId;

  LoadBills({
    required this.eventGroupId,
  });

  @override
  List<Object?> get props => [eventGroupId];
}

class FilterBills extends BillsEvent {
  final String? searchInput;

  FilterBills({
    this.searchInput,
  });

  @override
  List<Object?> get props => [searchInput];
}

class RemoveBill extends BillsEvent {
  final String billId;
  final Function? onSucceed;

  RemoveBill({
    required this.billId,
    this.onSucceed,
  });

  @override
  List<Object?> get props => [billId];
}
