import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sweet_bill/domain/entities/debt/debts_summary.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_summary/get_debts_summary_param.dart';

import '../../../domain/use_cases/debt/get_debts_summary/get_debts_summary_use_case.dart';

part 'debts_summary_state.dart';

class DebtsSummaryCubit extends Cubit<DebtsSummaryState> {
  final GetDebtsSummaryUseCase _getDebtsSummaryUseCase;

  DebtsSummaryCubit({
    required GetDebtsSummaryUseCase getDebtsSummaryUseCase,
  })  : _getDebtsSummaryUseCase = getDebtsSummaryUseCase,
        super(DebtsSummaryEmpty());

  Future<void> loadDebtsSummary(String eventGroupId) async {
    emit(DebtsSummaryLoading());

    final eitherResult = await _getDebtsSummaryUseCase(
        GetDebtsSummaryParam(eventGroupId: eventGroupId));

    eitherResult.fold(
      (_) => emit(DebtsSummaryError()),
      (res) => emit(DebtsSummaryLoaded(debtsSummary: res)),
    );
  }
}
