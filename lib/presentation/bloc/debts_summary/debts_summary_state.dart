part of 'debts_summary_cubit.dart';

@immutable
abstract class DebtsSummaryState extends Equatable {
  @override
  List<Object?> get props => [];
}

class DebtsSummaryEmpty extends DebtsSummaryState {}

class DebtsSummaryLoading extends DebtsSummaryState {}

class DebtsSummaryError extends DebtsSummaryState {}

class DebtsSummaryLoaded extends DebtsSummaryState {
  final DebtsSummary debtsSummary;

  DebtsSummaryLoaded({
    required this.debtsSummary,
  });

  @override
  List<Object?> get props => [debtsSummary];
}
