import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_active_event_group/get_active_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_members/get_members_param.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_members/get_members_use_case.dart';
import 'package:sweet_bill/domain/use_cases/empty_param.dart';

import '../../../domain/entities/event_group/event_group.dart';
import '../../../domain/entities/person/person.dart';

part 'transactions_state.dart';

class TransactionsCubit extends Cubit<TransactionsState> {
  final GetActiveEventGroupUseCase _getActiveEventGroupUseCase;
  final GetMembersUseCase _getMembersUseCase;

  late List<Person> members;

  TransactionsCubit({
    required GetActiveEventGroupUseCase getActiveEventGroupUseCase,
    required GetMembersUseCase getMembersUseCase,
  })  : _getActiveEventGroupUseCase = getActiveEventGroupUseCase,
        _getMembersUseCase = getMembersUseCase,
        super(TransactionsEmpty()) {
    members = [];
  }

  Future<void> loadActiveEventGroup() async {
    const defaultErrorMessage = 'Failed to load active group';
    emit(TransactionsLoading());

    final activeGroupEither = await _getActiveEventGroupUseCase(EmptyParam());

    if (activeGroupEither.isLeft()) {
      // emit(TransactionsError(
      //     message: activeGroupEither.asLeft().message ?? defaultErrorMessage));
      return;
    }

    final eventGroup = activeGroupEither.asRight();
    final membersEither =
        await _getMembersUseCase(GetMembersParam(eventGroupId: eventGroup.id));
    if (membersEither.isLeft()) {
      emit(TransactionsError(
          message: membersEither.asLeft().message ?? defaultErrorMessage));
      return;
    }

    members = membersEither.asRight();
    emit(TransactionsLoaded(eventGroup: eventGroup));
  }
}
