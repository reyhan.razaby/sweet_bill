part of 'transactions_cubit.dart';

@immutable
abstract class TransactionsState extends Equatable {

}

class TransactionsEmpty extends TransactionsState {
  @override
  List<Object?> get props => [];
}

class TransactionsLoading extends TransactionsState {
  @override
  List<Object?> get props => [];
}

class TransactionsLoaded extends TransactionsState {
  final EventGroup eventGroup;

  TransactionsLoaded({
    required this.eventGroup,
  });

  @override
  List<Object?> get props => [eventGroup];
}

class TransactionsError extends TransactionsState {
  final String message;

  TransactionsError({
    required this.message,
  });

  @override
  List<Object?> get props => [message];
}
