import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/core/utils/avatar_generator.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/use_cases/person/create_self/create_self_param.dart';

import '../../../domain/entities/event_group/event_group.dart';
import '../../../domain/use_cases/event_group/create_event_group/create_event_group_param.dart';
import '../../../domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import '../../../domain/use_cases/person/create_self/create_self_use_case.dart';

part 'welcome_state.dart';

class WelcomeCubit extends Cubit<WelcomeState> {
  final CreateSelfUseCase _createSelfUseCase;
  final CreateEventGroupUseCase _createEventGroupUseCase;

  WelcomeCubit({
    required CreateSelfUseCase createSelfUseCase,
    required CreateEventGroupUseCase createEventGroupUseCase,
  })  : _createSelfUseCase = createSelfUseCase,
        _createEventGroupUseCase = createEventGroupUseCase,
        super(const WelcomeState());

  void generatePerson(String name,
      {bool isSelf = false, PersonAvatar? avatar}) {
    Person newPerson = Person.initial(
      name: name,
      avatar: avatar ??
          AvatarGenerator.generatePersonAvatar(ownedAvatars: state.avatars),
    );
    if (isSelf) {
      emit(state.copyWith(self: newPerson));
    } else {
      emit(state.copyWith(
        friends: List.of(state.friends ?? [])..add(newPerson),
      ));
    }
  }

  void removeFriend(int index) {
    int friendsCount = state.friends?.length ?? 0;
    if (index >= friendsCount || index < 0) {
      return;
    }
    emit(state.copyWith(
      friends: List.of(state.friends!)..removeAt(index),
    ));
  }

  void renameFriend(String newName, int index) {
    int friendsCount = state.friends?.length ?? 0;
    if (index >= friendsCount || index < 0) {
      return;
    }

    Person friend = state.friends![index].copyWith(name: newName);
    emit(state.copyWith(
      friends: List.of(state.friends!)
        ..removeAt(index)
        ..insert(index, friend),
    ));
  }

  void setGroupName(String newName) {
    emit(state.copyWith(groupName: newName));
  }

  Future<void> submitAll() async {
    emit(state.copyWith(status: WelcomeStatus.submitting));

    WelcomeState currentState = state;
    if (currentState.self == null) {
      _emitToFailed();
      return;
    }

    final createSelfEither = await _createSelf(currentState);
    if (createSelfEither.isLeft()) {
      _emitToFailed();
      return;
    }

    Person self = createSelfEither.asRight();
    final createGroupEither = await _createEventGroup(self, currentState);
    if (createGroupEither.isLeft()) {
      _emitToFailed();
      return;
    }

    emit(state.copyWith(self: self, status: WelcomeStatus.submitSucceed));
  }

  Future<Either<Failure, EventGroup>> _createEventGroup(
      Person self, WelcomeState currentState) async {
    CreateEventGroupParam param = CreateEventGroupParam(
      members: [self, ...?currentState.friends],
      eventGroup: EventGroup.initial(name: currentState.groupName ?? ''),
      setAsActive: true,
    );
    return _createEventGroupUseCase(param);
  }

  Future<Either<Failure, Person>> _createSelf(WelcomeState currentState) async {
    CreateSelfParam param = CreateSelfParam(
      selfPerson: currentState.self!,
      name: currentState.self!.name,
    );
    return _createSelfUseCase(param);
  }

  void _emitToFailed() {
    emit(state.copyWith(status: WelcomeStatus.submitFailed));
    emit(state.copyWith(status: WelcomeStatus.idle));
  }
}
