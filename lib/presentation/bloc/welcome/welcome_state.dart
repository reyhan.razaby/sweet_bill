part of 'welcome_cubit.dart';

@immutable
class WelcomeState extends Equatable {
  final Person? self;
  final List<Person>? friends;
  final String? groupName;
  final WelcomeStatus status;

  const WelcomeState({
    this.self,
    this.friends,
    this.groupName,
    this.status = WelcomeStatus.idle,
  });

  @override
  List<Object?> get props => [self, friends, groupName, status];

  List<Person> get members {
    List<Person> result = [];
    if (self != null) {
      result.add(self!);
    }
    if (friends != null) {
      result.addAll(friends!);
    }
    return result;
  }

  List<PersonAvatar> get avatars => members.map((e) => e.avatar).toList();

  WelcomeState copyWith({
    Person? self,
    List<Person>? friends,
    String? groupName,
    WelcomeStatus? status,
  }) {
    return WelcomeState(
      self: self ?? this.self,
      friends: friends ?? this.friends,
      groupName: groupName ?? this.groupName,
      status: status ?? this.status,
    );
  }
}

enum WelcomeStatus { idle, submitting, submitFailed, submitSucceed }
