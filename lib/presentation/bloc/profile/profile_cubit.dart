import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/domain/use_cases/empty_param.dart';
import 'package:sweet_bill/domain/use_cases/person/get_self/get_self_use_case.dart';
import 'package:sweet_bill/domain/use_cases/settings/get_settings/get_settings_use_case.dart';

import '../../../domain/entities/person/person.dart';
import '../../../domain/entities/settings/settings.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  final GetSettingsUseCase _getSettingsUseCase;
  final GetSelfUseCase _getSelfUseCase;

  ProfileCubit({
    required GetSelfUseCase getSelfUseCase,
    required GetSettingsUseCase getSettingsUseCase,
  })  : _getSettingsUseCase = getSettingsUseCase,
        _getSelfUseCase = getSelfUseCase,
        super(ProfileEmpty());

  Future<Person?> getSelf() async {
    final selfEither = await _getSelfUseCase(EmptyParam());
    if (selfEither.isLeft()) {
      return null;
    }
    return selfEither.asRight();
  }

  Future<void> checkProfile() async {
    Person? self = await getSelf();
    if (self == null) {
      emit(ProfileEmpty());
      return;
    }
    addProfile(self: self);
  }

  void addProfile({required Person self}) async {
    final settingsEither = await _getSettingsUseCase(EmptyParam());
    if (settingsEither.isLeft()) {
      emit(ProfileEmpty());
      return;
    }

    emit(ProfileAdded(self: self, settings: settingsEither.asRight()));
  }

  void clearProfile() async {
    emit(ProfileEmpty());
  }
}
