part of 'profile_cubit.dart';

abstract class ProfileState extends Equatable {
  bool get isAdded => this is ProfileAdded;

  bool get isEmpty => this is ProfileEmpty;
}

class ProfileAdded extends ProfileState {
  final Person self;
  final Settings settings;

  ProfileAdded({required this.self, required this.settings});

  @override
  List<Object?> get props => [self, settings];
}

class ProfileEmpty extends ProfileState {
  @override
  List<Object?> get props => [];
}
