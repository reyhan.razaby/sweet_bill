part of 'bill_form_payment_bloc.dart';

abstract class BillFormPaymentState extends Equatable {
  const BillFormPaymentState();

  @override
  List<Object> get props => [];
}

class BillFormPaymentEmpty extends BillFormPaymentState {}

class BillFormPaymentLoading extends BillFormPaymentState {}

class BillFormPaymentLoaded extends BillFormPaymentState {
  final List<MemberPayment> payments;
  final bool isMultiplePayers;
  final double expectedTotalPayment;
  final int focusedPaymentIndex;

  const BillFormPaymentLoaded({
    required this.payments,
    required this.isMultiplePayers,
    required this.expectedTotalPayment,
    this.focusedPaymentIndex = -1,
  });

  double get totalPayment =>
      payments.map((e) => e.amount).fold(0, (a, b) => a + b);

  double get remainingTotalPayment => expectedTotalPayment - totalPayment;

  bool get isInvalidFocusedPaymentIndex =>
      focusedPaymentIndex < 0 || focusedPaymentIndex >= payments.length;

  MemberPayment? get focusedMemberPayment =>
      isInvalidFocusedPaymentIndex ? null : payments[focusedPaymentIndex];

  bool get isAdjustable =>
      focusedMemberPayment != null &&
      focusedMemberPayment!.amount + remainingTotalPayment >= 0;

  TotalPaymentStatus get totalPaymentStatus {
    double actual = MemberPayment.sumAmount(payments);
    if (actual == expectedTotalPayment) {
      return TotalPaymentStatus.exact;
    } else if (actual > expectedTotalPayment) {
      return TotalPaymentStatus.over;
    } else {
      return TotalPaymentStatus.under;
    }
  }

  @override
  List<Object> get props =>
      [payments, isMultiplePayers, expectedTotalPayment, focusedPaymentIndex];

  BillFormPaymentLoaded copyWith({
    List<MemberPayment>? payments,
    bool? isMultiplePayers,
    double? expectedTotalPayment,
    int? focusedPaymentIndex,
  }) {
    return BillFormPaymentLoaded(
      payments: payments ?? this.payments,
      isMultiplePayers: isMultiplePayers ?? this.isMultiplePayers,
      expectedTotalPayment: expectedTotalPayment ?? this.expectedTotalPayment,
      focusedPaymentIndex: focusedPaymentIndex ?? this.focusedPaymentIndex,
    );
  }
}

class MemberPayment extends Equatable {
  final Person person;
  final double amount;
  final bool isPayer;

  const MemberPayment({
    required this.person,
    required this.amount,
    this.isPayer = false,
  });

  static double sumAmount(List<MemberPayment> payments) {
    return payments.map((e) => e.amount).fold(0, (a, b) => a + b);
  }

  @override
  List<Object?> get props => [person, amount, isPayer];

  MemberPayment copyWith({
    Person? person,
    double? amount,
    bool? isPayer,
  }) {
    return MemberPayment(
      person: person ?? this.person,
      amount: amount ?? this.amount,
      isPayer: isPayer ?? this.isPayer,
    );
  }
}

enum TotalPaymentStatus { over, under, exact }

extension TotalPaymentStatusExtension on TotalPaymentStatus {
  bool get isExact => this == TotalPaymentStatus.exact;

  bool get isOver => this == TotalPaymentStatus.over;

  bool get isUnder => this == TotalPaymentStatus.under;
}
