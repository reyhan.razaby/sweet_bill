import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';

part 'bill_form_payment_event.dart';

part 'bill_form_payment_state.dart';

class BillFormPaymentBloc
    extends Bloc<BillFormPaymentEvent, BillFormPaymentState> {
  final BillFormBloc _billFormBloc;

  BillFormPaymentBloc({
    required BillFormBloc billFormBloc,
  })  : _billFormBloc = billFormBloc,
        super(BillFormPaymentEmpty()) {
    on<LoadPaymentInitialValues>(_onLoadInitialValues);
    on<ToggleMultiplePayers>(_onToggleMultiplePayers);
    on<SelectPayer>(_onSelectPayer);
    on<ChangeAmount>(_onChangeAmount);
    on<SavePayment>(_onSavePayment);
    on<AdjustFocusedPaymentToExact>(_onAdjustFocusedPaymentToExact);
    on<FocusAmountInput>(_onFocusAmountInput);
  }

  void _onToggleMultiplePayers(ToggleMultiplePayers event, emit) {
    BillFormPaymentState currentState = state;
    if (currentState is! BillFormPaymentLoaded) {
      return;
    }

    bool isMultiplePayers = !currentState.isMultiplePayers;

    if (isMultiplePayers) {
      _switchToMultiplePayers(currentState, emit);
    } else {
      _switchToSinglePayer(currentState, emit);
    }
  }

  void _switchToSinglePayer(BillFormPaymentLoaded currentState, emit) {
    var billFormState = _billFormBloc.state;

    List<MemberPayment> memberPayments = currentState.payments;
    BillPersonalPayment? currentPayer = !billFormState.isMultiplePayers &&
            billFormState.bill.personalPayments.isNotEmpty
        ? billFormState.bill.personalPayments[0]
        : null;

    List<MemberPayment>? newPayments;

    // Find the most as new payer
    MemberPayment? theMost;
    double maxAmount = 0;
    for (MemberPayment mp in memberPayments) {
      if (mp.amount > maxAmount) {
        maxAmount = mp.amount;
        theMost = mp;
      }
    }

    newPayments = memberPayments.map((mp) {
      if (theMost != null) {
        // If the most is found
        if (mp == theMost) {
          return theMost.copyWith(
              isPayer: true, amount: currentState.expectedTotalPayment);
        }
        return mp.copyWith(isPayer: false, amount: 0);
      } else {
        // If the most is not found
        if (currentPayer != null && currentPayer.personId == mp.person.id) {
          return mp.copyWith(
              isPayer: true, amount: currentState.expectedTotalPayment);
        }
        return mp.copyWith(isPayer: false, amount: 0);
      }
    }).toList();

    emit(currentState.copyWith(
      isMultiplePayers: false,
      payments: newPayments,
    ));
  }

  void _switchToMultiplePayers(BillFormPaymentLoaded currentState, emit) {
    emit(currentState.copyWith(
      isMultiplePayers: true,
      payments:
          currentState.payments.map((e) => e.copyWith(isPayer: false)).toList(),
    ));
  }

  void _onChangeAmount(ChangeAmount event, emit) {
    BillFormPaymentState currentState = state;
    if (currentState is! BillFormPaymentLoaded) {
      return;
    }
    List<MemberPayment> memberPayments = List.of(currentState.payments);

    MemberPayment changed = memberPayments[event.index];
    memberPayments[event.index] = changed.copyWith(
      amount: event.amount,
    );

    emit(currentState.copyWith(payments: memberPayments));
  }

  void _onSelectPayer(SelectPayer event, emit) {
    BillFormPaymentState currentState = state;
    if (currentState is! BillFormPaymentLoaded) {
      return;
    }

    List<MemberPayment> memberPayments = List.of(currentState.payments);

    // Remove old payer
    int oldPayerIdx = memberPayments.indexWhere((e) => e.isPayer);
    if (oldPayerIdx > -1) {
      MemberPayment oldPayer = memberPayments[oldPayerIdx];
      memberPayments[oldPayerIdx] = oldPayer.copyWith(
        isPayer: false,
        amount: 0,
      );
    }

    // Assign new payer
    MemberPayment newPayer = memberPayments[event.index];
    memberPayments[event.index] = newPayer.copyWith(
      isPayer: true,
      amount: currentState.expectedTotalPayment,
    );

    emit(currentState.copyWith(payments: memberPayments));
  }

  void _onLoadInitialValues(LoadPaymentInitialValues event, emit) {
    // Group payments by personId
    Map<String, BillPersonalPayment> personalPayments = {
      for (var payment in _billFormBloc.state.bill.personalPayments)
        payment.personId: payment
    };

    double expectedTotalPayment = _billFormBloc.state.totalCost;

    // Build payments from members
    bool isPayerFound = false;
    List<MemberPayment> payments = _billFormBloc.members.map((member) {
      BillPersonalPayment? payment = personalPayments[member.id];

      bool isPayer = false;
      if (!_billFormBloc.state.isMultiplePayers) {
        isPayer = !isPayerFound &&
            payment != null &&
            payment.amount == expectedTotalPayment;
        isPayerFound = isPayer;
      }

      return MemberPayment(
        person: member,
        amount: payment != null ? payment.amount : 0,
        isPayer: isPayer,
      );
    }).toList()
      ..sort((a, b) =>
          StringUtils.compareAlphabetically(a.person.name, b.person.name));

    bool isMultiplePayers = _billFormBloc.state.isMultiplePayers ||
        payments.where((e) => e.amount > 0).length > 1;
    emit(BillFormPaymentLoaded(
      payments: payments,
      isMultiplePayers: isMultiplePayers,
      expectedTotalPayment: expectedTotalPayment,
    ));
  }

  void _onFocusAmountInput(FocusAmountInput event, emit) {
    BillFormPaymentState currentState = state;
    if (currentState is! BillFormPaymentLoaded) {
      return;
    }

    emit(currentState.copyWith(focusedPaymentIndex: event.index));
  }

  void _onAdjustFocusedPaymentToExact(AdjustFocusedPaymentToExact event, emit) {
    BillFormPaymentState currentState = state;
    if (currentState is! BillFormPaymentLoaded) {
      return;
    }

    MemberPayment? focusedPayment = currentState.focusedMemberPayment;
    if (focusedPayment == null) {
      return;
    }

    double newAmount =
        focusedPayment.amount + currentState.remainingTotalPayment;
    if (newAmount < 0) {
      return;
    }

    List<MemberPayment> newMemberPayments = List.of(currentState.payments);
    newMemberPayments[currentState.focusedPaymentIndex] =
        focusedPayment.copyWith(amount: newAmount);
    emit(currentState.copyWith(payments: newMemberPayments));
  }

  void _onSavePayment(SavePayment event, emit) {
    BillFormPaymentState currentState = state;
    if (currentState is! BillFormPaymentLoaded) {
      return;
    }

    List<BillPersonalPayment> newPersonalPayments = currentState.payments
        .where((e) => e.amount > 0)
        .map((e) => BillPersonalPayment(
              personId: e.person.id,
              amount: e.amount,
            ))
        .toList();

    if (!currentState.isMultiplePayers) {
      // Auto-adjust member payments
      if (newPersonalPayments.isEmpty) {
        MemberPayment? selected =
            currentState.payments.firstWhereOrNull((e) => e.isPayer);
        if (selected != null) {
          newPersonalPayments.add(BillPersonalPayment(
              personId: selected.person.id, amount: selected.amount));
        }
      }
    }

    _billFormBloc.add(ChangeForm(
      payments: newPersonalPayments,
      isMultiplePayers: currentState.isMultiplePayers,
    ));
  }
}
