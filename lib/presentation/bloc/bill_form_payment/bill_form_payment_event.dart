part of 'bill_form_payment_bloc.dart';

abstract class BillFormPaymentEvent extends Equatable {
  const BillFormPaymentEvent();

  @override
  List<Object?> get props => [];
}

class LoadPaymentInitialValues extends BillFormPaymentEvent {}

class ToggleMultiplePayers extends BillFormPaymentEvent {}

class FocusAmountInput extends BillFormPaymentEvent {
  final int index;

  const FocusAmountInput({
    required this.index,
  });

  @override
  List<Object?> get props => [index];
}

class AdjustFocusedPaymentToExact extends BillFormPaymentEvent {}

class SelectPayer extends BillFormPaymentEvent {
  final int index;

  @override
  List<Object?> get props => [index];

  const SelectPayer({
    required this.index,
  });
}

class ChangeAmount extends BillFormPaymentEvent {
  final int index;
  final double amount;

  const ChangeAmount({
    required this.index,
    required this.amount,
  });

  @override
  List<Object?> get props => [index, amount];
}

class SavePayment extends BillFormPaymentEvent {}
