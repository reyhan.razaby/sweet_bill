part of 'nav_menu_cubit.dart';

@immutable
abstract class NavMenuState extends Equatable {
  final IconData icon;
  final String label;

  const NavMenuState({
    required this.icon,
    required this.label,
  });

  @override
  List<Object> get props => [icon, label];
}

class NavMenuTransactions extends NavMenuState {
  const NavMenuTransactions()
      : super(icon: Icons.account_balance_wallet, label: 'Transactions');
}

class NavMenuProfile extends NavMenuState {
  const NavMenuProfile() : super(icon: Icons.person, label: 'Profile');
}
