import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'nav_menu_state.dart';

class NavMenuCubit extends Cubit<NavMenuState> {
  NavMenuCubit() : super(const NavMenuTransactions()); // Initial active menu

  void selectMenuItem(NavMenuState state) {
    emit(state);
  }
}
