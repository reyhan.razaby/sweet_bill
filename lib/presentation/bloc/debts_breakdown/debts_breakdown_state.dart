part of 'debts_breakdown_cubit.dart';

abstract class DebtsBreakdownState extends Equatable {
  const DebtsBreakdownState();
}

class DebtsBreakdownLoading extends DebtsBreakdownState {
  @override
  List<Object> get props => [];
}

class DebtsBreakdownLoaded extends DebtsBreakdownState {
  final DebtsBreakdown debtsBreakdown;

  const DebtsBreakdownLoaded({
    required this.debtsBreakdown,
  });

  @override
  List<Object> get props => [debtsBreakdown];
}

class DebtsBreakdownError extends DebtsBreakdownState {
  @override
  List<Object> get props => [];
}
