import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_breakdown/get_debts_breakdown_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_breakdown/get_debts_breakdown_use_case.dart';

import '../../../domain/entities/debt/debts_breakdown.dart';

part 'debts_breakdown_state.dart';

class DebtsBreakdownCubit extends Cubit<DebtsBreakdownState> {
  final GetDebtsBreakdownUseCase _getDebtsBreakdownUseCase;

  DebtsBreakdownCubit({
    required GetDebtsBreakdownUseCase getDebtsBreakdownUseCase,
  })  : _getDebtsBreakdownUseCase = getDebtsBreakdownUseCase,
        super(DebtsBreakdownLoading());

  Future<void> loadDebtsBreakdown(String eventGroupId) async {
    GetDebtsBreakdownParam param =
        GetDebtsBreakdownParam(eventGroupId: eventGroupId);
    final res = await _getDebtsBreakdownUseCase(param);
    if (res.isLeft()) {
      emit(DebtsBreakdownError());
      return;
    }

    emit(DebtsBreakdownLoaded(debtsBreakdown: res.asRight()));
  }
}
