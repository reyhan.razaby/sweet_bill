part of 'bill_form_bloc.dart';

abstract class BillFormEvent extends Equatable {
  const BillFormEvent();

  @override
  List<Object?> get props => [];
}

class LoadInitialValues extends BillFormEvent {
  final String? eventGroupId;
  final Bill? bill;

  const LoadInitialValues({
    this.eventGroupId,
    this.bill,
  });
}

class RegisterNewMember extends BillFormEvent {
  final Person newMember;
  final Function(Person newMember)? onRegistered;

  const RegisterNewMember({
    required this.newMember,
    this.onRegistered,
  });

  @override
  List<Object?> get props => [newMember, onRegistered];
}

class ChangeForm extends BillFormEvent {
  final String? title;
  final BillIcons? icon;
  final DateTime? transactionTime;
  final List<BillPersonalCost>? costs;
  final List<BillPersonalPayment>? payments;
  final SplitStrategies? splitStrategy;
  final bool? isAutoAdjustTax;
  final double? totalCost;
  final bool? isMultiplePayers;

  const ChangeForm({
    this.title,
    this.icon,
    this.transactionTime,
    this.costs,
    this.payments,
    this.splitStrategy,
    this.isAutoAdjustTax,
    this.totalCost,
    this.isMultiplePayers,
  });

  @override
  List<Object?> get props => [
        title,
        icon,
        transactionTime,
        costs,
        payments,
        splitStrategy,
        isAutoAdjustTax,
        totalCost,
        isMultiplePayers,
      ];
}

class SaveBill extends BillFormEvent {}
