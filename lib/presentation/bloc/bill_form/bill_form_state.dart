part of 'bill_form_bloc.dart';

abstract class BillFormState extends Equatable {
  final Bill bill;
  final double totalCost;
  final bool isMultiplePayers;
  final double remainingCost;
  final double remainingPayment;

  const BillFormState({
    required this.bill,
    required this.totalCost,
    this.isMultiplePayers = true,
    this.remainingCost = 0,
    this.remainingPayment = 0,
  });

  bool isValid() {
    return remainingCost == 0 && remainingPayment == 0;
  }

  @override
  List<Object> get props => [
        bill,
        isMultiplePayers,
        totalCost,
        remainingCost,
        remainingPayment,
      ];
}

class BillFormLoaded extends BillFormState {
  const BillFormLoaded({
    required super.bill,
    required super.totalCost,
    super.isMultiplePayers,
    super.remainingCost,
    super.remainingPayment,
  });

  BillFormLoaded.copyWithState(
    BillFormState state, {
    Bill? bill,
    double? totalCost,
    bool? isMultiplePayers,
    double? remainingCost,
    double? remainingPayment,
  }) : super(
          bill: bill ?? state.bill,
          totalCost: totalCost ?? state.totalCost,
          isMultiplePayers: isMultiplePayers ?? state.isMultiplePayers,
          remainingCost: remainingCost ?? state.remainingCost,
          remainingPayment: remainingPayment ?? state.remainingPayment,
        );
}

class BillFormLoading extends BillFormState {
  BillFormLoading({required BillFormState currentState})
      : super(
          bill: currentState.bill,
          totalCost: currentState.totalCost,
          isMultiplePayers: currentState.isMultiplePayers,
          remainingCost: currentState.remainingCost,
          remainingPayment: currentState.remainingPayment,
        );
}

class BillFormError extends BillFormState {
  BillFormError({required BillFormState currentState})
      : super(
          bill: currentState.bill,
          totalCost: currentState.totalCost,
          isMultiplePayers: currentState.isMultiplePayers,
          remainingCost: currentState.remainingCost,
          remainingPayment: currentState.remainingPayment,
        );
}

class BillFormEmpty extends BillFormState {
  BillFormEmpty({Bill? bill})
      : super(
          bill: bill ?? Bill.initial(),
          totalCost: 0,
        );
}

class BillFormSaved extends BillFormState {
  const BillFormSaved({
    required super.bill,
    required super.totalCost,
  });

  BillFormSaved.copyWithState(
    BillFormState state, {
    Bill? bill,
    double? totalCost,
    bool? isMultiplePayers,
    double? remainingCost,
    double? remainingPayment,
  }) : super(
          bill: bill ?? state.bill,
          totalCost: totalCost ?? state.totalCost,
          isMultiplePayers: isMultiplePayers ?? state.isMultiplePayers,
          remainingCost: remainingCost ?? state.remainingCost,
          remainingPayment: remainingPayment ?? state.remainingPayment,
        );
}
