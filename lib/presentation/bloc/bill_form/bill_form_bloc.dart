import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_use_case.dart';

import '../../../../../core/constants/bill_icons.dart';
import '../../../../../domain/entities/bill/bill.dart';
import '../../../../../domain/entities/person/person.dart';
import '../../../core/constants/split_strategies.dart';
import '../../../core/utils/number_utils.dart';
import '../../../domain/use_cases/event_group/get_members/get_members_param.dart';
import '../../../domain/use_cases/event_group/get_members/get_members_use_case.dart';
import '../../../domain/use_cases/event_group/register_event_group_members/register_event_group_members_param.dart';
import '../../../domain/use_cases/event_group/register_event_group_members/register_event_group_members_use_case.dart';
import '../profile/profile_cubit.dart';

part 'bill_form_event.dart';
part 'bill_form_state.dart';

class BillFormBloc extends Bloc<BillFormEvent, BillFormState> {
  final GetMembersUseCase _getMembersUseCase;
  final CreateBillUseCase _createBillUseCase;
  final UpdateBillUseCase _updateBillUseCase;
  final RegisterEventGroupMembersUseCase _registerEventGroupMembersUseCase;
  final ProfileCubit _profileCubit;

  late List<Person> members;
  late BillFormLoaded? initialLoadedState;

  BillFormBloc({
    required GetMembersUseCase getMembersUseCase,
    required CreateBillUseCase createBillUseCase,
    required UpdateBillUseCase updateBillUseCase,
    required RegisterEventGroupMembersUseCase registerEventGroupMembersUseCase,
    required ProfileCubit profileCubit,
  })  : _getMembersUseCase = getMembersUseCase,
        _createBillUseCase = createBillUseCase,
        _updateBillUseCase = updateBillUseCase,
        _registerEventGroupMembersUseCase = registerEventGroupMembersUseCase,
        _profileCubit = profileCubit,
        super(BillFormEmpty()) {
    members = [];
    on<LoadInitialValues>(_onLoadInitialValues);
    on<SaveBill>(_onSaveBill);
    on<ChangeForm>(_onChangeForm);
    on<RegisterNewMember>(_onRegisterNewMember);
  }

  Future<void> _onLoadInitialValues(LoadInitialValues event, emit) async {
    emit(BillFormLoading(currentState: state));

    // TODO: handle null bill too
    final eventGroupId = event.eventGroupId ?? event.bill!.eventGroupId;
    final members = await getMembers(eventGroupId);
    _setMembers(members);

    if (event.bill == null) {
      _initWithNewBill(eventGroupId, emit);
    } else {
      _initWithExistingBill(event.bill!, emit);
    }
    BillFormState newState = state;
    if (newState is BillFormLoaded) {
      initialLoadedState = newState;
    }
  }

  void _setMembers(List<Person> members) {
    this.members = members;
    this
        .members
        .sort((a, b) => StringUtils.compareAlphabetically(a.name, b.name));
  }

  Future<List<Person>> getMembers(String eventGroupId) async {
    final membersEither = await _getMembersUseCase(
      GetMembersParam(eventGroupId: eventGroupId),
    );
    if (membersEither.isLeft()) {
      return [];
    }
    return List.of(membersEither.asRight());
  }

  Future<void> _initWithExistingBill(Bill bill, emit) async {
    emit(BillFormLoaded.copyWithState(
      state,
      bill: bill,
      totalCost: bill.totalCost,
      isMultiplePayers: bill.personalPayments.length > 1 ||
          bill.totalCost != bill.totalPayment,
    ));
  }

  Future<void> _initWithNewBill(String eventGroupId, emit) async {
    final profileState = _profileCubit.state;
    if (profileState is! ProfileAdded) {
      _profileCubit.clearProfile();
      emit(BillFormError(currentState: state));
      return;
    }
    Person self = profileState.self;
    Bill newBill = state.bill.copyWith(
      eventGroupId: eventGroupId,
      personalPayments: [BillPersonalPayment(personId: self.id, amount: 0)],
      personalCosts: members
          .map((m) =>
              BillPersonalCost(personId: m.id, cost: 0, additionalCost: 0))
          .toList(),
    );
    emit(BillFormLoaded.copyWithState(
      state,
      bill: newBill,
      isMultiplePayers: false,
    ));
  }

  Future<void> _onSaveBill(SaveBill event, emit) async {
    emit(BillFormLoading(currentState: state));

    if (state.bill.id == '') {
      // Create new bill
      CreateBillParam param = CreateBillParam(bill: state.bill);
      final res = await _createBillUseCase(param);
      res.fold(
        (_) => emit(BillFormError(currentState: state)),
        (id) {
          emit(BillFormSaved.copyWithState(
            state,
            bill: state.bill.copyWith(id: id),
          ));
        },
      );
    } else {
      // Update existing bill
      UpdateBillParam param = UpdateBillParam(bill: state.bill);
      final res = await _updateBillUseCase(param);
      res.fold(
        (_) => emit(BillFormError(currentState: state)),
        (_) => emit(BillFormSaved.copyWithState(state)),
      );
    }
  }

  Future<void> _onChangeForm(ChangeForm event, emit) async {
    BillFormState currentState = state;
    Bill currentBill = currentState.bill;

    String? newTitle = event.title;
    DateTime? newTransactionTime = event.transactionTime;
    BillIcons? newIcon = event.icon;
    double? newTotalCostTarget = event.totalCost;
    bool? newIsAutoAdjustTax = event.isAutoAdjustTax;
    List<BillPersonalCost>? newCosts = event.costs;
    List<BillPersonalPayment>? newPayments = event.payments;
    SplitStrategies? newSplitStrategy = event.splitStrategy;
    bool? newIsMultiplePayers = event.isMultiplePayers;
    double? newRemainingCost;
    double? newRemainingPayment;

    if (newTotalCostTarget != null) {
      if (currentBill.splitStrategy == SplitStrategies.equally) {
        newCosts = _divideEvenly(currentBill.personalCosts, newTotalCostTarget);
      }

      // Auto adjust payment (for single payer only)
      bool isMultiplePayers =
          newIsMultiplePayers ?? currentState.isMultiplePayers;
      if (!isMultiplePayers && currentBill.personalPayments.length == 1) {
        BillPersonalPayment payment = currentBill.personalPayments[0];
        if (payment.amount == currentState.totalCost) {
          newPayments = [payment.copyWith(amount: newTotalCostTarget)];
        }
      }
    }

    // Check remaining cost & payment
    double totalCostTarget = newTotalCostTarget ?? currentState.totalCost;

    List<BillPersonalCost> costs = newCosts ?? currentBill.personalCosts;
    newRemainingCost = totalCostTarget - BillPersonalCost.sumTotalCost(costs);
    List<BillPersonalPayment> payments =
        newPayments ?? currentBill.personalPayments;
    newRemainingPayment =
        totalCostTarget - BillPersonalPayment.sumAmount(payments);

    emit(BillFormLoaded.copyWithState(
      state,
      bill: currentBill.copyWith(
        title: newTitle,
        icon: newIcon,
        transactionTime: newTransactionTime,
        personalCosts: newCosts,
        personalPayments: newPayments,
        splitStrategy: newSplitStrategy,
        isAutoAdjustTax: newIsAutoAdjustTax,
      ),
      totalCost: newTotalCostTarget,
      isMultiplePayers: newIsMultiplePayers,
      remainingCost: newRemainingCost,
      remainingPayment: newRemainingPayment,
    ));
  }

  Future<void> _onRegisterNewMember(RegisterNewMember event, emit) async {
    BillFormState currentState = state;
    if (currentState is! BillFormLoaded) {
      emit(BillFormLoaded.copyWithState(state));
      return;
    }
    emit(BillFormLoading(currentState: state));

    final param = RegisterEventGroupMembersParam(
      eventGroupId: currentState.bill.eventGroupId,
      members: [event.newMember],
    );
    final either = await _registerEventGroupMembersUseCase(param);
    if (either.isLeft()) {
      emit(BillFormLoaded.copyWithState(state));
      return;
    }

    if (event.onRegistered != null && either.asRight().isNotEmpty) {
      Person registeredMember = either.asRight()[0];
      _setMembers(List.of(members)..add(registeredMember));
      event.onRegistered!(registeredMember);
    }
    emit(BillFormLoaded.copyWithState(state));
  }

  List<BillPersonalCost> _divideEvenly(
      List<BillPersonalCost> costs, double totalCost) {
    if (costs.isEmpty) {
      return costs;
    }

    int maxFractionDigits =
        (_profileCubit.state as ProfileAdded).settings.maxFractionDigits;
    List<double> amountPerPerson =
        NumberUtils.divideEvenly(totalCost, costs.length, maxFractionDigits);

    if (amountPerPerson.length != costs.length) {
      return costs;
    }

    List<BillPersonalCost> newCosts = [];
    for (int i = 0; i < costs.length; i++) {
      BillPersonalCost cost = costs[i];
      double newAmount = amountPerPerson[i];

      newCosts.add(cost.copyWith(cost: newAmount));
    }
    return newCosts;
  }
}
