class TextComponents {
  final String wholePart;
  final String fractionalPart;
  final int decimalSeparatorCount;

  const TextComponents({
    this.wholePart = '',
    this.fractionalPart = '',
    this.decimalSeparatorCount = 0,
  });

  TextComponents copyWith({
    String? wholePart,
    String? fractionalPart,
    int? decimalSeparatorCount,
  }) {
    return TextComponents(
      wholePart: wholePart ?? this.wholePart,
      fractionalPart: fractionalPart ?? this.fractionalPart,
      decimalSeparatorCount:
          decimalSeparatorCount ?? this.decimalSeparatorCount,
    );
  }

  String get joined {
    if (decimalSeparatorCount == 0) {
      return wholePart;
    } else if (decimalSeparatorCount == 1) {
      return '$wholePart.$fractionalPart';
    }
    return '';
  }
}
