part of 'calculator_bloc.dart';

abstract class CalculatorState extends Equatable {
  const CalculatorState();

  bool get isClosed => this is CalculatorClosed;

  bool get isOpened => this is CalculatorOpened;

  bool get isOpening => this is CalculatorOpening;

  bool get isClosing => this is CalculatorClosing;
}

class CalculatorClosing extends CalculatorState {
  @override
  List<Object?> get props => [];
}

class CalculatorClosed extends CalculatorState {
  @override
  List<Object?> get props => [];
}

class CalculatorOpening extends CalculatorState {
  final TextEditingController fieldController;
  final double value;
  final ValueChanged<double>? onValueChanged;
  final VoidCallback? onOpened;

  const CalculatorOpening({
    required this.fieldController,
    required this.value,
    this.onValueChanged,
    this.onOpened,
  });

  CalculatorOpened toOpened() {
    return CalculatorOpened(
      fieldController: fieldController,
      value: value,
      onValueChanged: onValueChanged,
    );
  }

  @override
  List<Object?> get props => [fieldController, value];
}

class CalculatorOpened extends CalculatorState {
  final TextEditingController fieldController;
  final double value;
  final bool hasOperator;
  final ValueChanged<double>? onValueChanged;
  final double? expressionResult;

  const CalculatorOpened({
    required this.fieldController,
    this.value = 0,
    this.hasOperator = false,
    this.expressionResult,
    this.onValueChanged,
  });

  double get expressionResultOrValue => expressionResult ?? value;

  void editControllerText(String newText, int startSelection,
      [int? endSelection]) {
    fieldController.text = newText;
    fieldController.selection = fieldController.selection.copyWith(
      baseOffset: newText.length,
      extentOffset: newText.length,
    );
  }

  @override
  List<Object?> get props =>
      [fieldController, value, expressionResult, hasOperator];

  CalculatorOpened copyWith({
    TextEditingController? fieldController,
    double? value,
    ValueChanged<double>? onValueChanged,
    double? expressionResult,
    bool? hasOperator,
    bool setExpressionResultToNull = false,
  }) {
    return CalculatorOpened(
      fieldController: fieldController ?? this.fieldController,
      value: value ?? this.value,
      hasOperator: value != null ? false : hasOperator ?? this.hasOperator,
      onValueChanged: onValueChanged ?? this.onValueChanged,
      expressionResult: setExpressionResultToNull
          ? null
          : (expressionResult ?? this.expressionResult),
    );
  }

  /// Changed if bare number inserted or valid expression evaluated
// double get value {
//   return 0.0;
// }

  /// If valid expression detected, then this property will give the expression
  /// result. Return null if no expression (just bare number) or invalid
  /// expression
// double? get expressionResult {
//   return 0.0;
// }
}
