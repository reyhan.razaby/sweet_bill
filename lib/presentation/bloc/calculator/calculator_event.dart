part of 'calculator_bloc.dart';

abstract class CalculatorEvent {
  const CalculatorEvent();
}

class OpenCalculator extends CalculatorEvent {
  final bool openImmediately;
  final TextEditingController fieldController;
  final VoidCallback? onOpened;
  final ValueChanged<double>? onValueChanged;
  final double? initialValue;

  const OpenCalculator({
    required this.fieldController,
    this.openImmediately = false,
    this.onOpened,
    this.onValueChanged,
    this.initialValue,
  });
}

class EndOpenCalculator extends CalculatorEvent {}

class CloseCalculator extends CalculatorEvent {}

class EndCloseCalculator extends CalculatorEvent {}

class ChangeValue extends CalculatorEvent {
  final double value;
  final bool notifyChangedValue;

  ChangeValue({
    required this.value,
    this.notifyChangedValue = false,
  });
}

class InputCalculator extends CalculatorEvent {
  const InputCalculator();
}

class InsertNumber extends InputCalculator {
  final String number;

  const InsertNumber({
    required this.number,
  });
}

class InsertOperator extends InputCalculator {
  final Operator operator;

  const InsertOperator({
    required this.operator,
  });
}

class Clear extends InputCalculator {}

class Backspace extends InputCalculator {}

class Evaluate extends InputCalculator {}
