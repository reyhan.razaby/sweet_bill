import 'package:sweet_bill/core/extensions/string.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/presentation/bloc/calculator/text_components.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';

/// Receive formatted text
/// Always return 2 elements:
/// Head (first) = text from 1st character until the last operator
/// Tail (last) = text after head
/// If no operator found, it will return empty list.
/// Negative sign is not counted as operator
List<String> splitUntilLastOp(String str) {
  if (str.isEmpty) {
    return [];
  }

  String pattern = '^.+(\\+|-|'
      '${Operator.multiply.text}|${Operator.divide.text}|${Operator.percent.text})';

  RegExp exp = RegExp(pattern);
  String leftSide;
  try {
    RegExpMatch? match = exp.firstMatch(str);
    leftSide = match![0]!;
  } catch (_) {
    return [];
  }

  String rightSide = str.substring(leftSide.length);
  return [leftSide, rightSide];
}

TextComponents extractComponents(String unformattedText) {
  int decimalSymbolCount = RegExp(r'\.').allMatches(unformattedText).length;
  if (decimalSymbolCount > 1) {
    // Return immediately for the invalid decimal symbol count
    return TextComponents(decimalSeparatorCount: decimalSymbolCount);
  } else if (decimalSymbolCount == 0) {
    return TextComponents(wholePart: unformattedText);
  }

  List<String> splitRes =
      unformattedText.split('.').where((e) => e.isNotEmpty).toList();

  String wholePart = '';
  String fractionalPart = '';
  if (splitRes.length == 2) {
    wholePart = splitRes[0];
    fractionalPart = splitRes[1];
  } else if (splitRes.length == 1) {
    String part = splitRes[0];
    if (unformattedText.firstChar == '.') {
      fractionalPart = part;
    } else if (unformattedText.lastChar == '.') {
      wholePart = part;
    }
  }

  return TextComponents(
    decimalSeparatorCount: decimalSymbolCount,
    wholePart: wholePart,
    fractionalPart: fractionalPart,
  );
}

String thousandSeparated(double amount,
    {required int maxFractionDigits, required String locale}) {
  return NumberUtils.getCurrency(
    amount,
    fractionDigits: maxFractionDigits,
    locale: locale,
    omitTrailingDecimalZero: true,
  );
}
