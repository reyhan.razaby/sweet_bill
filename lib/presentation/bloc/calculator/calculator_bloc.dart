import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/backspace_handler.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/clear_handler.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/evaluate_handler.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/insert_number_handler.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/insert_operator_handler.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

import 'calculator_utils.dart';

part 'calculator_event.dart';

part 'calculator_state.dart';

class CalculatorBloc extends Bloc<CalculatorEvent, CalculatorState> {
  late final Settings _settings;
  late final InsertNumberHandler _insertValueHandler;
  late final InsertOperatorHandler _insertOperatorHandler;
  late final ClearHandler _clearHandler;
  late final BackspaceHandler _backspaceHandler;
  late final EvaluateHandler _evaluateHandler;

  CalculatorBloc({
    required ProfileCubit profileCubit,
    required EvaluateExpressionUseCase evaluateExpressionUseCase,
    required ToastShower toastShower,
  }) : super(CalculatorClosed()) {
    ProfileState profileState = profileCubit.state;
    assert(profileState is ProfileAdded);
    _settings = (profileState as ProfileAdded).settings;

    _insertValueHandler = InsertNumberHandler(
        settings: _settings,
        toastShower: toastShower,
        evaluateExpressionUseCase: evaluateExpressionUseCase);
    _insertOperatorHandler = InsertOperatorHandler(
        settings: _settings,
        toastShower: toastShower,
        evaluateExpressionUseCase: evaluateExpressionUseCase);
    _clearHandler = ClearHandler(
        settings: _settings,
        toastShower: toastShower,
        evaluateExpressionUseCase: evaluateExpressionUseCase);
    _backspaceHandler = BackspaceHandler(
        settings: _settings,
        toastShower: toastShower,
        evaluateExpressionUseCase: evaluateExpressionUseCase);
    _evaluateHandler = EvaluateHandler(
        settings: _settings,
        toastShower: toastShower,
        evaluateExpressionUseCase: evaluateExpressionUseCase);

    on<OpenCalculator>(_onOpenCalculator);
    on<CloseCalculator>(_onCloseCalculator);
    on<EndOpenCalculator>(_onEndOpenCalculator);
    on<EndCloseCalculator>(_onEndCloseCalculator);
    on<ChangeValue>(_onChangeValue);
    on<InsertNumber>(_onInsertValue);
    on<InsertOperator>(_onInsertOperator);
    on<Clear>(_onClear);
    on<Backspace>(_onBackspace);
    on<Evaluate>(_onEvaluate);
  }

  void _onOpenCalculator(OpenCalculator event, emit) {
    if (state.isOpened) {
      emit(CalculatorOpened(
        fieldController: event.fieldController,
        value: event.initialValue ?? 0,
        onValueChanged: event.onValueChanged,
      ));
      return;
    }

    final openingState = CalculatorOpening(
      fieldController: event.fieldController,
      value: event.initialValue ?? 0,
      onOpened: event.onOpened,
      onValueChanged: event.onValueChanged,
    );
    emit(openingState);

    if (event.openImmediately) {
      emit(openingState.toOpened());
    }
  }

  void _onEndOpenCalculator(EndOpenCalculator event, emit) {
    final currentState = state;
    if (currentState is CalculatorOpening) {
      emit(CalculatorOpened(
        fieldController: currentState.fieldController,
        value: currentState.value,
        onValueChanged: currentState.onValueChanged,
      ));
      currentState.onOpened?.call();
    }
  }

  void _onCloseCalculator(CloseCalculator event, emit) {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    if (currentState.hasOperator) {
      double newValue = currentState.expressionResultOrValue;
      currentState.onValueChanged?.call(newValue);
      String newText = getFormattedText(newValue);
      currentState.editControllerText(newText, newText.length);
    }
    emit(CalculatorClosing());
  }

  void _onEndCloseCalculator(EndCloseCalculator event, emit) {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    emit(CalculatorClosed());
  }

  void _onChangeValue(ChangeValue event, emit) {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    String newText = getFormattedText(event.value);
    currentState.fieldController.text = newText;
    currentState.fieldController.selection =
        currentState.fieldController.selection.copyWith(
      baseOffset: newText.length,
      extentOffset: newText.length,
    );

    if (event.notifyChangedValue) {
      currentState.onValueChanged?.call(event.value);
    }
    emit(currentState.copyWith(
      value: event.value,
      setExpressionResultToNull: true,
    ));
  }

  void _onClear(Clear event, emit) {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    _clearHandler.handle(event, currentState, emit);
  }

  void _onBackspace(Backspace event, emit) {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    _backspaceHandler.handle(event, currentState, emit);
  }

  void _onEvaluate(Evaluate event, emit) {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    _evaluateHandler.handle(event, currentState, emit);
  }

  Future<void> _onInsertValue(InsertNumber event, emit) async {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    _insertValueHandler.handle(event, currentState, emit);
  }

  Future<void> _onInsertOperator(InsertOperator event, Emitter emit) async {
    final currentState = state;
    if (currentState is! CalculatorOpened) {
      return;
    }
    _insertOperatorHandler.handle(event, currentState, emit);
  }

  String getFormattedText(double value) {
    return thousandSeparated(
      value,
      maxFractionDigits: _settings.maxFractionDigits,
      locale: _settings.locale,
    );
  }

  String get decimalSeparator => _settings.decimalSeparator;

  int get maxFractionDigits => _settings.maxFractionDigits;
}
