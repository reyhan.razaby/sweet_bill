import 'package:dartz/dartz.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/extensions/string.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_param.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_utils.dart';
import 'package:sweet_bill/presentation/bloc/calculator/text_components.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

abstract class InputHandler<E extends InputCalculator> {
  final Settings settings;
  final EvaluateExpressionUseCase evaluateExpressionUseCase;
  final ToastShower toastShower;

  const InputHandler({
    required this.settings,
    required this.evaluateExpressionUseCase,
    required this.toastShower,
  });

  void handle(E input, CalculatorOpened state, Emitter emit);

  String get decimalSeparator => settings.decimalSeparator;

  String get thousandSeparator => settings.thousandSeparator;

  String get locale => settings.locale;

  int get maxFractionDigits => settings.maxFractionDigits;

  int get maxWholeDigits => settings.maxWholeDigits;

  void showErrorMessage(String? message) {
    toastShower.showToast(message ?? 'Invalid input');
  }

  String unFormatText(String formattedText) {
    return formattedText
        .replaceAll(thousandSeparator, '')
        .replaceAll(decimalSeparator, '.');
  }

  Future<Either<Failure, double>> evaluateExpression(String expression) async {
    final expParam = EvaluateExpressionParam(
      expression: expression,
      fractionDigits: maxFractionDigits,
    );
    return await evaluateExpressionUseCase(expParam);
  }

  double? toDouble(String input) {
    if (input == '.' || input.isEmpty) {
      return 0;
    }
    return double.tryParse(input);
  }

  Operator? getTrailingOperator(String text) {
    if (text.isNotEmpty) {
      String lastChar = text.lastChar;
      return Operator.fromText(lastChar);
    }
    return null;
  }

  String formatText(TextComponents component) {
    // Add thousand separator to the whole part only
    // Attach the fractional part
    String result = component.wholePart;
    if (result.isNotEmpty) {
      result = thousandSeparated(
        double.parse(result),
        maxFractionDigits: maxFractionDigits,
        locale: locale,
      );
    }
    if (component.decimalSeparatorCount == 1) {
      result += decimalSeparator;
    }
    if (component.fractionalPart.isNotEmpty) {
      result += component.fractionalPart;
    }
    return result;
  }
}
