import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_utils.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/input_handler.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';

class EvaluateHandler extends InputHandler<Evaluate> {
  EvaluateHandler({
    required super.settings,
    required super.toastShower,
    required super.evaluateExpressionUseCase,
  });

  @override
  Future<void> handle(
      Evaluate input, CalculatorOpened state, Emitter emit) async {
    TextEditingController fieldController = state.fieldController;
    TextSelection textSelection = fieldController.selection;

    String exp = fieldController.text;
    if (exp.isEmpty) {
      return;
    }
    String unformattedExp = unFormatText(exp);

    Operator? newTrailingOp = getTrailingOperator(unformattedExp);
    bool trailingNonPercent =
        newTrailingOp != null && newTrailingOp != Operator.percent;
    if (trailingNonPercent) {
      _notifyInvalidInput();
      return;
    } else if (newTrailingOp == Operator.percent && exp.length == 1) {
      _notifyInvalidInput();
      return;
    }

    final expResEither = await evaluateExpression(unformattedExp);
    double? newValue = expResEither.fold(
      (l) {
        showErrorMessage(l.message);
        return null;
      },
      (r) => r,
    );

    if (newValue == null) {
      return;
    }

    String newText = thousandSeparated(newValue,
        maxFractionDigits: maxFractionDigits, locale: locale);

    // ======= EMIT CHANGE & UI =======

    // New value detected
    if (newValue != state.value) {
      state.onValueChanged?.call(newValue);
    }

    // Update text field display
    fieldController.text = newText;
    fieldController.selection = textSelection.copyWith(
      baseOffset: newText.length,
      extentOffset: newText.length,
    );

    emit(state.copyWith(
      value: newValue,
      hasOperator: false,
      setExpressionResultToNull: true,
    ));
  }

  void _notifyInvalidInput() {
    showErrorMessage('Invalid input');
  }
}
