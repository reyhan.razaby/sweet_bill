import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/input_handler.dart';

class ClearHandler extends InputHandler<Clear> {
  ClearHandler({
    required super.settings,
    required super.toastShower,
    required super.evaluateExpressionUseCase,
  });

  @override
  Future<void> handle(
      Clear input, CalculatorOpened state, Emitter emit) async {
    state.fieldController.clear();
    double newValue = 0;

    if (newValue != state.value) {
      state.onValueChanged?.call(newValue);
    }

    emit(state.copyWith(
      value: newValue,
      setExpressionResultToNull: true,
    ));
  }
}
