import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_utils.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/input_handler.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';

class BackspaceHandler extends InputHandler<Backspace> {
  BackspaceHandler({
    required super.settings,
    required super.toastShower,
    required super.evaluateExpressionUseCase,
  });

  @override
  Future<void> handle(
      Backspace input, CalculatorOpened state, Emitter emit) async {
    TextEditingController fieldController = state.fieldController;
    TextSelection textSelection = fieldController.selection;

    String oldText = fieldController.text;
    if (oldText.isEmpty) {
      return;
    }
    String afterDeleted = oldText.substring(0, oldText.length - 1);

    Operator? newTrailingOp = getTrailingOperator(afterDeleted);
    double? newExpressionResult;

    if (newTrailingOp != null) {
      if (newTrailingOp == Operator.percent) {
        final expResEither =
            await evaluateExpression(unFormatText(afterDeleted));
        newExpressionResult = expResEither.fold((l) => null, (r) => r);
      }

      // Update UI
      fieldController.text = afterDeleted;
      fieldController.selection = textSelection.copyWith(
        baseOffset: afterDeleted.length,
        extentOffset: afterDeleted.length,
      );

      emit(state.copyWith(
        expressionResult: newExpressionResult,
        setExpressionResultToNull: newExpressionResult == null,
      ));
      return;
    }

    List<String> untilAndAfterOp = splitUntilLastOp(afterDeleted);
    bool hasOperator = untilAndAfterOp.length > 1;
    String head = hasOperator ? untilAndAfterOp.first : '';
    String tail = hasOperator ? untilAndAfterOp.last : '';

    double? newValue;
    String newFormattedText;
    if (hasOperator) {
      final expResEither = await evaluateExpression(unFormatText(afterDeleted));
      newExpressionResult = expResEither.fold((l) => null, (r) => r);

      String newTail = formatText(extractComponents(unFormatText(tail)));
      newFormattedText = head + newTail;
    } else {
      String newUnformattedText = unFormatText(afterDeleted);
      newValue = toDouble(newUnformattedText);
      newFormattedText = formatText(extractComponents(newUnformattedText));
    }

    // New value detected
    if (newValue != null && newValue != state.value) {
      state.onValueChanged?.call(newValue);
    }

    // Update text field display
    fieldController.text = newFormattedText;
    fieldController.selection = textSelection.copyWith(
      baseOffset: newFormattedText.length,
      extentOffset: newFormattedText.length,
    );

    emit(state.copyWith(
      value: newValue,
      hasOperator: hasOperator,
      expressionResult: newExpressionResult,
      setExpressionResultToNull: newExpressionResult == null,
    ));
  }
}
