import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_utils.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/input_handler.dart';
import 'package:sweet_bill/presentation/bloc/calculator/text_components.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';

class InsertNumberHandler extends InputHandler<InsertNumber> {
  InsertNumberHandler({
    required super.settings,
    required super.toastShower,
    required super.evaluateExpressionUseCase,
  });

  @override
  Future<void> handle(
      InsertNumber input, CalculatorOpened state, Emitter emit) async {
    TextEditingController fieldController = state.fieldController;
    TextSelection textSelection = fieldController.selection;
    String incomingNumber = input.number;

    String oldFormattedText = fieldController.text;
    Operator? trailingOp = getTrailingOperator(oldFormattedText);
    if (incomingNumber == decimalSeparator) {
      // Prevent decimal separator after trailing operator
      if (trailingOp != null) {
        return;
      }
      // Add zero before incoming decimal separator
      if (oldFormattedText.isEmpty) {
        oldFormattedText = '0';
      }
    }
    // Add "multiply" after trailing percent
    if (trailingOp == Operator.percent) {
      oldFormattedText += Operator.multiply.text;
    }

    // Split head & tail from old formatted text
    List<String> untilAndAfterOp = splitUntilLastOp(oldFormattedText);
    String head = ''; // Until the last operator
    String tail = ''; // After head
    bool hasOperator = untilAndAfterOp.length > 1;

    String newFormattedDirtyText;
    if (hasOperator) {
      head = untilAndAfterOp.first;
      tail = untilAndAfterOp.last;

      // Get tail only. Head will be re-attached later
      newFormattedDirtyText =
          tail.replaceRange(tail.length, tail.length, incomingNumber);
    } else {
      newFormattedDirtyText = oldFormattedText.replaceRange(
          oldFormattedText.length, oldFormattedText.length, incomingNumber);
    }
    String newUnformattedText = unFormatText(newFormattedDirtyText);

    // Check the whole digit length
    TextComponents components = extractComponents(newUnformattedText);
    if (components.wholePart.length > maxWholeDigits) {
      showErrorMessage('Max digits: $maxWholeDigits');
      return;
    }
    // Check multiple decimal separator
    if (components.decimalSeparatorCount > 1) {
      showErrorMessage('More than 1 decimal separator found');
      return;
    }
    // Trim fractional part if exceeded
    if (components.fractionalPart.length > maxFractionDigits) {
      String trimmed =
          components.fractionalPart.substring(0, maxFractionDigits);
      components = components.copyWith(fractionalPart: trimmed);
      newUnformattedText = components.joined;
      showErrorMessage('Max decimal digits: $maxFractionDigits');
    }

    String newFormattedText = formatText(components);
    double? newValue;
    double? newExpressionResult;
    if (hasOperator) {
      // Re-attach head + tail
      newUnformattedText = unFormatText(head) + newUnformattedText;
      newFormattedText = head + newFormattedText;

      final expResEither = await evaluateExpression(newUnformattedText);
      newExpressionResult = expResEither.fold((l) => null, (r) => r);
    } else {
      newValue = toDouble(newUnformattedText);
    }
    TextSelection newSelection = textSelection.copyWith(
      baseOffset: newFormattedText.length,
      extentOffset: newFormattedText.length,
    );

    // New value detected
    if (newValue != null && newValue != state.value) {
      state.onValueChanged?.call(newValue);
    }

    // Update text field display
    fieldController.text = newFormattedText;
    fieldController.selection = newSelection;

    emit(state.copyWith(
      value: newValue,
      hasOperator: hasOperator,
      expressionResult: newExpressionResult,
      setExpressionResultToNull: newExpressionResult == null,
    ));
  }
}
