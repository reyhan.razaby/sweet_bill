import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/input_handlers/input_handler.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';

class InsertOperatorHandler extends InputHandler<InsertOperator> {
  InsertOperatorHandler({
    required super.settings,
    required super.toastShower,
    required super.evaluateExpressionUseCase,
  });

  @override
  Future<void> handle(
      InsertOperator input, CalculatorOpened state, Emitter emit) async {
    TextEditingController fieldController = state.fieldController;
    TextSelection textSelection = fieldController.selection;
    Operator operator = input.operator;

    String oldText = fieldController.text;
    if (oldText.isEmpty) {
      emit(state.copyWith(hasOperator: false));
      return;
    }

    Operator? trailingOp = getTrailingOperator(oldText);
    // Replace the trailing operator (except percent)
    if (trailingOp != null && trailingOp != Operator.percent) {
      String afterReplaced = oldText.replaceRange(
          oldText.length - 1, oldText.length, operator.text);

      // Update UI
      fieldController.text = afterReplaced;
      fieldController.selection = textSelection.copyWith(
        baseOffset: afterReplaced.length,
        extentOffset: afterReplaced.length,
      );
      emit(state.copyWith(hasOperator: true));
      return;
    }

    String newFormattedText = oldText + operator.text;
    double? newValue;
    double? newExpressionResult;

    if (operator == Operator.percent) {
      String newUnformattedText = unFormatText(newFormattedText);
      final expResEither = await evaluateExpression(newUnformattedText);
      newExpressionResult = expResEither.fold((l) => null, (r) => r);
      newValue = toDouble(newUnformattedText);
    }

    // New value detected
    if (newValue != null && newValue != state.value) {
      state.onValueChanged?.call(newValue);
    }

    // Update text field display
    fieldController.text = newFormattedText;
    fieldController.selection = textSelection.copyWith(
      baseOffset: newFormattedText.length,
      extentOffset: newFormattedText.length,
    );

    emit(state.copyWith(
      value: newValue,
      hasOperator: true,
      expressionResult: newExpressionResult,
      setExpressionResultToNull: newExpressionResult == null,
    ));
  }
}
