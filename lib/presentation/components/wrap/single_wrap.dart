import 'package:flutter/material.dart';

class SingleWrap extends StatelessWidget {
  final Widget child;
  final WrapAlignment alignment;

  const SingleWrap({
    Key? key,
    required this.child,
    this.alignment = WrapAlignment.center,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.horizontal,
      alignment: alignment,
      children: [
        child,
      ],
    );
  }
}
