import 'package:flutter/cupertino.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/components/wrap/single_wrap.dart';

class DragBar extends StatelessWidget {
  final double height;
  final double width;
  final Color color;
  final EdgeInsetsGeometry margin;

  const DragBar({
    Key? key,
    this.height = 4,
    this.width = 32,
    this.color = ColorSet.grey,
    this.margin = const EdgeInsets.symmetric(vertical: 14),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleWrap(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: color,
        ),
        height: height,
        width: width,
        margin: margin,
      ),
    );
  }
}
