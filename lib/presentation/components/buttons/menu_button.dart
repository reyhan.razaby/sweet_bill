import 'package:flutter/material.dart';

import '../../../core/constants/color_set.dart';

class MenuButton extends StatelessWidget {
  final IconData? prefixIcon;
  final String text;
  final double? maxWidth;
  final MenuButtonSize size;
  final VoidCallback? onTap;
  final List<MenuButtonItem>? items;

  const MenuButton({
    Key? key,
    this.prefixIcon,
    required this.text,
    this.size = MenuButtonSize.medium,
    this.maxWidth,
    this.onTap,
    this.items,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double padding = size == MenuButtonSize.medium ? 8 : 6;
    final double fontSize = size == MenuButtonSize.medium ? 14 : 12;
    final double iconSize = size == MenuButtonSize.medium ? 18 : 14;
    final double textLeftPadding = size == MenuButtonSize.medium ? 6 : 4;
    final double borderWidth = size == MenuButtonSize.medium ? 3 : 2;

    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: PopupMenuButton(
        enabled: items != null && items!.isNotEmpty,
        itemBuilder: (_) =>
            items != null ? items!.map(_buildPopupMenuItem).toList() : [],
        child: Container(
          constraints: BoxConstraints(
            maxWidth: maxWidth ?? MediaQuery.of(context).size.width,
          ),
          padding: EdgeInsets.symmetric(
            vertical: padding * .2,
            horizontal: padding,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: ColorSet.light.withOpacity(.3),
              width: borderWidth,
            ),
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              if (prefixIcon != null)
                Icon(
                  prefixIcon,
                  color: ColorSet.white,
                  size: iconSize,
                ),
              Flexible(
                child: Padding(
                  padding: EdgeInsets.only(left: textLeftPadding),
                  child: Text(
                    text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      overflow: TextOverflow.ellipsis,
                      fontSize: fontSize,
                    ),
                  ),
                ),
              ),
              const Icon(
                Icons.keyboard_arrow_down,
                color: Colors.grey,
              ),
            ],
          ),
        ),
      ),
    );
  }

  PopupMenuItem _buildPopupMenuItem(MenuButtonItem item) {
    bool isText =
        item.text != null || (item.text == null && item.child == null);
    return PopupMenuItem(
      child: isText ? Text(item.text ?? '') : item.child,
      onTap: item.onSelect,
    );
  }
}

class MenuButtonItem {
  final String? text;
  final Widget? child;
  final VoidCallback onSelect;

  const MenuButtonItem({
    this.text,
    this.child,
    required this.onSelect,
  });
}

enum MenuButtonSize { small, medium }
