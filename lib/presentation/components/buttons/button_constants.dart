enum ButtonSize { large, medium, small }

enum ButtonType { contained, outlined }
