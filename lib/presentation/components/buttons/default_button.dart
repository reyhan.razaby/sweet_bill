import 'package:flutter/material.dart';
import 'package:sweet_bill/presentation/components/buttons/button_constants.dart';

class DefaultButton extends StatelessWidget {
  final String? label;
  final Widget? child;
  final VoidCallback? onPressed;
  final bool isDisabled;
  final ButtonSize size;
  final ButtonType type;
  final EdgeInsets? padding;
  final double? horizontalPadding;

  const DefaultButton({
    Key? key,
    this.label,
    required this.onPressed,
    this.child,
    this.isDisabled = false,
    this.size = ButtonSize.medium,
    this.type = ButtonType.contained,
    this.padding,
    this.horizontalPadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case ButtonType.contained:
        return ElevatedButton(
          style: ElevatedButton.styleFrom(
            padding: buildPadding(),
          ),
          child: buildChild(),
          onPressed: buildOnPressed(),
        );
      case ButtonType.outlined:
        return OutlinedButton(
          style: OutlinedButton.styleFrom(
            padding: buildPadding(),
          ),
          child: buildChild(),
          onPressed: buildOnPressed(),
        );
    }
  }

  EdgeInsets buildPadding() {
    return padding ??
        EdgeInsets.symmetric(
            vertical: buildVerticalPadding(),
            horizontal: buildHorizontalPadding());
  }

  VoidCallback? buildOnPressed() => isDisabled ? null : onPressed;

  Widget buildChild() {
    return child ??
        Text(
          label ?? '',
          style: TextStyle(fontSize: _buildFontSize()),
        );
  }

  double buildVerticalPadding() {
    switch (size) {
      case ButtonSize.large:
        return 14;
      case ButtonSize.medium:
        return 11;
      case ButtonSize.small:
        return 0;
    }
  }

  double buildHorizontalPadding() {
    if (horizontalPadding != null) {
      return horizontalPadding!;
    }
    switch (size) {
      case ButtonSize.large:
        return 28;
      case ButtonSize.medium:
        return 22;
      case ButtonSize.small:
        return 8;
    }
  }

  double _buildFontSize() {
    switch (size) {
      case ButtonSize.large:
        return 16;
      case ButtonSize.medium:
        return 14;
      case ButtonSize.small:
        return 11;
    }
  }
}
