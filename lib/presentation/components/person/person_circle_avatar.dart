import 'package:flutter/material.dart';

import '../../../core/constants/person_avatar.dart';

class PersonCircleAvatar extends StatelessWidget {
  final String emoji;
  final double radius;
  final double? emojiSize;

  const PersonCircleAvatar({
    Key? key,
    required this.emoji,
    this.radius = 24,
    this.emojiSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: PersonAvatar.getBackgroundColor(),
      radius: radius,
      child: Text(
        emoji,
        style: TextStyle(fontSize: emojiSize ?? (radius * .9)),
      ),
    );
  }
}
