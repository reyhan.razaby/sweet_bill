import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/components/person/person_circle_avatar.dart';

import '../../../domain/entities/person/person.dart';

class PersonListItem extends StatelessWidget {
  final Person person;
  final Widget? trailing;
  final double avatarBorderSize;
  final Color? avatarBorderColor;
  final double verticalPadding;
  final double horizontalPadding;
  final double avatarRadius;
  final double nameSize;
  final Color? nameColor;
  final Color? backgroundColor;
  final bool wrapWithCard;
  final GestureTapCallback? onTap;

  const PersonListItem({
    Key? key,
    required this.person,
    this.trailing,
    this.avatarBorderSize = 4,
    this.avatarBorderColor,
    this.verticalPadding = 6,
    this.horizontalPadding = 0,
    this.avatarRadius = 24,
    this.nameSize = 15,
    this.nameColor,
    this.backgroundColor,
    this.wrapWithCard = false,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final avatar = PersonCircleAvatar(
      emoji: person.avatar.emoji,
      radius: avatarRadius,
    );

    return InkWell(
      onTap: onTap,
      child: Ink(
        color:
            !wrapWithCard && backgroundColor != null ? backgroundColor : null,
        decoration: wrapWithCard
            ? const BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(8)),
                color: ColorSet.dark,
              )
            : null,
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: verticalPadding,
            horizontal: horizontalPadding,
          ),
          child: Row(
            children: [
              CircleAvatar(
                radius: avatar.radius + calculateAvatarBorderSize,
                backgroundColor: avatarBorderColor,
                child: avatar,
              ),
              const SizedBox(width: 12),
              Expanded(
                child: Text(
                  person.name,
                  style: TextStyle(
                    fontSize: nameSize,
                    color: nameColor,
                  ),
                ),
              ),
              if (trailing != null) trailing!,
            ],
          ),
        ),
      ),
      splashColor: Colors.transparent,
      highlightColor: ColorSet.dark.withOpacity(.8),
    );
  }

  double get calculateAvatarBorderSize =>
      avatarBorderColor != null ? avatarBorderSize : 0;
}
