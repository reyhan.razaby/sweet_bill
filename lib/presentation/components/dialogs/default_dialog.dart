import 'package:flutter/material.dart';

import '../buttons/button_constants.dart';
import '../buttons/default_button.dart';
import 'base_dialog.dart';

class DefaultDialog extends BaseDialog {
  final VoidCallback? onNegativeClicked;
  final VoidCallback? onPositiveClicked;
  final String negativeText;
  final String positiveText;

  const DefaultDialog({
    Key? key,
     String? title,
    required Widget body,
    this.onPositiveClicked,
    this.onNegativeClicked,
    this.negativeText = 'No',
    this.positiveText = 'Yes',
  }) : super(title: title, body: body, key: key);

  @override
  Widget buildBottom(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: DefaultButton(
            onPressed: () {
              onNegativeClicked?.call();
              Navigator.of(context).pop();
            },
            child: Text(negativeText),
            type: ButtonType.outlined,
          ),
        ),
        const SizedBox(width: 8),
        Expanded(
          child: DefaultButton(
            onPressed: () {
              onPositiveClicked?.call();
              Navigator.of(context).pop();
            },
            child: Text(positiveText),
          ),
        ),
      ],
    );
  }
}
