import 'package:flutter/material.dart';

import '../buttons/default_button.dart';
import '../wrap/single_wrap.dart';
import 'base_dialog.dart';

class MessageDialog extends BaseDialog {
  final VoidCallback? onDismiss;
  final String dismissText;

  const MessageDialog({
    Key? key,
    required String title,
    String? bodyMessage,
    this.onDismiss,
    this.dismissText = 'Okay',
  }) : super(title: title, bodyMessage: bodyMessage, key: key);

  @override
  Widget buildBottom(BuildContext context) {
    return SingleWrap(
      child: DefaultButton(
        onPressed: () {
          onDismiss?.call();
          Navigator.of(context).pop();
        },
        child: Text(dismissText),
      ),
    );
  }
}
