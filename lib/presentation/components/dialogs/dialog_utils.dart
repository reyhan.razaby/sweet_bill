import 'package:flutter/material.dart';

import 'message_dialog.dart';

class DialogUtils {
  static Future<dynamic> showErrorDialog(
    BuildContext context, {
    VoidCallback? onDismiss,
    String? bodyMessage,
  }) {
    return showDialog(
      context: context,
      builder: (_) {
        return MessageDialog(
          title: 'Unexpected error :(',
          bodyMessage: bodyMessage ??
              "Please close app and open again."
                  "\nWe're really really really really sorry",
          onDismiss: onDismiss,
        );
      },
    );
  }
}
