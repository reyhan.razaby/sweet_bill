import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/color_set.dart';

abstract class BaseDialog extends StatelessWidget {
  final String? title;
  final double? titleSize;
  final Widget? body;
  final String? bodyMessage;

  const BaseDialog({
    Key? key,
    this.title,
    this.titleSize = 17,
    this.body,
    this.bodyMessage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget? bottomWidget = buildBottom(context);

    return AlertDialog(
      title: Center(
        child: Column(
          children: [
            if (title != null)
              Text(
                title!,
                style: TextStyle(
                  fontSize: titleSize,
                  fontWeight: FontWeight.w500,
                  height: 1.35,
                ),
                textAlign: TextAlign.center,
              ),
            if (body != null || bodyMessage != null)
              Padding(
                padding: const EdgeInsets.only(top: 9),
                child: Center(
                  child: buildBody(),
                ),
              ),
          ],
        ),
      ),
      titlePadding: titlePadding(),
      content: bottomWidget,
      contentPadding: contentPadding(),
    );
  }

  Widget buildBody() {
    if (bodyMessage != null) {
      return Text(
        bodyMessage!,
        style: const TextStyle(
          fontSize: 14,
          color: ColorSet.lightGrey,
          height: 1.3,
        ),
        textAlign: TextAlign.center,
      );
    }
    return body!;
  }

  Widget? buildBottom(BuildContext context);

  EdgeInsetsGeometry titlePadding() {
    return const EdgeInsets.fromLTRB(22, 18, 22, 0);
  }

  EdgeInsetsGeometry contentPadding() {
    return const EdgeInsets.fromLTRB(20, 14, 20, 18);
  }
}
