import 'package:flutter/material.dart';

import '../../../core/constants/color_set.dart';

class OptionMenu extends StatelessWidget {
  final List<OptionMenuItem> items;
  final Function? onSelectItem;

  const OptionMenu({
    Key? key,
    required this.items,
    this.onSelectItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        OptionMenuItem item = items[index];

        return TextButton(
          onPressed: item.isDisabled
              ? null
              : () {
                  if (onSelectItem != null) {
                    onSelectItem!(item);
                  }
                  item.onSelect?.call();
                },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 9, horizontal: 12),
            child: Row(
              mainAxisAlignment: item.isCentered
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.start,
              children: [
                if (item.iconData != null)
                  Icon(
                    item.iconData,
                    color: getItemColor(item),
                    size: 20,
                  ),
                if (item.iconData != null) const SizedBox(width: 14),
                Text(
                  item.label,
                  style: TextStyle(
                    color: getItemColor(item),
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        );
      },
      itemCount: items.length,
    );
  }

  Color getItemColor(OptionMenuItem item) {
    if (item.isDisabled) {
      return Colors.blueGrey;
    }
    if (item.isDanger) {
      return ColorSet.red;
    }
    return ColorSet.white;
  }
}

class OptionMenuItem {
  final String label;
  final VoidCallback? onSelect;
  final bool isDisabled;
  final bool isDanger;
  final bool isCentered;
  final IconData? iconData;

  const OptionMenuItem({
    required this.label,
    this.onSelect,
    this.isDisabled = false,
    this.isCentered = false,
    this.isDanger = false,
    this.iconData,
  });

  OptionMenuItem.edit(
    this.onSelect, {
    this.label = 'Edit',
    this.isDisabled = false,
  })  : iconData = Icons.edit,
        isCentered = false,
        isDanger = false;

  OptionMenuItem.remove(
    this.onSelect, {
    this.label = 'Remove',
    this.isDisabled = false,
  })  : iconData = Icons.delete_outline,
        isCentered = false,
        isDanger = true;
}
