import 'package:flutter/material.dart';

import '../../../core/constants/color_set.dart';

class DefaultCheckBox extends StatelessWidget {
  final bool isChecked;
  final VoidCallback? onTap;
  final EdgeInsetsGeometry? margin;
  final double radius;

  const DefaultCheckBox({
    Key? key,
    required this.isChecked,
    this.onTap,
    this.margin,
    this.radius = 12,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: Container(
        margin: margin,
        width: radius * 2,
        height: radius * 2,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius * 0.7),
          border: Border.all(width: radius / 6, color: ColorSet.light),
          color: isChecked ? ColorSet.light : Colors.transparent,
        ),
        child: isChecked
            ? const Icon(
                Icons.check,
                color: ColorSet.white,
                size: 16,
              )
            : null,
      ),
    );
  }
}
