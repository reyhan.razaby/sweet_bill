import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sweet_bill/core/constants/color_set.dart';

class ToastShower {
  void showToast(
    String message, {
    Color backgroundColor = ColorSet.grey,
    Color textColor = ColorSet.white,
    double fontSize = 14,
  }) {
    Fluttertoast.cancel();
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      backgroundColor: backgroundColor,
      textColor: textColor,
      fontSize: fontSize,
    );
  }
}
