import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../core/constants/color_set.dart';

abstract class BaseTextField extends StatefulWidget {
  static const double defaultFontSize = 16;

  final double? height;
  final bool autofocus;
  final EdgeInsetsGeometry? contentPadding;
  final String? hintText;
  final double fontSize;
  final ValueChanged<String>? onChanged;
  final TextCapitalization textCapitalization;
  final bool isClearable;
  final TextInputType? keyboardType;
  final Widget? prefixIcon;
  final bool centerPrefixIcon;
  final List<TextInputFormatter>? inputFormatters;
  final FormFieldValidator<String>? validator;
  final TextEditingController? controller;
  final bool filled;
  final Color? fillColor;
  final InputBorder? focusedBorder;
  final InputBorder? enabledBorder;
  final int? maxLines;
  final int? maxLength;
  final Duration? debounceDuration;
  final String? initialValue;
  final ValueChanged<String>? onFieldSubmitted;
  final bool isCollapsed;
  final bool hideCounterText;
  final ValueChanged<bool>? onFocusChanged;
  final FocusNode? focusNode;
  final bool readOnly;
  final bool? showCursor;
  final bool enableInteractiveSelection;
  final ScrollController? scrollController;
  final GestureTapCallback? onTap;

  const BaseTextField({
    Key? key,
    this.height,
    this.hintText,
    this.autofocus = false,
    this.contentPadding,
    this.fontSize = defaultFontSize,
    this.onChanged,
    this.isClearable = false,
    this.textCapitalization = TextCapitalization.none,
    this.keyboardType,
    this.prefixIcon,
    this.centerPrefixIcon = false,
    this.inputFormatters,
    this.validator,
    this.controller,
    this.filled = false,
    this.fillColor = ColorSet.black,
    this.focusedBorder,
    this.enabledBorder,
    this.maxLines,
    this.maxLength,
    this.debounceDuration,
    this.initialValue,
    this.onFieldSubmitted,
    this.isCollapsed = false,
    this.hideCounterText = false,
    this.onFocusChanged,
    this.focusNode,
    this.readOnly = false,
    this.showCursor,
    this.enableInteractiveSelection = true,
    this.scrollController,
    this.onTap,
  }) : super(key: key);

  @override
  State<BaseTextField> createState() => _BaseTextFieldState();
}

class _BaseTextFieldState extends State<BaseTextField> {
  final TextEditingController defaultController = TextEditingController();
  final FocusNode defaultFocusNode = FocusNode();
  final ValueNotifier<bool> hasValueNotifier = ValueNotifier<bool>(false);

  Timer? debounceTimer;

  TextEditingController get editingController =>
      widget.controller ?? defaultController;

  FocusNode get focusNode => widget.focusNode ?? defaultFocusNode;

  EdgeInsetsGeometry? get contentPadding =>
      widget.height != null ? EdgeInsets.zero : widget.contentPadding;

  bool get isWithDebounce => widget.debounceDuration != null;

  double get defaultFontSize => BaseTextField.defaultFontSize;

  @override
  void initState() {
    super.initState();

    editingController.addListener(() {
      hasValueNotifier.value = editingController.text.isNotEmpty;
    });

    if (widget.initialValue != null) {
      _setInitialValue();
    }

    if (widget.onFocusChanged != null) {
      focusNode.addListener(() {
        widget.onFocusChanged!(focusNode.hasFocus);
      });
    }
  }

  @override
  void dispose() {
    if (widget.focusNode == null) {
      focusNode.dispose();
    }
    if (widget.controller == null) {
      editingController.dispose();
    }
    debounceTimer?.cancel();
    hasValueNotifier.dispose();
    super.dispose();
  }

  Widget buildBody(BuildContext context) {
    if (widget.initialValue != null &&
        widget.initialValue != editingController.text) {
      _setInitialValue();
    }

    return SizedBox(
      height: widget.height,
      child: TextFormField(
        onTap: widget.onTap,
        scrollController: widget.scrollController,
        textAlignVertical: TextAlignVertical.center,
        controller: editingController,
        style: TextStyle(fontSize: widget.fontSize),
        onChanged: _changeValue,
        autofocus: widget.autofocus,
        textCapitalization: widget.textCapitalization,
        maxLines: widget.maxLines,
        maxLength: widget.maxLength,
        focusNode: focusNode,
        readOnly: widget.readOnly,
        showCursor: widget.showCursor,
        enableInteractiveSelection: widget.enableInteractiveSelection,
        decoration: InputDecoration(
          isCollapsed: widget.isCollapsed,
          counterText: widget.hideCounterText ? '' : null,
          hintText: widget.hintText,
          contentPadding: contentPadding,
          counterStyle: const TextStyle(color: ColorSet.white),
          isDense: true,
          prefixIcon: widget.centerPrefixIcon
              ? _center(widget.prefixIcon)
              : widget.prefixIcon,
          prefixIconConstraints:
              widget.prefixIcon != null ? const BoxConstraints() : null,
          suffixIcon: widget.isClearable ? _buildClearButton() : null,
          suffixIconConstraints:
              widget.isClearable ? const BoxConstraints() : null,
          filled: widget.filled,
          fillColor: widget.fillColor,
          focusedBorder: widget.focusedBorder,
          enabledBorder: widget.enabledBorder,
        ),
        keyboardType: widget.keyboardType,
        inputFormatters: widget.inputFormatters,
        validator: widget.validator,
        onFieldSubmitted: widget.onFieldSubmitted,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return buildBody(context);
  }

  void _setInitialValue() {
    editingController.text = widget.initialValue!;
    editingController.selection = TextSelection.fromPosition(
        TextPosition(offset: editingController.text.length));
  }

  void _changeValue(String val) {
    if (isWithDebounce) {
      if (debounceTimer?.isActive ?? false) {
        debounceTimer?.cancel();
      }
      debounceTimer = Timer(widget.debounceDuration!, () {
        _emitChange(val);
      });
    } else {
      _emitChange(val);
    }
  }

  void _emitChange(val) {
    if (widget.onChanged != null) {
      widget.onChanged!(val);
    }
  }

  ValueListenableBuilder<bool> _buildClearButton() {
    return ValueListenableBuilder<bool>(
      builder: (_, hasValue, __) {
        if (hasValue == false) {
          return const Text('');
        }

        double iconSize = min(max(widget.fontSize * .9, 15), 20);
        double horizontalPadding = iconSize * 0.7;
        double verticalPadding =
            widget.filled ? iconSize * 0.4 : iconSize * 0.2;

        return GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            _clear();
          },
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: horizontalPadding,
              vertical: verticalPadding,
            ),
            child: Icon(
              Icons.cancel_rounded,
              color: Colors.grey.withOpacity(.5),
              size: iconSize,
            ),
          ),
        );
      },
      valueListenable: hasValueNotifier,
    );
  }

  void _clear() {
    editingController.clear();
    _changeValue('');
  }

  Widget? _center(Widget? prefixIcon) {
    if (prefixIcon == null) {
      return null;
    }
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        prefixIcon,
      ],
    );
  }
}
