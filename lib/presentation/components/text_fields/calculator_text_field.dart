import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/components/text_fields/filled_text_field.dart';

class CalculatorTextField extends StatefulWidget {
  final ValueChanged<double>? onChanged;
  final ValueChanged<bool>? onFocusChanged;
  final ValueNotifier<double>? valueChanger;
  final GlobalKey? containerKey;
  final double fontSize;
  final double initialValue;
  final bool alwaysFocused;
  final Color backgroundColor;
  final EdgeInsetsGeometry? contentPadding;

  const CalculatorTextField({
    Key? key,
    this.onChanged,
    this.onFocusChanged,
    this.containerKey,
    this.fontSize = 14,
    this.initialValue = 0,
    this.valueChanger,
    this.alwaysFocused = false,
    this.backgroundColor = ColorSet.black,
    this.contentPadding = const EdgeInsets.symmetric(vertical: 4),
  }) : super(key: key);

  @override
  State<CalculatorTextField> createState() => _CalculatorTextFieldState();
}

class _CalculatorTextFieldState extends State<CalculatorTextField> {
  final TextEditingController fieldController = TextEditingController();
  final ScrollController scrollController = ScrollController();
  late double currentValue;

  CalculatorBloc get calculatorBloc => BlocProvider.of<CalculatorBloc>(context);

  @override
  void initState() {
    super.initState();
    currentValue = widget.initialValue;
    fieldController.addListener(ensureCursorAtTheAnd);
    widget.valueChanger?.addListener(changeValueFromOutside);
    if (alwaysFocused) {
      openCalculator(openImmediately: true);
    }
  }

  @override
  void dispose() {
    widget.valueChanger?.dispose();
    scrollController.dispose();
    fieldController.dispose();
    super.dispose();
  }

  bool get alwaysFocused => widget.alwaysFocused;

  double get fontSize => widget.fontSize;

  Color? get backgroundColor => widget.backgroundColor;

  @override
  Widget build(BuildContext context) {
    return FilledTextField(
      fillColor: backgroundColor,
      fontSize: fontSize,
      autofocus: alwaysFocused,
      scrollController: scrollController,
      controller: fieldController,
      readOnly: true,
      showCursor: true,
      enableInteractiveSelection: false,
      initialValue: getFormattedValue(currentValue),
      hintText: '0',
      maxLines: 1,
      isClearable: true,
      prefixIcon: prefixIcon(),
      keyboardType: TextInputType.none,
      contentPadding: widget.contentPadding,
      onChanged: (String val) {
        if (val.isNotEmpty) {
          return;
        }

        final state = calculatorBloc.state;
        if (state is CalculatorOpened &&
            state.fieldController == fieldController) {
          calculatorBloc.add(Clear());
        } else {
          emitChange(0);
        }
      },
      onFocusChanged: onFocusChanged,
    );
  }

  void onFocusChanged(hasFocus) {
    if (alwaysFocused) {
      return;
    }
    emitFocusChanged(hasFocus);
    if (hasFocus) {
      onFocused();
    } else {
      onUnFocused();
    }
  }

  /// Change text field and state's value
  void changeValueFromOutside() {
    final state = calculatorBloc.state;
    if (state is! CalculatorOpened ||
        state.fieldController != fieldController) {
      return;
    }

    double newValue = widget.valueChanger!.value;
    if (newValue != currentValue) {
      currentValue = newValue;
      calculatorBloc.add(ChangeValue(value: newValue));
    }
  }

  void ensureCursorAtTheAnd() async {
    var text = fieldController.text;
    var selection = fieldController.selection;
    if (!selection.isValid) {
      return;
    }
    var textLength = text.length;
    bool isAtTheEnd = text.isEmpty ||
        (selection.start == textLength && selection.end == textLength);
    if (!isAtTheEnd) {
      fieldController.selection = selection.copyWith(
        baseOffset: textLength,
        extentOffset: textLength,
      );
    }

    // Let the UI updated first before scroll to the end
    Future.delayed(const Duration(milliseconds: 20))
        .then((value) => scrollToEnd());
  }

  void scrollToEnd() {
    if (!scrollController.hasClients) {
      return;
    }
    double maxScrollExtent = scrollController.position.maxScrollExtent;
    bool isScrollable = maxScrollExtent > 0;
    if (!isScrollable) {
      return;
    }
    scrollController.jumpTo(maxScrollExtent);
  }

  void onFocused() {
    openCalculator();
  }

  void openCalculator({bool openImmediately = false}) {
    calculatorBloc.add(
      OpenCalculator(
        initialValue: currentValue,
        fieldController: fieldController,
        onValueChanged: (val) {
          emitChange(val);
        },
        onOpened: ensureContainerVisible,
        openImmediately: openImmediately,
      ),
    );
  }

  void onUnFocused() {
    // Evaluate the text if has operator and valid
    final state = calculatorBloc.state;
    if (state is! CalculatorOpened ||
        state.fieldController != fieldController) {
      return;
    }
    if (!state.hasOperator) {
      return;
    }

    // Change the text only, not with state.
    // Because the state will focus on the other field
    double emittedValue = state.expressionResultOrValue;
    editControllerText(emittedValue);
    emitChange(emittedValue);
  }

  void editControllerText(double newValue) {
    String newText = getFormattedValue(newValue);
    fieldController.text = newText;
    fieldController.selection = fieldController.selection.copyWith(
      baseOffset: newText.length,
      extentOffset: newText.length,
    );
  }

  String getFormattedValue(double value) {
    if (value == 0) {
      return '';
    }
    return calculatorBloc.getFormattedText(value);
  }

  void emitChange(double value) {
    currentValue = value;
    widget.onChanged?.call(value);
  }

  void emitFocusChanged(bool hasFocus) {
    widget.onFocusChanged?.call(hasFocus);
  }

  Widget prefixIcon() {
    return Padding(
      padding: EdgeInsets.only(
        left: widget.fontSize * .5,
        right: widget.fontSize * .3,
      ),
      child: Text(
        'Rp',
        style: TextStyle(
          color: ColorSet.grey,
          fontSize: widget.fontSize,
        ),
      ),
    );
  }

  void ensureContainerVisible() {
    if (widget.containerKey == null ||
        widget.containerKey!.currentContext == null) {
      return;
    }
    Scrollable.ensureVisible(
      widget.containerKey!.currentContext!,
      duration: const Duration(milliseconds: 200),
      curve: Curves.easeInOutSine,
      alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtEnd,
    );
  }
}
