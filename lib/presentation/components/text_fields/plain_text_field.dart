import 'package:flutter/material.dart';
import 'package:sweet_bill/presentation/components/text_fields/base_text_field.dart';

class PlainTextField extends BaseTextField {
  const PlainTextField({
    Key? key,
    super.hintText,
    super.autofocus,
    super.contentPadding,
    super.fontSize,
    super.onChanged,
    super.isClearable,
    super.prefixIcon,
    super.centerPrefixIcon,
    super.inputFormatters,
    super.keyboardType,
    super.textCapitalization,
    super.validator,
    super.controller,
    super.maxLines,
    super.maxLength,
    super.debounceDuration,
    super.initialValue,
    super.onFieldSubmitted,
    super.hideCounterText,
    super.focusNode,
  }) : super(key: key);
}
