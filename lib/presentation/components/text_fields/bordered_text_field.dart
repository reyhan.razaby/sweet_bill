import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/components/text_fields/base_text_field.dart';

class BorderedTextField extends BaseTextField {
  BorderedTextField({
    Key? key,
    super.hintText,
    super.autofocus,
    super.contentPadding =
        const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
    super.fontSize,
    super.onChanged,
    super.isClearable,
    super.prefixIcon,
    super.centerPrefixIcon,
    super.inputFormatters,
    super.keyboardType,
    super.textCapitalization,
    super.validator,
    super.controller,
    super.maxLines,
    super.maxLength,
    super.debounceDuration,
    super.initialValue,
    super.height,
    super.onFieldSubmitted,
    super.isCollapsed = true,
    super.hideCounterText,
    double borderRadius = 8,
    double borderWidth = 1,
    Color? borderColor = ColorSet.grey,
    Color? focusedBorderColor = ColorSet.light,
  }) : super(
          key: key,
          focusedBorder:
              _inputBorder(borderRadius, borderWidth, focusedBorderColor),
          enabledBorder: _inputBorder(borderRadius, borderWidth, borderColor),
        );
}

InputBorder _inputBorder(double borderRadius, double borderWidth, borderColor) {
  return OutlineInputBorder(
    borderSide: BorderSide(width: borderWidth, color: borderColor),
    borderRadius: BorderRadius.circular(borderRadius),
  );
}
