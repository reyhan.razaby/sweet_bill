import 'package:flutter/material.dart';
import 'package:sweet_bill/presentation/components/text_fields/base_text_field.dart';

class FilledTextField extends BaseTextField {
  FilledTextField({
    Key? key,
    super.height,
    super.hintText,
    super.autofocus,
    super.contentPadding,
    super.fontSize,
    super.onChanged,
    super.isClearable,
    super.prefixIcon,
    super.centerPrefixIcon,
    super.inputFormatters,
    super.keyboardType,
    super.textCapitalization,
    super.validator,
    super.controller,
    super.maxLines,
    super.maxLength,
    super.debounceDuration,
    super.initialValue,
    super.onFieldSubmitted,
    super.isCollapsed = true,
    super.hideCounterText,
    super.onFocusChanged,
    super.focusNode,
    super.readOnly,
    super.showCursor,
    super.enableInteractiveSelection,
    super.scrollController,
    super.onTap,
    super.fillColor,
    double borderRadius = 8,
  }) : super(
          key: key,
          filled: true,
          focusedBorder: _inputBorder(borderRadius),
          enabledBorder: _inputBorder(borderRadius),
        );
}

OutlineInputBorder _inputBorder(double borderRadius) {
  return OutlineInputBorder(
    borderSide: const BorderSide(width: 0, color: Colors.transparent),
    borderRadius: BorderRadius.circular(borderRadius),
  );
}
