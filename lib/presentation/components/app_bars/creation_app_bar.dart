import 'package:flutter/material.dart';

class CreationAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget? body;
  final Widget? confirmButton;
  final VoidCallback? onCancel;

  const CreationAppBar({
    Key? key,
    this.body,
    this.confirmButton,
    this.onCancel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        onPressed: onCancel ??
            () {
              Navigator.of(context).pop();
            },
        icon: const Icon(Icons.close),
      ),
      title: body,
      actions: [if (confirmButton != null) Center(child: confirmButton)],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
