import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';

typedef EqualKeyBuilder = bool Function();

class DefaultCalculatorInput extends StatelessWidget {
  final double height;
  final ValueChanged<String>? onValuePressed;
  final ValueChanged<Operator>? onOperatorPressed;
  final VoidCallback? onDonePressed;
  final VoidCallback? onEqualPressed;
  final VoidCallback? onBackspacePressed;
  final VoidCallback? onClearPressed;
  final ValueChanged<String>? onDecimalSymbolPressed;
  final String decimalSeparator;
  final ValueNotifier<bool> showEqualKeyNotifier;
  final String? doneText;

  const DefaultCalculatorInput({
    Key? key,
    this.height = 200,
    this.onValuePressed,
    this.onOperatorPressed,
    this.onDonePressed,
    this.onEqualPressed,
    this.onBackspacePressed,
    this.onClearPressed,
    this.onDecimalSymbolPressed,
    this.doneText,
    required this.decimalSeparator,
    required this.showEqualKeyNotifier,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorSet.grey,
      height: height,
      child: Column(
        children: [
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _CalcKey.clear(onTap: () {
                  _emitClearPressed();
                }),
                _CalcKey.operator('÷', onTap: () {
                  _emitOperatorPressed(Operator.divide);
                }),
                _CalcKey.operator('×', onTap: () {
                  _emitOperatorPressed(Operator.multiply);
                }),
                _CalcKey.backspace(onTap: () {
                  _emitBackspacePressed();
                }),
              ],
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _CalcKey.num('7', onTap: () {
                  _emitValuePressed('7');
                }),
                _CalcKey.num('8', onTap: () {
                  _emitValuePressed('8');
                }),
                _CalcKey.num('9', onTap: () {
                  _emitValuePressed('9');
                }),
                _CalcKey.operator('%', onTap: () {
                  _emitOperatorPressed(Operator.percent);
                }),
              ],
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _CalcKey.num('4', onTap: () {
                  _emitValuePressed('4');
                }),
                _CalcKey.num('5', onTap: () {
                  _emitValuePressed('5');
                }),
                _CalcKey.num('6', onTap: () {
                  _emitValuePressed('6');
                }),
                _CalcKey.operator('–', onTap: () {
                  _emitOperatorPressed(Operator.subtract);
                }),
              ],
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _CalcKey.num('1', onTap: () {
                  _emitValuePressed('1');
                }),
                _CalcKey.num('2', onTap: () {
                  _emitValuePressed('2');
                }),
                _CalcKey.num('3', onTap: () {
                  _emitValuePressed('3');
                }),
                _CalcKey.operator('+', onTap: () {
                  _emitOperatorPressed(Operator.add);
                }),
              ],
            ),
          ),
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _CalcKey.num('0', onTap: () {
                  _emitValuePressed('0');
                }),
                _CalcKey.num('000', onTap: () {
                  _emitValuePressed('000');
                }),
                _CalcKey.num(
                  decimalSeparator,
                  onTap: () {
                    _emitDecimalSymbolPressed();
                  },
                ),
                ValueListenableBuilder<bool>(
                  builder: (_, showEqual, __) {
                    if (showEqual) {
                      return _CalcKey.equal(onTap: _emitEqualPressed);
                    } else {
                      return _CalcKey.done(
                        onTap: _emitDonePressed,
                        text: doneText,
                      );
                    }
                  },
                  valueListenable: showEqualKeyNotifier,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _emitValuePressed(String value) {
    onValuePressed?.call(value);
  }

  void _emitDecimalSymbolPressed() {
    onDecimalSymbolPressed?.call(decimalSeparator);
  }

  void _emitOperatorPressed(Operator operatorKey) {
    onOperatorPressed?.call(operatorKey);
  }

  void _emitDonePressed() {
    onDonePressed?.call();
  }

  void _emitEqualPressed() {
    onEqualPressed?.call();
  }

  void _emitBackspacePressed() {
    onBackspacePressed?.call();
  }

  void _emitClearPressed() {
    onClearPressed?.call();
  }
}

class _CalcKey extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final CalcKeyType type;
  final bool isDisabled;

  const _CalcKey({
    Key? key,
    required this.text,
    required this.onTap,
    required this.type,
    this.isDisabled = false,
  }) : super(key: key);

  const _CalcKey.num(
    String text, {
    Key? key,
    required VoidCallback onTap,
    bool isDisabled = false,
  }) : this(
          text: text,
          key: key,
          onTap: onTap,
          type: CalcKeyType.num,
          isDisabled: isDisabled,
        );

  const _CalcKey.operator(
    String text, {
    Key? key,
    required VoidCallback onTap,
  }) : this(text: text, key: key, onTap: onTap, type: CalcKeyType.operator);

  const _CalcKey.clear({
    Key? key,
    required VoidCallback onTap,
  }) : this(text: 'C', key: key, onTap: onTap, type: CalcKeyType.clear);

  const _CalcKey.done({
    Key? key,
    String? text,
    required VoidCallback onTap,
  }) : this(text: text ?? '', key: key, onTap: onTap, type: CalcKeyType.done);

  const _CalcKey.equal({
    Key? key,
    required VoidCallback onTap,
  }) : this(text: '=', key: key, onTap: onTap, type: CalcKeyType.equal);

  const _CalcKey.backspace({
    Key? key,
    required VoidCallback onTap,
  }) : this(text: '', key: key, onTap: onTap, type: CalcKeyType.backspace);

  static Map<CalcKeyType, Color> bgColors = {
    CalcKeyType.num: ColorSet.darkest,
    CalcKeyType.done: ColorSet.light,
    CalcKeyType.equal: ColorSet.accent,
  };

  static const Map<CalcKeyType, Color> fontColors = {
    CalcKeyType.done: ColorSet.white,
    CalcKeyType.equal: ColorSet.white,
    CalcKeyType.clear: ColorSet.accent,
  };

  static const Map<CalcKeyType, double> fontSizes = {
    CalcKeyType.clear: 18,
    CalcKeyType.backspace: 21,
    CalcKeyType.operator: 21,
    CalcKeyType.done: 18,
    CalcKeyType.equal: 22,
  };

  Color _getBgColor() {
    return bgColors[type] ?? ColorSet.dark;
  }

  double _getFontSize() {
    return fontSizes[type] ?? 20;
  }

  Color _getFontColor() {
    return fontColors[type] ?? ColorSet.lightGrey;
  }

  Widget _getLabel() {
    if (type == CalcKeyType.done && text == '') {
      return Icon(
        Icons.keyboard_arrow_down,
        color: _getFontColor(),
        size: 26,
      );
    }
    if (type == CalcKeyType.backspace) {
      return Icon(Icons.backspace_outlined,
          color: _getFontColor(), size: _getFontSize());
    }
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: _getFontSize(),
        color: _getFontColor(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: const EdgeInsets.all(.15),
        color: _getBgColor(),
        child: TextButton(
          onPressed: isDisabled ? null : onTap,
          child: _getLabel(),
        ),
      ),
    );
  }
}

enum CalcKeyType { num, operator, clear, backspace, done, equal }
