import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';

class CalculatorVisibilityBuilder extends StatelessWidget {
  final Widget Function(BuildContext context, bool isVisible) builder;

  const CalculatorVisibilityBuilder({
    Key? key,
    required this.builder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CalculatorBloc, CalculatorState>(
      builder: (context, state) {
        bool isVisible = state.isOpening || state.isOpened;
        return builder(context, isVisible);
      },
    );
  }
}
