enum Operator {
  add,
  subtract,
  divide,
  multiply,
  percent;

  static Operator? fromText(String text) {
    for (var op in Operator.values) {
      if (op.text == text) return op;
    }
    return null;
  }
}

extension OperatorKeyExtension on Operator {
  String get text {
    return operatorStrings[this] ?? '';
  }

  static const Map<Operator, String> operatorStrings = {
    Operator.add: '+',
    Operator.multiply: '×',
    Operator.subtract: '-',
    Operator.divide: '÷',
    Operator.percent: '%',
  };
}
