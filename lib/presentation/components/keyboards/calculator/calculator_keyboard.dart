import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/operator.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

import 'default_calculator_input.dart';

class CalculatorKeyboard extends StatefulWidget {
  static const double height = 244;
  final bool alwaysShow;
  final ValueChanged<double>? onDone;

  const CalculatorKeyboard({
    Key? key,
    this.alwaysShow = false,
    this.onDone,
  }) : super(key: key);

  const CalculatorKeyboard.alwaysShow({
    required this.onDone,
    Key? key,
  })  : alwaysShow = true,
        super(key: key);

  @override
  State<CalculatorKeyboard> createState() => _CalculatorKeyboardState();
}

class _CalculatorKeyboardState extends State<CalculatorKeyboard>
    with SingleTickerProviderStateMixin {
  final ValueNotifier<bool> showEqualKeyNotifier = ValueNotifier<bool>(false);
  late final AnimationController animationController = AnimationController(
    duration: const Duration(milliseconds: 250),
    vsync: this,
  );

  late final Animation<double> heightAnimation = Tween<double>(
    begin: widget.alwaysShow ? CalculatorKeyboard.height : 0,
    end: CalculatorKeyboard.height,
  ).animate(CurvedAnimation(
    parent: animationController,
    curve: Curves.easeInOutSine,
  ));

  @override
  void initState() {
    super.initState();
    if (widget.alwaysShow) {
    } else {
      animationController.addStatusListener(onListenAnimationStatus);
    }
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CalculatorBloc, CalculatorState>(
      listener: (context, state) async {
        if (state is CalculatorOpening) {
          animationController.forward();
        } else if (state is CalculatorClosing) {
          animationController.reverse();
        } else if (state is CalculatorOpened) {
          showEqualKeyNotifier.value = state.hasOperator;
        }
      },
      buildWhen: (_, current) {
        return current.isClosed || current.isOpening;
      },
      builder: buildBody,
    );
  }

  void onListenAnimationStatus(AnimationStatus status) {
    if (status == AnimationStatus.completed) {
      calculatorBloc.add(EndOpenCalculator());
    } else if (status == AnimationStatus.dismissed) {
      calculatorBloc.add(EndCloseCalculator());
    }
  }

  Widget buildBody(BuildContext context, CalculatorState state) {
    if (state is CalculatorClosed) {
      return Container();
    }
    return AnimatedBuilder(
      builder: (context, child) {
        return SizedBox(
          height: heightAnimation.value,
          child: child,
        );
      },
      animation: animationController,
      child: WillPopScope(
        onWillPop: () async {
          if (!widget.alwaysShow && calculatorBloc.state.isOpened) {
            done();
            return false;
          }
          return true;
        },
        child: SingleChildScrollView(
          physics: const NeverScrollableScrollPhysics(),
          child: DefaultCalculatorInput(
            height: CalculatorKeyboard.height,
            decimalSeparator: calculatorBloc.decimalSeparator,
            onValuePressed: insertValue,
            onDecimalSymbolPressed: insertDecimalSymbol,
            onOperatorPressed: insertOperator,
            onBackspacePressed: backspace,
            onDonePressed: done,
            onClearPressed: clear,
            onEqualPressed: evaluate,
            showEqualKeyNotifier: showEqualKeyNotifier,
            doneText: widget.alwaysShow ? 'Save' : null,
          ),
        ),
      ),
    );
  }

  void insertOperator(Operator operator) {
    calculatorBloc.add(InsertOperator(operator: operator));
  }

  void insertValue(String val) {
    calculatorBloc.add(InsertNumber(number: val));
  }

  void insertDecimalSymbol(String decimalSymbol) {
    if (calculatorBloc.maxFractionDigits > 0) {
      calculatorBloc.add(InsertNumber(number: decimalSymbol));
    } else {
      ToastShower().showToast("Decimal isn't available in this currency");
    }
  }

  void backspace() {
    calculatorBloc.add(Backspace());
  }

  void evaluate() {
    calculatorBloc.add(Evaluate());
  }

  void clear() {
    calculatorBloc.add(Clear());
  }

  void done() {
    if (widget.alwaysShow) {
      final state = calculatorBloc.state;
      final latestValue =
          state is CalculatorOpened ? state.expressionResultOrValue : 0.0;
      widget.onDone?.call(latestValue);
    } else {
      calculatorBloc.add(CloseCalculator());
      FocusManager.instance.primaryFocus?.unfocus();
    }
  }

  CalculatorBloc get calculatorBloc => BlocProvider.of<CalculatorBloc>(context);
}
