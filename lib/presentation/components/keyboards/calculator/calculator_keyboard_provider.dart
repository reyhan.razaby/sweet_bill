import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/injection_container/service_locator.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';

class CalculatorKeyboardProvider extends StatelessWidget {
  final Widget child;

  const CalculatorKeyboardProvider({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => srvLocator<CalculatorBloc>(),
      child: child,
    );
  }
}
