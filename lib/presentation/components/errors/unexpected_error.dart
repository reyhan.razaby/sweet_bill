import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';
import 'package:flutter/material.dart';

class UnexpectedError extends StatelessWidget {
  final VoidCallback onReload;

  const UnexpectedError({
    Key? key,
    required this.onReload,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/error_1.png',
            height: 180,
          ),
          const SizedBox(height: 36),
          _ReloadButton(onReload: onReload),
        ],
      ),
    );
  }
}

class _ReloadButton extends StatelessWidget {
  const _ReloadButton({
    Key? key,
    required this.onReload,
  }) : super(key: key);

  final VoidCallback onReload;

  @override
  Widget build(BuildContext context) {
    return DefaultButton(
      child: Wrap(
        direction: Axis.horizontal,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: const [
          Icon(Icons.refresh, color: ColorSet.white),
          SizedBox(width: 6),
          Text(
            'Try again',
            style: TextStyle(
              color: ColorSet.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      onPressed: onReload,
    );
  }
}
