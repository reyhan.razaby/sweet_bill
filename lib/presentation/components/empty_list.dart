import 'package:flutter/material.dart';

import '../../core/constants/color_set.dart';

class EmptyList extends StatelessWidget {
  const EmptyList({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/images/empty_1.png',
            height: 180,
          ),
          const SizedBox(height: 26),
          const Text(
            'No data here, add new one!',
            style: TextStyle(
              color: ColorSet.white,
              fontSize: 19,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
