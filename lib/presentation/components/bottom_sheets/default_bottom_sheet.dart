import 'package:flutter/material.dart';

import '../../../core/constants/color_set.dart';

class DefaultBottomSheet extends StatelessWidget {
  final Widget child;
  final Color backgroundColor;
  final EdgeInsetsGeometry padding;
  final String? title;

  static const double topBorderRadius = 14;

  const DefaultBottomSheet({
    Key? key,
    required this.child,
    this.backgroundColor = ColorSet.dark,
    this.padding = const EdgeInsets.symmetric(
      vertical: 8,
      horizontal: 0,
    ),
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(topBorderRadius),
          topRight: Radius.circular(topBorderRadius),
        ),
        color: backgroundColor,
      ),
      child: Padding(
        padding: padding,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.grey,
              ),
              height: 4,
              width: 32,
              margin: const EdgeInsets.symmetric(vertical: 4),
            ),
            if (title != null)
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 14),
                child: Text(
                  title!,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            if (title != null)
              Divider(
                height: 0,
                color: Colors.grey.withOpacity(0.4),
              ),
            child,
          ],
        ),
      ),
    );
  }
}
