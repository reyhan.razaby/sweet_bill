import 'package:flutter/material.dart';

import '../option_menu/option_menu.dart';
import 'default_bottom_sheet.dart';

class OptionMenuBottomSheet extends StatelessWidget {
  final List<OptionMenuItem> items;
  final String? title;

  const OptionMenuBottomSheet({
    Key? key,
    required this.items,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultBottomSheet(
      title: title,
      child: OptionMenu(
        items: items,
        onSelectItem: (_) {
          // Dismiss the bottom sheet
          Navigator.of(context).pop();
        },
      ),
    );
  }
}
