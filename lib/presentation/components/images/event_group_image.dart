import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sweet_bill/core/log.dart';

class EventGroupImage extends StatelessWidget {
  final String? imageFilePath;
  final VoidCallback? onTap;
  final double radius;

  const EventGroupImage({
    Key? key,
    this.imageFilePath,
    this.onTap,
    this.radius = 24,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: _buildImage(),
    );
  }

  Widget _buildImage() {
    if (imageFilePath != null && File(imageFilePath!).existsSync()) {
      try {
        File imageFile = File(imageFilePath!);
        return ClipOval(
          child: Image.file(
            imageFile,
            width: radius * 2,
            height: radius * 2,
            fit: BoxFit.cover,
          ),
        );
      } catch (e, s) {
        Log.error('Cannot load file', e, s);
      }
    }
    return _buildDefaultImage();
  }

  Widget _buildDefaultImage() {
    return CircleAvatar(
      backgroundColor: Colors.blueGrey,
      radius: radius,
      child: Icon(Icons.group, size: radius),
    );
  }
}
