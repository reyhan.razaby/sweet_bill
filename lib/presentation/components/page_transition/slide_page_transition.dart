import 'package:flutter/cupertino.dart';

class SlideTransitionRoute extends PageRouteBuilder {
  final Widget page;
  final Directions direction;

  SlideTransitionRoute(
      {required this.page, this.direction = Directions.rightToLeft})
      : super(
          transitionDuration: const Duration(milliseconds: 180),
          pageBuilder: (context, animation, secondaryAnimation) => page,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return SlideTransition(
              child: child,
              position: Tween<Offset>(
                begin: _getBeginOffset(direction),
                end: Offset.zero,
              ).animate(
                CurvedAnimation(
                  parent: animation,
                  curve: Curves.easeOutSine,
                ),
              ),
            );
          },
        );

  static Offset _getBeginOffset(Directions direction) {
    switch (direction) {
      case Directions.leftToRight:
        return const Offset(-1, 0);
      case Directions.rightToLeft:
        return const Offset(1, 0);
      case Directions.bottomToTop:
        return const Offset(0, 1);
      case Directions.topToBottom:
        return const Offset(0, -1);
    }
  }
}

enum Directions {
  leftToRight,
  rightToLeft,
  bottomToTop,
  topToBottom,
}
