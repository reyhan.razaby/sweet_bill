import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sweet_bill/core/constants/color_set.dart';

class FullScreenLoading extends StatelessWidget {
  final bool wrapWithScaffold;

  const FullScreenLoading({
    Key? key,
    this.wrapWithScaffold = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (wrapWithScaffold) {
      return const Scaffold(
        body: SpinKitDoubleBounce(
          color: ColorSet.light,
        ),
      );
    } else {
      return const SpinKitDoubleBounce(
        color: ColorSet.light,
      );
    }
  }
}
