import 'package:flutter/cupertino.dart';
import 'package:flutter_switch/flutter_switch.dart';

import '../../../core/constants/color_set.dart';

class DefaultSwitch extends StatelessWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  const DefaultSwitch({Key? key,
  required this.value,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = const Color(0xff395571);

    return FlutterSwitch(
      width: 50,
      height: 26,
      padding: 4,
      toggleSize: 18,
      activeColor: backgroundColor,
      inactiveColor: backgroundColor,
      activeToggleColor: ColorSet.light,
      inactiveToggleColor: ColorSet.white.withOpacity(.55),
      value: value,
      onToggle: onChanged,
    );
  }
}
