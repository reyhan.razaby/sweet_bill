import 'package:flutter/material.dart';

import '../../../core/constants/color_set.dart';

class CircleRadioButton extends StatelessWidget {
  final bool isSelected;
  final VoidCallback? onTap;
  final EdgeInsetsGeometry? margin;

  const CircleRadioButton({
    Key? key,
    required this.isSelected,
    this.onTap,
    this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const radius = 12;
    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: Container(
        margin: margin,
        width: radius * 2,
        height: radius * 2,
        padding: const EdgeInsets.all(3),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          border: Border.all(width: 2, color: ColorSet.light),
          color: Colors.transparent,
        ),
        child: Container(
          decoration: isSelected
              ? BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: ColorSet.light,
                )
              : null,
        ),
      ),
    );
  }
}
