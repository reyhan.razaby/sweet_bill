import 'package:flutter/material.dart';

import '../../core/constants/color_set.dart';

ThemeData buildThemeData(BuildContext context) {
  return ThemeData(
    appBarTheme: buildAppBarTheme(),
    scaffoldBackgroundColor: ColorSet.darkest,
    primaryColor: ColorSet.darkest,
    textTheme: Theme.of(context).textTheme.apply(bodyColor: ColorSet.white),
    bottomSheetTheme:
        BottomSheetThemeData(backgroundColor: Colors.black.withOpacity(0)),
    dialogTheme: buildDialogTheme(),
    elevatedButtonTheme: buildElevatedButtonThemeData(),
    outlinedButtonTheme: buildOutlinedButtonThemeData(),
    textButtonTheme: buildTextButtonThemeData(),
    textSelectionTheme: buildTextSelectionThemeData(),
    inputDecorationTheme: buildInputDecorationTheme(),
    popupMenuTheme: buildPopupMenuThemeData(),
  );
}

PopupMenuThemeData buildPopupMenuThemeData() {
  return const PopupMenuThemeData(
    color: ColorSet.white,
    textStyle: TextStyle(color: ColorSet.darkest, fontSize: 16),
  );
}

TextSelectionThemeData buildTextSelectionThemeData() {
  return const TextSelectionThemeData(
    cursorColor: ColorSet.light,
  );
}

InputDecorationTheme buildInputDecorationTheme() {
  return InputDecorationTheme(
    focusedBorder: const UnderlineInputBorder(
      borderSide: BorderSide(color: ColorSet.light),
    ),
    hintStyle: TextStyle(color: Colors.grey.withOpacity(0.4)),
    labelStyle: TextStyle(color: Colors.grey.withOpacity(0.4)),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: Colors.grey.withOpacity(0.4)),
    ),
  );
}

AppBarTheme buildAppBarTheme() {
  return const AppBarTheme(
    backgroundColor: ColorSet.darkest,
    titleTextStyle: TextStyle(
      fontSize: 16,
      color: ColorSet.white,
      fontWeight: FontWeight.bold,
    ),
    elevation: 0,
  );
}

TextButtonThemeData buildTextButtonThemeData() {
  return TextButtonThemeData(
      style: ButtonStyle(
    animationDuration: Duration.zero,
    foregroundColor: MaterialStateProperty.resolveWith(
      (states) {
        if (states.contains(MaterialState.disabled)) {
          return ColorSet.light.withOpacity(0.3);
        }
        return ColorSet.light;
      },
    ),
  ));
}

DialogTheme buildDialogTheme() {
  return const DialogTheme(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(8)),
    ),
    backgroundColor: ColorSet.dark,
    contentTextStyle: TextStyle(
      color: ColorSet.white,
    ),
    titleTextStyle: TextStyle(
      color: ColorSet.white,
    ),
  );
}

ElevatedButtonThemeData buildElevatedButtonThemeData() {
  return ElevatedButtonThemeData(
    style: ButtonStyle(
      animationDuration: Duration.zero,
      shape: MaterialStateProperty.all(const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)))),
      backgroundColor: MaterialStateProperty.resolveWith(
        (states) {
          if (states.contains(MaterialState.disabled)) {
            return ColorSet.light.withOpacity(0.3);
          }
          return ColorSet.light;
        },
      ),
      foregroundColor: MaterialStateProperty.resolveWith(
        (states) {
          if (states.contains(MaterialState.disabled)) {
            return ColorSet.white.withOpacity(0.3);
          }
          return ColorSet.white;
        },
      ),
    ),
  );
}

OutlinedButtonThemeData buildOutlinedButtonThemeData() {
  return OutlinedButtonThemeData(
    style: ButtonStyle(
      animationDuration: Duration.zero,
      shape: MaterialStateProperty.all(const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8)))),
      foregroundColor: MaterialStateProperty.resolveWith(
        (states) {
          if (states.contains(MaterialState.disabled)) {
            return ColorSet.white.withOpacity(0.3);
          }
          return ColorSet.white;
        },
      ),
      side: MaterialStateProperty.resolveWith(
        (states) {
          Color color = ColorSet.light;
          if (states.contains(MaterialState.disabled)) {
            color = color.withOpacity(.3);
          }
          return BorderSide(color: color);
        },
      ),
    ),
  );
}

TransitionBuilder datePickerThemeBuilder(BuildContext context) {
  return (_, child) {
    return Theme(
      data: Theme.of(context).copyWith(
        colorScheme: const ColorScheme.light(
          primary: ColorSet.light,
          onPrimary: ColorSet.darkest,
          onSurface: ColorSet.white,
        ),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            primary: ColorSet.light,
          ),
        ),
      ),
      child: child!,
    );
  };
}
