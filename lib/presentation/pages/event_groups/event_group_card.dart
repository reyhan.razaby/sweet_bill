import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_select_item.dart';
import 'package:sweet_bill/presentation/bloc/event_groups/event_groups_bloc.dart';
import 'package:sweet_bill/presentation/components/dialogs/confirmation_dialog.dart';

import '../../../core/constants/route_names.dart';
import '../../../core/utils/string_utils.dart';
import '../../components/bottom_sheets/option_menu_bottom_sheet.dart';
import '../../components/images/event_group_image.dart';
import '../../components/option_menu/option_menu.dart';
import '../../constants/display_text.dart';
import '../event_group_form/event_group_form_page.dart';

class EventGroupCard extends StatefulWidget {
  final EventGroupSelectItem item;

  const EventGroupCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  State<EventGroupCard> createState() => _EventGroupCardState();
}

class _EventGroupCardState extends State<EventGroupCard> {
  @override
  Widget build(BuildContext context) {
    const double hPadding = 16;
    const double vPadding = 14;
    const double contentHeight = 48;

    return GestureDetector(
      onTap: () {
        BlocProvider.of<EventGroupsBloc>(context)
            .add(FocusEventGroup(focusedId: widget.item.eventGroup.id));
      },
      onLongPress: () {
        showModalBottomSheet(
            context: context, builder: (_) => buildOptionBottomSheet());
      },
      child: BlocSelector<EventGroupsBloc, EventGroupsState, bool>(
        selector: (state) {
          return state.focusedId == widget.item.eventGroup.id;
        },
        builder: (context, isFocused) {
          return Container(
            decoration: buildBoxDecoration(isFocused),
            child: Padding(
              padding: const EdgeInsets.symmetric(
                  vertical: vPadding, horizontal: hPadding),
              child: Row(
                children: [
                  buildGroupImage(hPadding, contentHeight),
                  buildDescription(),
                  buildOptionButton(contentHeight, context)
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  String buildMembersDisplayText() {
    final members = widget.item.members;
    if (members.isEmpty) {
      return 'No member';
    } else if (members.length == 1 && members[0].isSelf) {
      return 'Only you';
    }
    return 'with ' +
        members
            .where((member) => !member.isSelf)
            .map((member) => member.name)
            .join(', ');
  }

  Widget buildOptionButton(double contentHeight, BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        showModalBottomSheet(
            context: context, builder: (_) => buildOptionBottomSheet());
      },
      child: const Padding(
        padding: EdgeInsets.fromLTRB(10, 10, 2, 10),
        child: Icon(
          Icons.more_vert_rounded,
          color: Color(0xffd2d2d2),
          size: 24,
        ),
      ),
    );
  }

  Widget buildOptionBottomSheet() {
    return OptionMenuBottomSheet(
      title: getDisplayName(),
      items: [
        OptionMenuItem(
            label: 'Choose',
            iconData: Icons.check_outlined,
            onSelect: () {
              eventGroupsBloc.add(
                  ActivateEventGroup(eventGroupId: widget.item.eventGroup.id));
              Navigator.of(context).pop();
            }),
        OptionMenuItem.edit(() {
          _goToEditPage().then((_) {
            BlocProvider.of<EventGroupsBloc>(context).add(LoadEventGroups());
          });
        }),
        OptionMenuItem.remove(
          () {
            _showRemovingConfirmation();
          },
          isDisabled: eventGroupsBloc.state.getEventGroupsOrEmpty.length <= 1,
        ),
      ],
    );
  }

  Future _goToEditPage() async {
    final arguments = EventGroupFormPageArgs(
      eventGroupId: widget.item.eventGroup.id,
      isEditing: true,
    );
    return Navigator.pushNamed(
      context,
      RouteNames.eventGroupForm,
      arguments: arguments,
    );
  }

  void _showRemovingConfirmation() {
    showDialog(
      context: context,
      builder: (_) {
        return ConfirmationDialog(
          positiveText: 'Remove',
          onPositiveClicked: () {
            eventGroupsBloc
                .add(RemoveEventGroup(eventGroupId: widget.item.eventGroup.id));
          },
          negativeText: 'Cancel',
        );
      },
    );
  }

  String getDisplayName() {
    return StringUtils.getOr(
        widget.item.eventGroup.name, DisplayText.emptyGroupName);
  }

  EventGroupsBloc get eventGroupsBloc =>
      BlocProvider.of<EventGroupsBloc>(context);

  Expanded buildDescription() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Flexible(
                child: Text(
                  getDisplayName(),
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              if (widget.item.isCurrentlyActive) buildActiveMark()
            ],
          ),
          const SizedBox(height: 4),
          Text(
            buildMembersDisplayText(),
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
                fontSize: 11,
                fontStyle: FontStyle.italic,
                color: Color(0xffbdbdbd)),
          ),
        ],
      ),
    );
  }

  Widget buildActiveMark() {
    return Padding(
      padding: const EdgeInsets.only(left: 4),
      child: Container(
        alignment: Alignment.centerLeft,
        padding: const EdgeInsets.symmetric(vertical: 2, horizontal: 5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorSet.accent,
        ),
        child: const Text(
          'Current',
          style: TextStyle(fontSize: 9),
        ),
      ),
    );
  }

  Padding buildGroupImage(double hPadding, double contentHeight) {
    return Padding(
      padding: EdgeInsets.only(right: hPadding),
      child: EventGroupImage(
        imageFilePath: widget.item.eventGroup.imagePath,
        radius: contentHeight / 2,
      ),
    );
  }

  BoxDecoration buildBoxDecoration(bool isFocused) {
    const double borderWidth = 2;

    if (isFocused) {
      return BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: Color.alphaBlend(ColorSet.light.withOpacity(0.3), ColorSet.dark),
        border: Border.all(color: ColorSet.light, width: borderWidth),
      );
    } else {
      return BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ColorSet.dark,
        border: Border.all(color: ColorSet.dark, width: borderWidth),
      );
    }
  }
}
