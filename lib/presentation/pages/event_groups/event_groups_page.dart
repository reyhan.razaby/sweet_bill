// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/core/constants/route_names.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/presentation/bloc/event_groups/event_groups_bloc.dart';
import 'package:sweet_bill/presentation/components/buttons/back_icon_button.dart';
import 'package:sweet_bill/presentation/components/buttons/button_constants.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';
import 'package:sweet_bill/presentation/components/dialogs/confirmation_dialog.dart';
import 'package:sweet_bill/presentation/components/errors/unexpected_error.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';

import 'event_group_selection.dart';

class EventGroupsPage extends StatefulWidget {
  const EventGroupsPage({Key? key}) : super(key: key);

  @override
  State<EventGroupsPage> createState() => _EventGroupsPageState();
}

class _EventGroupsPageState extends State<EventGroupsPage> {
  @override
  void initState() {
    super.initState();
    loadEventGroups();
  }

  void loadEventGroups() {
    BlocProvider.of<EventGroupsBloc>(context).add(LoadEventGroups());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackIconButton(),
        title: const Text('Group List'),
        backgroundColor: ColorSet.darkest,
        actions: [
          TextButton(
            onPressed: () {
              Navigator.pushNamed(context, RouteNames.eventGroupForm)
                  .then(onBackFromForm);
            },
            child: const Text('Add group'),
          )
        ],
      ),
      backgroundColor: ColorSet.darkest,
      body: BlocBuilder<EventGroupsBloc, EventGroupsState>(
        buildWhen: (previous, current) {
          if (current is EventGroupsLoaded) {
            return current.buildList;
          }
          return true;
        },
        builder: (context, state) {
          if (state is EventGroupsLoading) {
            return const FullScreenLoading();
          } else if (state is EventGroupsError) {
            return UnexpectedError(onReload: loadEventGroups);
          } else {
            return EventGroupSelection(
                eventGroups: state.getEventGroupsOrEmpty);
          }
        },
      ),
      bottomNavigationBar: Container(
        color: ColorSet.darkest,
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 14),
        child: BlocSelector<EventGroupsBloc, EventGroupsState, bool>(
          selector: (state) => state.activeEventGroupId != state.focusedId,
          builder: (context, isChanged) {
            return DefaultButton(
              isDisabled: !isChanged,
              onPressed: () {
                BlocProvider.of<EventGroupsBloc>(context)
                    .add(ActivateEventGroup());
                Navigator.pop(context);
              },
              label: 'Choose Group',
              size: ButtonSize.large,
            );
          },
        ),
      ),
    );
  }

  FutureOr<void> onBackFromForm(args) {
    if (args != null &&
        args is EventGroupsPageArgs &&
        args.createdEventGroup != null) {
      showDialog(
        context: context,
        builder: (_) {
          return ConfirmationDialog(
            title: 'Set ${args.createdEventGroup!.name} as current group?',
            titleSize: 15,
            positiveText: 'Yup',
            negativeText: 'No',
            onPositiveClicked: () {
              BlocProvider.of<EventGroupsBloc>(context).add(
                  ActivateEventGroup(eventGroupId: args.createdEventGroup!.id));
              Navigator.of(context).pop();
            },
          );
        },
      );
    }
    loadEventGroups();
  }
}

class EventGroupsPageArgs extends Equatable {
  final EventGroup? createdEventGroup;

  const EventGroupsPageArgs({
    this.createdEventGroup,
  });

  @override
  List<Object?> get props => [createdEventGroup];
}
