import 'package:sweet_bill/domain/entities/event_group/event_group_select_item.dart';
import 'package:sweet_bill/presentation/components/empty_list.dart';
import 'package:flutter/material.dart';

import '../../../domain/entities/event_group/event_group_select_item.dart';
import 'event_group_card.dart';

class EventGroupSelection extends StatefulWidget {
  final List<EventGroupSelectItem> eventGroups;

  const EventGroupSelection({
    Key? key,
    required this.eventGroups,
  }) : super(key: key);

  @override
  State<EventGroupSelection> createState() => _EventGroupSelectionState();
}

class _EventGroupSelectionState extends State<EventGroupSelection> {
  @override
  Widget build(BuildContext context) {
    if (widget.eventGroups.isEmpty) {
      return const EmptyList();
    }

    return ListView.builder(
      itemCount: widget.eventGroups.length,
      itemBuilder: (context, index) {
        final eventGroup = widget.eventGroups[index];
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: EventGroupCard(
            item: eventGroup,
          ),
        );
      },
    );
  }
}
