import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/core/constants/route_names.dart';
import 'package:sweet_bill/page_builder.dart';
import 'package:sweet_bill/presentation/bloc/nav_menu/nav_menu_cubit.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/bloc/transactions/transactions_cubit.dart';
import 'package:sweet_bill/presentation/pages/bill_form/bill_form_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileCubit, ProfileState>(
      listener: (context, state) {
        if (state is ProfileEmpty) {
          Navigator.of(context).pushNamed(RouteNames.welcome);
        }
      },
      child: Scaffold(
        body: PageBuilder.buildTransactionsPage(),
        floatingActionButton: FloatingActionButton(
          backgroundColor: ColorSet.light,
          child: const Text(
            '+',
            style: TextStyle(fontSize: 22),
          ),
          onPressed: () {
            TransactionsState state =
                BlocProvider.of<TransactionsCubit>(context).state;
            if (state is! TransactionsLoaded) {
              return;
            }
            var arg = BillFormPageArgs(eventGroupId: state.eventGroup.id);
            Navigator.of(context)
                .pushNamed(RouteNames.billForm, arguments: arg)
                .then((_) {
              BlocProvider.of<TransactionsCubit>(context)
                  .loadActiveEventGroup();
            });
          },
        ),
        // bottomNavigationBar: const BottomNavBar(),
      ),
    );
  }

  BlocBuilder<NavMenuCubit, NavMenuState> buildPageByNavMenu() =>
      BlocBuilder<NavMenuCubit, NavMenuState>(builder: getPage);

  Widget getPage(context, currentMenu) {
    if (currentMenu is NavMenuTransactions) {
      return PageBuilder.buildTransactionsPage();
    }
    if (currentMenu is NavMenuProfile) {
      return PageBuilder.buildProfilePage();
    }
    return PageBuilder.buildNotFoundPage();
  }
}
