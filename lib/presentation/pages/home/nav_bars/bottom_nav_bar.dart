import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';

import '../../../../core/constants/route_names.dart';
import '../../../bloc/nav_menu/nav_menu_cubit.dart';
import '../../../bloc/transactions/transactions_cubit.dart';
import '../../bill_form/bill_form_page.dart';

class BottomNavBar extends StatelessWidget {
  static const Color itemColor = ColorSet.grey;
  static const Color activeItemColor = ColorSet.white;
  static const Color backgroundColor = ColorSet.darkest;
  static const double iconSize = 24;
  static const double textSize = 9;
  static const double navBarHeight = kBottomNavigationBarHeight;

  const BottomNavBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: ColorSet.grey.withOpacity(.5), width: 0.5),
        ),
        color: backgroundColor,
      ),
      height: kBottomNavigationBarHeight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          buildMenuItem(context, const NavMenuTransactions()),
          buildAddButton(context),
          buildMenuItem(context, const NavMenuProfile()),
        ],
      ),
    );
  }

  Widget buildAddButton(context) {
    const double size = 48;
    return InkWell(
      onTap: () {
        goToBillForm(context);
      },
      child: Container(
        decoration: const BoxDecoration(
          color: ColorSet.light,
          shape: BoxShape.circle,
        ),
        height: size,
        width: size,
        child: const Icon(Icons.add, color: ColorSet.white),
      ),
    );
  }

  void goToBillForm(BuildContext context) {
    TransactionsState state = BlocProvider.of<TransactionsCubit>(context).state;
    if (state is! TransactionsLoaded) {
      return;
    }
    var arg = BillFormPageArgs(eventGroupId: state.eventGroup.id);
    Navigator.of(context)
        .pushNamed(RouteNames.billForm, arguments: arg)
        .then((_) {
      BlocProvider.of<TransactionsCubit>(context).loadActiveEventGroup();
    });
  }

  Widget buildMenuItem(context, NavMenuState navMenu) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        BlocProvider.of<NavMenuCubit>(context).selectMenuItem(navMenu);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BlocBuilder<NavMenuCubit, NavMenuState>(
            builder: (_, state) {
              return Icon(
                navMenu.icon,
                size: iconSize,
                color: state == navMenu ? activeItemColor : itemColor,
              );
            },
          ),
          Container(
            height: 4,
            constraints: const BoxConstraints(minWidth: 64),
          ),
          Flexible(
            child: BlocBuilder<NavMenuCubit, NavMenuState>(
              builder: (_, state) {
                return Text(
                  navMenu.label,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: textSize,
                    color: state == navMenu ? activeItemColor : itemColor,
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
