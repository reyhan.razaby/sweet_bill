import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';

import '../../bloc/bill_form_payment/bill_form_payment_bloc.dart';

abstract class BillFormPersonsAssignmentPage extends StatelessWidget {
  const BillFormPersonsAssignmentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    loadData(context);

    return BlocBuilder<BillFormBloc, BillFormState>(
      builder: (context, state) {
        if (state is BillFormLoading) {
          return const Scaffold(body: FullScreenLoading());
        }
        return buildPage(context);
      },
    );
  }

  Widget buildPage(BuildContext context);

  void loadData(BuildContext context);

  BillFormBloc getBillFormBloc(BuildContext context) =>
      BlocProvider.of<BillFormBloc>(context);

  BillFormPaymentBloc getPaymentBloc(BuildContext context) =>
      BlocProvider.of<BillFormPaymentBloc>(context);

  BillFormCostBloc getCostBloc(BuildContext context) =>
      BlocProvider.of<BillFormCostBloc>(context);
}
