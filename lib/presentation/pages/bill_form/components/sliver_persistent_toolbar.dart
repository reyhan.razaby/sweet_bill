import 'package:flutter/widgets.dart';

class SliverPersistentToolbar extends SliverPersistentHeaderDelegate {
  final double height;
  final Widget child;

  const SliverPersistentToolbar({
    required this.height,
    required this.child,
  });

  @override
  Widget build(BuildContext context, shrinkOffset, overlapsContent) => child;

  @override
  double get maxExtent => height;

  @override
  double get minExtent => height;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      false;
}
