import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';
import 'package:sweet_bill/presentation/components/text_fields/filled_text_field.dart';

import '../../../../domain/entities/person/person.dart';
import '../../../bloc/bill_form/bill_form_bloc.dart';
import '../../../bloc/friends/friends_bloc.dart';
import '../../../components/person/person_list_item.dart';

class AddMemberPage extends StatefulWidget {
  const AddMemberPage({
    Key? key,
  }) : super(key: key);

  @override
  State<AddMemberPage> createState() => _AddMemberPageState();
}

class _AddMemberPageState extends State<AddMemberPage> {
  late List<String> existingMemberIds;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add member'),
      ),
      body: BlocBuilder<FriendsBloc, FriendsState>(
        builder: (context, state) {
          if (state is FriendsLoading) {
            return const FullScreenLoading();
          }
          if (state is! FriendsLoaded) {
            return Container();
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              buildCreateButton(),
              buildFriendList(state.friends ?? []),
            ],
          );
        },
      ),
      backgroundColor: ColorSet.dark,
    );
  }

  Widget buildFriendList(List<Person> candidates) {
    return SingleChildScrollView(
      child: ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: candidates.length,
        itemBuilder: (_, index) {
          Person candidate = candidates[index];
          return PersonListItem(
            onTap: () {
              submit(candidate);
            },
            nameSize: 14,
            backgroundColor: ColorSet.darkest,
            avatarRadius: 18,
            person: candidate,
            verticalPadding: 14,
            horizontalPadding: 14,
          );
        },
        separatorBuilder: (_, __) {
          return Divider(
            indent: 54,
            height: 0,
            color: Colors.grey.withOpacity(0.5),
          );
        },
      ),
    );
  }

  Widget buildCreateButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 5),
      child: TextButton(
        child: const Text(
          '+ Create new',
          style: TextStyle(fontSize: 16),
        ),
        onPressed: () {},
      ),
    );
  }

  Widget buildFriendsSearchBar() {
    return Padding(
      padding: const EdgeInsets.only(left: 14, right: 14, bottom: 10),
      child: FilledTextField(
        borderRadius: 100,
        hintText: 'Search...',
        debounceDuration: const Duration(milliseconds: 200),
        prefixIcon: const SizedBox(
          width: 40,
          child: Icon(
            Icons.search,
            color: Colors.grey,
            size: 20,
          ),
        ),
        onChanged: (val) {},
        maxLines: 1,
        isClearable: true,
        contentPadding: const EdgeInsets.all(8),
      ),
    );
  }

  BillFormBloc get billFormBloc => BlocProvider.of<BillFormBloc>(context);

  FriendsBloc get friendsBloc => BlocProvider.of<FriendsBloc>(context);

  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    BillFormState billFormState = billFormBloc.state;
    if (billFormState is BillFormLoaded) {
      List<Person> existingMembers =
          await billFormBloc.getMembers(billFormState.bill.eventGroupId);
      existingMemberIds = existingMembers.map((e) => e.id).toList();
      friendsBloc.add(loadFriends());
    }
  }

  LoadFriends loadFriends() {
    return LoadFriends(
      includeSelf: true,
      excludeIds: existingMemberIds,
    );
  }

  void submit(Person candidate) {
    Navigator.pop(context, candidate);
  }
}
