import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';

import '../../../../core/constants/color_set.dart';

class IconPickerDialog extends StatelessWidget {
  final BillIcons? selectedIcon;
  final Function(BillIcons) onSelected;

  const IconPickerDialog({
    Key? key,
    this.selectedIcon,
    required this.onSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<BillIcons> icons = BillIcons.values;
    return AlertDialog(
      title: const Text('Choose an icon'),
      contentPadding: const EdgeInsets.symmetric(horizontal: 18),
      content: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 10),
          width: 300,
          height: 490,
          child: GridView.count(
            crossAxisCount: 4,
            children: icons
                .map((icon) => _buildSelectableIcon(context, icon))
                .toList(),
          ),
        ),
      ),
    );
  }

  Widget _buildSelectableIcon(context, BillIcons icon) {
    bool isSelected = icon == selectedIcon;
    return TextButton(
      onPressed: isSelected
          ? null
          : () {
              onSelected(icon);
              Navigator.of(context).pop();
            },
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: isSelected ? ColorSet.accent : Colors.transparent,
        ),
        child: icon.widget(size: IconSize.large),
      ),
    );
  }
}
