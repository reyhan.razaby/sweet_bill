import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';

import '../../../../bloc/bill_form_cost/bill_form_cost_bloc.dart';
import '../bottom_container.dart';
import 'bottoms/custom_amount_bottom.dart';
import 'bottoms/equally_bottom.dart';

class BottomSection extends StatelessWidget {
  const BottomSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomContainer(
      child:
          BlocSelector<BillFormCostBloc, BillFormCostState, BillFormCostState>(
        selector: (state) => state,
        builder: (_, state) {
          if (state is! BillFormCostLoaded) {
            return Container();
          }

          switch (state.splitStrategy) {
            case SplitStrategies.equally:
              return EquallyBottom(state: state);
            default:
              return CustomAmountBottom(state: state);
          }
        },
      ),
    );
  }
}
