import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/components/check_box/default_check_box.dart';
import 'package:sweet_bill/presentation/components/person/person_circle_avatar.dart';
import 'package:sweet_bill/presentation/components/text_fields/calculator_text_field.dart';

class CostTile extends StatelessWidget {
  final double hPadding;
  final int tileIndex;
  final GlobalKey tileKey = GlobalKey();

  CostTile({
    Key? key,
    required this.hPadding,
    required this.tileIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentState = getCostBloc(context).state;
    if (currentState is! BillFormCostLoaded) {
      return Container();
    }
    final memberCost = currentState.costs[tileIndex];

    return GestureDetector(
      onTap: () {
        final currentState = getCostBloc(context).state;
        bool clickable = currentState is BillFormCostLoaded &&
            currentState.splitStrategy == SplitStrategies.equally;
        if (clickable) {
          addCostBlocEvent(context, ToggleInvolved(index: tileIndex));
        }
      },
      child: Container(
        key: tileKey,
        padding: EdgeInsets.symmetric(
          horizontal: hPadding,
        ),
        constraints: const BoxConstraints(
          minHeight: 72,
        ),
        color: ColorSet.darkest,
        child: Row(
          children: [
            Expanded(
              child: MemberInfo(
                memberCost: memberCost,
                tileIndex: tileIndex,
              ),
            ),
            Container(
              constraints: const BoxConstraints(maxWidth: 180),
              child: BlocSelector<BillFormCostBloc, BillFormCostState,
                  SplitStrategies>(
                selector: (state) {
                  return (state as BillFormCostLoaded).splitStrategy;
                },
                builder: (_, splitStrategy) {
                  if (splitStrategy == SplitStrategies.equally) {
                    return EquallyDisplay(
                      memberCost: memberCost,
                      tileIndex: tileIndex,
                    );
                  }
                  return CustomAmountDisplay(
                    memberCost: memberCost,
                    tileIndex: tileIndex,
                    tileKey: tileKey,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  BillFormCostBloc getCostBloc(BuildContext context) =>
      BlocProvider.of<BillFormCostBloc>(context);

  void addCostBlocEvent(BuildContext context, BillFormCostEvent event) {
    getCostBloc(context).add(event);
  }
}

class MemberInfo extends StatelessWidget {
  final MemberCost memberCost;
  final int tileIndex;

  const MemberInfo({
    Key? key,
    required this.memberCost,
    required this.tileIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        BlocSelector<BillFormCostBloc, BillFormCostState, InvolveSelectorState>(
          selector: (state) {
            const defaultState =
                InvolveSelectorState(show: false, isInvolved: false);
            if (state is! BillFormCostLoaded ||
                state.splitStrategy != SplitStrategies.equally) {
              return defaultState;
            }
            return InvolveSelectorState(
              show: true,
              isInvolved: state.costs[tileIndex].isInvolved,
            );
          },
          builder: (context, state) {
            if (!state.show) {
              return Container();
            }
            return DefaultCheckBox(
              radius: 10,
              isChecked: state.isInvolved,
              margin: const EdgeInsets.only(right: 14),
            );
          },
        ),
        PersonCircleAvatar(
          emoji: memberCost.person.avatar.emoji,
          radius: 14,
          emojiSize: 14,
        ),
        const SizedBox(width: 10),
        Expanded(
          child: Text(
            memberCost.person.name,
            overflow: TextOverflow.ellipsis,
            style: const TextStyle(
              color: ColorSet.white,
              fontSize: 16,
            ),
          ),
        ),
      ],
    );
  }
}

class EquallyDisplay extends StatelessWidget {
  final MemberCost memberCost;
  final int tileIndex;

  const EquallyDisplay({
    Key? key,
    required this.tileIndex,
    required this.memberCost,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<BillFormCostBloc, BillFormCostState, double>(
      selector: (state) {
        if (state is! BillFormCostLoaded) return 0;
        return state.costs[tileIndex].cost;
      },
      builder: (context, cost) {
        return Text(
          NumberUtils.getCurrency(cost, showSymbol: true),
          textAlign: TextAlign.right,
          style: const TextStyle(color: ColorSet.white, fontSize: 15),
        );
      },
    );
  }
}

class CustomAmountDisplay extends StatelessWidget {
  final MemberCost memberCost;
  final int tileIndex;
  final GlobalKey tileKey;
  final ValueNotifier<double> fieldValueChanger = ValueNotifier<double>(0);

  CustomAmountDisplay({
    required this.memberCost,
    required this.tileIndex,
    required this.tileKey,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final initialValue = memberCost.cost;
    fieldValueChanger.value = initialValue;

    return BlocListener<BillFormCostBloc, BillFormCostState>(
      listenWhen: (prev, current) {
        if (prev is BillFormCostLoaded && current is BillFormCostLoaded) {
          return prev.costs[tileIndex].cost != current.costs[tileIndex].cost;
        }
        return false;
      },
      listener: (context, state) {
        if (state is BillFormCostLoaded) {
          fieldValueChanger.value = state.costs[tileIndex].cost;
        }
      },
      child: CalculatorTextField(
        fontSize: 15,
        initialValue: initialValue,
        onChanged: (double value) {
          getCostBloc(context)
              .add(ChangeCostAmount(cost: value, index: tileIndex));
        },
        onFocusChanged: (hasFocus) {
          if (hasFocus) {
            getCostBloc(context).add(FocusAmountInput(index: tileIndex));
          }
        },
        contentPadding: const EdgeInsets.symmetric(vertical: 6),
        valueChanger: fieldValueChanger,
        containerKey: tileKey,
      ),
    );
  }

  BillFormCostBloc getCostBloc(BuildContext context) =>
      BlocProvider.of<BillFormCostBloc>(context);
}

class InvolveSelectorState extends Equatable {
  final bool show;
  final bool isInvolved;

  const InvolveSelectorState({
    required this.show,
    required this.isInvolved,
  });

  @override
  List<Object> get props => [show, isInvolved];
}
