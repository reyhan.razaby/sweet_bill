import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'cost_tile.dart';

class CostList extends StatelessWidget {
  final double hPadding;

  const CostList({
    required this.hPadding,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BillFormCostState state = BlocProvider.of<BillFormCostBloc>(context).state;

    if (state is! BillFormCostLoaded) {
      return Container();
    }

    List<Widget> children = [];
    for (int index = 0; index < state.costs.length; index++) {
      children.add(CostTile(
        hPadding: hPadding,
        tileIndex: index,
      ));
    }

    return ListView.separated(
      itemCount: state.costs.length,
      itemBuilder: (context, index) {
        return CostTile(
          hPadding: hPadding,
          tileIndex: index,
        );
      },
      separatorBuilder: (context, index) {
        return Divider(
          indent: hPadding,
          endIndent: hPadding,
          height: 0,
          color: Colors.grey.withOpacity(0.5),
        );
      },
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
    );
  }
}
