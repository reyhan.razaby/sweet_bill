import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/components/buttons/menu_button.dart';

class TopToolbar extends StatelessWidget {
  final double hPadding;

  const TopToolbar({
    required this.hPadding,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorSet.dark,
      height: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: hPadding, vertical: 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text('Split'),
          const SizedBox(width: 6),
          BlocSelector<BillFormCostBloc, BillFormCostState, SplitStrategies>(
            selector: (state) {
              return (state as BillFormCostLoaded).splitStrategy;
            },
            builder: (_, currentSplitStrategy) {
              List<SplitStrategies> strategies = [
                SplitStrategies.equally,
                SplitStrategies.custom,
              ];
              return MenuButton(
                text: currentSplitStrategy.text,
                maxWidth: 160,
                size: MenuButtonSize.small,
                items: strategies
                    .map(
                      (s) => buildMenuButtonItem(
                          context, s, s == currentSplitStrategy),
                    )
                    .toList(),
              );
            },
          ),
        ],
      ),
    );
  }

  MenuButtonItem buildMenuButtonItem(
    context,
    SplitStrategies strategy,
    bool isSelected,
  ) {
    return MenuButtonItem(
      child: Row(
        children: [
          Text(strategy.text),
          if (isSelected) const Icon(Icons.check, color: ColorSet.accent),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
      onSelect: () {
        addCostBlocEvent(
          context,
          ChangeSplitStrategy(splitStrategy: strategy),
        );
      },
    );
  }

  BillFormCostBloc getCostBloc(BuildContext context) =>
      BlocProvider.of<BillFormCostBloc>(context);

  void addCostBlocEvent(BuildContext context, BillFormCostEvent event) {
    getCostBloc(context).add(event);
  }
}
