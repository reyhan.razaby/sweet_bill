import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/components/buttons/button_constants.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';

import 'bottom.dart';

class EquallyBottom extends Bottom {
  const EquallyBottom({
    required super.state,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double amountPerPerson;
    try {
      amountPerPerson = state.costs.firstWhere((el) => el.isInvolved).cost;
    } catch (_) {
      amountPerPerson = 0;
    }

    return Row(
      children: [
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                NumberUtils.getCurrency(amountPerPerson, showSymbol: true),
                style: const TextStyle(
                  color: ColorSet.light,
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
              const Text(
                '/person',
                style: TextStyle(
                  color: ColorSet.lightGrey,
                  height: 1.35,
                ),
              )
            ],
          ),
        ),
        buildAllSelectorButton(),
        const SizedBox(width: 8),
        buildSaveButton(context),
      ],
    );
  }

  BillFormCostBloc getCostBloc(BuildContext context) =>
      BlocProvider.of<BillFormCostBloc>(context);

  void addCostBlocEvent(BuildContext context, BillFormCostEvent event) {
    getCostBloc(context).add(event);
  }

  Widget buildAllSelectorButton() {
    return BlocSelector<BillFormCostBloc, BillFormCostState, AllCheckBoxState>(
      selector: (state) {
        BillFormCostLoaded loadedState = state as BillFormCostLoaded;
        final length = loadedState.costs.length;
        if (loadedState.splitStrategy != SplitStrategies.equally ||
            length <= 1) {
          return const AllCheckBoxState(
              involvedRate: 0, isAllInvolved: false, show: false);
        }

        final involvedLength =
            loadedState.costs.where((e) => e.isInvolved).length;
        return AllCheckBoxState(
          involvedRate: involvedLength / length,
          isAllInvolved: involvedLength == length,
          show: true,
        );
      },
      builder: (context, allCheckBoxState) {
        if (!allCheckBoxState.show) {
          return Container();
        }
        bool selectAll = !allCheckBoxState.isAllInvolved &&
            allCheckBoxState.involvedRate <= 0.5;
        return DefaultButton(
          type: ButtonType.outlined,
          horizontalPadding: selectAll ? 14 : 11,
          onPressed: () {
            addCostBlocEvent(
              context,
              ChangeInvolvedAll(
                isAllInvolved: selectAll,
              ),
            );
          },
          label: selectAll ? 'Select all' : 'Unselect all',
        );
      },
    );
  }
}

class AllCheckBoxState extends Equatable {
  final double involvedRate;
  final bool isAllInvolved;
  final bool show;

  const AllCheckBoxState({
    required this.involvedRate,
    required this.isAllInvolved,
    required this.show,
  });

  @override
  List<Object?> get props => [involvedRate, isAllInvolved, show];
}
