import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/mixins/adjustable_amount_mixin.dart';

import '../../../../../components/keyboards/calculator/calculator_visibility_builder.dart';
import 'bottom.dart';

class CustomAmountBottom extends Bottom with AdjustableAmountMixin {
  const CustomAmountBottom({
    required super.state,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: buildTotalDetail(
            actualTotal: state.totalCost,
            expectedTotal: state.expectedTotalCost,
          ),
        ),
        const SizedBox(width: 6),
        CalculatorVisibilityBuilder(
          builder: (BuildContext context, bool isVisible) {
            if (!isVisible || state.totalCostStatus.isExact) {
              return buildSaveButton(context);
            }

            return buildAdjustmentButton(
              actualTotal: state.totalCost,
              expectedTotal: state.expectedTotalCost,
              onPressed: () {
                getFormCostBloc(context).add(AdjustFocusedCostToExact());
              },
              isAdjustable: state.isAdjustable,
            );
          },
        ),
      ],
    );
  }

  BillFormCostBloc getFormCostBloc(BuildContext context) {
    return BlocProvider.of<BillFormCostBloc>(context);
  }
}
