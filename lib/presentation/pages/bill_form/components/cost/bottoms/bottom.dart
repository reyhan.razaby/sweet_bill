import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';

abstract class Bottom extends StatelessWidget   {
  final BillFormCostLoaded state;

  const Bottom({
    required this.state,
    Key? key,
  }) : super(key: key);

  Widget buildSaveButton(BuildContext context) {
    return SizedBox(
      width: 90,
      child: DefaultButton(
        onPressed: () {
          BlocProvider.of<BillFormCostBloc>(context).add(SaveCost());
          Navigator.of(context).pop();
        },
        label: 'Save',
      ),
    );
  }
}
