import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/presentation/components/buttons/button_constants.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';

mixin AdjustableAmountMixin {
  Widget buildTotalDetail(
      {required double actualTotal, required double expectedTotal}) {
    String actualTotalStr =
        NumberUtils.getCurrency(actualTotal, showSymbol: true);
    String expectedTotalStr =
        NumberUtils.getCurrency(expectedTotal, showSymbol: true);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const Text(
          'Total:',
          style: TextStyle(
            fontSize: 11,
            color: ColorSet.lightGrey,
          ),
        ),
        FittedBox(
          child: Text(
            actualTotalStr,
            style: const TextStyle(
              fontSize: 16,
              color: ColorSet.light,
              fontWeight: FontWeight.bold,
              height: 1.4,
            ),
          ),
        ),
        Text(
          'of $expectedTotalStr',
          style: const TextStyle(
            fontSize: 11,
            fontWeight: FontWeight.w500,
            color: ColorSet.lightGrey,
            height: 1.5,
          ),
        ),
      ],
    );
  }

  Widget buildAdjustmentButton({
    required double actualTotal,
    required double expectedTotal,
    required VoidCallback onPressed,
    required bool isAdjustable,
  }) {
    final remaining = expectedTotal - actualTotal;
    final remainingVal =
        NumberUtils.getCurrency(remaining.abs(), showSymbol: true);
    final remainingText =
        remaining > 0 ? '$remainingVal left' : 'Over by $remainingVal';
    final buttonText = remaining > 0 ? '+ Remains' : '– Excess';

    return Container(
      constraints: const BoxConstraints(maxWidth: 128, minWidth: 68),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FittedBox(
            child: Text(
              remainingText,
              style: const TextStyle(
                fontSize: 10,
                color: ColorSet.red,
              ),
            ),
          ),
          DefaultButton(
            type: ButtonType.outlined,
            onPressed: onPressed,
            child: Text(
              buttonText,
              style: const TextStyle(fontSize: 12),
              textAlign: TextAlign.center,
            ),
            padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 0),
            isDisabled: !isAdjustable,
          ),
        ],
      ),
    );
  }

  Text buildLabel(String text) {
    return Text(
      text,
      style: const TextStyle(
        color: ColorSet.lightGrey,
        fontWeight: FontWeight.w500,
        fontSize: 11,
      ),
    );
  }
}
