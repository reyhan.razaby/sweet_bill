import 'package:flutter/cupertino.dart';
import 'package:sweet_bill/core/constants/color_set.dart';

class BottomContainer extends StatelessWidget {
  static const double height = 80;
  final Widget child;

  const BottomContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      padding: const EdgeInsets.symmetric(horizontal: 14),
      decoration: const BoxDecoration(
        border: Border(top: BorderSide(color: ColorSet.grey, width: 0.3)),
        color: ColorSet.darkest,
      ),
      child: child,
    );
  }
}
