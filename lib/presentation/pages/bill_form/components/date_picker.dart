import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/sections/main_section.dart';

import '../../../../core/constants/color_set.dart';
import '../../../bloc/bill_form/bill_form_bloc.dart';
import '../../../themes/theme_data.dart';

class DatePicker extends StatelessWidget {
  const DatePicker({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const FormLabel('Date'),
        GestureDetector(
          onTap: () async {
            final DateTime now = DateTime.now();
            DateTime? selectedDateTime = await showDatePicker(
              context: context,
              initialDate: _getBillFormBloc(context).state.bill.transactionTime,
              firstDate: now.subtract(const Duration(days: 365 * 10)),
              lastDate: DateTime(now.year + 2),
              builder: datePickerThemeBuilder(context),
            );
            if (selectedDateTime != null) {
              _getBillFormBloc(context)
                  .add(ChangeForm(transactionTime: selectedDateTime));
            }
          },
          child: Container(
            padding: const EdgeInsets.only(bottom: 10, left: 2),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(color: ColorSet.grey.withOpacity(.3)),
              ),
            ),
            child: Row(
              children: [
                const Icon(
                  Icons.calendar_month,
                  color: ColorSet.white,
                  size: 22,
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: BlocSelector<BillFormBloc, BillFormState, DateTime>(
                    selector: (state) => state.bill.transactionTime,
                    builder: (context, transactionTime) {
                      return Text(
                        _buildDatePreview(context, transactionTime),
                        style: const TextStyle(fontSize: 16),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  String _buildDatePreview(context, DateTime transactionTime) {
    return DateFormat('EEE, dd MMMM yyyy').format(transactionTime);
  }

  BillFormBloc _getBillFormBloc(BuildContext context) =>
      BlocProvider.of<BillFormBloc>(context);
}
