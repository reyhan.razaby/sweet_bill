import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/color_set.dart';

class Copier extends StatelessWidget {
  const Copier({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {},
      icon: const Icon(
        Icons.copy,
        color: ColorSet.lightGrey,
      ),
    );
  }
}
