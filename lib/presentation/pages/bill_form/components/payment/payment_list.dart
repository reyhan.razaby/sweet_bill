import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_payment/bill_form_payment_bloc.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/payment/payment_tile.dart';

class PaymentList extends StatelessWidget {
  final double hPadding;

  const PaymentList({
    required this.hPadding,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    BillFormPaymentState state =
        BlocProvider.of<BillFormPaymentBloc>(context).state;

    if (state is! BillFormPaymentLoaded) {
      return Container();
    }

    return ListView.separated(
      itemCount: state.payments.length,
      itemBuilder: (context, index) {
        return PaymentTile(
          hPadding: hPadding,
          tileIndex: index,
        );
      },
      separatorBuilder: (context, index) {
        return Divider(
          indent: hPadding,
          endIndent: hPadding,
          height: 0,
          color: Colors.grey.withOpacity(0.5),
        );
      },
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
    );
  }
}
