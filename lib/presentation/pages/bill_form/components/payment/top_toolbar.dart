import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_payment/bill_form_payment_bloc.dart';
import 'package:sweet_bill/presentation/components/switches/default_switch.dart';

class TopToolbar extends StatelessWidget {
  final double hPadding;

  static const double radioButtonMargin = 2;

  const TopToolbar({
    required this.hPadding,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorSet.dark,
      padding: EdgeInsets.symmetric(horizontal: hPadding, vertical: 18),
      child: Row(
        children: [
          BlocSelector<BillFormPaymentBloc, BillFormPaymentState, bool>(
            selector: (state) {
              return (state as BillFormPaymentLoaded).isMultiplePayers;
            },
            builder: (context, isMultiplePayers) {
              return DefaultSwitch(
                value: isMultiplePayers,
                onChanged: (_) {
                  getPaymentBloc(context).add(ToggleMultiplePayers());
                },
              );
            },
          ),
          const SizedBox(width: 8),
          const Text(
            'Multiple payers',
            style: TextStyle(fontSize: 13),
          ),
        ],
      ),
    );
  }

  BillFormPaymentBloc getPaymentBloc(BuildContext context) =>
      BlocProvider.of<BillFormPaymentBloc>(context);
}
