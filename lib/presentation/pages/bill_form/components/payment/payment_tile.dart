import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/components/text_fields/calculator_text_field.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/payment/top_toolbar.dart';

import '../../../../../core/constants/color_set.dart';
import '../../../../bloc/bill_form_payment/bill_form_payment_bloc.dart';
import '../../../../components/person/person_circle_avatar.dart';
import '../../../../components/radio_buttons/circle_radio_button.dart';

class PaymentTile extends StatelessWidget {
  final double hPadding;
  final int tileIndex;
  final GlobalKey tileKey = GlobalKey();

  PaymentTile({
    Key? key,
    required this.hPadding,
    required this.tileIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final currentState = getPaymentBloc(context).state;
    if (currentState is! BillFormPaymentLoaded) {
      return Container();
    }
    final memberPayment = currentState.payments[tileIndex];

    return GestureDetector(
      onTap: () {
        final currentState = getPaymentBloc(context).state;
        bool clickable = currentState is BillFormPaymentLoaded &&
            !currentState.isMultiplePayers;
        if (clickable) {
          getPaymentBloc(context).add(SelectPayer(index: tileIndex));
        }
      },
      child: Container(
        key: tileKey,
        padding: EdgeInsets.symmetric(
          horizontal: hPadding,
        ),
        constraints: const BoxConstraints(
          minHeight: 72,
        ),
        color: ColorSet.darkest,
        child: Row(
          children: [
            Expanded(
              child: NameAndAvatar(memberPayment: memberPayment),
            ),
            Container(
              constraints: const BoxConstraints(maxWidth: 180),
              child:
                  BlocSelector<BillFormPaymentBloc, BillFormPaymentState, bool>(
                selector: (state) {
                  return (state as BillFormPaymentLoaded).isMultiplePayers;
                },
                builder: (context, isMultiplePayers) {
                  if (isMultiplePayers) {
                    return AmountInput(
                      tileIndex: tileIndex,
                      tileKey: tileKey,
                    );
                  }
                  return PayerChooser(
                    tileIndex: tileIndex,
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  BillFormPaymentBloc getPaymentBloc(BuildContext context) =>
      BlocProvider.of<BillFormPaymentBloc>(context);
}

class PayerChooser extends StatelessWidget {
  final int tileIndex;

  const PayerChooser({
    Key? key,
    required this.tileIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocSelector<BillFormPaymentBloc, BillFormPaymentState, bool>(
      selector: (state) {
        if (state is! BillFormPaymentLoaded) return false;
        return state.payments[tileIndex].isPayer;
      },
      builder: (context, isPayer) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (isPayer)
              const Text(
                "Me  ",
                style: TextStyle(
                  color: ColorSet.accent,
                  fontWeight: FontWeight.bold,
                ),
              ),
            CircleRadioButton(
              isSelected: isPayer,
              margin: const EdgeInsets.symmetric(
                horizontal: TopToolbar.radioButtonMargin,
              ),
            ),
          ],
        );
      },
    );
  }
}

class AmountInput extends StatelessWidget {
  final int tileIndex;
  final GlobalKey tileKey;
  final ValueNotifier<double> fieldValueChanger = ValueNotifier<double>(0);

  AmountInput({
    required this.tileIndex,
    required this.tileKey,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final state = getPaymentBloc(context).state;
    if (state is! BillFormPaymentLoaded) return Container();

    final memberPayment = state.payments[tileIndex];
    final initialValue = memberPayment.amount;
    fieldValueChanger.value = initialValue;

    return BlocListener<BillFormPaymentBloc, BillFormPaymentState>(
      listenWhen: (prev, current) {
        if (prev is BillFormPaymentLoaded && current is BillFormPaymentLoaded) {
          return prev.payments[tileIndex].amount !=
              current.payments[tileIndex].amount;
        }
        return false;
      },
      listener: (context, state) {
        if (state is BillFormPaymentLoaded) {
          fieldValueChanger.value = state.payments[tileIndex].amount;
        }
      },
      child: CalculatorTextField(
        fontSize: 15,
        initialValue: initialValue,
        onChanged: (val) {
          getPaymentBloc(context)
              .add(ChangeAmount(amount: val, index: tileIndex));
        },
        onFocusChanged: (hasFocus) {
          if (hasFocus) {
            getPaymentBloc(context).add(FocusAmountInput(index: tileIndex));
          }
        },
        contentPadding: const EdgeInsets.symmetric(vertical: 6),
        valueChanger: fieldValueChanger,
        containerKey: tileKey,
      ),
    );
  }

  BillFormPaymentBloc getPaymentBloc(BuildContext context) =>
      BlocProvider.of<BillFormPaymentBloc>(context);
}

class NameAndAvatar extends StatelessWidget {
  final MemberPayment memberPayment;

  const NameAndAvatar({
    Key? key,
    required this.memberPayment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            PersonCircleAvatar(
              emoji: memberPayment.person.avatar.emoji,
              radius: 14,
              emojiSize: 14,
            ),
            const SizedBox(width: 10),
            Expanded(
              child: Text(
                memberPayment.person.name,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  color: ColorSet.white,
                  fontSize: 16,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
