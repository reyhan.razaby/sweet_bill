import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/payment/bottoms/multiple_payers_bottom.dart';

import '../../../../bloc/bill_form_payment/bill_form_payment_bloc.dart';
import '../bottom_container.dart';
import 'bottoms/single_payer_bottom.dart';

class BottomSection extends StatelessWidget {
  const BottomSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomContainer(
      child: BlocSelector<BillFormPaymentBloc, BillFormPaymentState,
          BillFormPaymentState>(
        selector: (state) => state,
        builder: (_, state) {
          if (state is! BillFormPaymentLoaded) {
            return Container();
          }

          if (state.isMultiplePayers) {
            return MultiplePayersBottom(state: state);
          } else {
            return SinglePayerBottom(state: state);
          }
        },
      ),
    );
  }
}
