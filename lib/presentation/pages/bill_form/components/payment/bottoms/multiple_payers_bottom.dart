import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_payment/bill_form_payment_bloc.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/calculator_visibility_builder.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/mixins/adjustable_amount_mixin.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/payment/bottoms/bottom.dart';

class MultiplePayersBottom extends Bottom with AdjustableAmountMixin {
  const MultiplePayersBottom({
    required super.state,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: buildTotalDetail(
            actualTotal: state.totalPayment,
            expectedTotal: state.expectedTotalPayment,
          ),
        ),
        const SizedBox(width: 6),
        CalculatorVisibilityBuilder(
          builder: (context, isVisible) {
            if (!isVisible || state.totalPaymentStatus.isExact) {
              return buildSaveButton(context);
            }

            return buildAdjustmentButton(
              actualTotal: state.totalPayment,
              expectedTotal: state.expectedTotalPayment,
              onPressed: () {
                getFormPaymentBloc(context).add(AdjustFocusedPaymentToExact());
              },
              isAdjustable: state.isAdjustable,
            );
          },
        ),
      ],
    );
  }

  BillFormPaymentBloc getFormPaymentBloc(BuildContext context) {
    return BlocProvider.of<BillFormPaymentBloc>(context);
  }
}
