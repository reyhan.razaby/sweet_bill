import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_payment/bill_form_payment_bloc.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';

abstract class Bottom extends StatelessWidget {
  final BillFormPaymentLoaded state;

  const Bottom({
    required this.state,
    Key? key,
  }) : super(key: key);

  Widget buildSaveButton(BuildContext context) {
    return SizedBox(
      width: 110,
      child: DefaultButton(
        onPressed: () {
          BlocProvider.of<BillFormPaymentBloc>(context).add(SavePayment());
          Navigator.of(context).pop();
        },
        label: 'Save',
      ),
    );
  }
}
