import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_payment/bill_form_payment_bloc.dart';

import 'bottom.dart';

class SinglePayerBottom extends Bottom {
  const SinglePayerBottom({
    required super.state,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MemberPayment? payment = state.payments.firstWhereOrNull((p) => p.isPayer);
    if (payment == null) {
      return Container();
    }
    const textStyle = TextStyle(fontSize: 14, color: ColorSet.lightGrey);
    return Row(
      children: [
        Expanded(
          flex: 5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: Text(
                      payment.person.name.replaceAll("", "\u{200B}"),
                      style: textStyle,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const Text(
                    ' paid',
                    style: textStyle,
                  ),
                ],
              ),
              Text(
                NumberUtils.getCurrency(payment.amount, showSymbol: true),
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                  color: ColorSet.light,
                  height: 1.45,
                ),
              ),
            ],
          ),
        ),
        buildSaveButton(context),
      ],
    );
  }
}
