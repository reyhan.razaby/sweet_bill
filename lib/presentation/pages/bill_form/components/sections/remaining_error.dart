import 'package:flutter/material.dart';

import '../../../../../core/constants/color_set.dart';
import '../../../../../core/utils/number_utils.dart';

class RemainingError extends StatelessWidget {
  final double amount;

  const RemainingError({
    Key? key,
    required this.amount,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const style = TextStyle(
      color: ColorSet.red,
      fontSize: 12,
      height: 1.3,
    );

    if (amount > 0) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(NumberUtils.getCurrency(amount, showSymbol: false),
              style: style),
          const Text('left', style: style),
        ],
      );
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          const Text('Over by', style: style),
          Text(NumberUtils.getCurrency(amount.abs(), showSymbol: false),
              style: style),
        ],
      );
    }
  }
}
