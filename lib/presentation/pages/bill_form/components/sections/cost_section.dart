import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/core/utils/list_utils.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/sections/remaining_error.dart';

import '../../../../../core/constants/color_set.dart';
import '../../../../../core/utils/number_utils.dart';
import '../../../../../domain/entities/person/person.dart';
import '../../../../bloc/bill_form/bill_form_bloc.dart';
import 'base_section.dart';

class CostSection extends BaseSection {
  final VoidCallback _onTap;

  const CostSection({
    Key? key,
    required VoidCallback onTap,
  })  : _onTap = onTap,
        super(key: key);

  @override
  buildBody(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: _onTap,
      child: Row(
        children: [
          const PrefixIcon(icon: Icons.receipt_long),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocSelector<BillFormBloc, BillFormState, SplitStrategies>(
                  selector: (state) => state.bill.splitStrategy,
                  builder: (_, splitStrategy) => Text(
                    'Split ${splitStrategy.text}',
                    style: const TextStyle(
                      color: ColorSet.white,
                      fontSize: 14,
                    ),
                  ),
                ),
                const SizedBox(height: 4),
                BlocSelector<BillFormBloc, BillFormState,
                    List<BillPersonalCost>>(
                  selector: (state) => state.bill.personalCosts,
                  builder: (_, costs) => Text(
                    _buildParticipants(context, costs),
                    style: const TextStyle(
                      color: ColorSet.grey,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
          ),
          BlocSelector<BillFormBloc, BillFormState, BillFormState>(
            selector: (state) => state,
            builder: (_, state) {
              if (state.bill.splitStrategy == SplitStrategies.equally) {
                final costs = state.bill.personalCosts;
                return AmountPerPersonDisplay(
                  amountPerPerson: costs.isEmpty ? 0 : costs[0].totalCost,
                );
              }

              if (state.remainingCost != 0) {
                return RemainingError(amount: state.remainingCost);
              }
              return Container();
            },
          ),
          const ChevronRight(),
        ],
      ),
    );
  }

  String _buildParticipants(context, List<BillPersonalCost> costs) {
    final members = _getBillFormBloc(context).members;
    const emptyText = '(no one involved)';

    // Empty case
    if (costs.isEmpty) {
      return emptyText;
    }

    // Only 1 participant
    if (costs.length == 1) {
      Person? person =
          members.firstWhereOrNull((e) => e.id == costs[0].personId);
      if (person == null) {
        return emptyText;
      }
      if (isSelf(person, context)) {
        return 'Only you';
      }
      return 'Only for ${person.name}';
    }

    final memberIds = members.map((e) => e.id).toList()..sort();
    final participantIds = costs.map((e) => e.personId).toList()..sort();
    if (ListUtils.isEqualsIgnoreOrder(memberIds, participantIds)) {
      return 'for all members';
    }
    return 'for ${costs.length} people';
  }

  BillFormBloc _getBillFormBloc(context) {
    return BlocProvider.of<BillFormBloc>(context);
  }

  ProfileCubit _getProfileCubit(context) {
    return BlocProvider.of<ProfileCubit>(context);
  }

  bool isSelf(Person person, context) {
    var profileState = _getProfileCubit(context).state;
    return profileState is ProfileAdded && profileState.self.id == person.id;
  }
}

class AmountPerPersonDisplay extends StatelessWidget {
  final double amountPerPerson;

  const AmountPerPersonDisplay({
    Key? key,
    required this.amountPerPerson,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          NumberUtils.getCurrency(amountPerPerson, showSymbol: true),
          style: const TextStyle(
            color: ColorSet.white,
            fontSize: 13,
            fontWeight: FontWeight.bold,
          ),
        ),
        const Text(
          '/person',
          style: TextStyle(
            color: ColorSet.grey,
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}
