import 'package:flutter/material.dart';

import '../../../../../core/constants/color_set.dart';

abstract class BaseSection extends StatelessWidget {
  const BaseSection({Key? key}) : super(key: key);

  static const EdgeInsets _sectionMargin = EdgeInsets.only(top: 15);
  static const double _sectionHPadding = 16;
  static const double _sectionVPadding = 18;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: _sectionMargin,
      color: ColorSet.darkest,
      padding: const EdgeInsets.symmetric(
        horizontal: _sectionHPadding,
        vertical: _sectionVPadding,
      ),
      child: buildBody(context),
    );
  }

  Widget buildBody(BuildContext context);
}

class ChevronRight extends StatelessWidget {
  const ChevronRight({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Icon(
      Icons.chevron_right,
      color: ColorSet.grey,
      size: 36,
    );
  }
}

class PrefixIcon extends StatelessWidget {
  final IconData icon;

  const PrefixIcon({Key? key, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 14),
      child: Icon(
        icon,
        color: ColorSet.white,
        size: 24,
      ),
    );
  }
}
