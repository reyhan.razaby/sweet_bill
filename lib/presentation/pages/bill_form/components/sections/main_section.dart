import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/text_fields/filled_text_field.dart';
import 'package:sweet_bill/presentation/pages/bill_form/amount_input_page.dart';

import '../../../../../core/constants/color_set.dart';
import '../../../../components/text_fields/plain_text_field.dart';
import '../icon_picker_dialog.dart';
import 'base_section.dart';

class MainSection extends BaseSection {
  final bool isEditing;

  const MainSection({
    Key? key,
    required this.isEditing,
  }) : super(key: key);

  @override
  buildBody(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        IconAndTitle(isEditing: isEditing),
        const GapSpace(),
        TotalCost(isEditing: isEditing),
      ],
    );
  }
}

class TotalCost extends StatelessWidget {
  final bool isEditing;

  const TotalCost({
    Key? key,
    required this.isEditing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const FormLabel('Total cost'),
        const SizedBox(height: 8),
        BlocSelector<BillFormBloc, BillFormState, double>(
          selector: (state) => state.totalCost,
          builder: (context, totalCost) {
            final profileState = getProfileCubit(context).state;
            int maxFractionDigits = profileState is ProfileAdded
                ? profileState.settings.maxFractionDigits
                : 0;

            return FilledTextField(
              onTap: () {
                showModalBottomSheet(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16),
                  ),
                  backgroundColor: ColorSet.darkest,
                  isScrollControlled: true,
                  context: context,
                  builder: (_) {
                    return AmountInputScreen(
                      initialValue: totalCost,
                      onDone: (value) {
                        getBillFormBloc(context)
                            .add(ChangeForm(totalCost: value));
                      },
                    );
                  },
                );
              },
              initialValue: getFormattedAmount(totalCost, maxFractionDigits),
              readOnly: true,
              enableInteractiveSelection: false,
              contentPadding: const EdgeInsets.symmetric(vertical: 10),
              fontSize: 22,
              isCollapsed: true,
              prefixIcon: buildAmountPrefix(),
            );
          },
        ),
      ],
    );
  }

  String getFormattedAmount(double amount, int maxFractionDigits) {
    return NumberUtils.getCurrency(amount, fractionDigits: maxFractionDigits);
  }

  Padding buildAmountPrefix() {
    return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Text(
        'Rp',
        style: TextStyle(
          color: ColorSet.grey,
          fontSize: 22,
        ),
      ),
    );
  }
}

class IconAndTitle extends StatelessWidget {
  final bool isEditing;

  static const double totalWidth = 48;
  static const double borderWidth = 4;
  static const double editButtonWidth = totalWidth * .35;

  const IconAndTitle({
    Key? key,
    required this.isEditing,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        /// Icon picker
        BlocSelector<BillFormBloc, BillFormState, BillIcons>(
          selector: (state) => state.bill.icon,
          builder: (context, icon) {
            return GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                FocusManager.instance.primaryFocus?.unfocus();
                showIconPickerDialog(context, icon);
              },
              child: Stack(
                children: [
                  CircleAvatar(
                    backgroundColor: ColorSet.light.withOpacity(.3),
                    radius: totalWidth / 2,
                    child: CircleAvatar(
                      backgroundColor: ColorSet.accent,
                      radius: totalWidth / 2 - borderWidth,
                      child: icon.widget(),
                    ),
                  ),
                  const Positioned(
                    right: 0,
                    bottom: 0,
                    child: CircleAvatar(
                      backgroundColor: ColorSet.darkest,
                      radius: editButtonWidth / 2 + 2,
                      child: CircleAvatar(
                        backgroundColor: ColorSet.light,
                        radius: editButtonWidth / 2,
                        child: Icon(
                          Icons.edit,
                          size: editButtonWidth / 2,
                          color: ColorSet.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),

        /// Gap
        const SizedBox(width: 16),

        /// Title input
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const FormLabel('Title'),
              PlainTextField(
                autofocus: !isEditing,
                isClearable: true,
                fontSize: 21,
                maxLines: 1,
                hintText: 'Restaurant',
                initialValue: isEditing
                    ? getBillFormBloc(context).state.bill.title
                    : null,
                onChanged: (val) {
                  getBillFormBloc(context).add(ChangeForm(title: val));
                },
                textCapitalization: TextCapitalization.sentences,
                inputFormatters: [
                  LengthLimitingTextInputFormatter(30),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  void showIconPickerDialog(BuildContext context, BillIcons icon) {
    showDialog(
        context: context,
        builder: (_) {
          return IconPickerDialog(
            selectedIcon: icon,
            onSelected: (selectedIcon) {
              getBillFormBloc(context).add(ChangeForm(icon: selectedIcon));
            },
          );
        });
  }
}

class GapSpace extends StatelessWidget {
  const GapSpace({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SizedBox(height: 18);
  }
}

class FormLabel extends StatelessWidget {
  final String text;

  const FormLabel(
    this.text, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: TextAlign.left,
      style: const TextStyle(
        color: ColorSet.white,
        fontSize: 12,
      ),
    );
  }
}

BillFormBloc getBillFormBloc(BuildContext context) =>
    BlocProvider.of<BillFormBloc>(context);

ProfileCubit getProfileCubit(BuildContext context) =>
    BlocProvider.of<ProfileCubit>(context);
