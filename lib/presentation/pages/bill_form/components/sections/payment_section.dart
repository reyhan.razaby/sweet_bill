import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/sections/remaining_error.dart';

import '../../../../../core/constants/color_set.dart';
import '../../../../../domain/entities/person/person.dart';
import '../../../../bloc/profile/profile_cubit.dart';
import 'base_section.dart';

class PaymentSection extends BaseSection {
  final VoidCallback _onTap;

  const PaymentSection({
    Key? key,
    required VoidCallback onTap,
  })  : _onTap = onTap,
        super(key: key);

  @override
  buildBody(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: _onTap,
      child: Row(
        children: [
          const PrefixIcon(icon: Icons.account_balance_wallet_outlined),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Paid by',
                  style: TextStyle(
                    color: ColorSet.white,
                    fontSize: 14,
                  ),
                ),
                const SizedBox(height: 4),
                BlocSelector<BillFormBloc, BillFormState,
                    List<BillPersonalPayment>>(
                  selector: (state) => state.bill.personalPayments,
                  builder: (_, payments) => Text(
                    _buildPayers(context, payments),
                    style: const TextStyle(
                      color: ColorSet.grey,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
          ),
          BlocSelector<BillFormBloc, BillFormState, BillFormState>(
            selector: (state) => state,
            builder: (_, state) {
              if (state.remainingPayment != 0) {
                return RemainingError(amount: state.remainingPayment);
              }
              return Container();
            },
          ),
          const ChevronRight(),
        ],
      ),
    );
  }

  String _buildPayers(context, List<BillPersonalPayment> payments) {
    const emptyDisplay = '(none)';
    final members = _getBillFormBloc(context).members;

    if (payments.isEmpty) {
      return emptyDisplay;
    }

    if (payments.length == 1) {
      Person? person =
          members.firstWhereOrNull((e) => e.id == payments[0].personId);
      if (person == null) {
        return emptyDisplay;
      }
      if (isSelf(person, context)) {
        return 'You';
      }
    }

    List<String> payers = payments
        .map((e) => _getPersonName(members, e.personId))
        .where((e) => e != '')
        .toList();
    return payers.isNotEmpty ? payers.join(', ') : emptyDisplay;
  }

  String _getPersonName(List<Person> members, String personId) {
    try {
      return members.firstWhere((el) => el.id == personId).name;
    } catch (_) {
      return '';
    }
  }

  BillFormBloc _getBillFormBloc(context) {
    return BlocProvider.of<BillFormBloc>(context);
  }

  ProfileCubit _getProfileCubit(context) {
    return BlocProvider.of<ProfileCubit>(context);
  }

  bool isSelf(Person person, context) {
    var profileState = _getProfileCubit(context).state;
    return profileState is ProfileAdded && profileState.self.id == person.id;
  }
}
