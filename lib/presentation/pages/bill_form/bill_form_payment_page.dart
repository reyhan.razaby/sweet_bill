import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/calculator_keyboard.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/calculator_keyboard_provider.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/payment/bottom_section.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/payment/payment_list.dart';

import '../../../core/constants/color_set.dart';
import '../../bloc/bill_form_payment/bill_form_payment_bloc.dart';
import '../../components/buttons/back_icon_button.dart';
import 'bill_form_persons_assignment_page.dart';
import 'components/payment/top_toolbar.dart';
import 'components/sliver_persistent_toolbar.dart';

class BillFormPaymentPage extends BillFormPersonsAssignmentPage {
  const BillFormPaymentPage({Key? key}) : super(key: key);

  @override
  Widget buildPage(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackIconButton(),
        title: const Text('Who paid the bill?'),
      ),
      backgroundColor: ColorSet.dark,
      body: BlocBuilder<BillFormPaymentBloc, BillFormPaymentState>(
        builder: (context, state) {
          if (state is BillFormPaymentLoading ||
              state is BillFormPaymentEmpty) {
            return const FullScreenLoading();
          } else if (state is BillFormPaymentLoaded) {
            return CalculatorKeyboardProvider(
              child: buildBody(context),
            );
          }
          return Container();
        },
      ),
    );
  }

  @override
  void loadData(BuildContext context) {
    getPaymentBloc(context).add(LoadPaymentInitialValues());
  }

  Widget buildBody(BuildContext context) {
    const double hPadding = 15;
    return Column(
      children: const [
        Expanded(
          child: CustomScrollView(
            physics: BouncingScrollPhysics(),
            slivers: [
              SliverPersistentHeader(
                floating: true,
                delegate: SliverPersistentToolbar(
                  height: 64,
                  child: TopToolbar(hPadding: hPadding),
                ),
              ),
              SliverToBoxAdapter(
                child: PaymentList(
                  hPadding: hPadding,
                ),
              ),
            ],
          ),
        ),
        BottomSection(),
        CalculatorKeyboard(),
      ],
    );
  }
}

class BillFormPaymentPageArgs {
  final BillFormBloc billFormBloc;

  const BillFormPaymentPageArgs({
    required this.billFormBloc,
  });
}
