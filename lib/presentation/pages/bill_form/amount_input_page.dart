import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/components/dialogs/confirmation_dialog.dart';
import 'package:sweet_bill/presentation/components/drag_bar.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/calculator_keyboard.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/calculator_keyboard_provider.dart';
import 'package:sweet_bill/presentation/components/text_fields/calculator_text_field.dart';

class AmountInputScreen extends StatefulWidget {
  final double initialValue;
  final ValueChanged<double> onDone;

  const AmountInputScreen({
    Key? key,
    required this.initialValue,
    required this.onDone,
  }) : super(key: key);

  @override
  State<AmountInputScreen> createState() => _AmountInputScreenState();
}

class _AmountInputScreenState extends State<AmountInputScreen> {
  late double currentValue;

  @override
  void initState() {
    super.initState();
    currentValue = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (currentValue != widget.initialValue) {
          showPopConfirmationDialog(context);
          return false;
        }
        return true;
      },
      child: SizedBox(
        height: MediaQuery.of(context).copyWith().size.height * 0.88,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const DragBar(),
            buildLabel(),
            Flexible(
              child: buildContent(),
            ),
          ],
        ),
      ),
    );
  }

  Container buildLabel() {
    const double hPadding = 20;
    return Container(
      padding: const EdgeInsets.fromLTRB(hPadding, 6, hPadding, 12),
      width: double.infinity,
      child: const Text(
        'Total cost',
        style: TextStyle(
          color: ColorSet.lightGrey,
          fontWeight: FontWeight.w500,
        ),
        textAlign: TextAlign.left,
      ),
    );
  }

  void showPopConfirmationDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return ConfirmationDialog(
          title: 'Discard changes?',
          positiveText: 'Discard',
          onPositiveClicked: () {
            Navigator.of(context).pop();
          },
          negativeText: 'Stay',
        );
      },
    );
  }

  Widget buildContent() {
    return CalculatorKeyboardProvider(
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: CalculatorTextField(
                initialValue: widget.initialValue,
                alwaysFocused: true,
                fontSize: 24,
                backgroundColor: Colors.transparent,
                onChanged: (value) {
                  currentValue = value;
                },
              ),
            ),
          ),
          CalculatorKeyboard.alwaysShow(
            onDone: (value) {
              widget.onDone(value);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
}
