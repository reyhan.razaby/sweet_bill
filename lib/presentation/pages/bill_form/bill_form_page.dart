import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/components/app_bars/creation_app_bar.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';

import '../../../core/constants/color_set.dart';
import '../../../core/constants/route_names.dart';
import '../../../domain/entities/bill/bill.dart';
import '../../bloc/bill_form/bill_form_bloc.dart';
import '../../components/buttons/button_constants.dart';
import '../../components/buttons/default_button.dart';
import '../../components/dialogs/confirmation_dialog.dart';
import '../../components/dialogs/message_dialog.dart';
import 'bill_form_cost_page.dart';
import 'bill_form_payment_page.dart';
import 'components/sections/cost_section.dart';
import 'components/sections/main_section.dart';
import 'components/sections/payment_section.dart';

class BillFormPage extends StatefulWidget {
  final BillFormPageArgs args;

  const BillFormPage({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<BillFormPage> createState() => _BillFormPageState();
}

class _BillFormPageState extends State<BillFormPage> {
  @override
  Widget build(BuildContext context) {
    String appBarTitle = widget.args.isEditing ? 'Edit bill' : 'Add bill';

    return WillPopScope(
      onWillPop: () async {
        onBack();
        return false;
      },
      child: Scaffold(
        appBar: CreationAppBar(
          body: Text(appBarTitle),
          onCancel: onBack,
        ),
        backgroundColor: ColorSet.dark,
        body: BlocConsumer<BillFormBloc, BillFormState>(
          listener: (_, state) {
            if (state is BillFormSaved) {
              Navigator.of(context).pop();
            }
          },
          builder: (_, state) {
            if (state is BillFormLoading || state is BillFormEmpty) {
              return const FullScreenLoading();
            }
            return SingleChildScrollView(
              child: Column(
                children: [
                  MainSection(isEditing: widget.args.isEditing),
                  CostSection(onTap: () => goToSplitPage()),
                  PaymentSection(onTap: () => goToPaymentPage()),
                ],
              ),
            );
          },
        ),
        bottomNavigationBar: Container(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 14),
          color: ColorSet.darkest,
          child: BlocSelector<BillFormBloc, BillFormState, bool>(
            selector: (state) => state.isValid(),
            builder: (context, isValid) {
              return DefaultButton(
                isDisabled: !isValid,
                onPressed: () {
                  var billFormBloc = getBillFormBloc(context);
                  if (totalCostTarget > 0) {
                    billFormBloc.add(SaveBill());
                  } else {
                    showInvalidTotalCostDialog();
                  }
                },
                label: 'Save bill',
                size: ButtonSize.large,
              );
            },
          ),
        ),
      ),
    );
  }

  void onBack() async {
    var initialLoadedState = getBillFormBloc(context).initialLoadedState;
    var currentState = getBillFormBloc(context).state;
    if (initialLoadedState != null && currentState != initialLoadedState) {
      showDialog(
        context: context,
        builder: (_) {
          return ConfirmationDialog(
            title: 'Discard changes?',
            positiveText: 'Discard',
            onPositiveClicked: () {
              Navigator.of(context).pop();
            },
            negativeText: 'Stay here',
          );
        },
      );
    } else {
      Navigator.of(context).pop();
    }
  }

  @override
  void initState() {
    super.initState();

    getBillFormBloc(context).add(
      LoadInitialValues(
        eventGroupId: widget.args.eventGroupId,
        bill: widget.args.bill,
      ),
    );
  }

  void showInvalidTotalCostDialog() {
    showInvalidDialog('Put total cost first');
  }

  void showInvalidDialog(String message) {
    showDialog(
        context: context,
        builder: (_) {
          return MessageDialog(title: message);
        });
  }

  double get totalCostTarget => getBillFormBloc(context).state.totalCost;

  void goToPaymentPage() {
    unFocus();
    Navigator.of(context).pushNamed(
      RouteNames.billFormPayment,
      arguments: BillFormPaymentPageArgs(
        billFormBloc: getBillFormBloc(context),
      ),
    );
  }

  void goToSplitPage() {
    unFocus();
    Navigator.of(context).pushNamed(
      RouteNames.billFormCost,
      arguments: BillFormCostPageArgs(
        billFormBloc: getBillFormBloc(context),
      ),
    );
  }

  void unFocus() {
    FocusManager.instance.primaryFocus?.unfocus();
  }

  BillFormBloc getBillFormBloc(BuildContext context) =>
      BlocProvider.of<BillFormBloc>(context);
}

class BillFormPageArgs {
  final String? eventGroupId;
  final Bill? bill;
  final bool isEditing;

  const BillFormPageArgs({
    this.eventGroupId,
    this.bill,
    this.isEditing = false,
  });
}
