import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/calculator_keyboard.dart';
import 'package:sweet_bill/presentation/components/keyboards/calculator/calculator_keyboard_provider.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/add_member_page.dart';
import 'package:sweet_bill/presentation/pages/bill_form/components/sliver_persistent_toolbar.dart';

import '../../../core/constants/color_set.dart';
import '../../bloc/bill_form_cost/bill_form_cost_bloc.dart';
import '../../bloc/friends/friends_bloc.dart';
import '../../components/buttons/back_icon_button.dart';
import 'bill_form_persons_assignment_page.dart';
import 'components/cost/bottom_section.dart';
import 'components/cost/cost_list.dart';
import 'components/cost/top_toolbar.dart';

class BillFormCostPage extends BillFormPersonsAssignmentPage {
  const BillFormCostPage({Key? key}) : super(key: key);

  @override
  Widget buildPage(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackIconButton(),
        title: const Text('How do you split the bill?'),
      ),
      backgroundColor: ColorSet.dark,
      body: BlocBuilder<BillFormCostBloc, BillFormCostState>(
        builder: (context, state) {
          if (state is BillFormCostLoading || state is BillFormCostEmpty) {
            return const FullScreenLoading();
          } else if (state is BillFormCostLoaded) {
            return CalculatorKeyboardProvider(
              child: buildBody(context),
            );
          }
          return Container();
        },
      ),
    );
  }

  @override
  void loadData(BuildContext context) {
    getCostBloc(context).add(LoadCostInitialValues());
  }

  Widget buildBody(BuildContext context) {
    const double hPadding = 15;
    return Column(
      children: const [
        Expanded(
          child: CustomScrollView(
            physics: BouncingScrollPhysics(),
            slivers: [
              SliverPersistentHeader(
                floating: true,
                delegate: SliverPersistentToolbar(
                  height: 64,
                  child: TopToolbar(hPadding: hPadding),
                ),
              ),
              SliverToBoxAdapter(
                child: CostList(
                  hPadding: hPadding,
                ),
              ),
            ],
          ),
        ),
        BottomSection(),
        CalculatorKeyboard(),
      ],
    );
  }

  VoidCallback onClickAddMember(context) {
    return () async {
      final value = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (_) => buildAddMemberPage(context),
        ),
      );

      if (value != null && value is Person) {
        getBillFormBloc(context).add(
          RegisterNewMember(
            newMember: value,
            onRegistered: (person) {
              getCostBloc(context).add(AddNewMember(member: person));
            },
          ),
        );
      }
    };
  }

  Widget buildAddMemberPage(context) {
    return MultiBlocProvider(
      child: const AddMemberPage(),
      providers: [
        BlocProvider.value(
          value: getBillFormBloc(context),
        ),
        BlocProvider.value(
          value: getFriendsBloc(context),
        ),
      ],
    );
  }

  FriendsBloc getFriendsBloc(context) => BlocProvider.of<FriendsBloc>(context);
}

class BillFormCostPageArgs {
  final BillFormBloc billFormBloc;

  const BillFormCostPageArgs({
    required this.billFormBloc,
  });
}
