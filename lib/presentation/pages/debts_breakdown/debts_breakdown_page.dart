import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/domain/entities/debt/debts_breakdown.dart';
import 'package:sweet_bill/presentation/components/errors/unexpected_error.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';
import 'package:sweet_bill/presentation/pages/debts_breakdown/breakdown_section.dart';
import 'package:sweet_bill/presentation/pages/debts_breakdown/debts_section.dart';

import '../../bloc/debts_breakdown/debts_breakdown_cubit.dart';
import '../../components/app_bars/default_app_bar.dart';

class DebtsBreakdownPage extends StatefulWidget {
  final DebtsBreakdownPageArgs args;

  const DebtsBreakdownPage({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<DebtsBreakdownPage> createState() => _DebtsBreakdownPageState();
}

class _DebtsBreakdownPageState extends State<DebtsBreakdownPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const DefaultAppBar(
        title: Text('Debts'),
      ),
      body: BlocBuilder<DebtsBreakdownCubit, DebtsBreakdownState>(
        builder: (context, state) {
          if (state is DebtsBreakdownLoading) {
            return const FullScreenLoading();
          } else if (state is DebtsBreakdownLoaded) {
            return _buildData(state.debtsBreakdown);
          }
          return UnexpectedError(onReload: _loadData);
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadData();
  }

  void _loadData() {
    BlocProvider.of<DebtsBreakdownCubit>(context)
        .loadDebtsBreakdown(args.eventGroupId);
  }

  DebtsBreakdownPageArgs get args => widget.args;

  Widget _buildData(DebtsBreakdown debtsBreakdown) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        children: [
          DebtsSection(debts: debtsBreakdown.debts),
          BreakdownSection(
              personalBreakdowns: debtsBreakdown.personalBreakdowns),
        ],
      ),
    );
  }
}

class DebtsBreakdownPageArgs {
  final String eventGroupId;

  const DebtsBreakdownPageArgs({required this.eventGroupId});
}
