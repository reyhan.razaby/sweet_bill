import 'package:flutter/material.dart';

import '../../../core/constants/color_set.dart';

class Separator extends StatelessWidget {
  const Separator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 0,
      color: ColorSet.grey.withOpacity(.6),
    );
  }
}
