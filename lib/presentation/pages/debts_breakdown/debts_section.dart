import 'package:flutter/material.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';

import '../../../domain/entities/debt/debt.dart';
import 'separator.dart';

class DebtsSection extends StatelessWidget {
  final List<Debt> debts;

  const DebtsSection({
    Key? key,
    required this.debts,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Separator(),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 14),
          child: Table(
            columnWidths: const {
              0: MaxColumnWidth(
                  FractionColumnWidth(.45), IntrinsicColumnWidth()),
              1: IntrinsicColumnWidth(),
            },
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            children: _buildRows(debts),
          ),
        ),
      ],
    );
  }

  List<TableRow> _buildRows(List<Debt> debts) {
    const double fontSize = 15;
    const double textHeight = 1.35;
    const double verticalPadding = 5;
    const textStyle = TextStyle(fontSize: fontSize, height: textHeight);

    List<TableRow> rows = [];

    for (Debt debt in debts) {
      final row = TableRow(
        children: [
          Padding(
            padding: const EdgeInsets.only(
              right: 8,
              top: verticalPadding,
              bottom: verticalPadding,
            ),
            child: Text(debt.desc, style: textStyle),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 8,
              top: verticalPadding,
              bottom: verticalPadding,
            ),
            child: Text(
              NumberUtils.getCurrency(debt.amount, showSymbol: true),
              style: textStyle.copyWith(fontWeight: FontWeight.bold),
              textAlign: TextAlign.right,
            ),
          ),
        ],
      );
      rows.add(row);
    }
    return rows;
  }
}
