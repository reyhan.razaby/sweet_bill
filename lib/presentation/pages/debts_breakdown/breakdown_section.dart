import 'package:flutter/material.dart';
import 'package:sweet_bill/core/utils/number_utils.dart';
import 'package:sweet_bill/presentation/pages/debts_breakdown/separator.dart';

import '../../../core/constants/color_set.dart';
import '../../../domain/entities/debt/debts_breakdown.dart';

class BreakdownSection extends StatelessWidget {
  final List<PersonalBreakdown> personalBreakdowns;

  const BreakdownSection({
    Key? key,
    required this.personalBreakdowns,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const double hPadding = 16;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Separator(),
        const SizedBox(height: 10),
        const Padding(
          padding: EdgeInsets.only(
            top: 12,
            bottom: 24,
            left: hPadding,
          ),
          child: Text(
            'Breakdown',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: hPadding),
          child: TableBreakdown(personalBreakdowns),
        ),
      ],
    );
  }
}

class TableBreakdown extends StatelessWidget {
  final List<PersonalBreakdown> breakdowns;

  const TableBreakdown(
    this.breakdowns, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: _buildRows(breakdowns),
    );
  }

  List<TableRow> _buildRows(List<PersonalBreakdown> breakdowns) {
    const double fontSize = 13;
    List<TableRow> rows = [];

    rows.add(_buildHeaderRow());

    for (PersonalBreakdown breakdown in breakdowns) {
      rows.add(TableRow(
        children: [
          _buildCell(
            Text(
              breakdown.person.name,
              style: const TextStyle(
                fontSize: fontSize + 1,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          _buildCell(
            buildMemberAmount(breakdown.totalCost, fontSize),
          ),
          _buildCell(
            buildMemberAmount(breakdown.totalPayment, fontSize),
          ),
          _buildCell(
            _buildResult(breakdown),
          ),
        ],
      ));
    }

    return rows;
  }

  Widget buildMemberAmount(double amount, double fontSize) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      alignment: Alignment.centerRight,
      child: Text(
        NumberUtils.getCurrency(
          amount,
          showSymbol: false,
        ),
        style: TextStyle(
          fontSize: fontSize,
        ),
        textAlign: TextAlign.right,
      ),
    );
  }

  TableRow _buildHeaderRow() {
    return TableRow(
      children: [
        _buildHeaderText('Member', TextAlign.center),
        _buildHeaderText('Owe', TextAlign.right),
        _buildHeaderText('Pay', TextAlign.right),
        _buildHeaderText('Result', TextAlign.right),
      ],
    );
  }

  Widget _buildHeaderText(String data, TextAlign align) {
    const textStyle = TextStyle(
      color: ColorSet.accent,
    );
    return Padding(
      padding: const EdgeInsets.only(bottom: 4),
      child: _buildCell(
        Text(
          data,
          style: textStyle,
          textAlign: align,
        ),
      ),
    );
  }

  Widget _buildResult(PersonalBreakdown breakdown) {
    const double fontSize = 13;
    if (breakdown.totalCost == breakdown.totalPayment) {
      return const Text(
        'None',
        style: TextStyle(
          fontSize: fontSize,
        ),
      );
    }

    String label;
    String amount;
    Color amountColor;
    if (breakdown.totalCost > breakdown.totalPayment) {
      label = 'Borrows';
      amount = NumberUtils.getCurrency(
        breakdown.totalBorrowed,
        showSymbol: false,
      );
      amountColor = ColorSet.red;
    } else {
      label = 'Lends';
      amount = NumberUtils.getCurrency(
        breakdown.totalLent,
        showSymbol: false,
      );
      amountColor = ColorSet.light;
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Text(
          label,
          style: const TextStyle(
            fontSize: fontSize,
          ),
          textAlign: TextAlign.right,
        ),
        FittedBox(
          child: Text(
            amount,
            style: TextStyle(
              color: amountColor,
              height: 1.4,
              fontSize: fontSize,
            ),
            textAlign: TextAlign.right,
          ),
        ),
      ],
    );
  }

  Widget _buildCell(Widget content) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 4),
      child: content,
    );
  }
}
