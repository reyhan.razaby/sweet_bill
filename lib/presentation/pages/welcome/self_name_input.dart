import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:page_transition/page_transition.dart';
import 'package:sweet_bill/presentation/bloc/welcome/welcome_cubit.dart';
import 'package:sweet_bill/presentation/pages/welcome/group_input.dart';

import '../../../core/constants/color_set.dart';
import '../../../core/constants/text_lengths.dart';
import '../../components/buttons/default_button.dart';
import '../../components/text_fields/bordered_text_field.dart';

class SelfNameInput extends StatelessWidget {
  final TextEditingController textController = TextEditingController();

  SelfNameInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: buildContent(context),
            )
          ],
        ),
      ),
    );
  }

  Widget buildContent(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: MediaQuery.of(context).size.height / 3.5),
        const Text(
          'Welcome!',
          style: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 10),
        const Text(
          "What's your name?",
          style: TextStyle(
            fontSize: 16,
            color: ColorSet.grey,
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 34),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 240,
              child: BorderedTextField(
                hintText: 'Your short name',
                maxLines: 1,
                maxLength: personNameMaxLength,
                hideCounterText: true,
                contentPadding: const EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 14,
                ),
                textCapitalization: TextCapitalization.words,
                controller: textController,
                onFieldSubmitted: (val) {
                  submit(context);
                },
                isClearable: true,
              ),
            ),
            const SizedBox(width: 12),
            DefaultButton(
              onPressed: () {
                submit(context);
              },
              label: 'Next',
            ),
          ],
        ),
      ],
    );
  }

  void submit(context) {
    String name = textController.text.trim();
    if (name.isEmpty) {
      return;
    }
    BlocProvider.of<WelcomeCubit>(context).generatePerson(name, isSelf: true);
    FocusManager.instance.primaryFocus?.unfocus();
    Navigator.push(
      context,
      PageTransition(
        type: PageTransitionType.rightToLeftJoined,
        child: BlocProvider.value(
          value: BlocProvider.of<WelcomeCubit>(context),
          child: const GroupInput(),
        ),
        childCurrent: this,
        duration: const Duration(milliseconds: 250),
        curve: Curves.easeInOutSine,
      ),
    );
  }
}
