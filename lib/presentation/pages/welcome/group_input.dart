import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/bloc/welcome/welcome_cubit.dart';
import 'package:sweet_bill/presentation/components/dialogs/dialog_utils.dart';

import 'group_input_members.dart';

class GroupInput extends StatefulWidget {
  const GroupInput({Key? key}) : super(key: key);

  @override
  State<GroupInput> createState() => _GroupInputState();
}

class _GroupInputState extends State<GroupInput> {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocListener<WelcomeCubit, WelcomeState>(
        listener: cubitStateListener,
        child: Scaffold(
          body: buildBody(context),
        ),
      ),
    );
  }

  void cubitStateListener(BuildContext context, WelcomeState state) {
    if (state.status == WelcomeStatus.submitFailed || state.self == null) {
      DialogUtils.showErrorDialog(
        context,
        onDismiss: () {
          Navigator.of(context).pop();
        },
        bodyMessage: "Forgive us, let's do it again from the start",
      );
    } else if (state.status == WelcomeStatus.submitSucceed) {
      ProfileCubit profileCubit = BlocProvider.of<ProfileCubit>(context);
      profileCubit.clearProfile();
      profileCubit.addProfile(self: state.self!);
    }
  }

  Widget buildBody(context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const PageTitle(),
          GroupInputMembers(
            onDone: () {
              BlocProvider.of<WelcomeCubit>(context).submitAll();
            },
          ),
        ],
      ),
    );
  }

  void onBack(context) {}
}

class PageTitle extends StatelessWidget {
  const PageTitle({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(top: 74, bottom: 12),
      child: Text(
        'Make your "split bill" group',
        style: TextStyle(
          fontSize: 22,
          fontWeight: FontWeight.w500,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}
