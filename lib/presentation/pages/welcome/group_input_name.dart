import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/welcome/welcome_cubit.dart';
import 'package:sweet_bill/presentation/components/buttons/button_constants.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';
import 'package:sweet_bill/presentation/components/text_fields/bordered_text_field.dart';

import '../../../core/constants/color_set.dart';
import '../../../core/constants/text_lengths.dart';

class GroupInputName extends StatelessWidget {
  final TextEditingController textController = TextEditingController();
  final VoidCallback onPrevious;
  final VoidCallback onDone;

  GroupInputName({
    Key? key,
    required this.onPrevious,
    required this.onDone,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const SizedBox(height: 32),
        const Text(
          "Group name",
          style: TextStyle(
            fontSize: 14,
            color: ColorSet.lightGrey,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 14),
        BorderedTextField(
          hintText: "Travel to Bali",
          maxLines: 1,
          maxLength: eventGroupNameMaxLength,
          isClearable: true,
          controller: textController,
          textCapitalization: TextCapitalization.words,
          onChanged: (val) {
            String newName = val.trim();
            BlocProvider.of<WelcomeCubit>(context).setGroupName(newName);
          }
        ),
        const SizedBox(height: 14),
        Wrap(
          alignment: WrapAlignment.end,
          children: [
            DefaultButton(
              type: ButtonType.outlined,
              onPressed: onPrevious,
              label: 'Previous',
            ),
            const SizedBox(width: 10),
            DefaultButton(
              onPressed: onDone,
              label: 'Done',
            ),
          ],
        ),
      ],
    );
  }
}
