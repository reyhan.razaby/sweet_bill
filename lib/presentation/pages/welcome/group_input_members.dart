import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/presentation/components/buttons/button_constants.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';
import 'package:sweet_bill/presentation/components/drag_bar.dart';
import 'package:sweet_bill/presentation/components/option_menu/option_menu.dart';
import 'package:sweet_bill/presentation/components/text_fields/bordered_text_field.dart';

import '../../../core/constants/color_set.dart';
import '../../../core/constants/text_lengths.dart';
import '../../bloc/welcome/welcome_cubit.dart';
import '../../components/bottom_sheets/option_menu_bottom_sheet.dart';
import '../../components/person/person_circle_avatar.dart';

class GroupInputMembers extends StatelessWidget {
  final ScrollController membersScrollController = ScrollController();
  final VoidCallback onDone;

  GroupInputMembers({
    Key? key,
    required this.onDone,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: buildContent(context),
    );
  }

  Widget buildContent(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const Text(
          "List your involved friends"
          "\n(you can do it later)",
          style: TextStyle(
            fontSize: 14,
            color: ColorSet.grey,
            height: 1.35,
          ),
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 38),
        const Text(
          "Members",
          style: TextStyle(
            fontSize: 14,
            color: ColorSet.lightGrey,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(height: 14),
        _FriendsCard(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                constraints: const BoxConstraints(maxHeight: 400),
                padding: const EdgeInsets.only(top: 4),
                child: BlocBuilder<WelcomeCubit, WelcomeState>(
                  builder: (context, state) {
                    List<Person> members = state.members;

                    return ListView.separated(
                      controller: membersScrollController,
                      shrinkWrap: true,
                      physics: const BouncingScrollPhysics(),
                      itemCount: members.length,
                      itemBuilder: (_, index) {
                        Person member = members[index];
                        return _MemberItem(
                          member: member,
                          isSelf: index == 0,
                          onItemBuilt: () {
                            if (index == members.length - 1) {
                              scrollToNewMember();
                            }
                          },
                          onTap: () {
                            if (index == 0) return;
                            showFriendBottomSheet(context, member, index - 1);
                          },
                          onEditPressed: () {
                            if (index == 0) return;
                            showInputNameBottomSheet(
                              context,
                              friendIndex: index - 1,
                              friend: member,
                            );
                          },
                        );
                      },
                      separatorBuilder: (_, __) => const _ListDivider(),
                    );
                  },
                ),
              ),
              const _ListDivider(),
              buildAddMemberButton(context),
            ],
          ),
        ),
        const SizedBox(height: 12),
        Wrap(
          alignment: WrapAlignment.end,
          children: [
            DefaultButton(
              type: ButtonType.outlined,
              onPressed: () {
                Navigator.of(context).pop();
              },
              label: 'Previous',
            ),
            const SizedBox(width: 10),
            DefaultButton(onPressed: onDone, label: 'Done'),
          ],
        ),
      ],
    );
  }

  void showFriendBottomSheet(
      BuildContext context, Person friend, int friendIndex) {
    showModalBottomSheet(
        context: context,
        builder: (_) {
          return OptionMenuBottomSheet(
            items: [
              OptionMenuItem.edit(() {
                showInputNameBottomSheet(
                  context,
                  friendIndex: friendIndex,
                  friend: friend,
                );
              }),
              OptionMenuItem.remove(
                () {
                  BlocProvider.of<WelcomeCubit>(context)
                      .removeFriend(friendIndex);
                },
              ),
            ],
          );
        });
  }

  Widget buildAddMemberButton(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        showInputNameBottomSheet(context);
      },
      child: const Padding(
        padding: EdgeInsets.symmetric(vertical: 16),
        child: Text(
          '+ Add member',
          style: TextStyle(
            color: ColorSet.light,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  void showInputNameBottomSheet(
    BuildContext context, {
    int? friendIndex,
    Person? friend,
  }) {
    Widget input = AddMemberInput(
      friend: friend,
      friendIndex: friendIndex,
      welcomeCubit: BlocProvider.of<WelcomeCubit>(context),
    );
    showModalBottomSheet(
      backgroundColor: ColorSet.dark,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      context: context,
      builder: (context) {
        final MediaQueryData mediaQueryData = MediaQuery.of(context);
        return Padding(
          padding: mediaQueryData.viewInsets,
          child: input,
        );
      },
    );
  }

  void scrollToNewMember() {
    membersScrollController.animateTo(
      membersScrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 400),
      curve: Curves.fastOutSlowIn,
    );
  }
}

class AddMemberInput extends StatelessWidget {
  final Person? friend;
  final int? friendIndex;
  final WelcomeCubit welcomeCubit;

  const AddMemberInput({
    Key? key,
    required this.welcomeCubit,
    required this.friend,
    required this.friendIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textController = TextEditingController();
    if (friend != null && friend!.name.isNotEmpty) {
      textController.text = friend!.name;
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(18, 0, 18, 10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const DragBar(),
          const Text(
            "Your friend's name",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w500,
            ),
          ),
          const SizedBox(height: 12),
          BorderedTextField(
            controller: textController,
            autofocus: true,
            isClearable: true,
            textCapitalization: TextCapitalization.words,
            maxLines: 1,
            maxLength: personNameMaxLength,
          ),
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              DefaultButton(
                type: ButtonType.outlined,
                onPressed: () {
                  Navigator.of(context).pop();
                },
                label: 'Cancel',
                horizontalPadding: 15,
              ),
              const SizedBox(width: 8),
              DefaultButton(
                onPressed: () {
                  String name = textController.text.trim();
                  if (name.isEmpty) {
                    return;
                  }
                  if (friendIndex != null) {
                    welcomeCubit.renameFriend(name, friendIndex!);
                  } else {
                    welcomeCubit.generatePerson(name);
                  }
                  Navigator.of(context).pop();
                },
                label: friend != null ? 'Rename' : 'Add',
                horizontalPadding: friend != null ? 16 : 24,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _MemberItem extends StatelessWidget {
  final Person member;
  final VoidCallback? onItemBuilt;
  final VoidCallback? onTap;
  final VoidCallback? onEditPressed;
  final bool isSelf;

  const _MemberItem({
    Key? key,
    required this.member,
    this.onItemBuilt,
    this.onTap,
    this.onEditPressed,
    required this.isSelf,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      onItemBuilt?.call();
    });

    return GestureDetector(
      onTap: onTap,
      behavior: HitTestBehavior.translucent,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10),
        child: Row(
          children: [
            PersonCircleAvatar(
              emoji: member.avatar.emoji,
              radius: 16,
            ),
            const SizedBox(width: 14),
            Expanded(
              child: Text(
                '${member.name} ${isSelf ? '(You)' : ''}',
                style: const TextStyle(fontSize: 16),
                overflow: TextOverflow.ellipsis,
              ),
            ),
            if (!isSelf)
              IconButton(
                constraints: const BoxConstraints(),
                onPressed: onEditPressed,
                icon: const Icon(
                  Icons.edit,
                  color: ColorSet.grey,
                  size: 16,
                ),
              )
          ],
        ),
      ),
    );
  }
}

class _ListDivider extends StatelessWidget {
  const _ListDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 0,
      color: Colors.grey.withOpacity(0.5),
    );
  }
}

class _FriendsCard extends StatelessWidget {
  final Widget child;

  const _FriendsCard({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: ColorSet.dark,
      ),
      child: child,
    );
  }
}
