import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/pages/welcome/self_name_input.dart';

import '../../../core/constants/route_names.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfileCubit, ProfileState>(
      listener: (context, state) {
        if (state is ProfileAdded) {
          Navigator.of(context)
              .pushNamedAndRemoveUntil(RouteNames.home, (_) => false);
        }
      },
      child: SelfNameInput(),
    );
  }

  ProfileCubit get profileCubit => BlocProvider.of<ProfileCubit>(context);
}
