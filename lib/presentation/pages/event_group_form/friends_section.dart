import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/friends/friends_bloc.dart';
import 'package:sweet_bill/presentation/components/errors/unexpected_error.dart';

import '../../../core/utils/list_utils.dart';
import '../../../domain/entities/person/person.dart';
import '../../components/check_box/default_check_box.dart';
import '../../components/loadings/full_screen_loading.dart';
import '../../components/person/person_list_item.dart';

class FriendsSection extends StatefulWidget {
  const FriendsSection({Key? key}) : super(key: key);

  @override
  State<FriendsSection> createState() => _FriendsSectionState();
}

class _FriendsSectionState extends State<FriendsSection> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FriendsBloc, FriendsState>(
      builder: (context, state) {
        if (state is FriendsLoading) {
          return const SizedBox(height: 200, child: FullScreenLoading());
        } else if (state is FriendsLoaded &&
            ListUtils.isNotNullAndNotEmpty(state.friends)) {
          return buildList(state.friends!);
        } else if (state is FriendsError) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 32.0),
            child: UnexpectedError(onReload: reloadFriends),
          );
        } else {
          return Container();
        }
      },
    );
  }

  Widget buildList(List<Person> friends) {
    double wPadding = 18;
    return Column(
      children: [
        SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.fromLTRB(wPadding, 14, 0, 10),
            child: const Text(
              'Choose from friends',
              style: TextStyle(color: ColorSet.white, fontSize: 13),
            ),
          ),
        ),
        ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: friends.length,
          itemBuilder: (context, index) {
            Person friend = friends[index];
            return BlocSelector<EventGroupFormBloc, EventGroupFormState, bool>(
              selector: (state) {
                return formState.memberIds.contains(friend.id);
              },
              builder: (_, isMember) {
                return GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: onTapFriend(friend, isMember),
                  child: PersonListItem(
                    person: friend,
                    avatarBorderColor:
                        isMember ? ColorSet.light : ColorSet.darkest,
                    avatarRadius: 20,
                    verticalPadding: 11,
                    horizontalPadding: wPadding,
                    trailing: DefaultCheckBox(isChecked: isMember),
                  ),
                );
              },
            );
          },
        ),
      ],
    );
  }

  EventGroupFormState get formState =>
      BlocProvider.of<EventGroupFormBloc>(context).state;

  void reloadFriends() {
    BlocProvider.of<FriendsBloc>(context).add(LoadFriends());
  }

  GestureTapCallback onTapFriend(Person friend, bool isMember) {
    FocusManager.instance.primaryFocus?.unfocus();
    return () {
      if (isMember) {
        int memberIndex =
            formState.members.indexWhere((member) => member.id == friend.id);
        BlocProvider.of<EventGroupFormBloc>(context)
            .add(RemoveMember(index: memberIndex));
      } else {
        BlocProvider.of<EventGroupFormBloc>(context)
            .add(AddMember(member: EventGroupMember.friendPerson(friend)));
      }
    };
  }
}
