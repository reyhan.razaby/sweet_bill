import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/presentation/components/app_bars/creation_app_bar.dart';
import 'package:sweet_bill/presentation/components/buttons/button_constants.dart';
import 'package:sweet_bill/presentation/components/buttons/default_button.dart';
import 'package:sweet_bill/presentation/components/images/event_group_image.dart';
import 'package:sweet_bill/presentation/components/text_fields/plain_text_field.dart';
import 'package:sweet_bill/presentation/pages/event_groups/event_groups_page.dart';

import '../../../core/constants/color_set.dart';
import '../../../core/constants/text_lengths.dart';
import '../../bloc/event_group_form/event_group_form_bloc.dart';
import '../../bloc/friends/friends_bloc.dart';
import '../../components/bottom_sheets/option_menu_bottom_sheet.dart';
import '../../components/dialogs/confirmation_dialog.dart';
import '../../components/dialogs/message_dialog.dart';
import '../../components/loadings/full_screen_loading.dart';
import '../../components/option_menu/option_menu.dart';
import 'friends_section.dart';
import 'members_section.dart';

class EventGroupFormPage extends StatefulWidget {
  final EventGroupFormPageArgs args;

  static const double hPadding = 18;

  const EventGroupFormPage({
    Key? key,
    required this.args,
  }) : super(key: key);

  @override
  State<EventGroupFormPage> createState() => _EventGroupFormPageState();
}

class _EventGroupFormPageState extends State<EventGroupFormPage> {
  final formKey = GlobalKey<FormState>();
  final nameInputController = TextEditingController();

  @override
  void initState() {
    super.initState();
    eventGroupFormBloc.add(InitializeForm(
      onInitialized: () {
        BlocProvider.of<FriendsBloc>(context).add(LoadFriends());
      },
      eventGroupId: args.eventGroupId,
    ));
  }

  EventGroupFormPageArgs get args => widget.args;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        onBack();
        return false;
      },
      child: Scaffold(
        appBar: buildAppBar(),
        body: buildBody(),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 14),
          child: DefaultButton(
            label: 'Save',
            onPressed: () {
              saveEventGroup();
            },
            size: ButtonSize.large,
          ),
        ),
      ),
    );
  }

  EventGroupFormBloc get eventGroupFormBloc =>
      BlocProvider.of<EventGroupFormBloc>(context);

  Widget buildBody() {
    return BlocConsumer<EventGroupFormBloc, EventGroupFormState>(
      listener: (_, state) async {
        if (state is EventGroupSaved) {
          Navigator.of(context).pop(
              EventGroupsPageArgs(createdEventGroup: state.createdEventGroup));
        }
        if (state is FormError) {
          String title = state.titleMessage ?? 'Error';
          showDialog(
            context: context,
            builder: (_) {
              return MessageDialog(
                title: title,
                bodyMessage: state.bodyMessage,
              );
            },
          );
        }
      },
      buildWhen: (previous, current) {
        if (current is FormLoaded) {
          return current.buildForm;
        }
        return true;
      },
      builder: (context, state) {
        if (state is! FormLoaded) {
          return const FullScreenLoading();
        }

        return Form(
          key: formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                buildSubTitle('Group name'),
                buildNameSection(state),
                buildSubTitle('Members'),
                const MembersSection(),
                buildDivider(),
                buildFriendsSection(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget buildDivider() {
    return const Divider(
      color: ColorSet.dark,
      height: 1,
      thickness: 1,
    );
  }

  Widget buildNameSection(FormLoaded state) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 18,
        horizontal: EventGroupFormPage.hPadding,
      ),
      child: Row(
        children: [
          buildEventGroupImage(),
          const SizedBox(width: EventGroupFormPage.hPadding),
          buildGroupNameInput(state.name),
        ],
      ),
    );
  }

  Widget buildGroupNameInput(String currentName) {
    return Expanded(
      child: PlainTextField(
        controller: nameInputController,
        autofocus: StringUtils.isNullOrEmpty(currentName),
        fontSize: 24,
        maxLines: 1,
        textCapitalization: TextCapitalization.sentences,
        hintText: 'Travel to Bali',
        initialValue: args.isEditing ? currentName : null,
        validator: (value) {
          if (value != null) {
            value = value.trim();
          }
          if (value == null || value.isEmpty) {
            return 'No name? Come on...';
          }
          return null;
        },
        onChanged: (value) {
          eventGroupFormBloc.add(ChangeName(name: value.trim()));
        },
        isClearable: true,
        maxLength: eventGroupNameMaxLength,
      ),
    );
  }

  Widget buildEventGroupImage() {
    const double editIconRadius = 10;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
        onTapImage();
      },
      child: Stack(
        children: [
          BlocSelector<EventGroupFormBloc, EventGroupFormState, String?>(
            selector: (state) {
              return state.imagePath;
            },
            builder: (context, imagePath) {
              return EventGroupImage(imageFilePath: imagePath, radius: 32);
            },
          ),
          const Positioned(
            right: -2,
            bottom: -2,
            child: CircleAvatar(
              backgroundColor: ColorSet.darkest,
              radius: editIconRadius + 2.4,
              child: CircleAvatar(
                backgroundColor: ColorSet.light,
                radius: editIconRadius,
                child: Icon(
                  Icons.edit,
                  size: editIconRadius,
                  color: ColorSet.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void onTapImage() {
    showModalBottomSheet(
      context: context,
      builder: (_) => _buildImageBottomSheet(),
    );
  }

  Widget _buildImageBottomSheet() {
    bool hasImage = eventGroupFormBloc.state.imagePath != null;
    return OptionMenuBottomSheet(
      items: [
        OptionMenuItem(
            label: 'Pick from gallery',
            iconData: Icons.image_outlined,
            onSelect: () {
              pickImage(ImageSource.gallery);
            }),
        OptionMenuItem(
            label: 'Open camera',
            iconData: Icons.camera_alt_outlined,
            onSelect: () {
              pickImage(ImageSource.camera);
            }),
        if (hasImage)
          OptionMenuItem.remove(() {
            eventGroupFormBloc.add(RemoveImagePath());
          }),
      ],
    );
  }

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) {
        return;
      }

      eventGroupFormBloc.add(ChangeImagePath(imagePath: image.path));
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed to pick image: $e"),
      ));
    }
  }

  Widget buildSubTitle(String subTitle) {
    return Container(
      color: ColorSet.dark,
      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.symmetric(
            vertical: 9, horizontal: EventGroupFormPage.hPadding),
        child: Text(subTitle),
      ),
    );
  }

  PreferredSizeWidget buildAppBar() {
    return CreationAppBar(
      body: Text(args.isEditing ? 'Edit group' : 'Add group'),
      onCancel: onBack,
    );
  }

  void saveEventGroup() {
    if (!formKey.currentState!.validate()) {
      return;
    }
    eventGroupFormBloc.add(SaveEventGroup());
  }

  void onBack() {
    if (!eventGroupFormBloc.isChanged()) {
      Navigator.of(context).pop();
      return;
    }
    showDialog(
      context: context,
      builder: (_) {
        return ConfirmationDialog(
          title: 'Want to exit?',
          bodyMessage: 'All changes will be discarded',
          positiveText: 'Yup',
          onPositiveClicked: () {
            Navigator.of(context).pop();
          },
          negativeText: 'Stay here',
        );
      },
    );
  }

  Widget buildFriendsSection() {
    return const FriendsSection();
  }
}

class EventGroupFormPageArgs {
  final String? eventGroupId;
  final bool isEditing;

  const EventGroupFormPageArgs({
    this.eventGroupId,
    this.isEditing = false,
  });
}
