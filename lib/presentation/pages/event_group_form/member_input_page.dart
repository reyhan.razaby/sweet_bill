import 'package:flutter/material.dart';
import 'package:sweet_bill/presentation/components/text_fields/bordered_text_field.dart';

import '../../../core/constants/text_lengths.dart';
import '../../components/app_bars/default_app_bar.dart';
import '../../components/buttons/default_button.dart';

class MemberInputPage extends StatelessWidget {
  final Function onConfirm;
  final String? currentName;

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _textEditingController = TextEditingController();
  final ValueNotifier<bool> _hasValueNotifier = ValueNotifier<bool>(false);

  MemberInputPage({
    Key? key,
    this.currentName,
    required this.onConfirm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _textEditingController.text = currentName ?? '';
    _hasValueNotifier.value = _textEditingController.text.isNotEmpty;

    return Scaffold(
      appBar: const DefaultAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Form(
              key: _formKey,
              child: Column(
                children: [
                  BorderedTextField(
                    controller: _textEditingController,
                    initialValue: currentName,
                    isClearable: true,
                    autofocus: true,
                    maxLines: 1,
                    maxLength: personNameMaxLength,
                    hintText: "Enter member's name",
                    textCapitalization: TextCapitalization.words,
                    validator: (value) {
                      if (value != null) {
                        value = value.trim();
                      }
                      if (value == null || value.isEmpty) {
                        return 'Really? Your friend doesn\'t have name?';
                      } else if (value.length > personNameMaxLength) {
                        return 'Make it short dude (max $personNameMaxLength chars)';
                      }
                      return null;
                    },
                    onChanged: (val) {
                      _hasValueNotifier.value = val.isNotEmpty;
                    },
                  ),
                ],
              ),
            ),
            const SizedBox(height: 14),
            ValueListenableBuilder<bool>(
              builder: (_, hasValue, __) {
                return DefaultButton(
                  label: 'Add member',
                  onPressed: () {
                    if (!_formKey.currentState!.validate()) {
                      return;
                    }
                    String text = _textEditingController.text.trim();
                    onConfirm(text);
                    Navigator.of(context).pop();
                  },
                  isDisabled: !hasValue,
                );
              },
              valueListenable: _hasValueNotifier,
            )
          ],
        ),
      ),
    );
  }
}
