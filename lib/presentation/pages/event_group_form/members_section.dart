import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/core/utils/avatar_generator.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';

import 'event_group_form_page.dart';
import 'member_avatar.dart';
import 'member_input_page.dart';

class MembersSection extends StatefulWidget {
  static const double labelSize = 9;
  static const double imageRadius = 24;

  const MembersSection({Key? key}) : super(key: key);

  @override
  State<MembersSection> createState() => _MembersSectionState();
}

class _MembersSectionState extends State<MembersSection> {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxHeight: 110),
      child: Row(
        children: [
          buildAddMemberButton(),
          Expanded(
            child: ShaderMask(
              shaderCallback: (bounds) {
                return LinearGradient(
                  colors: [Colors.white.withOpacity(0), Colors.white],
                  stops: const [0, 0.05],
                ).createShader(bounds);
              },
              child: buildMembers(),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildAddMemberButton() {
    const Color color = Colors.grey;

    return Padding(
      padding:
          const EdgeInsets.only(left: EventGroupFormPage.hPadding, right: 6),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              unFocus();
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider.value(
                    value: eventGroupFormBloc,
                    child: MemberInputPage(onConfirm: addNewMember),
                  ),
                ),
              );
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 6, 0, 8),
              child: Container(
                width: MembersSection.imageRadius * 2,
                height: MembersSection.imageRadius * 2,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    width: 2.0,
                    color: color,
                  ),
                ),
                child: const Icon(
                  Icons.add,
                  color: color,
                ),
              ),
            ),
          ),
          const Text(
            'Add',
            style: TextStyle(
              fontSize: MembersSection.labelSize,
              color: color,
            ),
          ),
        ],
      ),
    );
  }

  void unFocus() {
    FocusManager.instance.primaryFocus?.unfocus();
  }

  void addNewMember(name) {
    eventGroupFormBloc.add(
      AddMember(
        member: EventGroupMember.newMember(
          name,
          AvatarGenerator.generatePersonAvatar(ownedAvatars: takenAvatars),
        ),
      ),
    );
  }

  Widget buildMembers() {
    return BlocSelector<EventGroupFormBloc, EventGroupFormState,
        List<EventGroupMember>>(
      selector: (state) => state.members,
      builder: (context, members) {
        return MemberListView(members);
      },
    );
  }

  EventGroupFormBloc get eventGroupFormBloc =>
      BlocProvider.of<EventGroupFormBloc>(context);

  List<PersonAvatar> get takenAvatars {
    return eventGroupFormBloc.state.members.map((e) => e.avatar).toList();
  }
}

class MemberListView extends StatefulWidget {
  final List<EventGroupMember> members;

  const MemberListView(
    this.members, {
    Key? key,
  }) : super(key: key);

  @override
  State<MemberListView> createState() => _MemberListViewState();
}

class _MemberListViewState extends State<MemberListView> {
  final ScrollController membersScrollController = ScrollController();
  GlobalKey? lastMemberKey;

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (detectNewMember(context)) {
        scrollToNewMember();
      }
    });

    return ListView.builder(
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
      itemBuilder: (listViewContext, index) {
        final bool isFirst = index == 0;
        final bool isLast = index == widget.members.length - 1;
        final double leftPadding = isFirst ? 11 : 0;
        final EventGroupMember member = widget.members[index];

        lastMemberKey = isLast ? GlobalKey() : null;
        return Container(
          key: lastMemberKey,
          padding: EdgeInsets.only(left: leftPadding),
          child: MemberAvatar(
            member: member,
            onTapRemove: () async {
              FocusManager.instance.primaryFocus?.unfocus();
              removeMember(context, index);
            },
            onTapEdit: () {
              FocusManager.instance.primaryFocus?.unfocus();
              editMember(context, member, index);
            },
          ),
        );
      },
      itemCount: widget.members.length,
    );
  }

  bool detectNewMember(context) {
    final state = getEventGroupFormBloc(context).state;
    if (state is FormLoaded) {
      return state.spawnNewMember;
    }
    return false;
  }

  void scrollToNewMember() {
    if (lastMemberKey != null && lastMemberKey!.currentContext != null) {
      Scrollable.ensureVisible(
        lastMemberKey!.currentContext!,
        duration: const Duration(milliseconds: 400),
        curve: Curves.easeInOutSine,
        alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtEnd,
      );
    }
  }

  EventGroupFormBloc getEventGroupFormBloc(context) =>
      BlocProvider.of<EventGroupFormBloc>(context);

  void removeMember(context, int index) {
    getEventGroupFormBloc(context).add(RemoveMember(index: index));
  }

  void editMember(context, EventGroupMember member, int index) {
    final page = BlocProvider.value(
      value: getEventGroupFormBloc(context),
      child: MemberInputPage(
        currentName: member.name,
        onConfirm: (newName) {
          getEventGroupFormBloc(context).add(
            EditMember(
              member: member.copyWith(name: newName),
              index: index,
            ),
          );
        },
      ),
    );
    Navigator.push(context, MaterialPageRoute(builder: (_) => page));
  }
}
