import 'package:flutter/material.dart';

import '../../../core/constants/color_set.dart';
import '../../bloc/event_group_form/event_group_form_bloc.dart';
import '../../components/person/person_circle_avatar.dart';
import 'members_section.dart';

class MemberAvatar extends StatelessWidget {
  final EventGroupMember member;
  final VoidCallback onTapRemove;
  final VoidCallback onTapEdit;

  const MemberAvatar({
    Key? key,
    required this.member,
    required this.onTapRemove,
    required this.onTapEdit,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 76,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 3),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(6, 6, 6, 8),
                  child: GestureDetector(
                    onTap: member.isNew ? onTapEdit : null,
                    child: PersonCircleAvatar(
                      emoji: member.avatar.emoji,
                      radius: MembersSection.imageRadius,
                    ),
                  ),
                ),
                if (!member.isSelf) buildRemoveButton(context),
              ],
            ),
            GestureDetector(
              onTap: member.isNew ? onTapEdit : null,
              behavior: HitTestBehavior.translucent,
              child: Container(
                decoration: member.isNew
                    ? const BoxDecoration(
                        border:
                            Border(bottom: BorderSide(color: ColorSet.white)))
                    : null,
                child: Text(
                  member.name,
                  style: const TextStyle(fontSize: MembersSection.labelSize),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Positioned buildRemoveButton(BuildContext context) {
    const double size = 22;
    return Positioned(
      right: 0,
      child: GestureDetector(
        onTap: onTapRemove,
        child: const CircleAvatar(
          backgroundColor: ColorSet.darkest,
          radius: size / 2,
          child: Icon(
            Icons.cancel_rounded,
            color: Colors.grey,
            size: size,
          ),
        ),
      ),
    );
  }
}
