import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/color_set.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/presentation/bloc/bills/bills_bloc.dart';
import 'package:sweet_bill/presentation/bloc/debts_summary/debts_summary_cubit.dart';
import 'package:sweet_bill/presentation/bloc/transactions/transactions_cubit.dart';
import 'package:sweet_bill/presentation/components/errors/unexpected_error.dart';
import 'package:sweet_bill/presentation/components/loadings/full_screen_loading.dart';
import 'package:sweet_bill/presentation/pages/transactions/components/bill_list_toolbar.dart';
import 'package:sweet_bill/presentation/pages/transactions/components/empty_display.dart';
import 'package:sweet_bill/presentation/pages/transactions/components/summary_card/summary_card.dart';
import 'package:sweet_bill/presentation/pages/transactions/components/transactions_app_bar.dart';

import 'components/bill_list.dart';

class TransactionsPage extends StatefulWidget {
  const TransactionsPage({Key? key}) : super(key: key);

  @override
  State<TransactionsPage> createState() => _TransactionsPageState();
}

class _TransactionsPageState extends State<TransactionsPage> {
  @override
  void initState() {
    super.initState();
    loadEventGroupTransactions();
  }

  void loadEventGroupTransactions() {
    getTransactionsCubit.loadActiveEventGroup();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const TransactionsAppBar(),
      body: BlocConsumer<TransactionsCubit, TransactionsState>(
        listener: (_, state) {
          if (state is TransactionsLoaded) {
            final groupId = state.eventGroup.id;
            getDebtsSummaryBloc.loadDebtsSummary(groupId);
            getBillsBloc.add(LoadBills(eventGroupId: groupId));
          }
        },
        builder: (context, state) {
          if (state is TransactionsLoading || state is TransactionsEmpty) {
            return const FullScreenLoading();
          } else if (state is TransactionsLoaded) {
            var eventGroup = state.eventGroup;
            return buildBody(eventGroup);
          } else {
            return UnexpectedError(onReload: loadEventGroupTransactions);
          }
        },
      ),
    );
  }

  DebtsSummaryCubit get getDebtsSummaryBloc =>
      BlocProvider.of<DebtsSummaryCubit>(context);

  BillsBloc get getBillsBloc => BlocProvider.of<BillsBloc>(context);

  TransactionsCubit get getTransactionsCubit =>
      BlocProvider.of<TransactionsCubit>(context);

  Widget buildBody(EventGroup eventGroup) {
    return BlocBuilder<BillsBloc, BillsState>(
      builder: (_, state) {
        double hPadding = 14;

        if (state is BillsError) {
          return UnexpectedError(onReload: loadEventGroupTransactions);
        } else if (state is! BillsLoaded) {
          return const FullScreenLoading();
        }

        if (state.bills.isEmpty) {
          return const EmptyDisplay();
        }

        return CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: hPadding),
                child: SummaryCard(
                  eventGroup: eventGroup,
                ),
              ),
            ),
            SliverPersistentHeader(
              floating: true,
              delegate: SliverPersistentToolbar(
                height: 52,
                horizontalPadding: hPadding,
              ),
            ),
            SliverToBoxAdapter(
              child: BillList(horizontalPadding: hPadding),
            ),
          ],
        );
      },
    );
  }
}

class SliverPersistentToolbar extends SliverPersistentHeaderDelegate {
  final double height;
  final double horizontalPadding;

  const SliverPersistentToolbar({
    required this.height,
    required this.horizontalPadding,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: ColorSet.darkest,
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      child: const Center(
        child: BillListToolbar(),
      ),
    );
  }

  @override
  double get maxExtent => height;

  @override
  double get minExtent => height;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) =>
      false;
}
