import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/domain/entities/debt/debts_summary.dart';
import 'package:sweet_bill/presentation/bloc/debts_summary/debts_summary_cubit.dart';
import 'package:sweet_bill/presentation/bloc/transactions/transactions_cubit.dart';

import '../../../../../core/constants/color_set.dart';
import '../../../../../core/constants/route_names.dart';
import '../../../../../core/utils/number_utils.dart';
import '../../../../../domain/entities/debt/debt.dart';
import '../../../../../domain/entities/event_group/event_group.dart';
import '../../../debts_breakdown/debts_breakdown_page.dart';

class SummaryCard extends StatelessWidget {
  final EventGroup eventGroup;

  const SummaryCard({
    Key? key,
    required this.eventGroup,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double verticalPadding = 4;
    return GestureDetector(
      onTap: () {
        final currentState = _getDebtsSummaryBloc(context).state;
        if (currentState is DebtsSummaryLoaded &&
            currentState.debtsSummary.debts.isNotEmpty) {
          goToBreakdownPage(context);
        }
      },
      child: Container(
        constraints: const BoxConstraints(minHeight: 140),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: ColorSet.dark,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: verticalPadding),
          child: BlocBuilder<DebtsSummaryCubit, DebtsSummaryState>(
            builder: (context, state) {
              if (state is DebtsSummaryLoading) {
                return const Center(
                  child: Text(
                    'Calculating...',
                    style: TextStyle(fontSize: 15),
                  ),
                );
              } else if (state is DebtsSummaryLoaded) {
                if (state.debtsSummary.debts.isEmpty) {
                  return const Center(
                    child: Text(
                      'No debts',
                      style: TextStyle(fontSize: 16),
                    ),
                  );
                }

                return Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: TotalCostDisplay(
                        debtsSummary: state.debtsSummary,
                      ),
                    ),
                    Expanded(
                      child: BlocSelector<DebtsSummaryCubit, DebtsSummaryState,
                          DebtsSummary?>(
                        selector: (state) {
                          if (state is DebtsSummaryLoaded) {
                            return state.debtsSummary;
                          }
                          return null;
                        },
                        builder: (context, debtsSummary) {
                          if (debtsSummary == null) {
                            return Container();
                          }
                          return DebtsDisplay(debtsSummary: debtsSummary);
                        },
                      ),
                    ),
                  ],
                );
              }
              return Container();
            },
          ),
        ),
      ),
    );
  }

  void goToBreakdownPage(BuildContext context) {
    final args = DebtsBreakdownPageArgs(eventGroupId: eventGroup.id);
    Navigator.pushNamed(context, RouteNames.debtsBreakdown, arguments: args)
        .then((_) => reload(context));
  }

  void reload(context) {
    getTransactionsCubit(context).loadActiveEventGroup();
  }
}

class DebtsDisplay extends StatelessWidget {
  final DebtsSummary debtsSummary;

  static final _dividerColor = ColorSet.white.withOpacity(.3);
  static const int maxVisible = 4;

  const DebtsDisplay({
    Key? key,
    required this.debtsSummary,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DebtsSummaryState currentState = _getDebtsSummaryBloc(context).state;
    if (currentState is! DebtsSummaryLoaded) {
      return Container();
    }

    final List<Debt> debts = debtsSummary.debts;

    if (debts.isNotEmpty && debts.length <= 2) {
      final List<Widget> widgets = [];
      TextAlign textAlign =
          debts.length == 1 ? TextAlign.center : TextAlign.left;

      for (int i = 0; i < debts.length; i++) {
        widgets.add(Text(
          debts[i].desc,
          textAlign: textAlign,
          maxLines: debts.length == 1 ? 2 : 1,
          overflow: TextOverflow.ellipsis,
        ));
        widgets.add(Text(
          NumberUtils.getCurrency(debts[0].amount, showSymbol: true),
          textAlign: textAlign,
          style: const TextStyle(
              fontWeight: FontWeight.bold, fontSize: 16, height: 1.4),
        ));
        if (i != debts.length - 1) {
          widgets.add(Divider(color: _dividerColor));
        }
      }
      return Padding(
        padding: const EdgeInsets.only(right: 4),
        child: Column(
          crossAxisAlignment: debts.length == 1
              ? CrossAxisAlignment.center
              : CrossAxisAlignment.start,
          children: widgets,
        ),
      );
    }

    final int visibleCount =
        debts.length <= maxVisible ? debts.length : maxVisible - 1;
    final int invisibleCount = debts.length - visibleCount;

    final List<Debt> visibleDebts = debts.sublist(0, visibleCount);
    final List<TableRow> debtRowList = [];

    for (int i = 0; i < visibleDebts.length; i++) {
      bool drawBottomDivider =
          invisibleCount > 0 || i != visibleDebts.length - 1;

      debtRowList.add(_buildDebt(
        visibleDebts[i],
        drawBottomDivider: drawBottomDivider,
      ));
    }

    if (invisibleCount > 0) {
      debtRowList.add(_buildRowText(desc: '+$invisibleCount more'));
    }

    return Table(
      columnWidths: const {
        1: MinColumnWidth(IntrinsicColumnWidth(), FractionColumnWidth(0.4))
      },
      children: debtRowList,
    );
  }

  TableRow _buildRowText(
      {Decoration? decoration, required String desc, double? amount}) {
    double verticalPadding = 7;
    return TableRow(decoration: decoration, children: [
      Padding(
        padding: EdgeInsets.symmetric(vertical: verticalPadding),
        child: Text(
          desc,
          overflow: TextOverflow.ellipsis,
          style: const TextStyle(
            fontSize: 13,
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.fromLTRB(4, verticalPadding, 12, verticalPadding),
        child: amount != null
            ? Text(
                NumberUtils.getCurrency(amount, showSymbol: true),
                textAlign: TextAlign.right,
                style: const TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                  color: ColorSet.white,
                ),
              )
            : const SizedBox(),
      )
    ]);
  }

  TableRow _buildDebt(Debt debt, {bool drawBottomDivider = true}) {
    BoxDecoration? boxDecoration = drawBottomDivider
        ? BoxDecoration(
            border: Border(bottom: BorderSide(color: _dividerColor)))
        : null;
    return _buildRowText(
      desc: debt.desc,
      decoration: boxDecoration,
      amount: debt.amount,
    );
  }
}

class TotalCostDisplay extends StatelessWidget {
  final DebtsSummary debtsSummary;

  const TotalCostDisplay({
    Key? key,
    required this.debtsSummary,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int numberOfBills = debtsSummary.numberOfBills;
    return SizedBox(
      width: 94,
      child: Column(
        children: [
          FittedBox(
            child: Text(
              '$numberOfBills bill${numberOfBills > 1 ? 's' : ''}',
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 10),
          const Text(
            'Total cost',
            style: TextStyle(
              fontSize: 10,
            ),
          ),
          FittedBox(
            child: Text(
              NumberUtils.getCurrency(debtsSummary.totalCost, showSymbol: true),
              style: const TextStyle(
                color: ColorSet.light,
                fontWeight: FontWeight.bold,
                height: 1.4,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

DebtsSummaryCubit _getDebtsSummaryBloc(context) =>
    BlocProvider.of<DebtsSummaryCubit>(context);

TransactionsCubit getTransactionsCubit(context) =>
    BlocProvider.of<TransactionsCubit>(context);
