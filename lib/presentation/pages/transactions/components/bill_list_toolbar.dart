import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sweet_bill/presentation/bloc/bills/bills_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/components/text_fields/filled_text_field.dart';

class BillListToolbar extends StatefulWidget {
  const BillListToolbar({Key? key}) : super(key: key);

  @override
  State<BillListToolbar> createState() => _BillListToolbarState();
}

class _BillListToolbarState extends State<BillListToolbar> {
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: buildSearchBar(),
        ),
        // const SizedBox(width: gapSize),
        // buildFilterButton(context),
      ],
    );
  }

  GestureDetector buildFilterButton(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        BlocProvider.of<ProfileCubit>(context).clearProfile();
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        child: SvgPicture.asset(
          'assets/icons/filter.svg',
          color: Colors.grey,
          width: 20,
        ),
      ),
    );
  }

  Widget buildSearchBar() {
    return FilledTextField(
      controller: controller,
      borderRadius: 100,
      debounceDuration: const Duration(milliseconds: 200),
      prefixIcon: const SizedBox(
        width: 40,
        child: Icon(
          Icons.search,
          color: Colors.grey,
          size: 20,
        ),
      ),
      contentPadding: const EdgeInsets.symmetric(vertical: 6),
      onChanged: (val) {
        filterBills(val);
      },
      maxLines: 1,
      isClearable: true,
    );
  }

  BillsBloc get billsBloc => BlocProvider.of<BillsBloc>(context);

  void filterBills(String query) {
    billsBloc.add(FilterBills(searchInput: query));
  }
}
