import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/route_names.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/presentation/bloc/transactions/transactions_cubit.dart';
import 'package:sweet_bill/presentation/components/images/event_group_image.dart';

import '../../../constants/display_text.dart';

class TransactionsAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  const TransactionsAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: BlocBuilder<TransactionsCubit, TransactionsState>(
        builder: (_, state) {
          if (state is TransactionsLoading) {
            return Container();
          } else if (state is TransactionsLoaded) {
            return buildEventGroupDisplay(context, state.eventGroup);
          } else {
            return const Text('Error');
          }
        },
      ),
    );
  }

  Widget buildEventGroupDisplay(
      BuildContext context, EventGroup activeEventGroup) {
    return Padding(
      padding: const EdgeInsets.all(28.0),
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Navigator.pushNamed(context, RouteNames.eventGroups).then((_) {
            BlocProvider.of<TransactionsCubit>(context).loadActiveEventGroup();
          });
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.black.withOpacity(0.5),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: Wrap(
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                EventGroupImage(
                  imageFilePath: activeEventGroup.imagePath,
                  radius: 10,
                ),
                const SizedBox(width: 8),
                Text(
                  getGroupName(activeEventGroup),
                  style: const TextStyle(fontSize: 13),
                ),
                const Icon(
                  Icons.arrow_drop_down,
                  size: 24,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  String getGroupName(EventGroup activeEventGroup) {
    return StringUtils.getOr(activeEventGroup.name, DisplayText.emptyGroupName);
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
