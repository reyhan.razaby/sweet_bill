import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';

import '../../../../core/constants/color_set.dart';
import '../../../../core/constants/route_names.dart';
import '../../../../core/utils/number_utils.dart';
import '../../../../core/utils/string_utils.dart';
import '../../../../domain/entities/bill/bill.dart';
import '../../../bloc/bills/bills_bloc.dart';
import '../../../bloc/debts_summary/debts_summary_cubit.dart';
import '../../../bloc/transactions/transactions_cubit.dart';
import '../../../components/bottom_sheets/option_menu_bottom_sheet.dart';
import '../../../components/dialogs/confirmation_dialog.dart';
import '../../../components/option_menu/option_menu.dart';
import '../../../constants/display_text.dart';
import '../../bill_form/bill_form_page.dart';

class BillTile extends StatelessWidget {
  final Bill bill;
  final int tileIndex;
  final double horizontalPadding;
  final double verticalPadding = 18;
  final double optionButtonSize = 52;

  const BillTile({
    Key? key,
    required this.bill,
    required this.tileIndex,
    required this.horizontalPadding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InkWell(
          onTap: () {
            openBill(context, bill);
          },
          onLongPress: () {
            showModalBottomSheet(
              context: context,
              builder: (_) => buildBillOptionBottomSheet(context, bill),
            );
          },
          highlightColor: ColorSet.dark,
          splashColor: Colors.transparent,
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: horizontalPadding,
              vertical: verticalPadding,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: optionButtonSize),
                  child: TitleWithAvatar(bill: bill),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(child: buildBillAmountDetail(context, bill)),
                    buildTotalAmount(bill),
                  ],
                )
              ],
            ),
          ),
        ),
        Positioned(
          right: 0,
          child: GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              showModalBottomSheet(
                context: context,
                builder: (_) => buildBillOptionBottomSheet(context, bill),
              );
            },
            child: SizedBox(
              width: optionButtonSize,
              height: optionButtonSize,
              child: const Icon(
                Icons.more_horiz,
                color: Color(0xffd2d2d2),
                size: 24,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildBillAmountDetail(context, Bill bill) {
    Color textColor = ColorSet.white.withOpacity(.8);
    double fontSize = 13.5;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 16),
        buildCostAssignment(context, bill.personalCosts, textColor, fontSize),
        const SizedBox(height: 10),
        buildPaymentAssignment(
            context, bill.personalPayments, textColor, fontSize),
      ],
    );
  }

  Widget buildCostAssignment(
      context, List<BillPersonalCost> costs, Color textColor, double fontSize) {
    bool containsAll =
        costs.length == getTransactionsCubit(context).members.length;
    List<String> names =
        costs.map((e) => getPersonName(context, e.personId)).toList();

    String displayedNames =
        containsAll ? 'all members' : StringUtils.join(names);
    if (displayedNames.length > 25) {
      displayedNames = '${names.length} people';
    }

    return Row(
      children: [
        Icon(
          Icons.receipt_long_outlined,
          color: textColor,
          size: 14,
        ),
        const SizedBox(width: 6),
        Expanded(
          child: Text(
            'For $displayedNames',
            style: TextStyle(
              color: textColor,
              fontSize: fontSize,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }

  Widget buildPaymentAssignment(context, List<BillPersonalPayment> payments,
      Color textColor, double fontSize) {
    bool containsAll =
        payments.length == getTransactionsCubit(context).members.length;
    List<String> names =
        payments.map((e) => getPersonName(context, e.personId)).toList();

    return Row(
      children: [
        Icon(
          Icons.account_balance_wallet_outlined,
          color: textColor,
          size: 14,
        ),
        const SizedBox(width: 6),
        Flexible(
          child: Text(
            'Paid by ${containsAll ? 'all members' : StringUtils.join(names)}',
            style: TextStyle(
              color: textColor,
              fontSize: fontSize,
            ),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }

  Widget buildTotalAmount(Bill bill) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, top: 8),
      child: Text(
        NumberUtils.getCurrency(bill.totalCost, showSymbol: true),
        textAlign: TextAlign.right,
        style: const TextStyle(
          color: ColorSet.white,
          fontWeight: FontWeight.bold,
          fontSize: 15,
        ),
      ),
    );
  }

  Widget buildBillOptionBottomSheet(context, Bill bill) {
    return OptionMenuBottomSheet(
      items: [
        OptionMenuItem(
            label: 'Open',
            iconData: Icons.open_in_new,
            onSelect: () {
              openBill(context, bill);
            }),
        OptionMenuItem.remove(() {
          showRemoveConfirmationDialog(
            context,
            () => doRemove(context, bill),
          );
        }),
      ],
    );
  }

  void doRemove(context, Bill bill) {
    getBillsBloc(context).add(RemoveBill(
        billId: bill.id,
        onSucceed: () {
          _getDebtsSummaryBloc(context).loadDebtsSummary(bill.eventGroupId);
        }));
  }

  void showRemoveConfirmationDialog(context, VoidCallback onConfirm) {
    showDialog(
      context: context,
      builder: (_) {
        return ConfirmationDialog(
          positiveText: 'Remove',
          onPositiveClicked: onConfirm,
          negativeText: 'Cancel',
        );
      },
    );
  }

  void openBill(context, Bill bill) {
    var arg = BillFormPageArgs(bill: bill, isEditing: true);
    Navigator.of(context)
        .pushNamed(RouteNames.billForm, arguments: arg)
        .then((_) {
      reloadTransactions(context);
    });
  }

  String getPersonName(context, String personId) {
    final members = getTransactionsCubit(context).members;
    try {
      return members.firstWhere((el) => el.id == personId).name;
    } catch (_) {
      return '';
    }
  }

  void reloadTransactions(context) {
    getTransactionsCubit(context).loadActiveEventGroup();
  }

  TransactionsCubit getTransactionsCubit(context) =>
      BlocProvider.of<TransactionsCubit>(context);

  DebtsSummaryCubit _getDebtsSummaryBloc(context) =>
      BlocProvider.of<DebtsSummaryCubit>(context);

  BillsBloc getBillsBloc(context) => BlocProvider.of<BillsBloc>(context);
}

class TitleWithAvatar extends StatelessWidget {
  final Bill bill;

  const TitleWithAvatar({
    Key? key,
    required this.bill,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          backgroundColor: ColorSet.accent,
          radius: 16,
          child: bill.icon.widget(),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 12),
            child: Text(
              buildTitle(bill.title),
              style: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 16,
              ),
            ),
          ),
        ),
      ],
    );
  }

  String buildTitle(String raw) {
    return raw.trim().isNotEmpty ? raw : DisplayText.emptyBillTitle;
  }
}
