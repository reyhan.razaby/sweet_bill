import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bills/bills_bloc.dart';

import '../../../../core/constants/color_set.dart';
import '../../../../domain/entities/bill/bill.dart';
import 'bill_tile.dart';

class BillList extends StatefulWidget {
  final double horizontalPadding;

  const BillList({
    Key? key,
    required this.horizontalPadding,
  }) : super(key: key);

  @override
  State<BillList> createState() => _BillListState();
}

class _BillListState extends State<BillList> {
  @override
  Widget build(BuildContext context) {
    BillsState currentState = _getBillsBloc.state;
    if (currentState is! BillsLoaded) {
      return Container();
    }

    List<Bill> bills = currentState.displayedBills;
    int itemCount = bills.length + 2;
    return ListView.separated(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: itemCount,
      itemBuilder: (_, index) {
        if (index == 0 || index == itemCount - 1) {
          return Container();
        }
        Bill bill = bills[index - 1];
        return BillTile(
          bill: bill,
          tileIndex: index - 1,
          horizontalPadding: widget.horizontalPadding,
        );
      },
      separatorBuilder: (_, index) {
        return const Divider(
          color: ColorSet.dark,
          height: 1,
          thickness: 2,
        );
      },
    );
  }

  BillsBloc get _getBillsBloc => BlocProvider.of<BillsBloc>(context);
}
