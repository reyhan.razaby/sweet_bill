import 'package:flutter/material.dart';
import 'package:sweet_bill/core/constants/color_set.dart';

class EmptyDisplay extends StatelessWidget {
  const EmptyDisplay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text(
            '🦭',
            style: TextStyle(fontSize: 56),
          ),
          SizedBox(height: 12),
          Text(
            'No bills. Add new one!',
            style: TextStyle(
              fontSize: 17,
            ),
            textAlign: TextAlign.center,
          ),
          Text(
            '(or the seal will stay here forever)',
            style: TextStyle(
              fontSize: 13,
              height: 1.5,
              color: ColorSet.grey,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
