import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/model_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/calculated_debts_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/event_group_model.dart';

import '../uuid_generator.dart';

class EventGroupSpGateway extends ModelSpGateway<EventGroupModel> {
  EventGroupSpGateway(SharedPreferences sp) : super(sp);

  static const String _billsOfEventGroup = 'billsOfEventGroup';
  static const String _personsOfEventGroup = 'personsOfEventGroup';
  static const String _activeEventGroupId = 'activeEventGroupId';
  static const String _calculatedDebts = 'eventGroup_calculatedDebts';

  @override
  Future<EventGroupModel> save(EventGroupModel model) async {
    EventGroupModel saved = model;
    if (saved.id == '') {
      saved = saved.copyWith(
        id: UuidGenerator.generate(content: saved.name),
      );
    }
    await saveObjectAndRegisterId(saved);
    return saved;
  }

  @override
  Future<EventGroupModel?> getById(String id) async {
    return getAndConvert(id, EventGroupModel.fromJson);
  }

  Future<void> saveBillIds(String eventGroupId, List<String> billIds) async {
    await sp.setStringList('$_billsOfEventGroup-$eventGroupId', billIds);
  }

  Future<void> appendBillIds(
      String eventGroupId, List<String> newBillIds) async {
    List<String> currentBills = await getBillIds(eventGroupId);
    currentBills.addAll(newBillIds);
    return saveBillIds(eventGroupId, currentBills);
  }

  Future<void> removeBillId(String eventGroupId, String billId) async {
    List<String> currentBills = await getBillIds(eventGroupId);
    currentBills.removeWhere((id) => id == billId);
    return saveBillIds(eventGroupId, currentBills);
  }

  Future<List<String>> getBillIds(String eventGroupId) async {
    return sp.getStringList('$_billsOfEventGroup-$eventGroupId') ?? [];
  }

  Future<void> savePersonIds(
      String eventGroupId, List<String> personIds) async {
    await sp.setStringList('$_personsOfEventGroup-$eventGroupId', personIds);
  }

  Future<void> appendPersonIds(
      String eventGroupId, List<String> newPersonIds) async {
    List<String> currentPersons = await getPersonIds(eventGroupId);

    // Prevent duplication
    final filteredPersonIds = List.of(newPersonIds)
      ..removeWhere((e) => currentPersons.contains(e));
    currentPersons.addAll(filteredPersonIds);
    return savePersonIds(eventGroupId, currentPersons);
  }

  Future<void> removePersonId(String eventGroupId, String personId) async {
    List<String> currentPersons = await getPersonIds(eventGroupId);
    currentPersons.removeWhere((id) => id == personId);
    return savePersonIds(eventGroupId, currentPersons);
  }

  Future<List<String>> getPersonIds(String eventGroupId) async {
    return sp.getStringList('$_personsOfEventGroup-$eventGroupId') ?? [];
  }

  Future<void> setActiveEventGroupId(String eventGroupId) async {
    await sp.setString(_activeEventGroupId, eventGroupId);
  }

  Future<String> getActiveEventGroupId() async {
    return Future.value(sp.getString(_activeEventGroupId) ?? '');
  }

  Future<void> saveCalculatedDebts(
      String eventGroupId, CalculatedDebtsModel calculatedDebts) async {
    await sp.setString('$_calculatedDebts-$eventGroupId',
        jsonEncode(calculatedDebts.toJson()));
  }

  Future<CalculatedDebtsModel?> getCalculatedDebts(String eventGroupId) async {
    String? stringJson = sp.getString('$_calculatedDebts-$eventGroupId');

    if (stringJson == null) {
      return null;
    }

    return CalculatedDebtsModel.fromJson(jsonDecode(stringJson));
  }
}
