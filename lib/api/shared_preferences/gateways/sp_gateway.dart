import 'package:shared_preferences/shared_preferences.dart';

abstract class SpGateway {
  final SharedPreferences _sp;

  SpGateway(this._sp);

  SharedPreferences get sp => _sp;
}
