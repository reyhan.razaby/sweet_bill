import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/model_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';

import '../uuid_generator.dart';

class PersonSpGateway extends ModelSpGateway<PersonModel> {
  PersonSpGateway(SharedPreferences sp) : super(sp);

  static const String selfPerson = 'selfPerson';

  @override
  Future<PersonModel> save(PersonModel model) async {
    PersonModel saved = model;
    if (saved.id == '') {
      saved = saved.copyWith(
        id: UuidGenerator.generate(content: saved.name),
      );
    }
    await saveObjectAndRegisterId(saved);
    return saved;
  }

  @override
  Future<PersonModel?> getById(String id) async {
    return getAndConvert(id, PersonModel.fromJson);
  }

  Future<void> saveSelfPersonId(String personId) async {
    sp.setString(selfPerson, personId);
  }

  Future<String?> getSelfPersonId() async {
    return sp.getString(selfPerson);
  }
}
