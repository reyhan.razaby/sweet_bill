import 'dart:convert';

import 'package:sweet_bill/api/shared_preferences/gateways/sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/entity_model.dart';

abstract class ModelSpGateway<M extends EntityModel> extends SpGateway {
  ModelSpGateway(super.sp);

  Future<M> save(M model);

  Future<M?> getById(String id);

  Future<bool> removeById(String id) async {
    if (sp.getString('$entityKey-$id') == null) {
      return Future.value(false);
    }
    await sp.remove('$entityKey-$id');
    List<String> ids = await getAllIds();
    ids.removeWhere((el) => el == id);
    return sp.setStringList(idsKey, ids);
  }

  Future<List<String>> getAllIds() async {
    var stringList = sp.getStringList(idsKey);
    return Future.value(stringList ?? []);
  }

  Future<List<M>> getAll() async {
    List<M> result = [];

    List<String> ids = sp.getStringList(idsKey) ?? [];
    for (var id in ids) {
      M? m = await getById(id);
      if (m != null) {
        result.add(m);
      }
    }
    return Future.value(result);
  }

  M? getAndConvert(String id, M Function(Map<String, dynamic>) jsonToModel) {
    String? jsonString = sp.getString('$entityKey-$id');
    if (jsonString == null) {
      return null;
    }
    return jsonToModel(jsonDecode(jsonString));
  }

  String get entityKey => M.toString();

  String get idsKey => '$entityKey-ids';

  Future<void> saveObjectAndRegisterId(M model) async {
    if (model.id == '') {
      throw Exception('Cannot register with empty id');
    }

    // Save as json string by id
    final String key = '$entityKey-${model.id}';
    await sp.setString(key, jsonEncode(model.toJson()));

    // Register to all id list
    List<String> allIds = sp.getStringList(idsKey) ?? [];
    if (!allIds.contains(model.id)) {
      allIds.add(model.id);
      await sp.setStringList(idsKey, allIds);
    }
  }

  String? getJsonString(String id) {
    return sp.getString('$entityKey-$id');
  }
}
