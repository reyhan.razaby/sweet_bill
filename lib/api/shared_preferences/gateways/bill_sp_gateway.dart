import 'package:sweet_bill/api/shared_preferences/gateways/model_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/bill_model.dart';
import 'package:sweet_bill/api/shared_preferences/uuid_generator.dart';

class BillSpGateway extends ModelSpGateway<BillModel> {
  BillSpGateway(super.sp);

  @override
  Future<BillModel> save(BillModel model) async {
    BillModel savedModel = model;
    if (savedModel.id == '') {
      savedModel = savedModel.copyWith(
        id: UuidGenerator.generate(content: model.title),
      );
    }
    await saveObjectAndRegisterId(savedModel);
    return savedModel;
  }

  @override
  Future<BillModel?> getById(String id) async {
    return getAndConvert(id, BillModel.fromJson);
  }
}
