import 'package:sweet_bill/api/shared_preferences/gateways/sp_gateway.dart';

class SettingsSpGateway extends SpGateway {
  SettingsSpGateway(super.sp);

  static const String _kMaxFractionDigits = 'maxFractionDigits';
  static const String _kLocale = 'locale';

  String? getLocale() {
    return sp.getString(_kLocale);
  }

  int? getMaxFractionDigits() {
    return sp.getInt(_kMaxFractionDigits);
  }

  void setLocale(String locale) async {
    await sp.setString(_kLocale, locale);
  }

  void setMaxFractionDigits(int maxFractionDigits) async {
    await sp.setInt(_kMaxFractionDigits, maxFractionDigits);
  }
}
