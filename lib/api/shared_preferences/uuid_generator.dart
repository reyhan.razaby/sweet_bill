import 'package:uuid/uuid.dart';

class UuidGenerator {
  static const Uuid _uuid = Uuid();

  static String generate({String? content}) {
    return _uuid.v5(Uuid.NAMESPACE_NIL, '$content${DateTime.now()}');
  }
}
