import 'package:equatable/equatable.dart';

class SettingsModel extends Equatable {
  final String? locale;
  final String? decimalSeparator;
  final String? thousandSeparator;
  final int? maxWholeDigits;
  final int? maxFractionDigits;

  const SettingsModel({
    this.locale,
    this.decimalSeparator,
    this.thousandSeparator,
    this.maxWholeDigits,
    this.maxFractionDigits,
  });

  @override
  List<Object?> get props => [
        locale,
        decimalSeparator,
        thousandSeparator,
        maxWholeDigits,
        maxFractionDigits,
      ];
}
