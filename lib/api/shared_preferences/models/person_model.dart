import 'package:sweet_bill/api/shared_preferences/models/entity_model.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

class PersonModel extends EntityModel {
  final String name;
  final PersonAvatar avatar;

  const PersonModel({
    required String id,
    required this.name,
    required DateTime creationTime,
    required this.avatar,
  }) : super(id, creationTime);

  @override
  List<Object?> get props => [id, name, avatar];

  PersonModel.unknown({
    id = '',
  })  : avatar = PersonAvatar.pa001,
        name = '',
        super(id, DateTime.now());

  @override
  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "name": name,
      "avatar": avatar.toString(),
      "creationTime": creationTime.toIso8601String(),
    };
  }

  factory PersonModel.fromJson(Map<String, dynamic> json) {
    return PersonModel(
      id: json["id"],
      creationTime: DateTime.parse(json["creationTime"]),
      name: json["name"],
      avatar: PersonAvatar.fromString(json["avatar"]),
    );
  }

  factory PersonModel.fromEntity(Person person) {
    return PersonModel(
      id: person.id,
      name: person.name,
      creationTime: person.creationTime ?? DateTime.now(),
      avatar: person.avatar,
    );
  }

  Person toEntity() {
    return Person(
      id: id,
      name: name,
      creationTime: creationTime,
      avatar: avatar,
    );
  }

  PersonModel copyWith({
    String? id,
    DateTime? creationTime,
    String? name,
    PersonAvatar? avatar,
  }) {
    return PersonModel(
      id: id ?? this.id,
      creationTime: creationTime ?? this.creationTime,
      name: name ?? this.name,
      avatar: avatar ?? this.avatar,
    );
  }
}
