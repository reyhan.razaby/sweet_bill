import 'package:equatable/equatable.dart';
import 'package:sweet_bill/api/shared_preferences/models/debt_model.dart';

class CalculatedDebtsModel extends Equatable {
  final String receivablesKey;
  final List<DebtModel> debts;

  const CalculatedDebtsModel({
    required this.receivablesKey,
    required this.debts,
  });

  Map<String, dynamic> toJson() {
    return {
      "receivablesKey": receivablesKey,
      "debts": debts.map((e) => e.toJson()).toList(),
    };
  }

  factory CalculatedDebtsModel.fromJson(Map<String, dynamic> json) {
    return CalculatedDebtsModel(
      receivablesKey: json["receivablesKey"],
      debts: List.of(json["debts"]).map((i) => DebtModel.fromJson(i)).toList(),
    );
  }

  @override
  List<Object?> get props => [receivablesKey, debts];

  CalculatedDebtsModel copyWith({
    String? receivablesKey,
    List<DebtModel>? debts,
  }) {
    return CalculatedDebtsModel(
      receivablesKey: receivablesKey ?? this.receivablesKey,
      debts: debts ?? this.debts,
    );
  }
}
