import 'package:equatable/equatable.dart';
import 'package:sweet_bill/api/shared_preferences/models/entity_model.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';
import 'package:sweet_bill/core/constants/split_strategies.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';

class BillModel extends EntityModel {
  final String title;
  final BillIcons icon;
  final String eventGroupId;
  final DateTime transactionTime;
  final List<BillPersonalCostModel> personalCosts;
  final List<BillPersonalPaymentModel> personalPayments;
  final SplitStrategies splitStrategy;
  final bool isAutoAdjustTax;

  const BillModel({
    required String id,
    required DateTime creationTime,
    required this.title,
    required this.icon,
    required this.eventGroupId,
    required this.transactionTime,
    required this.personalCosts,
    required this.personalPayments,
    required this.splitStrategy,
    required this.isAutoAdjustTax,
  }) : super(id, creationTime);

  @override
  List<Object?> get props => [
        id,
        title,
        icon,
        eventGroupId,
        transactionTime,
        personalCosts,
        personalPayments,
        splitStrategy,
        isAutoAdjustTax,
      ];

  @override
  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "creationTime": creationTime.toIso8601String(),
      "title": title,
      "eventGroupId": eventGroupId,
      "icon": icon.toString(),
      "transactionTime": transactionTime.toIso8601String(),
      "personalCosts": personalCosts.map((e) => e.toJson()).toList(),
      "personalPayments": personalPayments.map((e) => e.toJson()).toList(),
      "splitStrategy": splitStrategy.toString(),
      "isAutoAdjustTax": isAutoAdjustTax,
    };
  }

  factory BillModel.fromEntity(Bill bill) {
    return BillModel(
      id: bill.id,
      title: bill.title,
      eventGroupId: bill.eventGroupId,
      creationTime: DateTime.now(),
      icon: bill.icon,
      personalCosts: bill.personalCosts
          .map((e) => BillPersonalCostModel.fromEntity(e))
          .toList(),
      personalPayments: bill.personalPayments
          .map((e) => BillPersonalPaymentModel.fromEntity(e))
          .toList(),
      isAutoAdjustTax: bill.isAutoAdjustTax,
      splitStrategy: bill.splitStrategy,
      transactionTime: bill.transactionTime,
    );
  }

  Bill toEntity() {
    return Bill(
      id: id,
      title: title,
      icon: icon,
      eventGroupId: eventGroupId,
      transactionTime: transactionTime,
      personalCosts: personalCosts.map((e) => e.toEntity()).toList(),
      personalPayments: personalPayments.map((e) => e.toEntity()).toList(),
      splitStrategy: splitStrategy,
      isAutoAdjustTax: isAutoAdjustTax,
    );
  }

  factory BillModel.fromJson(Map<String, dynamic> json) {
    return BillModel(
      id: json["id"],
      creationTime: DateTime.parse(json["creationTime"]),
      title: json["title"],
      eventGroupId: json["eventGroupId"],
      icon: BillIcons.fromString(json["icon"]),
      transactionTime: DateTime.parse(json["transactionTime"]),
      personalCosts: List.of(json["personalCosts"])
          .map((i) => BillPersonalCostModel.fromJson(i))
          .toList(),
      personalPayments: List.of(json["personalPayments"])
          .map((i) => BillPersonalPaymentModel.fromJson(i))
          .toList(),
      splitStrategy: SplitStrategies.fromString(json["splitStrategy"]),
      isAutoAdjustTax: json["isAutoAdjustTax"],
    );
  }

  BillModel copyWith({
    String? id,
    DateTime? creationTime,
    String? title,
    String? eventGroupId,
    BillIcons? icon,
    DateTime? transactionTime,
    List<BillPersonalCostModel>? personalCosts,
    List<BillPersonalPaymentModel>? personalPayments,
    SplitStrategies? splitStrategy,
    bool? isAutoAdjustTax,
  }) {
    return BillModel(
      id: id ?? this.id,
      creationTime: creationTime ?? this.creationTime,
      title: title ?? this.title,
      icon: icon ?? this.icon,
      eventGroupId: eventGroupId ?? this.eventGroupId,
      transactionTime: transactionTime ?? this.transactionTime,
      personalCosts: personalCosts ?? this.personalCosts,
      personalPayments: personalPayments ?? this.personalPayments,
      splitStrategy: splitStrategy ?? this.splitStrategy,
      isAutoAdjustTax: isAutoAdjustTax ?? this.isAutoAdjustTax,
    );
  }
}

class BillPersonalCostModel extends Equatable {
  final String personId;
  final double cost;
  final double additionalCost;
  final String? note;

  const BillPersonalCostModel({
    required this.personId,
    required this.cost,
    required this.additionalCost,
    this.note,
  });

  Map<String, dynamic> toJson() {
    return {
      "personId": personId,
      "cost": cost,
      "additionalCost": additionalCost,
      "note": note,
    };
  }

  factory BillPersonalCostModel.fromEntity(BillPersonalCost personalCost) {
    return BillPersonalCostModel(
      personId: personalCost.personId,
      additionalCost: personalCost.additionalCost,
      cost: personalCost.cost,
      note: personalCost.note,
    );
  }

  BillPersonalCost toEntity() {
    return BillPersonalCost(
      personId: personId,
      cost: cost,
      additionalCost: additionalCost,
    );
  }

  factory BillPersonalCostModel.fromJson(Map<String, dynamic> json) {
    return BillPersonalCostModel(
      personId: json["personId"],
      cost: json["cost"],
      additionalCost: json["additionalCost"],
      note: json["note"],
    );
  }

  @override
  List<Object?> get props => [personId, cost, additionalCost, note];
}

class BillPersonalPaymentModel extends Equatable {
  final String personId;
  final double amount;

  const BillPersonalPaymentModel({
    required this.personId,
    required this.amount,
  });

  Map<String, dynamic> toJson() {
    return {
      "personId": personId,
      "amount": amount,
    };
  }

  factory BillPersonalPaymentModel.fromEntity(
      BillPersonalPayment personalPayment) {
    return BillPersonalPaymentModel(
      personId: personalPayment.personId,
      amount: personalPayment.amount,
    );
  }

  BillPersonalPayment toEntity() {
    return BillPersonalPayment(
      personId: personId,
      amount: amount,
    );
  }

  factory BillPersonalPaymentModel.fromJson(Map<String, dynamic> json) {
    return BillPersonalPaymentModel(
      personId: json["personId"],
      amount: json["amount"],
    );
  }

  @override
  List<Object> get props => [personId, amount];
}
