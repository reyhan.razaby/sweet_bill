import 'package:sweet_bill/api/shared_preferences/models/entity_model.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';

class EventGroupModel extends EntityModel {
  final String name;
  final String? description;
  final String? imagePath;

  const EventGroupModel({
    required String id,
    required DateTime creationTime,
    required this.name,
    this.description,
    this.imagePath,
  }) : super(id, creationTime);

  @override
  List<Object?> get props => [id, name, description, imagePath];

  factory EventGroupModel.fromJson(Map<String, dynamic> json) {
    return EventGroupModel(
      id: json["id"],
      creationTime: DateTime.parse(json["creationTime"]),
      name: json["name"],
      imagePath: json["imagePath"],
      description: json["description"],
    );
  }

  factory EventGroupModel.fromEntity(EventGroup eventGroup) {
    return EventGroupModel(
      id: eventGroup.id,
      name: eventGroup.name,
      description: eventGroup.description,
      imagePath: eventGroup.imagePath,
      creationTime: eventGroup.creationTime ?? DateTime.now(),
    );
  }

  EventGroup toEntity() {
    return EventGroup(
        id: id,
        name: name,
        description: description,
        imagePath: imagePath,
        creationTime: creationTime);
  }

  @override
  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "creationTime": creationTime.toIso8601String(),
      "name": name,
      "description": description,
      "imagePath": imagePath,
    };
  }

  EventGroupModel copyWith({
    String? id,
    DateTime? creationTime,
    String? name,
    String? description,
    String? imagePath,
  }) {
    return EventGroupModel(
      id: id ?? super.id,
      creationTime: creationTime ?? super.creationTime,
      name: name ?? this.name,
      description: description ?? this.description,
      imagePath: imagePath ?? this.imagePath,
    );
  }
}
