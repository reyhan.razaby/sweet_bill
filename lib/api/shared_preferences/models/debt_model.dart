import 'package:equatable/equatable.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';

import '../../../domain/entities/debt/debt.dart';

class DebtModel extends Equatable {
  final String borrowerId;
  final String lenderId;
  final double amount;

  const DebtModel({
    required this.borrowerId,
    required this.lenderId,
    this.amount = 0,
  });

  Map<String, dynamic> toJson() {
    return {
      "borrowerId": borrowerId,
      "lenderId": lenderId,
      "amount": amount,
    };
  }

  factory DebtModel.fromJson(Map<String, dynamic> json) {
    return DebtModel(
      borrowerId: json["borrowerId"],
      lenderId: json["lenderId"],
      amount: json["amount"],
    );
  }

  Debt toEntity({
    required PersonModel borrower,
    required PersonModel lender,
  }) {
    return Debt(
      borrower: borrower.toEntity(),
      lender: lender.toEntity(),
      amount: amount,
    );
  }

  factory DebtModel.fromEntity(Debt debt) {
    return DebtModel(
      borrowerId: debt.borrower.id,
      lenderId: debt.lender.id,
      amount: debt.amount,
    );
  }

  @override
  List<Object?> get props => [borrowerId, lenderId, amount];

  DebtModel copyWith({
    String? borrowerId,
    String? lenderId,
    double? amount,
  }) {
    return DebtModel(
      borrowerId: borrowerId ?? this.borrowerId,
      lenderId: lenderId ?? this.lenderId,
      amount: amount ?? this.amount,
    );
  }
}
