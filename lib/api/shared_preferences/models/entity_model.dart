import 'package:equatable/equatable.dart';

abstract class EntityModel extends Equatable {
  final String id;
  final DateTime creationTime;

  const EntityModel(this.id, this.creationTime);

  Map<String, dynamic> toJson();

  String? get creationTimeInString => creationTime.toIso8601String();
}
