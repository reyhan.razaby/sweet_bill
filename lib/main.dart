import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:sweet_bill/core/constants/route_names.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/bloc/transactions/transactions_cubit.dart';
import 'package:sweet_bill/presentation/themes/theme_data.dart';
import 'package:sweet_bill/routes.dart';

import 'core/injection_container/service_locator.dart';

Future<void> main() async {
  final widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);

  await initDependencies();
  await srvLocator<ProfileCubit>().checkProfile();

  FlutterNativeSplash.remove();

  runApp(MultiBlocProvider(
    providers: [
      BlocProvider<ProfileCubit>(
        create: (_) => srvLocator<ProfileCubit>(),
      ),
      BlocProvider<TransactionsCubit>(
        create: (_) => srvLocator<TransactionsCubit>()..loadActiveEventGroup(),
      ),
    ],
    child: const MainPage(),
  ));
}

class MainPage extends StatelessWidget {
  const MainPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPaintSizeEnabled = false;
    final profileState = BlocProvider.of<ProfileCubit>(context).state;
    return MaterialApp(
      title: 'Sweet Bill',
      theme: buildThemeData(context),
      initialRoute: profileState.isEmpty ? RouteNames.welcome : RouteNames.home,
      onGenerateRoute: Routes.getRoute,
    );
  }
}
