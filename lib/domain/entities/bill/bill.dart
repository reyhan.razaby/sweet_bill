import 'package:equatable/equatable.dart';
import 'package:sweet_bill/core/constants/bill_icons.dart';

import '../../../core/constants/split_strategies.dart';

class Bill extends Equatable {
  final String id;
  final String title;
  final BillIcons icon;
  final DateTime transactionTime;
  final String eventGroupId;
  final List<BillPersonalCost> personalCosts;
  final List<BillPersonalPayment> personalPayments;
  final SplitStrategies splitStrategy;
  final bool isAutoAdjustTax;

  const Bill({
    required this.id,
    required this.title,
    required this.icon,
    required this.transactionTime,
    required this.eventGroupId,
    required this.personalCosts,
    required this.personalPayments,
    required this.splitStrategy,
    required this.isAutoAdjustTax,
  });

  Bill.asDefault({
    required this.id,
    required this.title,
    this.eventGroupId = '',
    required this.personalCosts,
    required this.personalPayments,
  })  : icon = BillIcons.receipt,
        splitStrategy = SplitStrategies.custom,
        transactionTime = DateTime.now(),
        isAutoAdjustTax = true;

  Bill.initial({this.id = ''})
      : title = '',
        eventGroupId = '',
        icon = BillIcons.receipt,
        transactionTime = DateTime.now(),
        personalCosts = [],
        personalPayments = [],
        splitStrategy = SplitStrategies.equally,
        isAutoAdjustTax = true;

  double get totalCost {
    return personalCosts
        .map((p) => p.totalCost)
        .fold(0, (prev, amount) => prev + amount);
  }

  double get totalPayment {
    return personalPayments
        .map((payment) => payment.amount)
        .fold(0, (prev, amount) => prev + amount);
  }

  @override
  List<Object?> get props => [
        id,
        title,
        icon,
        eventGroupId,
        transactionTime,
        personalCosts,
        personalPayments,
        splitStrategy,
        isAutoAdjustTax,
      ];

  Bill copyWith({
    String? id,
    String? title,
    BillIcons? icon,
    String? eventGroupId,
    DateTime? transactionTime,
    List<BillPersonalCost>? personalCosts,
    List<BillPersonalPayment>? personalPayments,
    SplitStrategies? splitStrategy,
    bool? isAutoAdjustTax,
  }) {
    return Bill(
      id: id ?? this.id,
      title: title ?? this.title,
      icon: icon ?? this.icon,
      eventGroupId: eventGroupId ?? this.eventGroupId,
      transactionTime: transactionTime ?? this.transactionTime,
      personalCosts: personalCosts ?? this.personalCosts,
      personalPayments: personalPayments ?? this.personalPayments,
      splitStrategy: splitStrategy ?? this.splitStrategy,
      isAutoAdjustTax: isAutoAdjustTax ?? this.isAutoAdjustTax,
    );
  }
}

class BillPersonalCost extends Equatable {
  final String personId;
  final double cost;
  final double additionalCost;
  final String? note;

  const BillPersonalCost({
    required this.personId,
    required this.cost,
    this.additionalCost = 0,
    this.note,
  });

  double get totalCost => cost + additionalCost;

  @override
  List<Object?> get props => [personId, cost, additionalCost, note];

  BillPersonalCost copyWith({
    String? personId,
    double? cost,
    double? additionalCost,
    String? note,
  }) {
    return BillPersonalCost(
      personId: personId ?? this.personId,
      cost: cost ?? this.cost,
      additionalCost: additionalCost ?? this.additionalCost,
      note: note ?? this.note,
    );
  }

  static double sumTotalCost(List<BillPersonalCost> costs) =>
      costs.map((e) => e.totalCost).fold(0, (a, b) => a + b);
}

class BillPersonalPayment extends Equatable {
  final String personId;
  final double amount;

  const BillPersonalPayment({
    required this.personId,
    required this.amount,
  });

  @override
  List<Object> get props => [personId, amount];

  BillPersonalPayment copyWith({
    String? personId,
    double? amount,
  }) {
    return BillPersonalPayment(
      personId: personId ?? this.personId,
      amount: amount ?? this.amount,
    );
  }

  static double sumAmount(List<BillPersonalPayment> payments) =>
      payments.map((p) => p.amount).fold(0, (a, b) => a + b);
}
