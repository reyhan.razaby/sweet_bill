import 'package:equatable/equatable.dart';
import 'package:sweet_bill/core/constants/person_avatar.dart';

class Person extends Equatable {
  final String id;
  final String name;
  final String? phoneNumber;
  final PersonAvatar avatar;
  final DateTime? creationTime;

  const Person({
    required this.id,
    required this.name,
    this.phoneNumber,
    required this.avatar,
    this.creationTime,
  });

  const Person.initial({
    this.id = '',
    required this.name,
    this.phoneNumber,
    required this.avatar,
    this.creationTime,
  });

  Person.unknown({
    this.id = '',
    this.phoneNumber,
    DateTime? creationTime,
  })  : avatar = PersonAvatar.pa001,
        name = '',
        creationTime = creationTime ?? DateTime.now();

  @override
  List<Object?> get props => [id, name, phoneNumber, avatar, creationTime];

  Person copyWith({
    String? id,
    String? name,
    String? phoneNumber,
    PersonAvatar? avatar,
    DateTime? creationTime,
  }) {
    return Person(
      id: id ?? this.id,
      name: name ?? this.name,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      avatar: avatar ?? this.avatar,
      creationTime: creationTime ?? this.creationTime,
    );
  }
}
