enum PersonSortingField {
  nameAsc,
  nameDesc,
  creationTimeAsc,
  creationTimeDesc,
}
