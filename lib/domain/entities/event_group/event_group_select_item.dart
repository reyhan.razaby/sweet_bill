import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:equatable/equatable.dart';

class EventGroupSelectItem extends Equatable {
  final EventGroup eventGroup;
  final bool isCurrentlyActive;
  final List<EventGroupMember> members;

  const EventGroupSelectItem(
    this.eventGroup, {
    this.isCurrentlyActive = false,
    this.members = const [],
  });

  EventGroupSelectItem copyWith({
    EventGroup? eventGroup,
    bool? isCurrentlyActive,
    List<EventGroupMember>? members,
  }) {
    return EventGroupSelectItem(
      eventGroup ?? this.eventGroup,
      isCurrentlyActive: isCurrentlyActive ?? this.isCurrentlyActive,
      members: members ?? this.members,
    );
  }

  @override
  List<Object?> get props => [eventGroup, isCurrentlyActive, members];
}

class EventGroupMember extends Equatable {
  final String name;
  final bool isSelf;

  const EventGroupMember(this.name, {this.isSelf = false});

  @override
  List<Object?> get props => [name, isSelf];
}
