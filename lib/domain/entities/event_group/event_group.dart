import 'package:equatable/equatable.dart';

class EventGroup extends Equatable {
  final String id;
  final String name;
  final String? imagePath;
  final String? description;
  final DateTime? creationTime;

  const EventGroup({
    required this.id,
    required this.name,
    this.imagePath,
    this.description,
    this.creationTime,
  });

  const EventGroup.initial({
    required this.name,
    this.imagePath,
    this.description,
    this.creationTime,
  }) : id = '';

  EventGroup copyWith({
    String? id,
    String? name,
    String? imagePath,
    String? description,
    DateTime? creationTime,
  }) {
    return EventGroup(
      id: id ?? this.id,
      name: name ?? this.name,
      imagePath: imagePath ?? this.imagePath,
      description: description ?? this.description,
      creationTime: creationTime ?? this.creationTime,
    );
  }

  @override
  List<Object?> get props => [id, name, imagePath, description, creationTime];
}
