import 'package:equatable/equatable.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';

import '../person/person.dart';

class EventGroupDetail extends Equatable {
  final EventGroup eventGroup;
  final List<Person>? members;

  const EventGroupDetail({
    required this.eventGroup,
    this.members,
  });

  @override
  List<Object?> get props => [eventGroup, members];
}
