import 'package:equatable/equatable.dart';

class Settings extends Equatable {
  final String locale;
  final String decimalSeparator;
  final String thousandSeparator;
  final int maxWholeDigits;
  final int maxFractionDigits;

  const Settings({
    required this.locale,
    required this.decimalSeparator,
    required this.thousandSeparator,
    required this.maxWholeDigits,
    required this.maxFractionDigits,
  });

  @override
  List<Object> get props => [
        locale,
        decimalSeparator,
        thousandSeparator,
        maxWholeDigits,
        maxFractionDigits,
      ];
}
