import 'package:equatable/equatable.dart';

import 'debt.dart';

class CalculatedDebts extends Equatable {
  final String receivablesKey;
  final List<Debt> debts;

  const CalculatedDebts({
    required this.receivablesKey,
    required this.debts,
  });

  CalculatedDebts.empty()
      : receivablesKey = '',
        debts = [];

  @override
  List<Object?> get props => [receivablesKey, debts];
}
