import 'package:equatable/equatable.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

class Debt extends Equatable {
  final Person borrower;
  final Person lender;
  final double amount;

  const Debt({
    required this.borrower,
    required this.lender,
    this.amount = 0,
  });

  @override
  List<Object?> get props => [borrower, lender, amount];

  String get desc => '${borrower.name} owes ${lender.name}';
}
