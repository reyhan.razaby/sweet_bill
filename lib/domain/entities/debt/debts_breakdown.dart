import 'package:equatable/equatable.dart';

import '../person/person.dart';
import 'debt.dart';

class DebtsBreakdown extends Equatable {
  final List<Debt> debts;
  final List<PersonalBreakdown> personalBreakdowns;

  const DebtsBreakdown({
    required this.debts,
    required this.personalBreakdowns,
  });

  @override
  List<Object?> get props => [debts, personalBreakdowns];
}

class PersonalBreakdown extends Equatable {
  final Person person;
  final double totalCost;
  final double totalPayment;

  const PersonalBreakdown({
    required this.person,
    required this.totalCost,
    required this.totalPayment,
  });

  double get totalBorrowed => totalCost - totalPayment;

  double get totalLent => totalPayment - totalCost;

  @override
  List<Object?> get props => [person, totalCost, totalPayment];
}
