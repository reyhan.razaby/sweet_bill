import 'package:equatable/equatable.dart';

class Receivable extends Equatable {
  final String personId;
  final double receivableAmount;
  final double? totalCost;
  final double? totalPayment;

  const Receivable({
    required this.personId,
    required this.receivableAmount,
    this.totalCost,
    this.totalPayment,
  });

  @override
  List<Object?> get props =>
      [personId, receivableAmount, totalPayment, totalCost];
}
