import 'package:equatable/equatable.dart';

import 'debt.dart';

class DebtsSummary extends Equatable {
  final List<Debt> debts;
  final double totalCost;
  final int numberOfBills;

  const DebtsSummary({
    required this.debts,
    required this.totalCost,
    required this.numberOfBills,
  });

  @override
  List<Object?> get props => [debts, totalCost, numberOfBills];
}
