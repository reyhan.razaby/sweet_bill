import 'package:sweet_bill/core/log.dart';
import 'package:dartz/dartz.dart';

import '../../core/exceptions/invalid_argument_exception.dart';
import '../../core/failures/failure.dart';
import '../../core/failures/invalid_argument_failure.dart';

abstract class UseCase<R, P> {
  Future<Either<Failure, R>> call(P param) async {
    try {
      return await execute(param);
    } on InvalidArgumentException catch (e) {
      return Left(InvalidArgumentFailure(message: e.cause));
    } catch (e, s) {
      Log.error('Unexpected error', e, s);
      return const Left(Failure());
    }
  }

  Future<Either<Failure, R>> execute(P param);
}
