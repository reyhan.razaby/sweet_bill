import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/debt/calculated_debts.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generators/debts_generator.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generators/debts_generator_v1.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/receivable_key_builder.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/bill/bill.dart';
import '../../../entities/debt/debt.dart';
import '../../../entities/person/person.dart';
import '../../../repositories/debt_repository.dart';
import '../../bill/get_bills/get_bills_param.dart';
import '../get_receivables/get_receivables_param.dart';
import '../get_receivables/get_receivables_use_case.dart';
import 'generate_debts_param.dart';

class GenerateDebtsUseCase extends UseCase<List<Debt>, GenerateDebtsParam> {
  final PersonRepository _personRepository;
  final GetReceivablesUseCase _getReceivablesUseCase;
  final DebtRepository _debtRepository;
  final BillRepository _billRepository;
  final DebtsGenerator _debtsGenerator;

  GenerateDebtsUseCase({
    required GetReceivablesUseCase getReceivablesUseCase,
    required PersonRepository personRepository,
    required DebtRepository debtRepository,
    required BillRepository billRepository,
    DebtsGenerator? debtsGenerator,
  })  : _getReceivablesUseCase = getReceivablesUseCase,
        _personRepository = personRepository,
        _debtRepository = debtRepository,
        _billRepository = billRepository,
        _debtsGenerator = debtsGenerator ?? _getDebtsGenerator();

  static DebtsGenerator _getDebtsGenerator() {
    return DebtsGeneratorV1();
  }

  @override
  Future<Either<Failure, List<Debt>>> execute(GenerateDebtsParam param) async {
    final List<Receivable> receivables = await _getReceivables(param);
    final CalculatedDebts calculatedDebts = await _getCalculatedDebts(param);

    // Return existing debts immediately
    String receivablesKey = buildReceivablesKey(receivables);
    if (receivablesKey == calculatedDebts.receivablesKey) {
      return Right(calculatedDebts.debts);
    }

    List<RawDebt> rawDebts = _debtsGenerator.generate(receivables);

    Map<String, Person> persons = {};
    List<Debt> debts = [];

    for (RawDebt rawDebt in rawDebts) {
      Person? borrower = persons[rawDebt.borrowerId];
      borrower ??= await _getPerson(rawDebt.borrowerId);

      Person? lender = persons[rawDebt.lenderId];
      lender ??= await _getPerson(rawDebt.lenderId);

      debts.add(Debt(
        borrower: borrower,
        lender: lender,
        amount: rawDebt.amount,
      ));
    }

    await _debtRepository.createCalculatedDebts(
      param.eventGroupId,
      CalculatedDebts(receivablesKey: receivablesKey, debts: debts),
    );
    return Right(debts);
  }

  Future<CalculatedDebts> _getCalculatedDebts(GenerateDebtsParam param) async {
    final eitherRes =
        await _debtRepository.getCalculatedDebts(param.eventGroupId);
    if (eitherRes.isLeft()) {
      throw Exception(eitherRes.asLeft().message);
    }
    return eitherRes.asRight();
  }

  Future<List<Receivable>> _getReceivables(GenerateDebtsParam param) async {
    List<Bill> bills = await _getBills(param);
    final eitherRes =
        await _getReceivablesUseCase(GetReceivablesParam(bills: bills));
    if (eitherRes.isLeft()) {
      throw Exception(eitherRes.asLeft().message);
    }
    return eitherRes.asRight();
  }

  Future<Person> _getPerson(String id) async {
    final resEither = await _personRepository.getPerson(id);
    if (resEither.isLeft()) {
      return Person.unknown(id: id);
    }
    return resEither.asRight();
  }

  Future<List<Bill>> _getBills(GenerateDebtsParam param) async {
    if (param.bills != null) {
      return param.bills!;
    }
    GetBillsParam getBillsParam =
        GetBillsParam(eventGroupId: param.eventGroupId);
    final billsEither = await _billRepository.getBills(getBillsParam);
    return billsEither.getOrElse(() => []);
  }
}
