import 'dart:math';

import 'package:sweet_bill/domain/entities/debt/receivable.dart';

import 'debts_generator.dart';

class DebtsGeneratorV1 implements DebtsGenerator {
  @override
  List<RawDebt> generate(List<Receivable> receivables) {
    List<MapEntry<String, double>> remReceivables = [];
    List<MapEntry<String, double>> remDebts = [];
    fill(remReceivables, remDebts, receivables);

    List<RawDebt> result = [];

    while (remReceivables.isNotEmpty && remDebts.isNotEmpty) {
      _clean(remReceivables, sort: true);
      _clean(remDebts, sort: true);

      _matchAllExactlySame(
        remReceivables,
        remDebts,
        result,
      );

      _clean(remReceivables);
      _clean(remDebts);

      bool finishBorrower = false;
      while (
          !finishBorrower && remReceivables.isNotEmpty && remDebts.isNotEmpty) {
        String borrower = remDebts[0].key;
        double borrowedAmount = remDebts[0].value;

        String lender = remReceivables[0].key;
        double lentAmount = remReceivables[0].value;

        double amount = min(borrowedAmount, lentAmount);
        remReceivables[0] = MapEntry(lender, lentAmount - amount);
        remDebts[0] = MapEntry(borrower, borrowedAmount - amount);

        result.add(
          RawDebt(borrowerId: borrower, lenderId: lender, amount: amount),
        );

        if (remDebts[0].value == 0) {
          finishBorrower = true;
        } else {
          _clean(remReceivables);
          _clean(remDebts);
        }
      }
    }

    return result;
  }

  void _matchAllExactlySame(
    List<MapEntry<String, double>> receivables,
    List<MapEntry<String, double>> debts,
    List<RawDebt> result,
  ) {
    for (int r = 0; r < receivables.length; r++) {
      var receivable = receivables[r];

      for (int d = 0; d < debts.length; d++) {
        var debt = debts[d];

        if (receivable.value > debt.value) {
          break;
        }
        if (receivable.value < debt.value) {
          continue;
        }

        result.add(
          RawDebt(
            borrowerId: debt.key,
            lenderId: receivable.key,
            amount: receivable.value,
          ),
        );
        receivables[r] = MapEntry(receivable.key, 0);
        debts[d] = MapEntry(debt.key, 0);
        break;
      }
    }
  }

  void _clean(List<MapEntry<String, double>> entries, {bool sort = false}) {
    entries.removeWhere((e) => e.value == 0);
    if (sort) {
      entries.sort((a, b) => b.value.compareTo(a.value));
    }
  }

  void fill(
    List<MapEntry<String, double>> receivableEntries,
    List<MapEntry<String, double>> debtEntries,
    List<Receivable> receivables,
  ) {
    for (var receivable in receivables) {
      if (receivable.receivableAmount == 0) {
        continue;
      }

      if (receivable.receivableAmount > 0) {
        receivableEntries.add(MapEntry(receivable.personId, receivable.receivableAmount));
      } else {
        debtEntries.add(MapEntry(receivable.personId, receivable.receivableAmount.abs()));
      }
    }
  }
}
