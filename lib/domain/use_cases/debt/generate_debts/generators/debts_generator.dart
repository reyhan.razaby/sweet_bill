import 'package:equatable/equatable.dart';

import '../../../../entities/debt/receivable.dart';

abstract class DebtsGenerator {
  List<RawDebt> generate(List<Receivable> receivables);
}

class RawDebt extends Equatable {
  final String borrowerId;
  final String lenderId;
  final double amount;

  const RawDebt({
    required this.borrowerId,
    required this.lenderId,
    required this.amount,
  });

  @override
  List<Object?> get props => [borrowerId, lenderId, amount];
}
