import '../../../entities/debt/receivable.dart';

String buildReceivablesKey(List<Receivable> receivables) {
  List<Receivable> sorted = List.of(receivables)
    ..sort((a, b) => a.personId.compareTo(b.personId))
    ..removeWhere((e) => e.receivableAmount == 0);

  return sorted
      .map((e) =>
          '${e.personId}:${e.receivableAmount}')
      .join('_');
}
