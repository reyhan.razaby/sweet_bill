import 'package:equatable/equatable.dart';

import '../../../entities/bill/bill.dart';

class GenerateDebtsParam extends Equatable {
  final String eventGroupId;
  final List<Bill>? bills;

  const GenerateDebtsParam({
    required this.eventGroupId,
    this.bills,
  });

  @override
  List<Object?> get props => [eventGroupId, bills];
}
