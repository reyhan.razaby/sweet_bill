import 'package:equatable/equatable.dart';

import '../../../entities/bill/bill.dart';

class GetReceivablesParam extends Equatable {
  final List<Bill> bills;
  final bool ignoreInvalidBills;

  @override
  List<Object> get props => [bills, ignoreInvalidBills];

  const GetReceivablesParam({
    required this.bills,
    this.ignoreInvalidBills = false,
  });
}
