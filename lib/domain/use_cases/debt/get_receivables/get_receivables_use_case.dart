import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/debt/receivable.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/bill/bill.dart';
import 'get_receivables_param.dart';

class GetReceivablesUseCase
    extends UseCase<List<Receivable>, GetReceivablesParam> {
  @override
  Future<Either<Failure, List<Receivable>>> execute(
      GetReceivablesParam param) async {
    if (param.bills.isEmpty) {
      return const Right([]);
    }

    // Store total amounts with personId as key
    Map<String, double> totalCosts = {};
    Map<String, double> totalPayments = {};

    Set<String> personIds = {};

    for (Bill bill in param.bills) {
      for (var cost in bill.personalCosts) {
        totalCosts.update(
          cost.personId,
          (value) => value + cost.totalCost,
          ifAbsent: () => cost.totalCost,
        );
        personIds.add(cost.personId);
      }
      for (var payment in bill.personalPayments) {
        totalPayments.update(
          payment.personId,
          (value) => value + payment.amount,
          ifAbsent: () => payment.amount,
        );
        personIds.add(payment.personId);
      }
    }

    List<Receivable> receivables = [];
    for (String personId in personIds) {
      double totalCost = totalCosts[personId] ?? 0;
      double totalPayment = totalPayments[personId] ?? 0;
      receivables.add(Receivable(
        personId: personId,
        receivableAmount: totalPayment - totalCost,
        totalPayment: totalPayment,
        totalCost: totalCost,
      ));
    }

    return Right(receivables);
  }
}
