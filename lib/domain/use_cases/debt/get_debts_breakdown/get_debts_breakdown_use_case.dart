import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_param.dart';

import '../../../entities/bill/bill.dart';
import '../../../entities/debt/debt.dart';
import '../../../entities/debt/debts_breakdown.dart';
import '../../../entities/debt/receivable.dart';
import '../../../entities/person/person.dart';
import '../../../repositories/bill_repository.dart';
import '../../../repositories/person_repository.dart';
import '../../bill/get_bills/get_bills_param.dart';
import '../../use_case.dart';
import '../generate_debts/generate_debts_param.dart';
import '../generate_debts/generate_debts_use_case.dart';
import '../get_receivables/get_receivables_use_case.dart';
import 'get_debts_breakdown_param.dart';

class GetDebtsBreakdownUseCase
    extends UseCase<DebtsBreakdown, GetDebtsBreakdownParam> {
  final GenerateDebtsUseCase _generateDebtsUseCase;
  final GetReceivablesUseCase _getReceivablesUseCase;
  final BillRepository _billRepository;
  final PersonRepository _personRepository;

  GetDebtsBreakdownUseCase({
    required GenerateDebtsUseCase generateDebtsUseCase,
    required GetReceivablesUseCase getReceivablesUseCase,
    required BillRepository billRepository,
    required PersonRepository personRepository,
  })  : _generateDebtsUseCase = generateDebtsUseCase,
        _getReceivablesUseCase = getReceivablesUseCase,
        _billRepository = billRepository,
        _personRepository = personRepository;

  @override
  Future<Either<Failure, DebtsBreakdown>> execute(
      GetDebtsBreakdownParam param) async {
    List<Bill> bills = await _getBills(param.eventGroupId);
    List<Debt> debts = await _generateDebts(param.eventGroupId, bills);
    List<PersonalBreakdown> breakdowns = await _getBreakdowns(bills);

    return Right(
      DebtsBreakdown(
        debts: debts,
        personalBreakdowns: breakdowns,
      ),
    );
  }

  Future<List<Bill>> _getBills(String eventGroupId) async {
    GetBillsParam param = GetBillsParam(eventGroupId: eventGroupId);
    final billsEither = await _billRepository.getBills(param);
    return billsEither.getOrElse(() => []);
  }

  Future<List<Debt>> _generateDebts(
      String eventGroupId, List<Bill> bills) async {
    final generatedDebtsEither = await _generateDebtsUseCase(
        GenerateDebtsParam(eventGroupId: eventGroupId, bills: bills));
    return generatedDebtsEither.getOrElse(() => []);
  }

  Future<List<PersonalBreakdown>> _getBreakdowns(List<Bill> bills) async {
    GetReceivablesParam param = GetReceivablesParam(bills: bills);
    final receivablesEither = await _getReceivablesUseCase(param);
    if (receivablesEither.isLeft()) {
      return [];
    }
    List<Receivable> receivables = receivablesEither.asRight();

    List<PersonalBreakdown> result = [];
    for (Receivable receivable in receivables) {
      Person person = await _getPerson(receivable.personId);
      result.add(PersonalBreakdown(
        person: person,
        totalCost: receivable.totalCost ?? 0,
        totalPayment: receivable.totalPayment ?? 0,
      ));
    }

    return result
      ..sort((a, b) =>
          StringUtils.compareAlphabetically(a.person.name, b.person.name));
  }

  Future<Person> _getPerson(String id) async {
    final resEither = await _personRepository.getPerson(id);
    if (resEither.isLeft()) {
      return Person.unknown(id: id);
    }
    return resEither.asRight();
  }
}
