import 'package:equatable/equatable.dart';

class GetDebtsBreakdownParam extends Equatable {
  final String eventGroupId;

  const GetDebtsBreakdownParam({
    required this.eventGroupId,
  });

  @override
  List<Object?> get props => [eventGroupId];
}
