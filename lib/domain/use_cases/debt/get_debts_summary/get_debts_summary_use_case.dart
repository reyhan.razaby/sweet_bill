import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/debt/debts_summary.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_param.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/bill/bill.dart';
import '../../../entities/debt/debt.dart';
import '../../../repositories/bill_repository.dart';
import '../generate_debts/generate_debts_use_case.dart';
import 'get_debts_summary_param.dart';

class GetDebtsSummaryUseCase
    extends UseCase<DebtsSummary, GetDebtsSummaryParam> {
  final GenerateDebtsUseCase _generateDebtsUseCase;
  final BillRepository _billRepository;

  GetDebtsSummaryUseCase({
    required GenerateDebtsUseCase generateDebtsUseCase,
    required BillRepository billRepository,
  })  : _generateDebtsUseCase = generateDebtsUseCase,
        _billRepository = billRepository;

  @override
  Future<Either<Failure, DebtsSummary>> execute(
      GetDebtsSummaryParam param) async {
    List<Bill> bills = await _getBills(param.eventGroupId);
    final double totalBillAmount =
        bills.map((e) => e.totalCost).fold(0, (a, b) => a + b);

    List<Debt> debts = await _generateDebts(param.eventGroupId, bills);

    return Right(DebtsSummary(
      debts: debts,
      totalCost: totalBillAmount,
      numberOfBills: bills.length,
    ));
  }

  Future<List<Debt>> _generateDebts(
      String eventGroupId, List<Bill> bills) async {
    final generatedDebtsEither = await _generateDebtsUseCase(
        GenerateDebtsParam(eventGroupId: eventGroupId, bills: bills));
    return generatedDebtsEither.getOrElse(() => []);
  }

  Future<List<Bill>> _getBills(String eventGroupId) async {
    GetBillsParam param = GetBillsParam(eventGroupId: eventGroupId);
    final billsEither = await _billRepository.getBills(param);
    return billsEither.getOrElse(() => []);
  }
}
