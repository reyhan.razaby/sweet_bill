import 'package:equatable/equatable.dart';

class GetDebtsSummaryParam extends Equatable {
  final String eventGroupId;

  @override
  List<Object> get props => [eventGroupId];

  const GetDebtsSummaryParam({
    required this.eventGroupId,
  });
}
