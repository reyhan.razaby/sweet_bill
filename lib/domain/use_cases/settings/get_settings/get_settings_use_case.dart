import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/settings/settings.dart';
import 'package:sweet_bill/domain/repositories/settings_repository.dart';
import 'package:sweet_bill/domain/use_cases/empty_param.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

class GetSettingsUseCase extends UseCase<Settings, EmptyParam> {
  final SettingsRepository settingsRepository;

  static const String comma = ',';
  static const String period = '.';

  static const String _defaultLocale = 'ID';
  static const int _defaultMaxFractionDigits = 2;
  static const int _defaultMaxWholeDigits = 15;

  static const Map _localeThousandSeparators = {'ID': '.'};
  static const Map _localeFractionDigits = {'ID': 0};

  @override
  Future<Either<Failure, Settings>> execute(EmptyParam param) async {
    String locale = await settingsRepository.getOrSetLocale(
      defaultValue: _defaultLocale,
    );

    int defaultMaxFractionDigits =
        _localeFractionDigits[locale] ?? _defaultMaxFractionDigits;
    int maxFractionDigits = await settingsRepository.getOrSetMaxFractionDigits(
      defaultValue: defaultMaxFractionDigits,
    );
    String thousandSeparator = _localeThousandSeparators[locale] ?? comma;
    String decimalSeparator = thousandSeparator != comma ? comma : period;

    final settings = Settings(
      locale: locale,
      thousandSeparator: thousandSeparator,
      decimalSeparator: decimalSeparator,
      maxWholeDigits: _defaultMaxWholeDigits,
      maxFractionDigits: maxFractionDigits,
    );

    return Right(settings);
  }

  GetSettingsUseCase({
    required this.settingsRepository,
  });
}
