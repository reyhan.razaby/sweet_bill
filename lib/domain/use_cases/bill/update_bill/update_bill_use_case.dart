import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/validate_bill/validate_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

class UpdateBillUseCase extends UseCase<bool, UpdateBillParam> {
  final BillRepository _billRepository;
  final ValidateBillUseCase _validateBillUseCase;

  UpdateBillUseCase({
    required BillRepository billRepository,
    required ValidateBillUseCase validateBillUseCase,
  })  : _billRepository = billRepository,
        _validateBillUseCase = validateBillUseCase;

  @override
  Future<Either<Failure, bool>> execute(UpdateBillParam param) async {
    final validationRes = await _validateBillUseCase(param.bill);
    if (validationRes.isLeft()) {
      return Left(validationRes.asLeft());
    }

    return _billRepository.updateBill(param);
  }
}
