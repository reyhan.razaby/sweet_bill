import 'package:equatable/equatable.dart';

import '../../../entities/bill/bill.dart';

class UpdateBillParam extends Equatable {
  final Bill bill;

  const UpdateBillParam({
    required this.bill,
  });

  @override
  List<Object> get props => [bill];

  UpdateBillParam copyWith({
    Bill? bill,
  }) {
    return UpdateBillParam(
      bill: bill ?? this.bill,
    );
  }
}
