import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/extensions/double.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/core/failures/invalid_argument_failure.dart';
import 'package:sweet_bill/domain/repositories/settings_repository.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/bill/bill.dart';

class ValidateBillUseCase extends UseCase<bool, Bill> {
  final SettingsRepository settingsRepository;

  @override
  Future<Either<Failure, bool>> execute(Bill param) async {
    if (param.personalCosts.isEmpty) {
      return const Left(InvalidArgumentFailure(
        message: 'Cost cannot be empty',
      ));
    }

    if (param.personalPayments.isEmpty) {
      return const Left(InvalidArgumentFailure(
        message: 'Payment cannot be empty',
      ));
    }

    int fractionDigits = await getMaxFractionDigits();
    if (!param.totalCost.equals(
      param.totalPayment,
      fractionDigits: fractionDigits,
    )) {
      return const Left(InvalidArgumentFailure(
        message: 'Total cost and payment amount do not match',
      ));
    }

    return const Right(true);
  }

  Future<int> getMaxFractionDigits() async {
    int? res = await settingsRepository.getMaxFractionDigits();
    if (res == null) {
      throw Exception('Cannot find maxFractionDigits');
    }
    return res;
  }

  ValidateBillUseCase({
    required this.settingsRepository,
  });
}
