import 'package:equatable/equatable.dart';

class GetBillsParam extends Equatable {
  final String eventGroupId;
  final String? involvedPersonId;

  const GetBillsParam({
    required this.eventGroupId,
    this.involvedPersonId,
  });

  @override
  List<Object?> get props => [eventGroupId, involvedPersonId];
}
