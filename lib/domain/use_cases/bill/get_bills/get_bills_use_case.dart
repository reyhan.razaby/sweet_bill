import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/bill/bill.dart';
import 'get_bills_param.dart';

class GetBillsUseCase extends UseCase<List<Bill>, GetBillsParam> {
  final BillRepository _billRepository;

  GetBillsUseCase({
    required BillRepository billRepository,
  }) : _billRepository = billRepository;

  @override
  Future<Either<Failure, List<Bill>>> execute(GetBillsParam param) async {
    return await _billRepository.getBills(param);
  }
}
