import 'package:equatable/equatable.dart';

import '../../../entities/bill/bill.dart';

class CreateBillParam extends Equatable {
  final Bill bill;

  const CreateBillParam({
    required this.bill,
  });

  @override
  List<Object> get props => [bill];

  CreateBillParam copyWith({
    Bill? bill,
  }) {
    return CreateBillParam(
      bill: bill ?? this.bill,
    );
  }
}
