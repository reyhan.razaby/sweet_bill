import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_param.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/use_cases/bill/validate_bill/validate_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

class CreateBillUseCase extends UseCase<String, CreateBillParam> {
  final BillRepository _billRepository;
  final ValidateBillUseCase _validateBillUseCase;

  CreateBillUseCase({
    required BillRepository billRepository,
    required ValidateBillUseCase validateBillUseCase,
  })  : _billRepository = billRepository,
        _validateBillUseCase = validateBillUseCase;

  @override
  Future<Either<Failure, String>> execute(CreateBillParam param) async {
    final validationRes = await _validateBillUseCase(param.bill);
    if (validationRes.isLeft()) {
      return Left(validationRes.asLeft());
    }

    return _billRepository.createBill(param);
  }
}
