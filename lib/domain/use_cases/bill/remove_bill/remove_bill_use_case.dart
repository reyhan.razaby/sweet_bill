import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

class RemoveBillUseCase extends UseCase<bool, String> {
  final BillRepository _billRepository;

  RemoveBillUseCase({
    required BillRepository billRepository,
  }) : _billRepository = billRepository;

  @override
  Future<Either<Failure, bool>> execute(String param) async {
    return _billRepository.removeBill(param);
  }
}
