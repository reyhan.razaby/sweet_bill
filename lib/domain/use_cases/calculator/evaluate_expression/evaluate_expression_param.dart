import 'package:equatable/equatable.dart';

class EvaluateExpressionParam extends Equatable {
  final String expression;
  final int? fractionDigits;

  const EvaluateExpressionParam({
    required this.expression,
    this.fractionDigits,
  });

  @override
  List<Object?> get props => [expression, fractionDigits];
}
