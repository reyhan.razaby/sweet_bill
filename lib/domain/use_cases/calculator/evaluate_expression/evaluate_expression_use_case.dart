import 'package:dartz/dartz.dart';
import 'package:math_expressions/math_expressions.dart';
import 'package:sweet_bill/core/extensions/double.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import 'evaluate_expression_param.dart';

class EvaluateExpressionUseCase
    extends UseCase<double, EvaluateExpressionParam> {
  @override
  Future<Either<Failure, double>> execute(EvaluateExpressionParam param) async {
    String rawExpression = param.expression;
    String expression = rawExpression
        .replaceAll('×', '*')
        .replaceAll('÷', '/')
        .replaceAll('%', '/100');

    double result;
    try {
      Expression exp = Parser().parse(expression);
      result = exp.evaluate(EvaluationType.REAL, ContextModel());
    } catch (e) {
      return const Left(Failure(message: 'Invalid input'));
    }

    if (result.isInfinite || result.isNaN) {
      return const Left(Failure(message: "Can't divide by zero"));
    }

    return Right(result.roundToDecimal(param.fractionDigits ?? 0));
  }
}
