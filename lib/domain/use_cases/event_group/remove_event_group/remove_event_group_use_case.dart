import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';
import 'package:dartz/dartz.dart';

class RemoveEventGroupUseCase extends UseCase<bool, String> {
  final EventGroupRepository _eventGroupRepository;

  RemoveEventGroupUseCase({
    required EventGroupRepository eventGroupRepository,
  }) : _eventGroupRepository = eventGroupRepository;

  @override
  Future<Either<Failure, bool>> execute(String param) async {
    await _eventGroupRepository.removeEventGroup(param);

    final activeId =
        (await _eventGroupRepository.getActive()).getOrElse(() => '');

    if (activeId == param) {
      final eventGroupsRes = await _eventGroupRepository.getEventGroupIds();
      final eventGroupIds = eventGroupsRes.getOrElse(() => []);
      if (eventGroupIds.isNotEmpty) {
        await _eventGroupRepository.setAsActive(eventGroupIds[0]);
      }
    }

    return const Right(true);
  }
}
