import 'package:equatable/equatable.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

class UpdateEventGroupParam extends Equatable {
  final EventGroup eventGroup;
  final List<Person> members;

  const UpdateEventGroupParam({
    required this.eventGroup,
    required this.members,
  });

  @override
  List<Object> get props => [eventGroup, members];
}
