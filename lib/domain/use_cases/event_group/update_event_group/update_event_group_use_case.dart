import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/event_group/update_event_group/update_event_group_param.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/person/person.dart';

class UpdateEventGroupUseCase
    extends UseCase<EventGroup, UpdateEventGroupParam> {
  final EventGroupRepository _eventGroupRepository;
  final PersonRepository _personRepository;

  UpdateEventGroupUseCase({
    required EventGroupRepository eventGroupRepository,
    required PersonRepository personRepository,
  })  : _eventGroupRepository = eventGroupRepository,
        _personRepository = personRepository;

  @override
  Future<Either<Failure, EventGroup>> execute(
      UpdateEventGroupParam param) async {
    // TODO: due to duplicated code as CreateEventGroupUseCase, wrap this members creation to separated use case
    List<Person> members = [];

    for (Person member in param.members) {
      String memberId = member.id;

      // Empty memberId means new person
      if (memberId == '') {
        final newMemberRes = await _personRepository.createPerson(member);
        newMemberRes.fold(
          // TODO: Notify to refresh friend list
          (_) => throw Exception('Member ${member.name} failed to create'),
          (newPerson) => members.add(newPerson),
        );
      } else {
        members.add(member);
      }
    }

    members.sort((a, b) => a.name.compareTo(b.name));

    return _eventGroupRepository.updateEventGroup(
      param.eventGroup,
      members.map((e) => e.id).toList(),
    );
  }
}
