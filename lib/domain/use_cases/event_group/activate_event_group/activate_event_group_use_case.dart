import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';
import 'package:dartz/dartz.dart';

class ActivateEventGroupUseCase extends UseCase<bool, String> {
  final EventGroupRepository _eventGroupRepository;

  ActivateEventGroupUseCase({
    required EventGroupRepository eventGroupRepository,
  }) : _eventGroupRepository = eventGroupRepository;

  @override
  Future<Either<Failure, bool>> execute(String param) async {
    await _eventGroupRepository.setAsActive(param);
    return const Right(true);
  }
}
