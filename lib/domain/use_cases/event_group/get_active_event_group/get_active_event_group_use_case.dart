import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/empty_param.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/person/person.dart';

class GetActiveEventGroupUseCase extends UseCase<EventGroup, EmptyParam> {
  final EventGroupRepository _eventGroupRepository;
  final PersonRepository _personRepository;

  GetActiveEventGroupUseCase({
    required EventGroupRepository eventGroupRepository,
    required PersonRepository personRepository,
  })  : _eventGroupRepository = eventGroupRepository,
        _personRepository = personRepository;

  @override
  Future<Either<Failure, EventGroup>> execute(EmptyParam param) async {
    String activeId =
        (await _eventGroupRepository.getActive()).getOrElse(() => '');

    if (activeId == '') {
      final eventGroupsRes = await _eventGroupRepository.getEventGroupIds();
      final eventGroupIds = eventGroupsRes.getOrElse(() => []);
      if (eventGroupIds.isNotEmpty) {
        activeId = eventGroupIds[0];
      } else {
        final self = await getSelf();
        if (self == null) {
          return const Left(Failure(message: 'No self found'));
        }
        final createResult = await _eventGroupRepository
            .createEventGroup(const EventGroup(id: '', name: ''), [self.id]);
        activeId = createResult.fold(
          (l) => '',
          (r) => r.id,
        );
      }
      await _eventGroupRepository.setAsActive(activeId);
    }

    final egResult = await _eventGroupRepository.getEventGroup(activeId);
    return egResult.fold(
      (failure) => Left(failure),
      (eventGroup) => Right(eventGroup),
    );
  }

  Future<Person?> getSelf() async {
    final res = await _personRepository.getSelf();
    return res.fold((l) => null, (r) => r);
  }
}
