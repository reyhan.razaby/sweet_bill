import 'package:equatable/equatable.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

class CreateEventGroupParam extends Equatable {
  final EventGroup eventGroup;
  final List<Person> members;
  final bool setAsActive;

  const CreateEventGroupParam({
    required this.eventGroup,
    required this.members,
    this.setAsActive = false,
  });

  @override
  List<Object> get props => [eventGroup, members, setAsActive];

  CreateEventGroupParam copyWith({
    EventGroup? eventGroup,
    List<Person>? members,
    bool? setAsActive,
  }) {
    return CreateEventGroupParam(
      eventGroup: eventGroup ?? this.eventGroup,
      members: members ?? this.members,
      setAsActive: setAsActive ?? this.setAsActive,
    );
  }
}
