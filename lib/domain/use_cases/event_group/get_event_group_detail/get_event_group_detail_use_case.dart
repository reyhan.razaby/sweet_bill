import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_detail.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/person/person.dart';
import 'get_event_groups_param.dart';

class GetEventGroupDetailUseCase
    extends UseCase<EventGroupDetail, GetEventGroupDetailParam> {
  final EventGroupRepository _eventGroupRepository;

  GetEventGroupDetailUseCase({
    required EventGroupRepository eventGroupRepository,
  }) : _eventGroupRepository = eventGroupRepository;

  @override
  Future<Either<Failure, EventGroupDetail>> execute(
      GetEventGroupDetailParam param) async {
    final egEither =
        await _eventGroupRepository.getEventGroup(param.eventGroupId);
    if (egEither.isLeft()) {
      return const Left(Failure());
    }
    EventGroup eventGroup = egEither.asRight();
    List<Person>? members;

    if (param.fetchMembers) {
      members = await _getMembers(eventGroup.id);
    }

    EventGroupDetail data = EventGroupDetail(
      eventGroup: eventGroup,
      members: members,
    );
    return Right(data);
  }

  Future<List<Person>> _getMembers(String eventGroupId) async {
    final membersEither = await _eventGroupRepository.getMembers(eventGroupId);
    if (membersEither.isLeft()) {
      return [];
    }
    return membersEither.asRight();
  }
}
