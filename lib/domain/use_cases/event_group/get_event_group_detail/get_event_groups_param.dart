import 'package:equatable/equatable.dart';

class GetEventGroupDetailParam extends Equatable {
  final String eventGroupId;
  final bool fetchMembers;

  const GetEventGroupDetailParam({
    required this.eventGroupId,
    this.fetchMembers = false,
  });

  @override
  List<Object?> get props => [eventGroupId, fetchMembers];
}
