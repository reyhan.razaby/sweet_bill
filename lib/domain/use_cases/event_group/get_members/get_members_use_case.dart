import 'package:dartz/dartz.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_members/get_members_param.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';

import '../../../../core/failures/failure.dart';
import '../../use_case.dart';

class GetMembersUseCase extends UseCase<List<Person>, GetMembersParam> {
  final EventGroupRepository _eventGroupRepository;

  GetMembersUseCase({
    required EventGroupRepository eventGroupRepository,
  }) : _eventGroupRepository = eventGroupRepository;

  @override
  Future<Either<Failure, List<Person>>> execute(GetMembersParam param) async {
    return _eventGroupRepository.getMembers(param.eventGroupId);
  }
}
