class GetMembersParam {
  final String eventGroupId;

  const GetMembersParam({
    required this.eventGroupId,
  });
}
