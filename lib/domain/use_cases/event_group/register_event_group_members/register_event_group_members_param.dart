import 'package:equatable/equatable.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

class RegisterEventGroupMembersParam extends Equatable {
  final String eventGroupId;
  final List<Person> members;

  const RegisterEventGroupMembersParam({
    required this.eventGroupId,
    required this.members,
  });

  @override
  List<Object> get props => [eventGroupId, members];
}
