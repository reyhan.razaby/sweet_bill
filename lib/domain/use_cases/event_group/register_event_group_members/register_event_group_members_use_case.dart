import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/event_group/register_event_group_members/register_event_group_members_param.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

import '../../../entities/person/person.dart';

class RegisterEventGroupMembersUseCase
    extends UseCase<List<Person>, RegisterEventGroupMembersParam> {
  final EventGroupRepository _eventGroupRepository;
  final PersonRepository _personRepository;

  RegisterEventGroupMembersUseCase({
    required EventGroupRepository eventGroupRepository,
    required PersonRepository personRepository,
  })  : _eventGroupRepository = eventGroupRepository,
        _personRepository = personRepository;

  @override
  Future<Either<Failure, List<Person>>> execute(
      RegisterEventGroupMembersParam param) async {
    List<Person> members = [];

    // TODO: due to duplicated code as CreateEventGroupUseCase, wrap this members creation to separated use case
    for (Person member in param.members) {
      String memberId = member.id;

      // Empty memberId means new person
      if (memberId == '') {
        final newMemberRes = await _personRepository.createPerson(member);
        newMemberRes.fold(
          // TODO: Notify to refresh friend list
          (_) => throw Exception('Member ${member.name} failed to create'),
          (newPerson) => members.add(newPerson),
        );
      } else {
        members.add(member);
      }

      await _eventGroupRepository.registerMember(param.eventGroupId, memberId);
    }

    return Right(members);
  }
}
