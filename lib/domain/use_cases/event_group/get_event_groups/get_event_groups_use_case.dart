import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/extensions/either.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/core/log.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group_select_item.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_param.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/use_cases/use_case.dart';

class GetEventGroupsUseCase
    extends UseCase<List<EventGroupSelectItem>, GetEventGroupsParam> {
  final EventGroupRepository _eventGroupRepository;
  final PersonRepository _personRepository;

  GetEventGroupsUseCase({
    required EventGroupRepository eventGroupRepository,
    required PersonRepository personRepository,
  })  : _eventGroupRepository = eventGroupRepository,
        _personRepository = personRepository;

  @override
  Future<Either<Failure, List<EventGroupSelectItem>>> execute(
      GetEventGroupsParam param) async {
    List<EventGroupSelectItem> result;

    final eventGroupsRes = await _eventGroupRepository.getEventGroups();
    if (eventGroupsRes.isLeft()) {
      return Left(eventGroupsRes.asLeft());
    }
    final eventGroups = eventGroupsRes.getOrElse(() => []);

    String? activeId;
    final futureResult = eventGroups.map((eventGroup) async {
      bool isActive = false;
      if (param.activeOnTop) {
        activeId = activeId ?? await getActiveEventGroupId();
        isActive = eventGroup.id == activeId;
      }
      return EventGroupSelectItem(
        eventGroup,
        isCurrentlyActive: isActive,
        members: await getMemberNames(eventGroup.id),
      );
    }).toList();

    result = await Future.wait(futureResult);

    if (param.activeOnTop) {
      await putActiveToTop(result);
    }

    return Right(result);
  }

  Future<String> getActiveEventGroupId() async =>
      (await _eventGroupRepository.getActive()).getOrElse(() => '');

  Future<void> putActiveToTop(List<EventGroupSelectItem> eventGroups) async {
    try {
      int activeIdx = eventGroups.indexWhere((eg) => eg.isCurrentlyActive);
      EventGroupSelectItem activeEg = eventGroups.elementAt(activeIdx);
      eventGroups.removeAt(activeIdx);
      eventGroups.insert(0, activeEg);
    } catch (e, s) {
      Log.error(
          'Error when moving active EventGroupSelectItem to the top', e, s);
    }
  }

  Future<List<EventGroupMember>> getMemberNames(String eventGroupId) async {
    final membersRes = await _eventGroupRepository.getMembers(eventGroupId);
    final String? selfPersonId = await getSelfPersonId();
    return membersRes.fold(
      (_) => [],
      (data) {
        // Map and sort
        final list = data
            .map((person) => EventGroupMember(person.name,
                isSelf: person.id == selfPersonId))
            .toList()
          ..sort((a, b) => StringUtils.compareAlphabetically(a.name, b.name));

        // Move self to the first position
        int selfIndex = list.indexWhere((el) => el.isSelf);
        if (selfIndex >= 0) {
          final moved = list.removeAt(selfIndex);
          list.insert(0, moved);
        }

        return list;
      },
    );
  }

  Future<String?> getSelfPersonId() async {
    final res = await _personRepository.getSelf();
    return res.fold((l) => null, (r) => r.id);
  }
}
