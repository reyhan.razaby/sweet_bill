import '../../../entities/person/person_sorting_field.dart';

class GetPersonsParam {
  final bool includeSelf;
  final String? name;
  final PersonSortingField? sortBy;
  final List<String>? excludeIds;

  const GetPersonsParam({
    required this.includeSelf,
    this.name,
    this.sortBy,
    this.excludeIds,
  });
}
