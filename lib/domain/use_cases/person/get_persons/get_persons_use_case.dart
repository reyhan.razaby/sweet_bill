import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/failures/failure.dart';
import 'get_persons_param.dart';
import '../../../repositories/person_repository.dart';
import '../../use_case.dart';

class GetPersonsUseCase extends UseCase<List<Person>, GetPersonsParam> {
  final PersonRepository _personRepository;

  GetPersonsUseCase({
    required PersonRepository personRepository,
  }) : _personRepository = personRepository;

  @override
  Future<Either<Failure, List<Person>>> execute(GetPersonsParam param) async {
    return _personRepository.getPersons(
      includeSelf: param.includeSelf,
      excludedIds: param.excludeIds,
      sortBy: param.sortBy,
    );
  }
}
