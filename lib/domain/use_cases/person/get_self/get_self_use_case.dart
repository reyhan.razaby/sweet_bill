import 'package:dartz/dartz.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

import '../../../../core/failures/failure.dart';
import '../../../repositories/person_repository.dart';
import '../../empty_param.dart';
import '../../use_case.dart';

class GetSelfUseCase extends UseCase<Person, EmptyParam> {
  final PersonRepository _personRepository;

  GetSelfUseCase({
    required PersonRepository personRepository,
  }) : _personRepository = personRepository;

  @override
  Future<Either<Failure, Person>> execute(EmptyParam param) async {
    return _personRepository.getSelf();
  }
}
