import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/utils/avatar_generator.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

import '../../../../core/failures/failure.dart';
import '../../../repositories/person_repository.dart';
import '../../use_case.dart';
import 'create_self_param.dart';

class CreateSelfUseCase extends UseCase<Person, CreateSelfParam> {
  final PersonRepository _personRepository;

  CreateSelfUseCase({
    required PersonRepository personRepository,
  }) : _personRepository = personRepository;

  @override
  Future<Either<Failure, Person>> execute(CreateSelfParam param) async {
    // Check existing. If present, return it
    final selfEither = await _personRepository.getSelf();
    if (selfEither.isRight()) {
      return selfEither;
    }

    Person self = param.selfPerson ??
        Person.initial(
          name: param.name,
          avatar: AvatarGenerator.generatePersonAvatar(),
        );
    return _personRepository.createPerson(self, isSelf: true);
  }
}
