import '../../../entities/person/person.dart';

class CreateSelfParam {
  String name;
  Person? selfPerson;

  CreateSelfParam({
    required this.name,
    this.selfPerson,
  });
}
