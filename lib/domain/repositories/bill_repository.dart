import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';

import '../use_cases/bill/update_bill/update_bill_param.dart';

abstract class BillRepository {
  Future<Either<Failure, String>> createBill(CreateBillParam param);

  Future<Either<Failure, bool>> updateBill(UpdateBillParam param);

  Future<Either<Failure, List<Bill>>> getBills(GetBillsParam param);

  Future<Either<Failure, bool>> removeBill(String billId);
}
