import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';

abstract class EventGroupRepository {
  Future<Either<Failure, EventGroup>> createEventGroup(
      EventGroup eventGroup, List<String> memberIds,
      {bool setAsActive = false});

  Future<Either<Failure, EventGroup>> updateEventGroup(
      EventGroup eventGroup, List<String> memberIds);

  Future<Either<Failure, EventGroup>> getEventGroup(String id);

  Future<Either<Failure, List<EventGroup>>> getEventGroups();

  Future<Either<Failure, List<String>>> getEventGroupIds();

  Future<Either<Failure, List<Person>>> getMembers(String eventGroupId);

  Future<Either<Failure, bool>> removeEventGroup(String id);

  Future<Either<Failure, bool>> setAsActive(String id);

  Future<Either<Failure, String>> getActive();

  Future<Either<Failure, bool>> registerMember(
      String eventGroupId, String personId);
}
