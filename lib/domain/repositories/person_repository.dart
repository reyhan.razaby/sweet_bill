import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:dartz/dartz.dart';
import 'package:sweet_bill/domain/entities/person/person_sorting_field.dart';

abstract class PersonRepository {
  Future<Either<Failure, Person>> createPerson(Person person, {bool isSelf});

  Future<Either<Failure, Person>> getSelf();

  Future<Either<Failure, List<Person>>> getPersons({
    bool includeSelf,
    List<String>? excludedIds,
    PersonSortingField? sortBy,
  });

  Future<Either<Failure, Person>> getPerson(String id);
}
