abstract class SettingsRepository {
  Future<String> getOrSetLocale({required String defaultValue});

  Future<int?> getMaxFractionDigits();

  Future<int> getOrSetMaxFractionDigits({required int defaultValue});

  Future<void> setLocale(String locale);

  Future<void> setMaxFractionDigits(int maxFractionDigits);
}
