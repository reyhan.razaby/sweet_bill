import 'package:dartz/dartz.dart';
import 'package:sweet_bill/core/failures/failure.dart';

import '../entities/debt/calculated_debts.dart';

abstract class DebtRepository {
  Future<Either<Failure, bool>> createCalculatedDebts(
      String eventGroupId, CalculatedDebts calculatedDebts);

  Future<Either<Failure, CalculatedDebts>> getCalculatedDebts(
      String eventGroupId);
}
