import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bills/bills_bloc.dart';
import 'package:sweet_bill/presentation/bloc/debts_summary/debts_summary_cubit.dart';
import 'package:sweet_bill/presentation/pages/errors/not_found_page.dart';
import 'package:sweet_bill/presentation/pages/profile/profile_page.dart';
import 'package:sweet_bill/presentation/pages/transactions/transactions_page.dart';

import 'core/injection_container/service_locator.dart';

class PageBuilder {
  static Widget buildTransactionsPage() {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => srvLocator<BillsBloc>()),
        BlocProvider(create: (_) => srvLocator<DebtsSummaryCubit>())
      ],
      child: const TransactionsPage(),
    );
  }

  static Widget buildProfilePage() {
    return const ProfilePage();
  }

  static Widget buildNotFoundPage() {
    return const NotFoundPage();
  }
}
