import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sweet_bill/page_builder.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_cost/bill_form_cost_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bill_form_payment/bill_form_payment_bloc.dart';
import 'package:sweet_bill/presentation/bloc/debts_breakdown/debts_breakdown_cubit.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/event_groups/event_groups_bloc.dart';
import 'package:sweet_bill/presentation/bloc/friends/friends_bloc.dart';
import 'package:sweet_bill/presentation/bloc/nav_menu/nav_menu_cubit.dart';
import 'package:sweet_bill/presentation/bloc/welcome/welcome_cubit.dart';
import 'package:sweet_bill/presentation/components/page_transition/slide_page_transition.dart';
import 'package:sweet_bill/presentation/pages/bill_detail/bill_detail_page.dart';
import 'package:sweet_bill/presentation/pages/bill_form/bill_form_cost_page.dart';
import 'package:sweet_bill/presentation/pages/bill_form/bill_form_page.dart';
import 'package:sweet_bill/presentation/pages/bill_form/bill_form_payment_page.dart';
import 'package:sweet_bill/presentation/pages/debts_breakdown/debts_breakdown_page.dart';
import 'package:sweet_bill/presentation/pages/event_group_form/event_group_form_page.dart';
import 'package:sweet_bill/presentation/pages/event_groups/event_groups_page.dart';
import 'package:sweet_bill/presentation/pages/home/home_page.dart';
import 'package:sweet_bill/presentation/pages/welcome/welcome_page.dart';

import 'core/constants/route_names.dart';
import 'core/injection_container/service_locator.dart';

class Routes {
  static final Map<String, RouteBuilder> _routeBuilders = _initBuilders();

  static Route<dynamic> getRoute(RouteSettings settings) {
    final routeBuilder = _routeBuilders[settings.name] ?? _notFoundRoute;
    return routeBuilder(settings);
  }

  static Route<dynamic> _billFormCostRoute(RouteSettings settings) {
    final args = settings.arguments as BillFormCostPageArgs;

    return SlideTransitionRoute(
      page: MultiBlocProvider(
        child: const BillFormCostPage(),
        providers: [
          BlocProvider.value(
            value: args.billFormBloc,
          ),
          BlocProvider(
            create: (_) => BillFormCostBloc(
                billFormBloc: args.billFormBloc, profileCubit: srvLocator()),
          ),
          BlocProvider(
            create: (_) => FriendsBloc(getPersonsUseCase: srvLocator()),
          ),
        ],
      ),
    );
  }

  static Route<dynamic> _billFormPaymentRoute(RouteSettings settings) {
    final args = settings.arguments as BillFormPaymentPageArgs;

    return SlideTransitionRoute(
      page: MultiBlocProvider(
        child: const BillFormPaymentPage(),
        providers: [
          BlocProvider.value(
            value: args.billFormBloc,
          ),
          BlocProvider(
            create: (_) => BillFormPaymentBloc(billFormBloc: args.billFormBloc),
          ),
        ],
      ),
    );
  }

  static Route<dynamic> _billFormRoute(RouteSettings settings) =>
      SlideTransitionRoute(
        page: BlocProvider(
            create: (_) => srvLocator<BillFormBloc>(),
            child: BillFormPage(
              args: settings.arguments as BillFormPageArgs,
            )),
        direction: Directions.bottomToTop,
      );

  static Route<dynamic> _eventGroupsRoute(settings) {
    return SlideTransitionRoute(
      page: BlocProvider(
        create: (_) => srvLocator<EventGroupsBloc>(),
        child: const EventGroupsPage(),
      ),
    );
  }

  static Route<dynamic> _homeRoute(settings) {
    return SlideTransitionRoute(
      page: BlocProvider(
        create: (_) => NavMenuCubit(),
        child: const HomePage(),
      ),
    );
  }

  static Route<dynamic> _welcomeRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (_) => BlocProvider(
        create: (_) => srvLocator<WelcomeCubit>(),
        child: const WelcomePage(),
      ),
    );
  }

  static Route<dynamic> _notFoundRoute(settings) {
    return MaterialPageRoute(
      builder: (_) => PageBuilder.buildNotFoundPage(),
    );
  }

  static Route<dynamic> _transactionsRoute(settings) {
    return MaterialPageRoute(
      builder: (_) => PageBuilder.buildTransactionsPage(),
    );
  }

  static Route<dynamic> _billDetailRoute(settings) {
    return SlideTransitionRoute(
      page: const BillDetailPage(),
    );
  }

  static Route<dynamic> _eventGroupFormRoute(settings) {
    final args = settings.arguments ?? const EventGroupFormPageArgs();

    return SlideTransitionRoute(
      page: MultiBlocProvider(providers: [
        BlocProvider(create: (_) => srvLocator<EventGroupFormBloc>()),
        BlocProvider(create: (_) => srvLocator<FriendsBloc>())
      ], child: EventGroupFormPage(args: args)),
      direction: Directions.bottomToTop,
    );
  }

  static Route<dynamic> _debtsBreakdownRoute(settings) {
    return SlideTransitionRoute(
      page: BlocProvider(
        create: (_) => DebtsBreakdownCubit(
          getDebtsBreakdownUseCase: srvLocator(),
        ),
        child: DebtsBreakdownPage(
          args: settings.arguments as DebtsBreakdownPageArgs,
        ),
      ),
      direction: Directions.rightToLeft,
    );
  }

  static Route<dynamic> _profileRoute(settings) {
    return MaterialPageRoute(
      builder: (_) => PageBuilder.buildProfilePage(),
    );
  }

  // Register routes here
  static Map<String, RouteBuilder> _initBuilders() {
    return {
      RouteNames.home: _homeRoute,
      RouteNames.welcome: _welcomeRoute,
      RouteNames.transactions: _transactionsRoute,
      RouteNames.eventGroups: _eventGroupsRoute,
      RouteNames.eventGroupForm: _eventGroupFormRoute,
      RouteNames.billForm: _billFormRoute,
      RouteNames.billFormCost: _billFormCostRoute,
      RouteNames.billFormPayment: _billFormPaymentRoute,
      RouteNames.billDetail: _billDetailRoute,
      RouteNames.debtsBreakdown: _debtsBreakdownRoute,
      RouteNames.profile: _profileRoute,
    };
  }
}

typedef RouteBuilder = Route<dynamic> Function(RouteSettings settings);
