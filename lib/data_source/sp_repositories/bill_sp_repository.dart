import 'package:dartz/dartz.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/bill_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/bill_model.dart';
import 'package:sweet_bill/core/exceptions/invalid_argument_exception.dart';
import 'package:sweet_bill/core/exceptions/not_found_exception.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/core/failures/invalid_argument_failure.dart';
import 'package:sweet_bill/core/failures/not_found_failure.dart';
import 'package:sweet_bill/domain/entities/bill/bill.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/use_cases/bill/create_bill/create_bill_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_param.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_param.dart';

class BillSpRepository implements BillRepository {
  final BillSpGateway _billSpGateway;
  final PersonSpGateway _personSpGateway;
  final EventGroupSpGateway _eventGroupSpGateway;

  const BillSpRepository({
    required BillSpGateway billSpGateway,
    required PersonSpGateway personSpGateway,
    required EventGroupSpGateway eventGroupSpGateway,
  })  : _billSpGateway = billSpGateway,
        _personSpGateway = personSpGateway,
        _eventGroupSpGateway = eventGroupSpGateway;

  @override
  Future<Either<Failure, String>> createBill(CreateBillParam param) async {
    try {
      Set<String> personIds =
          param.bill.personalCosts.map((e) => e.personId).toSet();
      personIds.addAll(param.bill.personalPayments.map((e) => e.personId).toSet());

      await _validatePersonIds(personIds);
      await _validateEventGroupId(param.bill.eventGroupId);

      BillModel model = BillModel.fromEntity(param.bill);
      model = await _billSpGateway.save(model);

      if (model.id == '') {
        return const Left(
            Failure(message: 'id of BillModel has not been generated'));
      }

      _eventGroupSpGateway.appendBillIds(param.bill.eventGroupId, [model.id]);

      return Right(model.id);
    } on InvalidArgumentException catch (e) {
      return Left(InvalidArgumentFailure(message: e.cause));
    } on NotFoundException catch (e) {
      return Left(NotFoundFailure(message: e.cause));
    }
  }

  @override
  Future<Either<Failure, bool>> updateBill(UpdateBillParam param) async {
    Set<String> personIds = param.bill.personalCosts.map((e) => e.personId).toSet();
    personIds.addAll(param.bill.personalPayments.map((e) => e.personId).toSet());

    await _validatePersonIds(personIds);
    await _validateEventGroupId(param.bill.eventGroupId);

    BillModel model = BillModel.fromEntity(param.bill);
    model = await _billSpGateway.save(model);

    return const Right(true);
  }

  @override
  Future<Either<Failure, List<Bill>>> getBills(GetBillsParam param) async {
    final ids = await _eventGroupSpGateway.getBillIds(param.eventGroupId);
    List<Bill> result = [];
    for (var id in ids) {
      BillModel? billModel = await _billSpGateway.getById(id);
      if (billModel == null) {
        continue;
      }

      String? involvedPersonId = param.involvedPersonId;
      if (involvedPersonId == null ||
          isPersonInvolved(billModel, involvedPersonId)) {
        result.add(billModel.toEntity());
      }
    }
    return Right(result);
  }

  bool isPersonInvolved(BillModel billModel, String personId) {
    if (billModel.personalCosts.any((bm) => bm.personId == personId)) {
      return true;
    }
    if (billModel.personalPayments.any((bm) => bm.personId == personId)) {
      return true;
    }
    return false;
  }

  Future<void> _validatePersonIds(Set<String> personIds) async {
    for (var personId in personIds) {
      if (await _personSpGateway.getById(personId) == null) {
        throw NotFoundException("Person $personId doesn't exist");
      }
    }
  }

  Future<void> _validateEventGroupId(String eventGroupId) async {
    if (await _eventGroupSpGateway.getById(eventGroupId) == null) {
      throw NotFoundException("Event group $eventGroupId doesn't exist");
    }
  }

  @override
  Future<Either<Failure, bool>> removeBill(String billId) async {
    BillModel? billModel = await _billSpGateway.getById(billId);
    if (billModel == null) {
      return Left(Failure(message: "Bill id $billId doesn't exist"));
    }
    await _eventGroupSpGateway.removeBillId(billModel.eventGroupId, billId);
    await _billSpGateway.removeById(billId);
    return const Right(true);
  }
}
