import 'package:dartz/dartz.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/event_group_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/core/failures/not_found_failure.dart';
import 'package:sweet_bill/core/utils/dynamic_utils.dart';
import 'package:sweet_bill/domain/entities/event_group/event_group.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';

class EventGroupSpRepository implements EventGroupRepository {
  final EventGroupSpGateway _eventGroupSpGateway;
  final PersonSpGateway _personSpGateway;

  const EventGroupSpRepository({
    required EventGroupSpGateway eventGroupSpGateway,
    required PersonSpGateway personSpGateway,
  })  : _eventGroupSpGateway = eventGroupSpGateway,
        _personSpGateway = personSpGateway;

  @override
  Future<Either<Failure, EventGroup>> createEventGroup(
      EventGroup eventGroup, List<String> memberIds,
      {bool setAsActive = false}) async {
    EventGroupModel model = EventGroupModel.fromEntity(eventGroup);
    model = await _eventGroupSpGateway.save(model);

    // Register persons as group members
    await _eventGroupSpGateway.appendPersonIds(model.id, memberIds.toList());

    if (setAsActive) {
      await _eventGroupSpGateway.setActiveEventGroupId(model.id);
    }

    return Right(eventGroup.copyWith(id: model.id));
  }

  @override
  Future<Either<Failure, EventGroup>> updateEventGroup(
      EventGroup eventGroup, List<String> memberIds) async {
    EventGroupModel model = EventGroupModel.fromEntity(eventGroup);
    model = await _eventGroupSpGateway.save(model);

    await _eventGroupSpGateway.savePersonIds(model.id, memberIds.toList());

    return Right(eventGroup);
  }

  @override
  Future<Either<Failure, List<EventGroup>>> getEventGroups() async {
    List<EventGroupModel> models = await _eventGroupSpGateway.getAll();
    List<EventGroup> result = models
        .where((m) => DynamicUtils.isAllNotNull([m.id, m.name, m.creationTime]))
        .map((m) => m.toEntity())
        .toList();
    return Right(result);
  }

  @override
  Future<Either<Failure, bool>> removeEventGroup(String id) async {
    return Right(await _eventGroupSpGateway.removeById(id));
  }

  @override
  Future<Either<Failure, bool>> setAsActive(String id) async {
    await _eventGroupSpGateway.setActiveEventGroupId(id);
    return const Right(true);
  }

  @override
  Future<Either<Failure, String>> getActive() async {
    String activeId = await _eventGroupSpGateway.getActiveEventGroupId();
    if (activeId == '') {
      return const Left(NotFoundFailure(message: 'No active group found'));
    }
    return Right(activeId);
  }

  @override
  Future<Either<Failure, List<String>>> getEventGroupIds() async {
    return Right(await _eventGroupSpGateway.getAllIds());
  }

  @override
  Future<Either<Failure, EventGroup>> getEventGroup(String id) async {
    EventGroupModel? model = await _eventGroupSpGateway.getById(id);
    if (model == null) {
      return Left(NotFoundFailure(message: "Event group $id doesn't exist"));
    }

    return Right(model.toEntity());
  }

  @override
  Future<Either<Failure, List<Person>>> getMembers(String eventGroupId) async {
    final ids = await _eventGroupSpGateway.getPersonIds(eventGroupId);
    List<Person> result = [];
    for (var id in ids) {
      PersonModel? personModel = await _personSpGateway.getById(id);
      if (personModel != null) {
        result.add(personModel.toEntity());
      }
    }
    return Right(result);
  }

  @override
  Future<Either<Failure, bool>> registerMember(
      String eventGroupId, String personId) async {
    await _eventGroupSpGateway.appendPersonIds(eventGroupId, [personId]);
    return const Right(true);
  }
}
