import 'package:dartz/dartz.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/person_model.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/core/failures/not_found_failure.dart';
import 'package:sweet_bill/core/utils/date_time_utils.dart';
import 'package:sweet_bill/core/utils/string_utils.dart';
import 'package:sweet_bill/domain/entities/person/person.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';

import '../../domain/entities/person/person_sorting_field.dart';

class PersonSpRepository implements PersonRepository {
  final PersonSpGateway _personSpGateway;

  const PersonSpRepository({
    required PersonSpGateway personSpGateway,
  }) : _personSpGateway = personSpGateway;

  @override
  Future<Either<Failure, Person>> createPerson(Person person,
      {bool isSelf = false}) async {
    PersonModel model = PersonModel.fromEntity(person);
    model = await _personSpGateway.save(model);

    if (model.id == '') {
      return const Left(
          Failure(message: 'id of PersonModel has not been generated'));
    }

    if (isSelf) {
      _personSpGateway.saveSelfPersonId(model.id);
    }

    return Right(person.copyWith(id: model.id));
  }

  @override
  Future<Either<Failure, Person>> getSelf() async {
    String? personId = await _personSpGateway.getSelfPersonId();
    if (personId == null) {
      return const Left(NotFoundFailure());
    }

    PersonModel? model = await _personSpGateway.getById(personId);
    if (model == null) {
      return const Left(NotFoundFailure());
    }

    return Right(model.toEntity());
  }

  @override
  Future<Either<Failure, List<Person>>> getPersons({
    bool includeSelf = true,
    List<String>? excludedIds,
    PersonSortingField? sortBy,
  }) async {
    List<PersonModel> list = await _personSpGateway.getAll();

    if (!includeSelf) {
      String? selfId = await _personSpGateway.getSelfPersonId();
      if (selfId != null) {
        list.removeWhere((p) => p.id == selfId);
      }
    }

    final entityList = list
        .where((model) {
          return _doFilter(excludedIds, model);
        })
        .map((model) => model.toEntity())
        .toList();

    if (sortBy != null) {
      entityList.sort((m1, m2) {
        switch (sortBy) {
          case PersonSortingField.nameAsc:
            return StringUtils.compareAlphabetically(m1.name, m2.name);
          case PersonSortingField.nameDesc:
            return StringUtils.compareAlphabetically(m2.name, m1.name);
          case PersonSortingField.creationTimeAsc:
            return DateTimeUtils.compareNullable(
                m1.creationTime, m2.creationTime);
          case PersonSortingField.creationTimeDesc:
            return DateTimeUtils.compareNullable(
                m2.creationTime, m1.creationTime);
          default:
            return 0;
        }
      });
    }
    return Right(entityList);
  }

  bool _doFilter(List<String>? excludedIds, PersonModel model) {
    if (excludedIds == null) {
      return true;
    }
    return !excludedIds.contains(model.id);
  }

  @override
  Future<Either<Failure, Person>> getPerson(String id) async {
    PersonModel? model = await _personSpGateway.getById(id);
    if (model == null) {
      return Left(NotFoundFailure(message: "Event group $id doesn't exist"));
    }
    return Right(model.toEntity());
  }
}
