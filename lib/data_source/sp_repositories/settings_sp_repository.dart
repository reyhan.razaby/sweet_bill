import 'package:sweet_bill/api/shared_preferences/gateways/settings_sp_gateway.dart';
import 'package:sweet_bill/domain/repositories/settings_repository.dart';

class SettingsSpRepository implements SettingsRepository {
  final SettingsSpGateway settingsSpGateway;

  const SettingsSpRepository({
    required this.settingsSpGateway,
  });

  @override
  Future<String> getOrSetLocale({required String defaultValue}) async {
    String? locale = settingsSpGateway.getLocale();
    if (locale != null) {
      return locale;
    }
    setLocale(defaultValue);
    // TODO send report if still null
    return settingsSpGateway.getLocale()!;
  }

  @override
  Future<int?> getMaxFractionDigits() async {
    return settingsSpGateway.getMaxFractionDigits();
  }

  @override
  Future<int> getOrSetMaxFractionDigits({required int defaultValue}) async {
    int? result = settingsSpGateway.getMaxFractionDigits();
    if (result != null) {
      return result;
    }
    setMaxFractionDigits(defaultValue);
    // TODO send report if still null
    return settingsSpGateway.getMaxFractionDigits()!;
  }

  @override
  Future<void> setLocale(String locale) async {
    settingsSpGateway.setLocale(locale);
  }

  @override
  Future<void> setMaxFractionDigits(int maxFractionDigits) async {
    settingsSpGateway.setMaxFractionDigits(maxFractionDigits);
  }
}
