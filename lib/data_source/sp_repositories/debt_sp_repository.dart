import 'package:dartz/dartz.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/models/calculated_debts_model.dart';
import 'package:sweet_bill/api/shared_preferences/models/debt_model.dart';
import 'package:sweet_bill/core/failures/failure.dart';
import 'package:sweet_bill/domain/entities/debt/debt.dart';
import 'package:sweet_bill/domain/repositories/debt_repository.dart';

import '../../api/shared_preferences/gateways/person_sp_gateway.dart';
import '../../api/shared_preferences/models/person_model.dart';
import '../../domain/entities/debt/calculated_debts.dart';

class DebtSpRepository implements DebtRepository {
  final EventGroupSpGateway _eventGroupSpGateway;
  final PersonSpGateway _personSpGateway;

  const DebtSpRepository({
    required EventGroupSpGateway eventGroupSpGateway,
    required PersonSpGateway personSpGateway,
  })  : _eventGroupSpGateway = eventGroupSpGateway,
        _personSpGateway = personSpGateway;

  @override
  Future<Either<Failure, bool>> createCalculatedDebts(
      String eventGroupId, CalculatedDebts calculatedDebts) async {
    await _eventGroupSpGateway.saveCalculatedDebts(
      eventGroupId,
      CalculatedDebtsModel(
        receivablesKey: calculatedDebts.receivablesKey,
        debts:
            calculatedDebts.debts.map((e) => DebtModel.fromEntity(e)).toList(),
      ),
    );
    return const Right(true);
  }

  @override
  Future<Either<Failure, CalculatedDebts>> getCalculatedDebts(
      String eventGroupId) async {
    CalculatedDebtsModel? model =
        await _eventGroupSpGateway.getCalculatedDebts(eventGroupId);

    if (model == null) {
      return Right(CalculatedDebts.empty());
    }

    List<DebtModel> debtModels = model.debts;

    List<Debt> debts = await Future.wait(
      debtModels.map((dm) async {
        PersonModel borrower = await _getPerson(dm.borrowerId);
        PersonModel lender = await _getPerson(dm.lenderId);
        return dm.toEntity(borrower: borrower, lender: lender);
      }).toList(),
    );

    return Right(
      CalculatedDebts(receivablesKey: model.receivablesKey, debts: debts),
    );
  }

  Future<PersonModel> _getPerson(String personId) async {
    PersonModel? person = await _personSpGateway.getById(personId);
    person ??= PersonModel.unknown(id: personId);
    return person;
  }
}
