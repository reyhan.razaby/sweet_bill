import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

abstract class IoGateway {
  String readFile(String name);

  Future<File> saveFileToAppDir(String filePath);

  Future<bool> deleteFile(String filePath);
}

class IoGatewayImpl implements IoGateway {
  @override
  String readFile(String name) => File(name).readAsStringSync();

  @override
  Future<File> saveFileToAppDir(String filePath) async {
    final newDir = await getApplicationDocumentsDirectory();
    final fileName = basename(filePath);
    final newFile = File('${newDir.path}/$fileName');
    return File(filePath).copy(newFile.path);
  }

  @override
  Future<bool> deleteFile(String filePath) async {
    File file = File(filePath);
    if (file.existsSync()) {
      file.deleteSync();
      return true;
    }
    return false;
  }
}
