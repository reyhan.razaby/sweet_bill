extension StringExtension on String {
  String get lastChar => this[length - 1];

  String get firstChar => this[0];

  String repeat(int count) {
    if (isNotEmpty) {
      String res = '';
      for (int i = 0; i < count; i++) {
        res += this;
      }
      return res;
    }
    return this;
  }
}
