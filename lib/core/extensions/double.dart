extension DoubleExtension on double {
  double roundToDecimal(int fractionDigits) =>
      double.parse(toStringAsFixed(fractionDigits));

  bool equals(double other, {int fractionDigits = 0}) {
    if (fractionDigits < 0) {
      fractionDigits = 0;
    }
    return roundToDecimal(fractionDigits) ==
        other.roundToDecimal(fractionDigits);
  }
}
