part of 'service_locator.dart';

void _initApis() {
  srvLocator.registerLazySingleton(() => EventGroupSpGateway(srvLocator()));
  srvLocator.registerLazySingleton(() => PersonSpGateway(srvLocator()));
  srvLocator.registerLazySingleton(() => BillSpGateway(srvLocator()));
  srvLocator.registerLazySingleton(() => SettingsSpGateway(srvLocator()));
}
