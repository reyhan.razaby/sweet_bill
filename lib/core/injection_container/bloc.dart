part of 'service_locator.dart';

void _initBloc() {
  srvLocator.registerFactory(() => WelcomeCubit(
        createEventGroupUseCase: srvLocator(),
        createSelfUseCase: srvLocator(),
      ));
  srvLocator.registerFactory(() => EventGroupsBloc(
        getEventGroupsUseCase: srvLocator(),
        removeEventGroupUseCase: srvLocator(),
        activateEventGroupUseCase: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => ProfileCubit(
        getSelfUseCase: srvLocator(),
        getSettingsUseCase: srvLocator(),
      ));
  srvLocator.registerFactory(() => EventGroupFormBloc(
        createEventGroupUseCase: srvLocator(),
        updateEventGroupUseCase: srvLocator(),
        profileCubit: srvLocator(),
        getEventGroupDetailUseCase: srvLocator(),
        getBillsUseCase: srvLocator(),
        ioGateway: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => TransactionsCubit(
        getActiveEventGroupUseCase: srvLocator(),
        getMembersUseCase: srvLocator(),
      ));
  srvLocator
      .registerFactory(() => FriendsBloc(getPersonsUseCase: srvLocator()));
  srvLocator.registerFactory(() => BillsBloc(
        getBillsUseCase: srvLocator(),
        removeBillUseCase: srvLocator(),
      ));
  srvLocator.registerFactory(() => DebtsSummaryCubit(
        getDebtsSummaryUseCase: srvLocator(),
      ));
  srvLocator.registerFactory(() => BillFormBloc(
        getMembersUseCase: srvLocator(),
        createBillUseCase: srvLocator(),
        updateBillUseCase: srvLocator(),
        registerEventGroupMembersUseCase: srvLocator(),
        profileCubit: srvLocator(),
      ));
  srvLocator.registerFactory(() => CalculatorBloc(
        profileCubit: srvLocator(),
        toastShower: srvLocator(),
        evaluateExpressionUseCase: srvLocator(),
      ));
}
