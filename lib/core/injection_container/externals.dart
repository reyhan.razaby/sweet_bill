part of 'service_locator.dart';

Future<void> _initExternals() async {
  final sharedPreferences = await SharedPreferences.getInstance();
  srvLocator.registerLazySingleton(() => sharedPreferences);

  srvLocator.registerLazySingleton<IoGateway>(() => IoGatewayImpl());

  srvLocator.registerLazySingleton(() => ToastShower());
}
