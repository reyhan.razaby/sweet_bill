part of 'service_locator.dart';

void _initRepositories() {
  srvLocator.registerLazySingleton<EventGroupRepository>(
    () => EventGroupSpRepository(
      eventGroupSpGateway: srvLocator(),
      personSpGateway: srvLocator(),
    ),
  );
  srvLocator.registerLazySingleton<BillRepository>(
    () => BillSpRepository(
      billSpGateway: srvLocator(),
      personSpGateway: srvLocator(),
      eventGroupSpGateway: srvLocator(),
    ),
  );
  srvLocator.registerLazySingleton<PersonRepository>(
    () => PersonSpRepository(personSpGateway: srvLocator()),
  );
  srvLocator.registerLazySingleton<DebtRepository>(
    () => DebtSpRepository(
      personSpGateway: srvLocator(),
      eventGroupSpGateway: srvLocator(),
    ),
  );
  srvLocator.registerLazySingleton<SettingsRepository>(
    () => SettingsSpRepository(settingsSpGateway: srvLocator()),
  );
}
