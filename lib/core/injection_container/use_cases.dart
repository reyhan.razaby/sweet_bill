part of 'service_locator.dart';

void _initUseCases() {
  srvLocator.registerLazySingleton(() => GetEventGroupsUseCase(
        eventGroupRepository: srvLocator(),
        personRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => CreateEventGroupUseCase(
        eventGroupRepository: srvLocator(),
        personRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => UpdateEventGroupUseCase(
        eventGroupRepository: srvLocator(),
        personRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => RegisterEventGroupMembersUseCase(
        eventGroupRepository: srvLocator(),
        personRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(
      () => RemoveEventGroupUseCase(eventGroupRepository: srvLocator()));
  srvLocator.registerLazySingleton(
      () => ActivateEventGroupUseCase(eventGroupRepository: srvLocator()));
  srvLocator.registerLazySingleton(() => GetActiveEventGroupUseCase(
        eventGroupRepository: srvLocator(),
        personRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(
      () => CreateSelfUseCase(personRepository: srvLocator()));
  srvLocator.registerLazySingleton(
      () => GetSelfUseCase(personRepository: srvLocator()));
  srvLocator.registerLazySingleton(
      () => GetPersonsUseCase(personRepository: srvLocator()));
  srvLocator.registerLazySingleton(
      () => GetMembersUseCase(eventGroupRepository: srvLocator()));
  srvLocator.registerLazySingleton(() => ValidateBillUseCase(
        settingsRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => CreateBillUseCase(
        billRepository: srvLocator(),
        validateBillUseCase: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => UpdateBillUseCase(
        billRepository: srvLocator(),
        validateBillUseCase: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => GetBillsUseCase(
        billRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => RemoveBillUseCase(
        billRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => GetReceivablesUseCase());
  srvLocator.registerLazySingleton(() => GenerateDebtsUseCase(
        getReceivablesUseCase: srvLocator(),
        personRepository: srvLocator(),
        debtRepository: srvLocator(),
        billRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => GetDebtsSummaryUseCase(
        generateDebtsUseCase: srvLocator(),
        billRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => GetDebtsBreakdownUseCase(
        generateDebtsUseCase: srvLocator(),
        billRepository: srvLocator(),
        personRepository: srvLocator(),
        getReceivablesUseCase: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => GetEventGroupDetailUseCase(
        eventGroupRepository: srvLocator(),
      ));
  srvLocator.registerLazySingleton(() => EvaluateExpressionUseCase());
  srvLocator.registerLazySingleton(() => GetSettingsUseCase(
        settingsRepository: srvLocator(),
      ));
}
