import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/bill_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/event_group_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/person_sp_gateway.dart';
import 'package:sweet_bill/api/shared_preferences/gateways/settings_sp_gateway.dart';
import 'package:sweet_bill/core/io/io_gateway.dart';
import 'package:sweet_bill/data_source/sp_repositories/bill_sp_repository.dart';
import 'package:sweet_bill/data_source/sp_repositories/debt_sp_repository.dart';
import 'package:sweet_bill/data_source/sp_repositories/event_group_sp_repository.dart';
import 'package:sweet_bill/data_source/sp_repositories/settings_sp_repository.dart';
import 'package:sweet_bill/domain/repositories/bill_repository.dart';
import 'package:sweet_bill/domain/repositories/debt_repository.dart';
import 'package:sweet_bill/domain/repositories/event_group_repository.dart';
import 'package:sweet_bill/domain/repositories/person_repository.dart';
import 'package:sweet_bill/domain/repositories/settings_repository.dart';
import 'package:sweet_bill/domain/use_cases/bill/get_bills/get_bills_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/remove_bill/remove_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/bill/update_bill/update_bill_use_case.dart';
import 'package:sweet_bill/domain/use_cases/calculator/evaluate_expression/evaluate_expression_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/generate_debts/generate_debts_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_breakdown/get_debts_breakdown_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_debts_summary/get_debts_summary_use_case.dart';
import 'package:sweet_bill/domain/use_cases/debt/get_receivables/get_receivables_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/activate_event_group/activate_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/create_event_group/create_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_active_event_group/get_active_event_group_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_group_detail/get_event_group_detail_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_event_groups/get_event_groups_use_case.dart';
import 'package:sweet_bill/domain/use_cases/event_group/get_members/get_members_use_case.dart';
import 'package:sweet_bill/domain/use_cases/person/get_persons/get_persons_use_case.dart';
import 'package:sweet_bill/domain/use_cases/person/get_self/get_self_use_case.dart';
import 'package:sweet_bill/domain/use_cases/settings/get_settings/get_settings_use_case.dart';
import 'package:sweet_bill/presentation/bloc/bill_form/bill_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/bills/bills_bloc.dart';
import 'package:sweet_bill/presentation/bloc/calculator/calculator_bloc.dart';
import 'package:sweet_bill/presentation/bloc/debts_summary/debts_summary_cubit.dart';
import 'package:sweet_bill/presentation/bloc/event_group_form/event_group_form_bloc.dart';
import 'package:sweet_bill/presentation/bloc/event_groups/event_groups_bloc.dart';
import 'package:sweet_bill/presentation/bloc/friends/friends_bloc.dart';
import 'package:sweet_bill/presentation/bloc/profile/profile_cubit.dart';
import 'package:sweet_bill/presentation/bloc/transactions/transactions_cubit.dart';
import 'package:sweet_bill/presentation/bloc/welcome/welcome_cubit.dart';
import 'package:sweet_bill/presentation/components/toast/toast_shower.dart';

import '../../data_source/sp_repositories/person_sp_repository.dart';
import '../../domain/use_cases/bill/create_bill/create_bill_use_case.dart';
import '../../domain/use_cases/bill/validate_bill/validate_bill_use_case.dart';
import '../../domain/use_cases/event_group/register_event_group_members/register_event_group_members_use_case.dart';
import '../../domain/use_cases/event_group/remove_event_group/remove_event_group_use_case.dart';
import '../../domain/use_cases/event_group/update_event_group/update_event_group_use_case.dart';
import '../../domain/use_cases/person/create_self/create_self_use_case.dart';

part 'apis.dart';
part 'bloc.dart';
part 'externals.dart';
part 'repositories.dart';
part 'use_cases.dart';

final srvLocator = GetIt.instance;

Future<void> initDependencies() async {
  _initBloc();
  _initUseCases();
  _initRepositories();
  _initApis();
  await _initExternals();
}
