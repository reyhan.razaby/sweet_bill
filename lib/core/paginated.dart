import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class Paginated<T> extends Equatable {
  List<T> result;
  int totalCount;
  int totalPage;

  Paginated({
    required this.result,
    required this.totalCount,
    required this.totalPage,
  });

  Paginated.singlePage(this.result)
      : totalCount = result.length,
        totalPage = 1;

  Paginated.of(this.result, this.totalCount, this.totalPage);

  @override
  List<Object?> get props => [result, totalCount, totalPage];
}
