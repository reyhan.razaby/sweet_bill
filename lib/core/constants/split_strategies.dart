enum SplitStrategies {
  equally('equally'),
  percentage('by percentage'),
  custom('by custom amount'),
  ;

  final String text;

  const SplitStrategies(this.text);

  static SplitStrategies fromString(String str) {
    try {
      return SplitStrategies.values.firstWhere((e) => e.toString() == str);
    } catch (_) {
      return SplitStrategies.custom;
    }
  }
}
