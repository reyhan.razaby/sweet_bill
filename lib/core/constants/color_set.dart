import 'dart:ui';

class ColorSet {
  static const darkest = Color(0xff161e28);
  static const dark = Color(0xff1a2938);
  static const light = Color(0xff41adab);
  static const accent = Color(0xffcd8313);
  static const red = Color(0xffdb6969);
  static const black = Color(0xff020317);
  static const white = Color(0xffececec);
  static const grey = Color(0xff878787);
  static const lightGrey = Color(0xffb6b6b6);
}
