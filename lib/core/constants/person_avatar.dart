import 'dart:ui';

enum PersonAvatar {
  pa001('🤠'),
  pa002('👻'),
  pa003('🦦'),
  pa004('🐸'),
  pa005('😽'),
  pa006('👾'),
  pa007('🗿'),
  pa008('⛄'),
  pa009('🎃'),
  pa010('🐼'),
  pa011('🦊'),
  pa012('🦑'),
  pa013('🤡'),
  pa014('🐊'),
  pa015('🦄'),
  pa016('👽'),
  pa017('🥷'),
  pa018('🦉'),
  pa019('🦕'),
  pa020('🐳'),
  pa021('🦜'),
  pa022('🐰'),
  pa023('🧑'),
  pa024('👀'),
  pa025('🧛'),
  pa026('🧑'),
  pa027('🐲'),
  pa028('🐝'),
  pa029('🌛'),
  pa030('🧸'),
  ;

  final String emoji;

  const PersonAvatar(this.emoji);

  static PersonAvatar fromString(String str) {
    try {
      return PersonAvatar.values.firstWhere((e) => e.toString() == str);
    } catch (_) {
      return PersonAvatar.pa001;
    }
  }

  static Color getBackgroundColor() => const Color(0xffcdd9ca);
}
