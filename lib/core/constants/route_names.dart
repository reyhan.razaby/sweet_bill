class RouteNames {
  static const String root = '/';
  static const String home = 'home';
  static const String welcome = 'welcome';
  static const String transactions = 'transactions';
  static const String eventGroups = 'eventGroups';
  static const String eventGroupForm = 'eventGroupForm';
  static const String billDetail = 'billDetail';
  static const String billForm = 'billForm';
  static const String billFormCost = 'billFormCost';
  static const String billFormPayment = 'billFormPayment';
  static const String debtsBreakdown = 'debtsBreakdown';
  static const String profile = 'profile';
}
