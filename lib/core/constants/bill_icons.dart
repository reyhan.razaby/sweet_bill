import 'package:flutter/material.dart';

import 'color_set.dart';

// TODO: complete all icons (needs: mountain, swimming, ticket, beach)
enum BillIcons {
  receipt,
  hotel,
  gasStation,
  toilet,
  cafe,
  restaurant,
  wine,
  shopping,
  car,
  motorcycle,
  bus,
  airplane,
  train,
  boat,
  house,
  hiking,
  ski,
  sport,
  music,
  game,
  gift,
  movie,
  temple,
  explore,
  work,
  sofa,
  umbrella,
  group,
  ;

  static BillIcons fromString(String str) {
    try {
      return BillIcons.values.firstWhere((e) => e.toString() == str);
    } catch (_) {
      return BillIcons.receipt;
    }
  }
}

extension BillIconExtension on BillIcons {
  Widget widget({IconSize size = IconSize.medium}) {
    switch (this) {
      case BillIcons.car:
        return _buildIcon(Icons.directions_car, size);
      case BillIcons.gasStation:
        return _buildIcon(Icons.local_gas_station, size);
      case BillIcons.umbrella:
        return _buildIcon(Icons.beach_access, size);
      case BillIcons.restaurant:
        return _buildIcon(Icons.restaurant, size);
      case BillIcons.hotel:
        return _buildIcon(Icons.hotel, size);
      case BillIcons.boat:
        return _buildIcon(Icons.directions_boat, size);
      case BillIcons.work:
        return _buildIcon(Icons.work, size);
      case BillIcons.sofa:
        return _buildIcon(Icons.weekend, size);
      case BillIcons.cafe:
        return _buildIcon(Icons.coffee, size);
      case BillIcons.wine:
        return _buildIcon(Icons.wine_bar, size);
      case BillIcons.group:
        return _buildIcon(Icons.group, size);
      case BillIcons.motorcycle:
        return _buildIcon(Icons.motorcycle, size);
      case BillIcons.airplane:
        return _buildIcon(Icons.airplanemode_active, size);
      case BillIcons.shopping:
        return _buildIcon(Icons.shopping_cart, size);
      case BillIcons.toilet:
        return _buildIcon(Icons.wc, size);
      case BillIcons.house:
        return _buildIcon(Icons.house, size);
      case BillIcons.train:
        return _buildIcon(Icons.train, size);
      case BillIcons.bus:
        return _buildIcon(Icons.directions_bus, size);
      case BillIcons.ski:
        return _buildIcon(Icons.downhill_skiing, size);
      case BillIcons.hiking:
        return _buildIcon(Icons.hiking, size);
      case BillIcons.music:
        return _buildIcon(Icons.music_note, size);
      case BillIcons.game:
        return _buildIcon(Icons.sports_esports, size);
      case BillIcons.gift:
        return _buildIcon(Icons.card_giftcard, size);
      case BillIcons.movie:
        return _buildIcon(Icons.movie, size);
      case BillIcons.sport:
        return _buildIcon(Icons.sports_basketball, size);
      case BillIcons.temple:
        return _buildIcon(Icons.temple_buddhist, size);
      case BillIcons.explore:
        return _buildIcon(Icons.travel_explore, size);
      default:
        return _buildIcon(Icons.receipt, size);
    }
  }

  Icon _buildIcon(IconData iconData, IconSize size) {
    double sizeAmount;
    switch (size) {
      case IconSize.small:
        sizeAmount = 14;
        break;
      case IconSize.medium:
        sizeAmount = 21;
        break;
      case IconSize.large:
        sizeAmount = 32;
        break;
    }

    return Icon(
      iconData,
      color: ColorSet.white,
      size: sizeAmount,
    );
  }
}

enum IconSize { small, medium, large }
