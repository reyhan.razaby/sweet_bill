class NotFoundException implements Exception {
  final String? cause;

  NotFoundException(this.cause);
}
