class InvalidArgumentException implements Exception {
  final String? cause;

  InvalidArgumentException(this.cause);
}
