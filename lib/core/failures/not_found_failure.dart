import 'failure.dart';

class NotFoundFailure extends Failure {
  const NotFoundFailure({
    String? message,
  }) : super(message: message);
}
