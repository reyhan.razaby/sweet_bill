import 'package:equatable/equatable.dart';

class Failure extends Equatable {
  final String? message;

  @override
  List<Object?> get props => [message];

  const Failure({
    this.message = 'Got unexpected error',
  });
}
