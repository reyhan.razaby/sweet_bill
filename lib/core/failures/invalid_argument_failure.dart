import 'failure.dart';

class InvalidArgumentFailure extends Failure {
  const InvalidArgumentFailure({
    String? message,
  }) : super(message: message);
}
