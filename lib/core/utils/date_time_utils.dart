class DateTimeUtils {
  static DateTime? parseOrNull(String? source) {
    if (source == null) {
      return null;
    }

    try {
      return DateTime.parse(source);
    } catch (_) {
      return null;
    }
  }

  static int compareNullable(DateTime? d1, DateTime? d2,
      {putNullToFirst = false}) {
    if (d1 != null && d2 != null) {
      return d1.compareTo(d2);
    }

    if (d1 == null && d2 != null) {
      return putNullToFirst ? -1 : 1;
    }

    if (d1 != null && d2 == null) {
      return putNullToFirst ? 1 : -1;
    }

    return 0;
  }
}
