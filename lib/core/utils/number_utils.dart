import 'dart:math';

import 'package:intl/intl.dart';
import 'package:sweet_bill/core/extensions/double.dart';
import 'package:sweet_bill/core/extensions/string.dart';

class NumberUtils {
  static String getCurrency(
    double amount, {
    bool showSymbol = false,
    String? locale = 'ID',
    int fractionDigits = 0,
    bool omitTrailingDecimalZero = true,
  }) {
    String decimalFormat = '';
    if (fractionDigits > 0) {
      if (omitTrailingDecimalZero) {
        decimalFormat = '.' + '#'.repeat(fractionDigits);
      } else {
        decimalFormat = '.' + '0'.repeat(fractionDigits);
      }
    }
    String newAmountText = NumberFormat(
      '#,###$decimalFormat',
      locale,
    ).format(amount);

    String symbol = showSymbol ? 'Rp' : '';

    return '$symbol$newAmountText';
  }

  static List<double> divideEvenly(
      double value, int divisor, int fractionDigits) {
    if (divisor == 0) {
      return List.empty();
    }

    double divided = (value / divisor).roundToDecimal(fractionDigits);
    double multiplied = (divided * divisor).roundToDecimal(fractionDigits);
    double delta = (value - multiplied).roundToDecimal(fractionDigits);

    if (delta == 0) {
      return List.filled(divisor, divided);
    }

    double adjustmentPerElement =
        (delta / delta.abs()) / pow(10, fractionDigits);

    int adjustedSize = (delta / adjustmentPerElement)
        .abs()
        .roundToDecimal(fractionDigits)
        .toInt();

    final normalList = List.filled(divisor - adjustedSize, divided);
    final adjustedList = List.filled(adjustedSize,
        (divided + adjustmentPerElement).roundToDecimal(fractionDigits));
    return [...normalList, ...adjustedList];
  }
}
