class DynamicUtils {
  static bool isAllNotNull(List<dynamic> data) =>
      data.every((el) => el != null);
}
