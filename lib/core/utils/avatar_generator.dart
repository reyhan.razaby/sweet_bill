import 'dart:math';

import '../constants/person_avatar.dart';

class AvatarGenerator {
  static final Random _random = Random();

  static PersonAvatar generatePersonAvatar(
      {Set<PersonAvatar>? scopedAvatars, List<PersonAvatar>? ownedAvatars}) {
    Set<PersonAvatar> allAvatars;
    if (scopedAvatars == null || scopedAvatars.isEmpty) {
      allAvatars = PersonAvatar.values.toSet();
    } else {
      allAvatars = scopedAvatars;
    }
    assert(allAvatars.isNotEmpty);

    if (ownedAvatars == null || ownedAvatars.isEmpty) {
      return _getRandom(allAvatars);
    }

    // Do intersection to ensure all owned avatars are included in "allAvatars"
    Set<PersonAvatar> uniqueOwnedAvatars =
        ownedAvatars.toSet().intersection(allAvatars);

    if (uniqueOwnedAvatars.length < allAvatars.length) {
      if (uniqueOwnedAvatars.isEmpty) {
        return _getRandom(allAvatars);
      }
      Set<PersonAvatar> unowned = Set.of(allAvatars)
        ..removeWhere((e) => uniqueOwnedAvatars.contains(e));
      return _getRandom(unowned);
    }

    List<_OccurrenceData<PersonAvatar>> occurrences = [];
    for (PersonAvatar avatar in uniqueOwnedAvatars) {
      int count = ownedAvatars.where((el) => el == avatar).toList().length;
      occurrences.add(_OccurrenceData(data: avatar, occurrence: count));
    }
    occurrences.sort((a, b) => a.occurrence.compareTo(b.occurrence));

    return occurrences.elementAt(0).data;
  }

  static E _getRandom<E>(Iterable<E> iterable) {
    return iterable.elementAt(_random.nextInt(iterable.length));
  }
}

class _OccurrenceData<T> {
  final T data;
  final int occurrence;

  _OccurrenceData({
    required this.data,
    required this.occurrence,
  });
}
