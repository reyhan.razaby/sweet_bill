import 'package:collection/collection.dart';

class ListUtils {
  static bool isNotNullAndNotEmpty(List? token) =>
      token != null && token.isNotEmpty;

  static bool isNullOrEmpty(List? token) => token == null || token.isEmpty;

  static bool isEqualsIgnoreOrder(List a, List b) {
    Function unOrdDeepEq = const DeepCollectionEquality.unordered().equals;
    return unOrdDeepEq(a, b);
  }
}
