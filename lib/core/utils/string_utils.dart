class StringUtils {
  static bool isNotNullAndNotEmpty(String? token) =>
      token != null && token.isNotEmpty;

  static bool isNullOrEmpty(String? token) => token == null || token.isEmpty;

  static String getOr(String? token, String defaultString) =>
      isNotNullAndNotEmpty(token) ? token! : defaultString;

  static String join(List<String> tokens,
      {String separator = ', ', bool isWithAnd = true}) {
    String result;
    if (isWithAnd && tokens.length > 1) {
      tokens = List.of(tokens);
      String last = tokens.removeLast();
      result = '${tokens.join(separator)} and $last';
    } else {
      result = tokens.join(separator);
    }
    return result;
  }

  static int compareAlphabetically(String str1, String str2) {
    return str1.toLowerCase().compareTo(str2.toLowerCase());
  }
}
